import { notification } from 'antd'
import { addNotificationMessenger } from 'common/components/header/NotificationSlice'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useLocation } from 'react-router'
import { WebSocket } from 'services/WebSocket'
import { SHOP_EVENT } from 'utils/socketConstants'
import { ResponseSocketModel } from '../../utils/socketConstants'
import {
  getCountNotificationOrderShop,
  getListNotificationShop,
} from '../components/header/NotificationApi'

export const useShopNotification = () => {
  const authState = useSelector((state: any) => state.authReducer)
  const [pageNotiMore, setPageNotiMore] = useState<number>(0)
  const [loading, setLoading] = useState<boolean>(false)

  const location = useLocation()
  const dispatch = useDispatch()

  const [messenger, setMessenger] = useState<any>()
  const [
    listNotificationMessenger,
    setListNotificationMessenger,
  ] = useState<any>([])
  const [listNotificationOrder, setListNotificationOrder] = useState<any>([])
  const [countNotificationOrder, setCountNotificationOrder] = useState(0)

  const [param, setParam] = useState({ page: 1 })
  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const getDataNotificationOrder = async () => {
    try {
      setLoading(true)
      const response = await getListNotificationShop({ page: 1 })
      setPageNotiMore(
        Math.ceil(response.paging?.totalItemCount / response.paging?.limit)
      )
      setListNotificationOrder(response.data)
    } catch (error) {
      console.log(error)
    } finally {
      setLoading(false)
    }
  }

  const handleCountNotificationOrder = async () => {
    try {
      const response = await getCountNotificationOrderShop()
      setCountNotificationOrder(response.data.count)
    } catch (error) {
      console.log(error)
    }
  }

  const loadMoreNotification = async () => {
    const response = await getListNotificationShop(param)
    let newList = listNotificationOrder.concat(response.data)
    setListNotificationOrder(newList)
    setPaging({
      total: response.paging.totalItemCount,
      current: response.paging.page,
      pageSize: response.paging.limit,
    })
  }

  useEffect(() => {
    loadMoreNotification()
  }, [param])

  useEffect(() => {
    getDataNotificationOrder()
    WebSocket.socketClient?.on(
      `shop_id_${authState.userInfo.shop_id}`,
      async (data: ResponseSocketModel) => {
        console.log('socket: ', data)
        switch (data?.type_action) {
          case SHOP_EVENT.NEW_MESSAGE:
            if (data?.data?.Messages[0].shop_id) {
              return
            } else {
              setMessenger(data)
            }
            break

          case SHOP_EVENT.NEW_NOTIFICATION:
            data.id = data.data.id
            setListNotificationOrder([data, ...listNotificationOrder])
            handleCountNotificationOrder()
            openNotification({
              title: 'Thông báo',
              description: data?.data?.content,
            })
            break
          default:
            break
        }
      }
    )
  }, [])

  useEffect(() => {
    if (messenger) {
      if (!location.pathname.includes(`${ENTERPRISE_ROUTER_PATH.CHAT}`)) {
        dispatch(addNotificationMessenger(messenger))
        openNotification({
          title: 'Tin nhắn mới',
          description: `${messenger?.data?.User?.name}: ${
            messenger?.data?.Messages[0]?.content || 'đã gửi một ảnh'
          }`,
        })
      }
    }
  }, [messenger])

  const openNotification = (data: { title: string; description: string }) => {
    notification.open({
      message: data.title,
      description: data.description,
      duration: 2,
    })
  }

  return {
    authState,
    setParam,
    paging,
    countNotificationOrder,
    listNotificationOrder,
    handleCountNotificationOrder,
    getDataNotificationOrder,
    listNotificationMessenger,
    setListNotificationMessenger,
    pageNotiMore,
    loading,
  }
}
