import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { SESSION_ID } from 'common/config'
import Cookie from 'js-cookie'
import {
  getListNotification,
  getCountNotification,
} from '../components/header/NotificationApi'
import io from 'socket.io-client'
import { WebSocket } from 'services/WebSocket'

export const useHeaderAdmin = () => {
  const authState = useSelector((state: any) => state.authReducer)
  const [listNotification, setListNotification] = useState<Array<any>>([])
  const [countNotification, setCountNotification] = useState(0)
  const [loadMoreNotification, setLoadMoreNotification] = useState(false)
  const [pageNotiMore, setPageNotiMore] = useState<number>(0)
  const cookie = Cookie.get(SESSION_ID)
  const [param, setParam] = useState({
    page: 1,
  })

  useEffect(() => {
    console.log('socket')

    getNotification()
    WebSocket.socketClient.on(`admin`, async (data: any) => {
      data.create_at = new Date()
      data.is_read = 0
      setListNotification(prevState => {
        return [data, ...prevState]
      })
      const res_data = await getCountNotification()
      console.log('res_data', res_data)
      setCountNotification(res_data.data.count)
    })
  }, [])

  useEffect(() => {
    moreNotification()
  }, [param])
  console.log('page', pageNotiMore)
  const getNotification = async () => {
    try {
      const res = await getListNotification(param)
      setPageNotiMore(Math.ceil(res.paging?.totalItemCount / res.paging?.limit))
      setListNotification(res.data)
      const res_data = await getCountNotification()
      setCountNotification(res_data.data.count)
    } catch (error) {}
  }

  const moreNotification = async () => {
    try {
      const res = await getListNotification(param)
      const new_list = listNotification.concat(res.data)
      setListNotification(new_list)
    } catch (error) {
    } finally {
      setLoadMoreNotification(false)
    }
  }
  return {
    authState,
    listNotification,
    countNotification,
    loadMoreNotification,
    cookie,
    param,
    setParam,
    setLoadMoreNotification,
    getNotification,
    pageNotiMore,
  }
}
