import { Col, Row } from 'antd'
import React from 'react'
import { StyledContainer } from './StyledContainer'

type Props = {
  children?: any
  filterComponent?: any
  contentComponent: any
  header?: any
  footer?: any
}

const Container = ({
  children,
  filterComponent,
  contentComponent,
  header = () => {},
}: Props) => {
  return (
    <StyledContainer>
      {/* {header!()} */}
      {/* <Spin size="large" spinning={false}> */}
      <Row>
        {filterComponent && (
          <Col style={{}} lg={4} md={5} xs={0}>
            {typeof filterComponent == 'function'
              ? filterComponent!()
              : filterComponent}
          </Col>
        )}
        <Col
          style={{
            backgroundColor: 'white',
            minHeight: '100px',
          }}
          lg={20}
          md={19}
          xs={24}
        >
          {typeof contentComponent == 'function'
            ? contentComponent!()
            : contentComponent}
        </Col>
      </Row>

      {/* </Spin> */}
    </StyledContainer>
  )
}
export default Container
