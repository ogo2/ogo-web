import { Badge, Empty, Menu } from 'antd'
import '../styles/Notification.css'
import { BellOutlined } from '@ant-design/icons'
import { useHistory } from 'react-router'
import { ADMIN_ROUTER_PATH } from 'common/config'
import moment from 'moment'
export default function NotificationAdmin({ listNotification }: any) {
  const history = useHistory()
  return (
    <div style={{ overflowY: 'scroll', height: '250px' }}>
      {listNotification.length > 0 ? (
        listNotification.map((item: any, index: number) => {
          return (
            <Menu.Item
              key={`notification_${index}`}
              style={{
                display: 'flex',
                borderBottom: '1px solid gray',
                width: '300px',
              }}
              onClick={() => {
                if (item.type === 10) {
                  // localStorage.setItem('gift_id', item.data.gift_id)
                  history.replace(ADMIN_ROUTER_PATH.GIFT)
                } else {
                  history.replace(ADMIN_ROUTER_PATH.GIFT_CHANGE)
                }
              }}
            >
              <div className="iconNotification">
                <Badge dot>
                  <BellOutlined
                    style={{
                      textAlign: 'center',
                      fontSize: '18px',
                      justifyContent: 'center',
                    }}
                  />
                </Badge>
              </div>
              <div className="inforNotification">
                <div className="contendNotification">
                  {item.content || item.title}
                </div>
                <br></br>
                <div className="timeNotification">
                  {moment(item.create_at).format('HH:mm DD/MM/YYYY')}
                </div>
              </div>
            </Menu.Item>
          )
        })
      ) : (
        <Empty />
      )}
    </div>
  )
}
