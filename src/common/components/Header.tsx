import {
  Affix,
  Button,
  Col,
  Divider,
  Form,
  Input,
  message,
  Modal,
  Row,
  Spin,
} from 'antd'
import { logoutAction } from 'features/auth/AuthSlice'
import { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { ApiClient } from 'services/ApiService'
import { ADMIN_ROLE, SHOP_ROLE } from 'utils/constants'
import history from 'utils/history'
import HeaderAdmin from './header/HeaderAdmin'
import HeaderAdminEditor from './header/HeaderAdminEditor'
import HeaderAdminSellManager from './header/HeaderAdminSellManager'
import HeaderAdminServiceManager from './header/HeaderAdminServiceManger'
import HeaderLogout from './header/HeaderLogout'
import HeaderShop from './header/HeaderShop'
import AdminEditorMenu from './menu/AdminEditorMenu'
import AdminMenu from './menu/AdminMenu'
import AdminSellManagerMenu from './menu/AdminSellManagerMenu'
import AdminServiceManagerMenu from './menu/AdminServiceManagerMenu'
import ShopMemberMenu from './menu/ShopMemberMenu'
import ShopMenu from './menu/ShopMenu'

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
}

interface dataChangePassword {
  new_password: string
  old_password: string
}

export default function Header(props: any) {
  const dispatch = useDispatch()
  const authState = useSelector((state: any) => state.authReducer)
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false)
  const [isLoading, setisLoading] = useState<boolean>(false)
  const [form] = Form.useForm()

  function handleClick(e: any) {
    if (e.key === 'logout') {
      dispatch(logoutAction())
    } else if (e.key === 'changePassword') {
      setIsModalVisible(true)
    } else {
      history.push(e.key)
    }
  }

  function handleGetCurrentRouter() {
    return window.location.pathname
  }

  const onFinish = async (value: dataChangePassword) => {
    const payload = {
      new_password: value.new_password,
      old_password: value.old_password,
    }
    if (payload.new_password === payload.old_password) {
      message.warn('Mật khẩu mới không được trùng với mật khẩu cũ!')
    } else {
      try {
        setisLoading(true)
        const response = await ApiClient.put(
          '/users/change-password-customer',
          payload
        )
        form.resetFields()
        setIsModalVisible(false)
        message.success('Thay đổi mật khẩu thành công!')
      } catch (error) {
        console.log(error)
      } finally {
        setisLoading(false)
      }
    }
  }
  return (
    <>
      <Spin spinning={authState.dialogLoading}>
        <div>
          <Affix>
            <Row
              style={{
                backgroundColor: 'white',
                paddingTop: 5,
                paddingLeft: 10,
                paddingRight: 10,
                width: '100vw',
              }}
            >
              {authState?.userInfo?.df_type_user_id === ADMIN_ROLE.ADMIN ? (
                <HeaderAdmin handleClick={handleClick} />
              ) : authState?.userInfo?.df_type_user_id ===
                ADMIN_ROLE.ADMIN_EDITOR ? (
                <HeaderAdminEditor
                  handleClick={handleClick}
                  authState={authState}
                />
              ) : authState?.userInfo?.df_type_user_id ===
                ADMIN_ROLE.ADMIN_SELL_MANAGER ? (
                <HeaderAdminSellManager
                  handleClick={handleClick}
                  authState={authState}
                />
              ) : authState?.userInfo?.df_type_user_id ===
                ADMIN_ROLE.ADMIN_SERVICE_MANAGER ? (
                <HeaderAdminServiceManager
                  handleClick={handleClick}
                  authState={authState}
                />
              ) : authState?.userInfo?.df_type_user_id === SHOP_ROLE.SHOP ? (
                <HeaderShop handleClick={handleClick} />
              ) : authState?.userInfo?.df_type_user_id ===
                SHOP_ROLE.SHOP_MEMBER ? (
                <HeaderShop handleClick={handleClick} />
              ) : (
                <HeaderLogout handleClick={handleClick} authState={authState} />
                // <div></div>
              )}
            </Row>
          </Affix>

          <Divider
            style={{
              margin: 0,
            }}
          />
          {authState?.userInfo?.df_type_user_id === ADMIN_ROLE.ADMIN ? (
            <AdminMenu
              handleClick={handleClick}
              handleGetCurrentRouter={handleGetCurrentRouter}
            />
          ) : authState?.userInfo?.df_type_user_id ===
            ADMIN_ROLE.ADMIN_EDITOR ? (
            <AdminEditorMenu
              handleClick={handleClick}
              handleGetCurrentRouter={handleGetCurrentRouter}
            />
          ) : authState?.userInfo?.df_type_user_id ===
            ADMIN_ROLE.ADMIN_SELL_MANAGER ? (
            <AdminSellManagerMenu
              handleClick={handleClick}
              handleGetCurrentRouter={handleGetCurrentRouter}
            />
          ) : authState?.userInfo?.df_type_user_id ===
            ADMIN_ROLE.ADMIN_SERVICE_MANAGER ? (
            <AdminServiceManagerMenu
              handleClick={handleClick}
              handleGetCurrentRouter={handleGetCurrentRouter}
            />
          ) : authState?.userInfo?.df_type_user_id === SHOP_ROLE.SHOP ? (
            <ShopMenu
              handleClick={handleClick}
              handleGetCurrentRouter={handleGetCurrentRouter}
            />
          ) : authState?.userInfo?.df_type_user_id === SHOP_ROLE.SHOP_MEMBER ? (
            <ShopMemberMenu
              handleClick={handleClick}
              handleGetCurrentRouter={handleGetCurrentRouter}
            />
          ) : (
            <></>
          )}
        </div>
      </Spin>

      {/* Thay đổi mật khẩu */}
      <Modal
        title="Đổi mật khẩu"
        visible={isModalVisible}
        onCancel={() => {
          form.resetFields()
          setIsModalVisible(false)
        }}
        footer={null}
      >
        <Form
          {...formItemLayout}
          form={form}
          name="register"
          labelAlign="left"
          scrollToFirstError
          onFinish={(values: dataChangePassword) => onFinish(values)}
        >
          <Form.Item
            name="old_password"
            label="Mật khẩu cũ"
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập mật khẩu!',
              },
              {
                min: 6,
                max: 20,
                message: 'Vui lòng nhập từ 6 đến 20 ký tự!',
              },
            ]}
          >
            <Input.Password placeholder="Nhập mật khẩu" />
          </Form.Item>

          <Form.Item
            name="new_password"
            label="Mật khẩu mới"
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập mật khẩu!',
              },
              {
                min: 6,
                max: 20,
                message: 'Vui lòng nhập từ 6 đến 20 ký tự!',
              },
            ]}
            hasFeedback
          >
            <Input.Password placeholder="Nhập mật khẩu" />
          </Form.Item>

          <Form.Item
            name="confirm"
            label="Xác nhận mật khẩu"
            dependencies={['new_password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Nhập lại mật khẩu!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('new_password') === value) {
                    return Promise.resolve()
                  }
                  return Promise.reject(new Error('Mật khẩu không khớp!'))
                },
              }),
            ]}
          >
            <Input.Password placeholder="Nhập lại mật khẩu" />
          </Form.Item>

          <Form.Item>
            <Row style={{ width: '100%' }}>
              <Col span={6} offset={10}>
                <Button
                  danger
                  onClick={() => {
                    form.resetFields()
                    setIsModalVisible(false)
                  }}
                >
                  Thoát
                </Button>
              </Col>
              <Col span={6} offset={2}>
                <Button loading={isLoading} type="primary" htmlType="submit">
                  Lưu
                </Button>
              </Col>
            </Row>
          </Form.Item>
        </Form>
      </Modal>
    </>
  )
  //
}
