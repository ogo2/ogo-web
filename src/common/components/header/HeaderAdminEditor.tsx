import React from 'react'
import R from 'utils/R'
import { Menu, Typography } from 'antd'
import { LockOutlined, LogoutOutlined, UserOutlined } from '@ant-design/icons'

const { SubMenu } = Menu

type Props = {
  handleClick: any
  authState: any
}

export default function HeaderAdminEditor({ handleClick, authState }: Props) {
  return (
    <>
      <img
        alt=""
        src={R.images.img_logo}
        style={{
          width: 30,
          height: 30,
          padding: 3,
          objectFit: 'contain',
          marginTop: '7px',
          marginBottom: '7px',
        }}
      />
      <Typography.Title
        children="Quản lý biên tập"
        style={{ margin: 0, fontSize: '20px', marginTop: 8 }}
      />

      <div
        style={{
          flex: 1,
        }}
      >
        <Menu
          key="MenuHeader"
          style={{
            textAlign: 'end',
            lineHeight: '28px',
            borderBottom: 0,
            marginTop: 5,
          }}
          triggerSubMenuAction="click"
          onClick={handleClick}
          mode="horizontal"
        >
          <SubMenu
            key="sub15"
            icon={<UserOutlined />}
            title={authState?.userInfo ? authState?.userInfo.name : 'Refer'}
          >
            <Menu.Item key={'changePassword'} icon={<LockOutlined />}>
              Đổi mật khẩu
            </Menu.Item>
            <Menu.Item key={'logout'} icon={<LogoutOutlined />}>
              Đăng xuất
            </Menu.Item>
          </SubMenu>
        </Menu>
      </div>
    </>
  )
}
