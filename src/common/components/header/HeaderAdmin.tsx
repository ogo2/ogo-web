import R from 'utils/R'
import { Badge, Empty, Menu, Typography } from 'antd'
import {
  BellOutlined,
  LoadingOutlined,
  LockOutlined,
  LogoutOutlined,
  UserOutlined,
} from '@ant-design/icons'
import '../styles/Notification.css'
import { ADMIN_ROUTER_PATH } from 'common/config'
import { readNotification } from './NotificationApi'
import moment from 'moment'
import { useHeaderAdmin } from 'common/hooks/useHeaderAdmin'
import { useEffect, useRef, useState } from 'react'
import { number } from 'utils/ruleForm'

const { SubMenu } = Menu

export default function HeaderAdmin({ handleClick }: any) {
  const {
    authState,
    listNotification,
    countNotification,
    loadMoreNotification,
    param,
    setLoadMoreNotification,
    setParam,
    getNotification,
    pageNotiMore,
  } = useHeaderAdmin()
  const [paramHeader, setParamHeader] = useState<number>(0)
  useEffect(() => {
    setParamHeader(param.page)
  }, [])

  return (
    <>
      <img
        alt=""
        src={R.images.img_logo}
        style={{
          width: 30,
          height: 30,
          padding: 3,
          objectFit: 'contain',
          marginTop: '7px',
          marginBottom: '7px',
        }}
      />
      <Typography.Title
        children="Quản trị viên"
        style={{ margin: 0, fontSize: '20px', marginTop: 8 }}
      />

      <div
        style={{
          flex: 1,
        }}
      >
        <Menu
          key="MenuHeader"
          style={{
            textAlign: 'end',
            lineHeight: '28px',
            borderBottom: 0,
            marginTop: 5,
          }}
          triggerSubMenuAction="click"
          onClick={handleClick}
          mode="horizontal"
        >
          <SubMenu
            key="sub16"
            icon={
              countNotification ? (
                <Badge count={countNotification}>
                  <BellOutlined
                    style={{
                      textAlign: 'center',
                      fontSize: '18px',
                      justifyContent: 'center',
                    }}
                  />
                </Badge>
              ) : (
                <BellOutlined
                  style={{
                    textAlign: 'center',
                    fontSize: '18px',
                    justifyContent: 'center',
                  }}
                />
              )
            }
          >
            <div
              style={{
                paddingTop: '15px',
                paddingLeft: '10px',
                fontSize: '18px',
                borderBottom: '2px red solid',
                height: '30px',
              }}
            >
              Thông báo
            </div>
            {listNotification.length > 0 ? (
              listNotification.map((item: any, index: number) => {
                return (
                  <Menu.Item
                    key={
                      item.data?.gift_id
                        ? ADMIN_ROUTER_PATH.GIFT_CHANGE +
                          `/${item.data.gift_id}/id?=${item.id}`
                        : ADMIN_ROUTER_PATH.GIFT_CHANGE + `/${item.id} `
                    }
                    onClick={() => {
                      if (!item.is_read) {
                        readNotification(item.id)
                        getNotification()
                      }
                    }}
                    style={{
                      display: 'flex',
                      borderBottom: '1px solid gray',
                      width: '350px',
                      height: '70px',
                    }}
                  >
                    <div className="iconNotification">
                      {item.is_read ? (
                        <BellOutlined
                          style={{
                            textAlign: 'center',
                            fontSize: '18px',
                            justifyContent: 'center',
                          }}
                        />
                      ) : (
                        <Badge dot>
                          <BellOutlined
                            style={{
                              textAlign: 'center',
                              fontSize: '18px',
                              justifyContent: 'center',
                            }}
                          />
                        </Badge>
                      )}
                    </div>
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                        paddingLeft: '12px',
                        paddingRight: '4px',
                      }}
                    >
                      <div className="contendNotification">{item.content}</div>

                      <div className="timeNotification">
                        {moment(item.create_at).format('HH:mm DD/MM/YYYY')}
                      </div>
                    </div>
                  </Menu.Item>
                )
              })
            ) : (
              <Empty />
            )}
            <div
              style={{
                fontSize: '25px',
                display: 'flex',
                justifyContent: 'center',
              }}
            >
              {loadMoreNotification ? <LoadingOutlined /> : ''}
            </div>
            {paramHeader < pageNotiMore ? (
              <div
                style={{
                  marginTop: '5px',
                  height: '30px',
                  display: 'flex',
                  justifyContent: 'center',
                  cursor: 'pointer',
                }}
                onClick={() => {
                  setLoadMoreNotification(true)
                  setParamHeader(paramHeader + 1)
                  setParam({ ...param, page: paramHeader })
                }}
              >
                Xem thêm...
              </div>
            ) : (
              <div
                style={{
                  marginTop: '5px',
                  height: '30px',
                  display: 'flex',
                  justifyContent: 'center',
                }}
              ></div>
            )}
          </SubMenu>
          <SubMenu
            key="sub15"
            icon={<UserOutlined />}
            title={authState?.userInfo ? authState?.userInfo.name : 'Refer'}
          >
            <Menu.Item key={'changePassword'} icon={<LockOutlined />}>
              Đổi mật khẩu
            </Menu.Item>
            <Menu.Item key={'logout'} icon={<LogoutOutlined />}>
              Đăng xuất
            </Menu.Item>
          </SubMenu>
        </Menu>
      </div>
    </>
  )
}
