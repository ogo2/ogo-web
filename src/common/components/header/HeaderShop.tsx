import {
  BellOutlined,
  LoadingOutlined,
  LockOutlined,
  LogoutOutlined,
  MessageOutlined,
  UserOutlined,
} from '@ant-design/icons'
import { Affix, Avatar, Badge, Empty, Menu, Typography } from 'antd'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import { useShopNotification } from 'common/hooks/useShopNotification'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import R from 'utils/R'
import '../styles/Notification.css'
import { readNotification } from './NotificationApi'
import { deleteNotificationMessenger } from './NotificationSlice'

const { SubMenu } = Menu
const { Text } = Typography

type Props = {
  handleClick: any
}

export default function HeaderShop({ handleClick }: Props) {
  const {
    authState,
    setParam,
    paging,
    countNotificationOrder,
    listNotificationOrder,
    handleCountNotificationOrder,
    getDataNotificationOrder,
    pageNotiMore,
    loading,
  } = useShopNotification()
  const notificationMessenger = useSelector(
    (state: any) => state.notificationReducer.dataMessenger
  )

  const dispatch = useDispatch()

  useEffect(() => {
    handleCountNotificationOrder()
  }, [])

  return (
    <>
      <img
        alt=""
        src={R.images.img_logo}
        style={{
          width: 30,
          height: 30,
          padding: 3,
          objectFit: 'contain',
          marginTop: '7px',
          marginBottom: '7px',
        }}
      />
      <Typography.Title
        children="Nhà cung cấp"
        style={{ margin: 0, fontSize: '20px', marginTop: 8 }}
      />

      <div
        style={{
          flex: 1,
        }}
      >
        <Menu
          key="MenuHeader"
          style={{
            textAlign: 'end',
            lineHeight: '28px',
            borderBottom: 0,
            marginTop: 5,
          }}
          triggerSubMenuAction="click"
          onClick={handleClick}
          mode="horizontal"
        >
          <SubMenu
            className="domListNotificationOrder"
            key="notification"
            icon={
              <Badge count={countNotificationOrder}>
                <BellOutlined
                  style={{
                    textAlign: 'center',
                    fontSize: '20px',
                    justifyContent: 'center',
                    paddingLeft: '10px',
                  }}
                />
              </Badge>
            }
          >
            <>
              <div
                style={{
                  paddingTop: '15px',
                  paddingLeft: '10px',
                  fontSize: '18px',
                  borderBottom: '2px red solid',
                  height: '30px',
                  position: 'sticky',
                  backgroundColor: 'white',
                  zIndex: 1000,
                  width: '100%',
                  top: 0,
                }}
              >
                Thông báo
              </div>

              <>
                {listNotificationOrder.map((item: any, index: number) => (
                  <Menu.Item
                    className="item-notification"
                    key={`${ENTERPRISE_ROUTER_PATH.ORDER_DETAIL}/${item?.data.order_id}`}
                    style={{
                      display: 'flex',
                      borderBottom: '1px solid #eeeeee',
                      width: '450px',
                      height: '70px',
                    }}
                    onClick={async () => {
                      if (!item.is_read) {
                        try {
                          await readNotification(item.id)
                          handleCountNotificationOrder()
                          getDataNotificationOrder()
                        } catch (error) {
                          console.log(error)
                        }
                      }
                    }}
                  >
                    <div className="iconNotification">
                      {item.is_read ? (
                        <BellOutlined
                          style={{
                            textAlign: 'center',
                            fontSize: '20px',
                            justifyContent: 'center',
                            color: 'gray',
                          }}
                        />
                      ) : (
                        <Badge dot>
                          <BellOutlined
                            style={{
                              textAlign: 'center',
                              fontSize: '20px',
                              justifyContent: 'center',
                              color: 'gray',
                            }}
                          />
                        </Badge>
                      )}
                    </div>
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                        paddingLeft: '12px',
                        paddingRight: '4px',
                        fontSize: '13px',
                        color: 'gray',
                        lineHeight: '30px',
                      }}
                    >
                      <div>{item.data?.content}</div>

                      <div>
                        {moment(item.create_at).format('HH:mm DD/MM/YYYY')}
                      </div>
                    </div>
                  </Menu.Item>
                ))}
              </>
              {loading ? (
                <div style={{ width: '100%', textAlign: 'center' }}>
                  <LoadingOutlined spin />
                </div>
              ) : (
                ''
              )}
              {paging.current < pageNotiMore ? (
                <div
                  style={{
                    marginTop: '5px',
                    height: '30px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    cursor: 'pointer',
                  }}
                  onClick={() => {
                    setParam({ page: paging.current + 1 })
                  }}
                >
                  Xem thêm...
                </div>
              ) : (
                <div
                  style={{
                    marginTop: '5px',
                    height: '30px',
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center',
                    cursor: 'pointer',
                  }}
                ></div>
              )}
            </>
          </SubMenu>

          <SubMenu
            key="messenger"
            icon={
              <Badge count={notificationMessenger.length}>
                <MessageOutlined
                  style={{
                    textAlign: 'center',
                    fontSize: '20px',
                    justifyContent: 'center',
                    paddingLeft: '10px',
                  }}
                />
              </Badge>
            }
          >
            {/* <Messenger listNotification={listNotification} /> */}
            {notificationMessenger.length > 0 ? (
              notificationMessenger.map((item: any, index: number) => (
                <Menu.Item
                  key={`${ENTERPRISE_ROUTER_PATH.CHAT}/${item.id}`}
                  style={{
                    display: 'flex',
                    borderBottom: '1px solid #eeeeee',
                    // width: '400px',
                    justifyContent: 'space-between',
                    height: '60px',
                  }}
                  onClick={() => {
                    dispatch(deleteNotificationMessenger(item.id))
                  }}
                >
                  <div style={{ display: 'flex', marginRight: '20px' }}>
                    <div style={{ alignSelf: 'center', marginRight: '10px' }}>
                      <Badge dot />
                    </div>
                    <div style={{ alignSelf: 'center' }}>
                      <Avatar
                        style={{
                          textAlign: 'center',
                          fontSize: '18px',
                          justifyContent: 'center',
                        }}
                        src={item.User.profile_picture_url}
                        icon={
                          item.User.profile_picture_url === null && (
                            <UserOutlined />
                          )
                        }
                      />
                    </div>
                  </div>
                  <div
                    style={{
                      display: 'flex',
                      flexDirection: 'column',
                      flex: 1,
                    }}
                  >
                    <div
                      style={{
                        lineHeight: '30px',
                        fontSize: '13px',
                        color: 'gray',
                      }}
                    >
                      <Text
                        style={true ? { width: 190 } : undefined}
                        ellipsis={true ? { tooltip: item.User.name } : false}
                      >
                        {item.User.name}
                      </Text>
                    </div>
                    <div
                      style={{
                        lineHeight: '30px',
                        fontSize: '13px',
                        color: 'gray',
                      }}
                    >
                      <Text
                        style={true ? { width: 190 } : undefined}
                        ellipsis={
                          true ? { tooltip: item.Messages[0].content } : false
                        }
                      >
                        {item.Messages[0].content
                          ? item.Messages[0].content
                          : 'đã gửi một ảnh'}
                      </Text>
                    </div>
                  </div>
                  <div
                    style={{
                      alignSelf: 'center',
                      paddingRight: '10px',
                      fontSize: '13px',
                      color: 'gray',
                    }}
                  >
                    {moment(item.time_last_send).format('HH:mm DD-MM-YYYY')}
                  </div>
                </Menu.Item>
              ))
            ) : (
              <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
            )}
          </SubMenu>

          <SubMenu
            key="sub15"
            icon={<UserOutlined />}
            title={authState?.userInfo ? authState?.userInfo.name : 'Refer'}
          >
            <Menu.Item key={'changePassword'} icon={<LockOutlined />}>
              Đổi mật khẩu
            </Menu.Item>
            <Menu.Item key={'logout'} icon={<LogoutOutlined />}>
              Đăng xuất
            </Menu.Item>
          </SubMenu>
        </Menu>
      </div>
    </>
  )
}
