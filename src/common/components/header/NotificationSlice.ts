import { createSlice } from '@reduxjs/toolkit'

const initialState: any = {
  dataMessenger: [],
}

export const notificationSlice = createSlice({
  name: 'notification',
  initialState,
  reducers: {
    addNotificationMessenger: (state = initialState, action) => {
      const newList = state.dataMessenger.filter(
        (item: any) => item.id !== action.payload.data.id
      )
      state.dataMessenger = [action.payload.data, ...newList]
    },

    deleteNotificationMessenger: (state, action) => {
      const newList = state.dataMessenger.filter(
        (item: any) => item.id !== action.payload
      )
      state.dataMessenger = [...newList]
    },
  },
})

export const {
  addNotificationMessenger,
  deleteNotificationMessenger,
} = notificationSlice.actions
export default notificationSlice.reducer
