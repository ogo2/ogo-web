import {
  AppstoreOutlined,
  AreaChartOutlined,
  FileTextOutlined,
  FontSizeOutlined,
  HomeOutlined,
  MailOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons'
import { Menu } from 'antd'
import { ADMIN_ROUTER_PATH } from 'common/config'
import React from 'react'
import R from 'utils/R'

const { SubMenu } = Menu

type Props = {
  handleClick: any
  handleGetCurrentRouter: any
}

export default function AdminMenu({
  handleClick,
  handleGetCurrentRouter,
}: Props) {
  // console.log('string:', R.strings().title_header_order)
  return (
    <Menu
      // triggerSubMenuAction="click"
      onClick={handleClick}
      // style={{ width: '100%' }}
      // mode="horizontal"
      mode="inline"
      selectedKeys={[handleGetCurrentRouter()]}
      // defaultOpenKeys={['product', 'post', 'point', 'report']}
    >
      <Menu.Item
        key={ADMIN_ROUTER_PATH.DASH_BOARD}
        icon={<MailOutlined />}
        children={R.strings().title_header_dashboard}
      />

      <Menu.Item key={ADMIN_ROUTER_PATH.SHOP} icon={<HomeOutlined />}>
        {R.strings().title_header_shop}
      </Menu.Item>

      <SubMenu
        key="product"
        icon={<AppstoreOutlined />}
        title={R.strings().title_header_product}
      >
        <Menu.Item key={ADMIN_ROUTER_PATH.PRODUCT}>
          {R.strings().title_header_product}
        </Menu.Item>

        <Menu.Item key={ADMIN_ROUTER_PATH.CATEGORY_PRODUCT}>
          {R.strings().title_header_category_product}
        </Menu.Item>
      </SubMenu>

      <Menu.Item key={ADMIN_ROUTER_PATH.CUSTOMER} icon={<UserOutlined />}>
        {R.strings().title_header_customer}
      </Menu.Item>

      <Menu.Item key={ADMIN_ROUTER_PATH.ORDER} icon={<FileTextOutlined />}>
        {R.strings().title_header_order}
      </Menu.Item>

      <Menu.Item
        key={ADMIN_ROUTER_PATH.LIVE_STREAM}
        icon={<VideoCameraOutlined />}
      >
        {R.strings().title_header_live_stream}
      </Menu.Item>

      <Menu.Item
        key={ADMIN_ROUTER_PATH.SERVICE_PACK}
        icon={<AppstoreOutlined />}
      >
        {R.strings().title_header_service_pack}
      </Menu.Item>

      <SubMenu
        key="post"
        icon={<FontSizeOutlined />}
        title={R.strings().title_header_post}
      >
        <Menu.Item key={ADMIN_ROUTER_PATH.TOPIC_POST}>
          {R.strings().title_header_topic_post}
        </Menu.Item>

        <Menu.Item key={ADMIN_ROUTER_PATH.COMMUNITY_POST}>
          {R.strings().title_header_community_post}
        </Menu.Item>
      </SubMenu>

      <SubMenu
        key="point"
        icon={<AppstoreOutlined />}
        title={R.strings().title_header_point}
      >
        <Menu.Item key={ADMIN_ROUTER_PATH.GIFT_CHANGE}>
          {R.strings().title_header_gift_change}
        </Menu.Item>

        <Menu.Item key={ADMIN_ROUTER_PATH.GIFT}>
          {R.strings().title_header_gift}
        </Menu.Item>
      </SubMenu>

      <SubMenu
        key="report"
        icon={<AreaChartOutlined />}
        title={R.strings().title_header_report}
      >
        <Menu.Item key={ADMIN_ROUTER_PATH.SALES_REPORT}>
          {R.strings().title_header_sales_report}
        </Menu.Item>

        <Menu.Item key={ADMIN_ROUTER_PATH.LIVE_STREAM_REPORT}>
          {R.strings().title_header_live_stream_report}
        </Menu.Item>

        <Menu.Item key={ADMIN_ROUTER_PATH.SHOP_REPORT}>
          {R.strings().title_header_shop_report}
        </Menu.Item>
      </SubMenu>

      <Menu.Item key={ADMIN_ROUTER_PATH.CONFIG} icon={<AppstoreOutlined />}>
        {R.strings().title_header_config}
      </Menu.Item>

      <Menu.Item key={ADMIN_ROUTER_PATH.ACCOUNTS} icon={<UserOutlined />}>
        {R.strings().title_header_account}
      </Menu.Item>
    </Menu>
  )
}
