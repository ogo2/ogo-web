import {
  AppstoreOutlined,
  AreaChartOutlined,
  FileTextOutlined,
  FontSizeOutlined,
  HomeOutlined,
  MailOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons'
import { Menu } from 'antd'
import { ADMIN_ROUTER_PATH } from 'common/config'
import React from 'react'
import R from 'utils/R'

const { SubMenu } = Menu

type Props = {
  handleClick: any
  handleGetCurrentRouter: any
}

export default function AdminSellManagerMenu({
  handleClick,
  handleGetCurrentRouter,
}: Props) {
  return (
    <Menu
      onClick={handleClick}
      mode="inline"
      selectedKeys={[handleGetCurrentRouter()]}
    >
      <Menu.Item key={ADMIN_ROUTER_PATH.PRODUCT} icon={<AppstoreOutlined />}>
        {R.strings().title_header_product}
      </Menu.Item>

      <Menu.Item key={ADMIN_ROUTER_PATH.CUSTOMER} icon={<UserOutlined />}>
        {R.strings().title_header_customer}
      </Menu.Item>

      <Menu.Item key={ADMIN_ROUTER_PATH.ORDER} icon={<FileTextOutlined />}>
        {R.strings().title_header_order}
      </Menu.Item>
    </Menu>
  )
}
