import {
  AppstoreOutlined,
  AreaChartOutlined,
  FileTextOutlined,
  FontSizeOutlined,
  HomeOutlined,
  MailOutlined,
  MessageOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons'
import { Menu, Badge } from 'antd'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import { useListMessageNotRead } from 'features/enterprise/chat/hooks/useListMessageNotRead'
import React from 'react'
import R from 'utils/R'

const { SubMenu } = Menu

type Props = {
  handleClick: any
  handleGetCurrentRouter: any
}

export default function ShopMemberMenu({
  handleClick,
  handleGetCurrentRouter,
}: Props) {
  const { listTopicMessageNotRead } = useListMessageNotRead()
  return (
    <Menu
      onClick={handleClick}
      mode="inline"
      selectedKeys={[handleGetCurrentRouter()]}
    >
      <Menu.Item
        key={ENTERPRISE_ROUTER_PATH.DASH_BOARD}
        icon={<MailOutlined />}
        children={R.strings().title_header_dashboard}
      />
      <Menu.Item
        key={ENTERPRISE_ROUTER_PATH.PRODUCT}
        icon={<AppstoreOutlined />}
      >
        {R.strings().title_header_product}
      </Menu.Item>

      <Menu.Item key={ENTERPRISE_ROUTER_PATH.STOCK} icon={<HomeOutlined />}>
        {R.strings().title_header_stock}
      </Menu.Item>

      <Menu.Item key={ENTERPRISE_ROUTER_PATH.CUSTOMER} icon={<UserOutlined />}>
        {R.strings().title_header_customer}
      </Menu.Item>

      <Menu.Item key={ENTERPRISE_ROUTER_PATH.ORDER} icon={<FileTextOutlined />}>
        {R.strings().title_header_order}
      </Menu.Item>

      <Menu.Item
        key={ENTERPRISE_ROUTER_PATH.LIVE_STREAM}
        icon={<VideoCameraOutlined />}
      >
        {R.strings().title_header_live_stream}
      </Menu.Item>

      <Menu.Item
        key={ENTERPRISE_ROUTER_PATH.CHAT}
        icon={
          <Badge size="small" count={listTopicMessageNotRead?.length}>
            <MessageOutlined />
          </Badge>
        }
      >
        {R.strings().title_header_chat}
      </Menu.Item>

      <SubMenu
        key="sub4"
        icon={<FontSizeOutlined />}
        title={R.strings().title_header_post}
      >
        <Menu.Item key={ENTERPRISE_ROUTER_PATH.FORUM_POST}>
          {R.strings().title_header_forum_post}
        </Menu.Item>
      </SubMenu>

      <SubMenu
        key="sub5"
        icon={<AreaChartOutlined />}
        title={R.strings().title_header_report}
      >
        <Menu.Item key={ENTERPRISE_ROUTER_PATH.SALES_REPORT}>
          {R.strings().title_header_sales_report}
        </Menu.Item>

        <Menu.Item key={ENTERPRISE_ROUTER_PATH.LIVE_STREAM_REPORT}>
          {R.strings().title_header_live_stream_report}
        </Menu.Item>
      </SubMenu>
    </Menu>
  )
}
