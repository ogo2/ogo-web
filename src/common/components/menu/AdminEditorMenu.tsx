import { AppstoreOutlined, FontSizeOutlined } from '@ant-design/icons'
import { Menu } from 'antd'
import { ADMIN_ROUTER_PATH } from 'common/config'
import React from 'react'
import R from 'utils/R'

const { SubMenu } = Menu

type Props = {
  handleClick: any
  handleGetCurrentRouter: any
}

export default function AdminEditorMenu({
  handleClick,
  handleGetCurrentRouter,
}: Props) {
  return (
    <Menu
      onClick={handleClick}
      mode="inline"
      selectedKeys={[handleGetCurrentRouter()]}
    >
      <Menu.Item key={ADMIN_ROUTER_PATH.PRODUCT} icon={<AppstoreOutlined />}>
        {R.strings().title_header_product}
      </Menu.Item>

      <Menu.Item
        key={ADMIN_ROUTER_PATH.CATEGORY_PRODUCT}
        icon={<AppstoreOutlined />}
      >
        {R.strings().title_header_category_product}
      </Menu.Item>

      <Menu.Item key={ADMIN_ROUTER_PATH.TOPIC_POST} icon={<FontSizeOutlined />}>
        {R.strings().title_header_topic_post}
      </Menu.Item>

      <Menu.Item key={ADMIN_ROUTER_PATH.GIFT} icon={<AppstoreOutlined />}>
        {R.strings().title_header_gift}
      </Menu.Item>
    </Menu>
  )
}
