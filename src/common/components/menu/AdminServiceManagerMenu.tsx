import {
  AppstoreOutlined,
  UserOutlined,
  VideoCameraOutlined,
} from '@ant-design/icons'
import { Menu } from 'antd'
import { ADMIN_ROUTER_PATH } from 'common/config'
import React from 'react'
import R from 'utils/R'

const { SubMenu } = Menu

type Props = {
  handleClick: any
  handleGetCurrentRouter: any
}

export default function AdminServiceManagerMenu({
  handleClick,
  handleGetCurrentRouter,
}: Props) {
  return (
    <Menu
      onClick={handleClick}
      mode="inline"
      selectedKeys={[handleGetCurrentRouter()]}
    >
      <Menu.Item key={ADMIN_ROUTER_PATH.CUSTOMER} icon={<UserOutlined />}>
        {R.strings().title_header_customer}
      </Menu.Item>

      <Menu.Item
        key={ADMIN_ROUTER_PATH.LIVE_STREAM}
        icon={<VideoCameraOutlined />}
      >
        {R.strings().title_header_live_stream}
      </Menu.Item>

      <Menu.Item
        key={ADMIN_ROUTER_PATH.SERVICE_PACK}
        icon={<AppstoreOutlined />}
      >
        {R.strings().title_header_service_pack}
      </Menu.Item>

      <Menu.Item
        key={ADMIN_ROUTER_PATH.GIFT_CHANGE}
        icon={<AppstoreOutlined />}
      >
        {R.strings().title_header_gift_change}
      </Menu.Item>
    </Menu>
  )
}
