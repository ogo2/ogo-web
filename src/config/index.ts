export const environment = {
  api_host: process.env.REACT_APP_API_HOST,
  ws_host: process.env.REACT_APP_WS_HOST,
  api_host_test_spam: process.env.REACT_APP_API_TEST_SPAM,
  ws_host_test_spam: process.env.REACT_APP_WS_TEST_SPAM,
}
