import { ApiClient } from 'services/ApiService'
import { MEDIA_TYPE } from 'utils/constants'

const requestUploadImageProduct = (payload: any) =>
  ApiClient.post(`/files/upload/single/${payload.type}`, payload.image)
const requestUploadVideoProduct = (payload: any) =>
  ApiClient.post(`/files/upload/single/${payload.type}`, payload.video)

interface ImageUploadResponse {
  filename: string
  path: string
  url: string
}
export const uploadImage = async (file: any): Promise<ImageUploadResponse> => {
  try {
    const dataImage = new FormData()
    dataImage.append('image', file.fileList[0].originFileObj)
    const payload = {
      type: MEDIA_TYPE.IMAGE,
      image: dataImage,
    }
    const resUploadImage = await requestUploadImageProduct(payload)
    return resUploadImage.data as ImageUploadResponse
  } catch (error) {
    throw error
  }
}

export const uploadVideo = async (file: any): Promise<ImageUploadResponse> => {
  try {
    const dataVideo = new FormData()
    dataVideo.append('video', file.fileList[0].originFileObj)
    const payload = {
      type: MEDIA_TYPE.VIDEO,
      image: dataVideo,
    }
    const resUploadImage = await requestUploadImageProduct(payload)
    return resUploadImage.data as ImageUploadResponse
  } catch (error) {
    throw error
  }
}
