import R from './R'

export const REG_PHONE = /(84|0[3|5|7|8|9])+([0-9]{8})\b/

export const IS_ACTIVE = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const IS_LIVESTREAM = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const IS_GIFT_ACTIVE = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const IS_DEFAULT = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const CONFIG_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const CONFIG_TYPE = {
  ATTENDANCE: 1,
  ORDER_PROMOTION: 2,
  DAILY_TURN: 3,
  REFERRAL_APP: 4,
  REFERRAL_CODE: 5,
  REFERRAL_MEMBER: 6,
}
export const GIFT_STATUS = {
  PENDING: 0, // đang đợi xác nhận gửi quà
  CONFIRMED: 1, // đã xác nhận
  SENT_GIFT: 2, // đã gửi quà cho người nhận
  USED: 3, // trạng thái quà đã sử dụng
}

export const FILTER_TYPE = {
  PRICE_MIN_MAX: 2, // Giá từ thấp đến cao
  PRICE_MAX_MIN: 1, // Giá từ cao đến thấp
  TIME_NEW_OLD: 1, // Tin đăng mới nhất
  TIME_OLE_NEW: 2, // Tin đăng cũ nhất
}
export const BUY_TYPE = {
  ALL: 0,
  SELL: 1,
  NOTSELL: 2,
}
export const IS_PUSH = {
  PUSHED: 1,
  UN_PUSHED: 0,
}
export const DF_NOTIFICATION = {
  ALL: 1, // thông báo tất cả
  ORDER_SHOP: 2, // thông báo trạng thái đơn hàng
  COMMENT_POST: 3, // thông báo có người bình luận bài viết
  LIKE_POST: 4, // thông báo có người thích bài viết
  SEND_COMMENT: 5, // thông báo shop trả lời bình luận
  LIKE_COMMENT: 6, // thông báo shop thích bình luận
  SHOP_CREATE_LIVESTREAM: 7, // thông báo shop tạo livestream
  REGISTER_USER: 8, // thông báo đăng kí tài khoản thành công được cộng điểm
  PURCHASE_GIFT: 9, // thông báo có yêu cầu đổi quà web admin
  CONFIRM_PURCHASE_GIFT: 10, // Thông báo trạng thái đổi quà của bạn
  NEW_ORDER: 11, // Thông báo shop có đơn hàng cần duyệt
  GIFT_EXCHANGE: 12, // Thông báo trừ điểm
}
export const SOCKET = {
  ADMIN: 'admin',
  SHOP: 'shop',
}
export const STOKE_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const ORDER_STATUS = {
  PENDING: 1, // chờ xác nhận
  CONFIRMED: 2, // đã xác nhận
  SHIP: 3, // đang giao
  SUCCCESS: 4, // hoàn thành
  CANCELED: 5, // huỷ
}
export const TYPE_POINT_TRANSACTION_HISTORY = {
  ADD: 1,
  SUB: 0,
}
export const PRODUCT_STATUS = {
  AVAILABLE: 1,
  UNAVAILABLE: 0,
}
export const PRODUCT_STOCK_STATUS = {
  AVAILABLE: 1,
  UNAVAILABLE: 0,
}
export const PRODUCT_PRICE_STATUS = {
  AVAILABLE: 1,
  UNAVAILABLE: 0,
}
export const MEDIA_TYPE = {
  IMAGE: 0,
  VIDEO: 1,
}
export const GENDER = {
  MALE: 0,
  FEMALE: 1,
}
export const CUSTOMER = {
  GENERAL: 1,
  ORDER: 2,
  HISTORY: 3,
}
export const CART_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const USER_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const CATEGORY_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const TOPIC_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const DAILY_ATTENDANCE_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const IS_READ = {
  READ: 1,
  NOT_READ: 0,
}
export const USER_TOPIC_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const TAB = {
  PRODUCT: 0,
  CATEGORY: 1,
  LIVESTREAM: 2,
  POST: 3,
}
export const USER_TOPIC_MESSAGE_STATUS = {
  UNBLOCK: 1,
  BLOCK: 0,
}

export const MODE_MESSAGE = {
  DEFAULT: 1,
  SEARCH: 2,
  NOT_READ: 3,
}

export const LIVESTREAM_ENABLE = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const REACTION_TYPE = {
  LIKE: 1,
  HEART: 2,
  LOVE: 3,
  HAHA: 4,
  SURPRISE: 5,
  SAD: 6,
  ANGRY: 7,
}
export const LIVESTREAM_STATUS = {
  INITIAL: 0,
  STREAMING: 1,
  FINISHED: 2,
}
export const SHOP_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
}
export const POST_STATUS = {
  PRIVATE: 0, // chỉ riếng mình tôi thấy
  ALL: 1, // tất cả thấy
  CUSTOMER: 2, // chỉ khách hàng thấy
  SHOP: 3, // chỉ tài khoản gian hàng thấy
  SHOP_INTERNAL: 4, // chỉ nội bộ shop thấy
}
export const PAKAGE_STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
}

export const API_STATUS = {
  UNAUTHORIZED: 401,
  FORBIDEN: 403,
  NOT_FOUND: 404,
}

export const CHANGE_STATUS = {
  ACTIVE: 'Bật hoạt động',
  INACTIVE: 'Tạm dừng hoạt động',
}

export const PRODUCT_TYPE_VIDEO_IMAGE = {
  IMAGE: 0,
  VIDEO: 1,
}

export const STATUS = {
  ACTIVE: 1,
  INACTIVE: 0,
}

export const PAKAGE_CATEGORY = {
  ADD_PAKAGE: 1,
  SUB_PAKAGE: 2,
}

export const TAB_SHOP = {
  HISTORY: 0,
  PRODUCT: 1,
  ORDER: 2,
  LIVESTREAM: 3,
}

// export const ROLE = {
//   ADMIN: 1,
//   SHOP: 2,
//   CUSTOMER: 3,
//   ADMIN_EDITOR: 4,
//   ADMIN_SELL_MANAGER: 5,
//   ADMIN_SERVICE_MANAGER: 6,
//   SHOP_MEMBER: 7,
// }

export const ROLE_NAMES = {
  ADMIN: 'admin',
  ADMIN_EDITOR: 'admin_editor',
  ADMIN_SELL_MANAGER: 'admin_sell_manager',
  ADMIN_SERVICE_MANAGER: 'admin_service_manager',
  SHOP: 'shop',
  SHOP_MEMBER: 'shop_member',
  CUSTOMER: 'customer',
}
export const SHOP_ROLE = {
  SHOP: 2,
  SHOP_MEMBER: 7,
}
export const ADMIN_ROLE = {
  ADMIN: 1,
  ADMIN_EDITOR: 4,
  ADMIN_SELL_MANAGER: 5,
  ADMIN_SERVICE_MANAGER: 6,
}
export const ADMIN_POST_TOPIC = {
  PROMOTION: 1, // Khuyến mãi
  NOTIFICATION: 2, // Thông báo
}

export const PRODUCT_TYPE = {
  DETAIL: 1, //Thông tin sản phẩm
  SELL: 2, //Khách hàng mua sản phẩm
  ORDER: 3, //Danh sách đơn hàng
}

export const REPORT_TYPE = {
  CATEGORY: 1, //Nhóm sản phẩm
  CUSTOMER: 2, //Theo khách hàng
}
