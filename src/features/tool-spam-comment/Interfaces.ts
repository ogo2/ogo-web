export interface IResBody {
  status: number;
  code: number;
  message: string;
}

export interface IResCheckHasAccountAPI extends IResBody {
  data: {
    phone?: string
  }
}

export interface IResLoginAPI extends IResBody {
  data: {
    token: string
    phone: string
  }
}

export interface IResJoinLiveAPI extends IResBody {

}
