import { useEffect, useState } from 'react'
import { requestGetListGoogleAccount } from '../configApi'

export const useGetListGoogleAccount = () => {
  const [listGoogleAccount, setListGoogleAccount] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [params, setParams] = useState<any>({
    search: '',
    page: 1,
    status: undefined,
  })
  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  useEffect(() => {
    getListGoogleAccount()
  }, [params])

  const getListGoogleAccount = async () => {
    setIsLoading(true)
    try {
      const res = await requestGetListGoogleAccount(params)
      const tableData = res.data.map((item: any, index: number) => {
        return {
          ...item,
          key: item.id,
          stt: index + 1,
          name_shop: item.Shop?.name,
        }
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setListGoogleAccount(tableData)
      setPaging(formattedPaging)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }
  return {
    listGoogleAccount,
    setListGoogleAccount,
    isLoading,
    setIsLoading,
    params,
    setParams,
    paging,
    setPaging,
    getListGoogleAccount,
  }
}
