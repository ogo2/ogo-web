import { Card } from 'antd'
import styled from 'styled-components'

export const CardStyled = styled(Card)`
  width: 100%;
  margin-bottom: 20px;
`
