import { PlusOutlined } from '@ant-design/icons'
import { Button, Input, Space, Col, Select } from 'antd'
import { useState, useRef } from 'react'
import './css/Header.css'
import { requestCreateGoogleAccount } from '../configApi'

const { Search } = Input
const { Option } = Select

type HeaderProps = {
  onSearchSubmit: (searchKey: string) => void
  onStatusSubmit: (status: number) => void
  isSearchLoading: boolean
}

export const Header = ({
  onSearchSubmit,
  onStatusSubmit,
  isSearchLoading,
}: HeaderProps) => {
  const [searchKey, setSearchKey] = useState('')
  const [isTyping, setIsTyping] = useState(false)
  const timeOut = useRef<any>(null)

  return (
    <Space>
      <Search
        loading={isTyping}
        className="input-search_account"
        value={searchKey}
        placeholder="Tìm kiếm tài khoản"
        onChange={(e: any) => {
          setSearchKey(e.target.value === '' ? '' : e.target.value)
          if (timeOut.current) {
            setIsTyping(true)
            clearTimeout(timeOut.current)
          }
          timeOut.current = setTimeout(() => {
            onSearchSubmit(searchKey)
            setIsTyping(false)
          }, 300)
        }}
      />
      <Col>
        <Select
          style={{ minWidth: '180px' }}
          className="select-status_account"
          placeholder="Trạng thái"
          onChange={(value: number) => {
            onStatusSubmit(value)
          }}
          allowClear
        >
          <Option value={1}>Hoạt động</Option>
          <Option value={0}>Ngừng hoạt động</Option>
        </Select>
      </Col>
      <Button
        type="primary"
        style={{ minWidth: '70px' }}
        onClick={async () => {
          const res = await requestCreateGoogleAccount()
          window.open(res.data.url)
          window.focus()
        }}
      >
        <PlusOutlined />
        Thêm tài khoản
      </Button>
    </Space>
  )
}
