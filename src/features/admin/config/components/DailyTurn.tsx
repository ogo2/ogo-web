import React, { useState, useEffect, useRef } from 'react'
import { Row, InputNumber, Spin, message } from 'antd'
import { CardStyled } from './Styled'
import { getDetailConfig, updateDataConfig } from '../configApi'
import { CONFIG_TYPE } from 'utils/constants'

function DailyTurn() {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [dataDailyTurn, setDataDailyTurn] = useState<any>(null)
  const [isTyping, setIsTyping] = useState<boolean>(false)
  const timeOut = useRef<any>(null)

  useEffect(() => {
    if (!isTyping && dataDailyTurn) requestChangeValue()
  }, [isTyping])

  useEffect(() => {
    getDataDailyTurn()
  }, [])

  const getDataDailyTurn = async () => {
    try {
      setLoading(true)
      const res = await getDetailConfig(CONFIG_TYPE.DAILY_TURN)
      setDataDailyTurn(res.data)
    } catch (error) {
      console.log('error', error)
    } finally {
      setLoading(false)
    }
  }

  const requestChangeValue = async () => {
    try {
      setLoading(true)
      const res = await updateDataConfig(CONFIG_TYPE.DAILY_TURN, {
        status: dataDailyTurn?.status,
        value: dataDailyTurn?.value,
      })
      console.log('res', res)
      message.success('Cập nhật thành công.')
    } catch (error) {
      console.log('error', error)
      message.error('Cập nhật thất bại.')
    } finally {
      setLoading(false)
    }
  }

  const onChangeValue = (value: number) => {
    setDataDailyTurn({ ...dataDailyTurn, value })
    if (timeOut.current) {
      setIsTyping(true)
      clearTimeout(timeOut.current)
    }
    timeOut.current = setTimeout(() => {
      setIsTyping(false)
    }, 300)
  }

  return (
    <CardStyled title={<strong>Vòng quay may mắn</strong>}>
      <Spin spinning={isLoading}>
        <Row style={{ width: '100%' }} justify="space-between">
          <label htmlFor="">Số lượt quay trên ngày: </label>
          <InputNumber
            value={dataDailyTurn?.value || 0}
            onChange={onChangeValue}
            style={{ width: '50%' }}
            placeholder="Nhập số lượt quay trên ngày."
          />
        </Row>
      </Spin>
    </CardStyled>
  )
}
export default DailyTurn
