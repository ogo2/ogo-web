import React, { useState, useEffect, useRef } from 'react'
import { Row, Input, Spin, message } from 'antd'
import { CardStyled } from './Styled'
import { getDetailConfig, updateDataConfig } from '../configApi'
import { CONFIG_TYPE } from 'utils/constants'
import { formatPrice } from 'utils/ruleForm'
import { notificationError } from 'utils/notification'
function Referral() {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [dataReferralApp, setDataReferralApp] = useState<any>(null)
  const [dataReferralCode, setDataReferralCode] = useState<any>(null)
  const [dataReferralMember, setDataReferralMember] = useState<any>(null)

  const [isTypingReferralApp, setIsTypingReferralApp] = useState<boolean>(false)
  const [isTypingReferralCode, setIsTypingReferralCode] = useState<boolean>(
    false
  )
  const [isTypingReferralMember, setIsTypingReferralMember] = useState<boolean>(
    false
  )
  const timeOut = useRef<any>(null)

  useEffect(() => {
    if (!isTypingReferralApp && dataReferralApp?.value)
      if (dataReferralApp?.value < 5000000) {
        requestChangeValueReferralApp()
      }
  }, [isTypingReferralApp])

  useEffect(() => {
    if (!isTypingReferralCode && dataReferralCode?.value)
      if (dataReferralCode?.value < 5000000) {
        requestChangeValueReferralCode()
      }
  }, [isTypingReferralCode])

  useEffect(() => {
    if (!isTypingReferralMember && dataReferralMember?.value)
      if (dataReferralMember?.value < 5000000) {
        requestChangeValueReferralMember()
      }
  }, [isTypingReferralMember])

  useEffect(() => {
    getDataReferralApp()
    getDataReferralCode()
    getDataReferralMember()
  }, [])
  const getDataReferralApp = async () => {
    try {
      setLoading(true)
      const res = await getDetailConfig(CONFIG_TYPE.REFERRAL_APP)
      setDataReferralApp(res.data)
    } catch (error) {
      console.log('error', error)
    } finally {
      setLoading(false)
    }
  }
  const getDataReferralCode = async () => {
    try {
      setLoading(true)
      const res = await getDetailConfig(CONFIG_TYPE.REFERRAL_CODE)
      setDataReferralCode(res.data)
    } catch (error) {
      console.log('error', error)
    } finally {
      setLoading(false)
    }
  }
  const getDataReferralMember = async () => {
    try {
      setLoading(true)
      const res = await getDetailConfig(CONFIG_TYPE.REFERRAL_MEMBER)
      setDataReferralMember(res.data)
    } catch (error) {
      console.log('error', error)
    } finally {
      setLoading(false)
    }
  }

  const requestChangeValueReferralApp = async () => {
    try {
      setLoading(true)
      const res = await updateDataConfig(CONFIG_TYPE.REFERRAL_APP, {
        status: dataReferralApp?.status,
        value: dataReferralApp?.value,
      })
      message.success('Cập nhật thành công.')
    } catch (error) {
      console.log('error', error)
      message.error('Cập nhật thất bại.')
    } finally {
      setLoading(false)
    }
  }
  const requestChangeValueReferralCode = async () => {
    try {
      setLoading(true)
      const res = await updateDataConfig(CONFIG_TYPE.REFERRAL_CODE, {
        status: dataReferralCode?.status,
        value: dataReferralCode?.value,
      })
      message.success('Cập nhật thành công.')
    } catch (error) {
      console.log('error', error)
      message.error('Cập nhật thất bại.')
    } finally {
      setLoading(false)
    }
  }
  const requestChangeValueReferralMember = async () => {
    try {
      setLoading(true)
      const res = await updateDataConfig(CONFIG_TYPE.REFERRAL_MEMBER, {
        status: dataReferralMember?.status,
        value: dataReferralMember?.value,
      })
      message.success('Cập nhật thành công.')
    } catch (error) {
      console.log('error', error)
      message.error('Cập nhật thất bại.')
    } finally {
      setLoading(false)
    }
  }

  const onChangeValueReferralApp = (value: number) => {
    setDataReferralApp({ ...dataReferralApp, value })
    if (timeOut.current) {
      setIsTypingReferralApp(true)
      clearTimeout(timeOut.current)
    }
    timeOut.current = setTimeout(() => {
      setIsTypingReferralApp(false)
    }, 300)
  }

  const onChangeValueReferralCode = (value: number) => {
    setDataReferralCode({ ...dataReferralCode, value })
    if (timeOut.current) {
      setIsTypingReferralCode(true)
      clearTimeout(timeOut.current)
    }
    timeOut.current = setTimeout(() => {
      setIsTypingReferralCode(false)
    }, 300)
  }

  const onChangeValueReferralMember = (value: number) => {
    setDataReferralMember({ ...dataReferralMember, value })
    if (timeOut.current) {
      setIsTypingReferralMember(true)
      clearTimeout(timeOut.current)
    }
    timeOut.current = setTimeout(() => {
      setIsTypingReferralMember(false)
    }, 300)
  }

  return (
    <CardStyled title={<strong>Giới thiệu</strong>}>
      <Spin spinning={isLoading}>
        <Row style={{ width: '100%' }} justify="space-between">
          <label htmlFor="">Giới thiệu ứng dụng: </label>
          <Input
            value={
              dataReferralApp?.value ? formatPrice(dataReferralApp?.value) : 0
            }
            onChange={(e: any) => {
              let value = e.target.value?.replace(/[^\w\s]/gi, '')
              if (dataReferralApp.value != value)
                onChangeValueReferralApp(parseInt(value))
            }}
            style={{ width: '50%' }}
            placeholder="Nhập số điểm."
          />
        </Row>
        <Row style={{ marginTop: 5 }} justify="end">
          <span style={{ color: '#990000', fontSize: 'x-small' }}>
            * Với đơn hàng đầu tiên của người được giới thiệu.
          </span>
        </Row>
        <Row style={{ marginTop: 5 }} justify="end">
          <span style={{ color: 'red' }}>
            {dataReferralApp?.value > 5000000 || dataReferralApp?.value <= 0
              ? 'Vui lòng nhập số dương từ 1 tới 5,000,000'
              : ''}
          </span>
        </Row>

        <Row style={{ width: '100%', marginTop: 20 }} justify="space-between">
          <label htmlFor="">Đăng kí với mã giới thiệu: </label>
          <Input
            value={
              dataReferralCode?.value ? formatPrice(dataReferralCode?.value) : 0
            }
            onChange={(e: any) => {
              let value = e.target.value?.replace(/[^\w\s]/gi, '')
              if (dataReferralCode.value != value)
                onChangeValueReferralCode(parseInt(value))
            }}
            style={{ width: '50%' }}
            placeholder="Nhập số điểm."
            min={0}
          />
        </Row>
        <Row style={{ marginTop: 5 }} justify="end">
          <span style={{ color: 'red' }}>
            {dataReferralCode?.value > 5000000 || dataReferralCode?.value <= 0
              ? 'Vui lòng nhập số dương từ 1 tới 5,000,000'
              : ''}
          </span>
        </Row>
        <Row style={{ width: '100%', marginTop: 20 }} justify="space-between">
          <label htmlFor="">Đăng kí thành viên: </label>
          <Input
            value={
              dataReferralMember?.value
                ? formatPrice(dataReferralMember?.value)
                : 0
            }
            onChange={(e: any) => {
              let value = e.target.value?.replace(/[^\w\s]/gi, '')
              if (dataReferralMember.value != value)
                onChangeValueReferralMember(parseInt(value))
            }}
            style={{ width: '50%' }}
            placeholder="Nhập số điểm."
            min={0}
          />
        </Row>
        <Row style={{ marginTop: 5 }} justify="end">
          <span style={{ color: 'red' }}>
            {dataReferralMember?.value > 5000000 ||
            dataReferralMember?.value <= 0
              ? 'Vui lòng nhập số dương từ 1 tới 5,000,000'
              : ''}
          </span>
        </Row>
      </Spin>
    </CardStyled>
  )
}
export default Referral
