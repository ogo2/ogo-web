import {
  Button,
  Checkbox,
  Col,
  Form,
  Input,
  message,
  Modal,
  Row,
  Select,
} from 'antd'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import {} from '../configApi'
// import { formatPrice } from 'utils/ruleForm'
type Props = {
  visible: boolean
  onCancel?: any
  data?: any
  onCreateNewService?: any
  onUpdateService?: any
  isLoading: boolean
}

function convertDataToFrom(data: any) {
  if (!data) {
    return {
      is_default: null,
      minus: null,
      name: null,
      pakage_category_id: null,
      price: null,
    }
  } else {
    return {
      data,
    }
  }
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
}

export const AddEditGifts = ({
  visible,
  onCancel,
  data,
  onCreateNewService,
  onUpdateService,
  isLoading,
}: Props) => {
  const [form] = Form.useForm()
  const initialValue = convertDataToFrom(data)
  const [listCategory, setListCategory] = useState<Array<any>>([])

  const onFinish = async (values: any) => {
    try {
      if (!data) {
        console.log('value', values)

        onCreateNewService(values, form.resetFields)
      } else {
        onUpdateService(values, form.resetFields)
      }
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <Modal
      onCancel={() => {
        form.resetFields()
        onCancel()
      }}
      maskClosable={false}
      footer={null}
      title={data ? 'Sửa quà tặng ' : 'Thêm quà tặng '}
      visible={visible}
    >
      <Form
        {...formItemLayout}
        form={form}
        name="add"
        labelAlign="left"
        onFinish={(values: any) => onFinish(values)}
        initialValues={initialValue.data}
        scrollToFirstError
      >
        <Form.Item
          name="name"
          label="Tên quà tặng"
          rules={[
            {
              type: 'string',
              message: 'Nhập tên quà tặng',
            },
            {
              required: true,
              message: 'Vui lòng nhập tên quà tặng!',
              validator: (_, value) => {
                if (value.trim() === '') {
                  return Promise.reject()
                }
                return Promise.resolve()
              },
            },
            {
              min: 3,
              max: 50,
              message: 'Vui lòng nhập từ 3 đến 50 ký tự!',
            },
            // {
            //   message: 'Vui lòng không nhập kí tự đặc biệt',
            //   validator: (_, value) => {
            //     const reg = /[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềếýểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]/u
            //     if (!reg.test(value)) {
            //       return Promise.resolve()
            //     }
            //     return Promise.reject()
            //   },
            // },
          ]}
        >
          <Input placeholder="Nhập tên quà tặng" />
        </Form.Item>

        <Form.Item
          name="value"
          label="Số điểm"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập điểm',
            },
            {
              message: 'Vui lòng chỉ nhập số ',
              validator: (_, value) => {
                const reg = /^\d+$/
                if (!reg.test(value)) {
                  return Promise.reject()
                }
                return Promise.resolve()
              },
            },
          ]}
        >
          <Input placeholder="Nhập số điểm" />
        </Form.Item>

        <Form.Item
          name="percent"
          label="Tỉ lệ trúng"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập tỉ lệ trúng',
            },
            {
              message: 'Vui lòng nhập giá là số',
              validator: (_, value) => {
                const reg = /^\d+$/
                if (reg.test(value)) {
                  return Promise.resolve()
                }
                return Promise.reject()
              },
            },
            {
              message: 'Vui lòng nhập giá dưới 100',
              validator: (_, value) => {
                if (value > 100) {
                  return Promise.reject()
                }
                return Promise.resolve()
              },
            },
          ]}
        >
          <Input placeholder="Nhập tỉ lệ trúng" />
        </Form.Item>
        <Form.Item
          name="max_per_day"
          label="Số lượt quay trong ngày"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập số lượt quay trong ngày',
            },
            {
              message: 'Vui lòng nhập giá là số',
              validator: (_, value) => {
                const reg = /^\d+$/
                if (reg.test(value)) {
                  return Promise.resolve()
                }
                return Promise.reject()
              },
            },
            {
              message: 'Vui lòng nhập giá dưới 100',
              validator: (_, value) => {
                if (value > 100) {
                  return Promise.reject()
                }
                return Promise.resolve()
              },
            },
          ]}
        >
          <Input placeholder="Nhập số lượt quay trong ngày" />
        </Form.Item>

        <Form.Item>
          <Row gutter={[16, 16]}>
            <Col xs={{ span: 6, offset: 10 }}>
              <Button
                danger
                onClick={() => {
                  form.resetFields()
                  onCancel()
                }}
              >
                Huỷ
              </Button>
            </Col>
            <Col xs={{ span: 6, offset: 2 }}>
              <Button loading={isLoading} type="primary" htmlType="submit">
                {data ? 'Cập nhật' : 'Thêm quà tặng'}
              </Button>
            </Col>
          </Row>
        </Form.Item>
      </Form>
    </Modal>
  )
}
