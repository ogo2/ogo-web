import React, { useState, useEffect, useRef } from 'react'
import { Row, Switch, InputNumber, Spin, message } from 'antd'
import { CardStyled } from './Styled'
import {
  getDetailConfig,
  changeStatusConfig,
  updateDataConfig,
} from '../configApi'
import { CONFIG_TYPE, CONFIG_STATUS } from 'utils/constants'

function Attendance() {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [isLoadingSwitch, setLoadingSwitch] = useState<boolean>(false)
  const [dataAttendance, setDataAttendance] = useState<any>(null)
  const [isTyping, setIsTyping] = useState<boolean>(false)
  const timeOut = useRef<any>(null)

  useEffect(() => {
    if (!isTyping && dataAttendance?.value) requestChangeValue()
  }, [isTyping])

  useEffect(() => {
    getDataAttendance()
  }, [])
  const getDataAttendance = async () => {
    try {
      setLoading(true)
      const res = await getDetailConfig(CONFIG_TYPE.ATTENDANCE)
      setDataAttendance(res.data)
    } catch (error) {
      console.log('error', error)
    } finally {
      setLoading(false)
    }
  }
  const requestChangeValue = async () => {
    try {
      setLoading(true)
      const res = await updateDataConfig(CONFIG_TYPE.ATTENDANCE, {
        status: dataAttendance?.status,
        value: dataAttendance?.value,
      })
      console.log('res', res)
      message.success('Cập nhật thành công.')
    } catch (error) {
      console.log('error', error)
      message.error('Cập nhật thất bại.')
    } finally {
      setLoading(false)
    }
  }
  const onChangeStatus = async (checked: boolean, e: any) => {
    e.stopPropagation()
    try {
      setLoadingSwitch(true)
      const res = await changeStatusConfig(CONFIG_TYPE.ATTENDANCE)
      setDataAttendance({ ...dataAttendance, status: res.data.status })
      message.success('Thay đổi trạng thái thành công.')
    } catch (error) {
      console.log('error', error)
      message.error('Đổi trạng thái thất bại.')
    } finally {
      setLoadingSwitch(false)
    }
  }
  const onChangeValue = (value: number) => {
    // setValueAttendance(value)
    setDataAttendance({ ...dataAttendance, value })
    if (timeOut.current) {
      setIsTyping(true)
      clearTimeout(timeOut.current)
    }
    timeOut.current = setTimeout(() => {
      setIsTyping(false)
    }, 300)
  }
  return (
    <CardStyled title={<strong>Điểm danh</strong>}>
      <Spin spinning={isLoading}>
        <Row style={{ width: '100%' }} justify="space-between">
          <label htmlFor="">Áp dụng trương trình: </label>
          <Switch
            loading={isLoadingSwitch}
            checked={
              dataAttendance?.status === CONFIG_STATUS.ACTIVE ? true : false
            }
            onChange={onChangeStatus}
          />
        </Row>
        <Row style={{ width: '100%', marginTop: 20 }} justify="space-between">
          <label htmlFor="">Số điểm: </label>
          <InputNumber
            onChange={onChangeValue}
            value={dataAttendance?.value || 0}
            style={{ width: '50%' }}
            placeholder="Nhập số điểm"
          />
        </Row>
      </Spin>
    </CardStyled>
  )
}
export default Attendance
