import React, { useState, useEffect, useRef } from 'react'
import { Row, Input, Spin, message } from 'antd'
import { CardStyled } from './Styled'
import { getDetailConfig, updateDataConfig } from '../configApi'
import { CONFIG_TYPE } from 'utils/constants'
import { formatPrice } from 'utils/ruleForm'

function OrderPromotion() {
  const [isLoading, setLoading] = useState<boolean>(false)
  const [dataOrderPromotion, setDataOrderPromotion] = useState<any>(null)
  const [isTyping, setIsTyping] = useState<boolean>(false)
  const timeOut = useRef<any>(null)

  useEffect(() => {
    if (
      !isTyping &&
      dataOrderPromotion &&
      dataOrderPromotion?.value < 100 &&
      dataOrderPromotion?.value > 0
    )
      requestChangeValue()
  }, [isTyping])

  useEffect(() => {
    getDataOrderPromotion()
  }, [])
  const getDataOrderPromotion = async () => {
    try {
      setLoading(true)
      const res = await getDetailConfig(CONFIG_TYPE.ORDER_PROMOTION)
      setDataOrderPromotion(res.data)
    } catch (error) {
      console.log('error', error)
    } finally {
      setLoading(false)
    }
  }
  const requestChangeValue = async () => {
    try {
      setLoading(true)
      const res = await updateDataConfig(CONFIG_TYPE.ORDER_PROMOTION, {
        status: dataOrderPromotion?.status,
        value: dataOrderPromotion?.value,
      })
      message.success('Cập nhật thành công.')
    } catch (error) {
      console.log('error', error)
      message.error('Cập nhật thất bại.')
    } finally {
      setLoading(false)
    }
  }
  const onChangeValue = (value: number) => {
    setDataOrderPromotion({ ...dataOrderPromotion, value })
    if (timeOut.current) {
      setIsTyping(true)
      clearTimeout(timeOut.current)
    }
    timeOut.current = setTimeout(() => {
      setIsTyping(false)
    }, 300)
  }

  return (
    <CardStyled title={<strong>Mua hàng</strong>}>
      <Spin spinning={isLoading}>
        <Row style={{ width: '100%' }} justify="space-between">
          <label htmlFor="">Tích điểm(%): </label>
          <Input
            onChange={(e: any) => {
              let value = e.target.value?.replace(/[^\w\s]/gi, '')
              if (dataOrderPromotion.value != value)
                onChangeValue(parseInt(value))
            }}
            value={
              dataOrderPromotion?.value
                ? formatPrice(dataOrderPromotion?.value)
                : 0
            }
            style={{ width: '50%' }}
            placeholder="Nhập phần trăm tích điểm."
          />
        </Row>
        <Row style={{ marginTop: 5 }} justify="end">
          <span style={{ color: 'red' }}>
            {dataOrderPromotion?.value > 100 || dataOrderPromotion?.value <= 0
              ? 'Vui lòng nhập số dương từ 1 tới 100'
              : ''}
          </span>
        </Row>
      </Spin>
    </CardStyled>
  )
}
export default OrderPromotion
