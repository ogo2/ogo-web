import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  DeleteFilled,
} from '@ant-design/icons'
import { Button, Card, Descriptions, Popconfirm } from 'antd'
import moment from 'moment'
import { CHANGE_STATUS, STATUS } from 'utils/constants'

type Props = {
  data: any
  getData: any
  onDeleteGoogleAccount: any
  onChageStatusGoogleAccount: any
}

const ContentView = (data: any) => {
  return (
    <Descriptions size="default" column={2}>
      <Descriptions.Item label="Tên gian hàng">
        {data.name_shop ? data.name_shop : '--'}
      </Descriptions.Item>
      <Descriptions.Item label="Email">{data.email}</Descriptions.Item>
      <Descriptions.Item label="Trạng thái">
        {data.status ? 'Đang hoạt động' : 'Ngừng hoạt động'}
      </Descriptions.Item>
      <Descriptions.Item label="Ngày tạo">
        {moment(data.create_at).format('DD-MM-YYYY')}
      </Descriptions.Item>
    </Descriptions>
  )
}

function ManageGoogleAccountDetail({
  data,
  getData,
  onDeleteGoogleAccount,
  onChageStatusGoogleAccount,
}: Props) {
  return (
    <div>
      <Card
        style={{
          backgroundColor: '#f6f9ff',
          borderColor: '#1890ff',
          borderTop: 'none',
        }}
        actions={[
          <Popconfirm
            title={
              data.status === STATUS.ACTIVE
                ? 'Bạn chắc chắn muốn ngừng hoạt động tài khoản này'
                : 'Bạn chắc chắn muốn bật hoạt động tài khoản này'
            }
            onConfirm={() => {
              onChageStatusGoogleAccount(data.id)
            }}
            okText={
              data.status === STATUS.ACTIVE
                ? CHANGE_STATUS.INACTIVE
                : CHANGE_STATUS.ACTIVE
            }
            cancelText="Quay lại"
            okButtonProps={
              data.status === STATUS.ACTIVE
                ? {
                    danger: true,
                    type: 'primary',
                  }
                : {
                    danger: false,
                    type: 'primary',
                  }
            }
          >
            <Button
              type="text"
              size="large"
              style={{
                color: data.status === STATUS.ACTIVE ? 'red' : '#389e0d',
              }}
              icon={
                data.status === STATUS.ACTIVE ? (
                  <CloseCircleOutlined />
                ) : (
                  <CheckCircleOutlined />
                )
              }
            >
              {data.status === 1
                ? CHANGE_STATUS.INACTIVE
                : CHANGE_STATUS.ACTIVE}
            </Button>
          </Popconfirm>,
          <Popconfirm
            title={'Bạn chắc chắn muốn xoá tài khoản này'}
            onConfirm={async () => {
              try {
                onDeleteGoogleAccount(data.id)
              } catch (error) {
              } finally {
              }
            }}
            okText="Xoá"
            cancelText="Quay lại"
            okButtonProps={{
              danger: true,
              type: 'primary',
            }}
          >
            <Button
              type="text"
              size="large"
              icon={<DeleteFilled />}
              style={{ color: 'red' }}
            >
              Xoá tài khoản
            </Button>
          </Popconfirm>,
        ]}
      >
        {ContentView(data)}
      </Card>
    </div>
  )
}

export default ManageGoogleAccountDetail
