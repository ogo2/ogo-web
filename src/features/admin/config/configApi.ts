import { ApiClient } from 'services/ApiService'

export const getDetailConfig = (config_type: number) =>
  ApiClient.get(`/config/${config_type}/type`)

export const updateDataConfig = (
  config_type: number,
  payload: { status?: number; value: number }
) => ApiClient.put(`/config/${config_type}/type`, payload)

export const changeStatusConfig = (config_type: number) =>
  ApiClient.put(`/config/${config_type}/type/status`)

//gifts
export const getListGifts = (payload: any) =>
  ApiClient.get('/config/lucky-spin-gift')
export const createGifts = (payload: any) =>
  ApiClient.post('/config/lucky-spin-gift', payload)
export const updateGifts = (payload: any) =>
  ApiClient.put(`/config/${payload.id}/lucky-spin-gift`, payload.data)
export const changeStatusGifts = (payload: any) =>
  ApiClient.put(`/config/${payload}/lucky-spin-gift/status`)
export const deleteGifts = (payload: any) =>
  ApiClient.delete('/config/lucky-spin-gift', payload)

// Google Account
export const requestGetListGoogleAccount = (payload: any) =>
  ApiClient.get('admin/youtube-worker', payload)
export const requestCreateGoogleAccount = () =>
  ApiClient.post('admin/youtube-worker')
export const changeStatusGoogleAccount = (id: number) =>
  ApiClient.put(`admin/youtube-worker/${id}/status`)
