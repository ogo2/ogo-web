import { Affix, message, PageHeader, Space, Table } from 'antd'
import { useEffect, useState } from 'react'
import reactotron from 'ReactotronConfig'
import moment from 'moment'
import { Header } from './components/Header'
import ManageGoogleAccountDetail from './components/ManageGoogleAccountDetail'
import { useGetListGoogleAccount } from './hooks/UseGetListGoogleAccount'
import { changeStatusGoogleAccount } from './configApi'
import { notificationError, notificationSuccess } from 'utils/notification'

function ManageGoogleAccount() {
  const {
    listGoogleAccount,
    isLoading,
    params,
    setParams,
    paging,
    getListGoogleAccount,
  } = useGetListGoogleAccount()
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [currentSelected, setCurrentSelected] = useState({ id: -1 })

  const contentView = () => {
    const columns = [
      {
        width: 70,
        title: 'STT',
        dataIndex: 'index',
        key: 'index',
        render: (text: any, record: any, index: any) => (
          <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
            {(paging.current - 1) * paging.pageSize + index + 1}
          </td>
        ),
        ellipsis: true,
      },
      {
        title: 'Gian hàng',
        dataIndex: 'name_shop',
        key: 'name_shop',
        render: (value: any) => {
          return <>{value ? value : '--'}</>
        },
      },
      { title: 'Email', dataIndex: 'email', key: 'email' },
      {
        title: 'Trạng thái',
        dataIndex: 'status',
        key: 'status',
        render: (value: any) => {
          return <>{value ? 'Đang hoạt động' : 'Ngừng hoạt động'}</>
        },
      },

      {
        title: 'Ngày tạo',
        dataIndex: 'create_at',
        key: 'create_at',
        render: (value: any) => {
          return <>{moment(value).format('DD-MM-YYYY')}</>
        },
      },
    ]

    return (
      <div
        style={{
          margin: '2px 5px 2px 5px',
        }}
      >
        <Affix>
          <PageHeader
            style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}
            extra={[
              <Space>
                <Header
                  isSearchLoading={isSearchLoading}
                  onSearchSubmit={(searchKey: string) => {
                    setIsSearchLoading(true)
                    setParams({
                      ...params,
                      search: searchKey === '' ? undefined : searchKey.trim(),
                      page: 1,
                    })
                  }}
                  onStatusSubmit={(status: number) => {
                    setParams({
                      ...params,
                      status: status,
                      page: 1,
                    })
                  }}
                />
              </Space>,
            ]}
          />
        </Affix>
        <div
          style={{
            backgroundColor: 'white',
            margin: '0px 10px 15px',
            padding: '10px 20px',
          }}
        >
          <Table
            scroll={{
              x: 800,
              scrollToFirstRowOnChange: true,
              y: 'calc(100vh - 280px)',
            }}
            bordered
            dataSource={listGoogleAccount}
            loading={isLoading}
            columns={columns}
            expandedRowKeys={[currentSelected.id]}
            onRow={r => ({
              onClick: () => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            })}
            expandable={{
              expandedRowRender: (record: any) => (
                <ManageGoogleAccountDetail
                  onDeleteGoogleAccount={async (id: any) => {
                    try {
                      // message.success('Xóa tài khoản thành công')
                      // getListGoogleAccount()
                    } catch (error) {
                      console.log(error)
                    }
                  }}
                  onChageStatusGoogleAccount={async (id: any) => {
                    try {
                      changeStatusGoogleAccount(id)
                      notificationSuccess('Đổi trạng thái thành công')
                      getListGoogleAccount()
                    } catch (error) {
                      notificationError('Đổi trạng thái thất bại')
                    }
                  }}
                  data={record}
                  getData={getListGoogleAccount}
                />
              ),
              onExpand: (status: any, r: any) => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            }}
            pagination={{
              ...paging,
              onChange: async (page, pageSize) => {
                setParams({ ...params, page })
              },
            }}
          />
        </div>
      </div>
    )
  }

  return contentView()
}
export default ManageGoogleAccount
