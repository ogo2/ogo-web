import React from 'react'
import { Row, Col, Card, Switch, Input } from 'antd'
import styled from 'styled-components'
import Attendance from './components/Attendance'
import Referral from './components/Referral'
import OrderPromotion from './components/OrderPromotion'
import DailyTurn from './components/DailyTurn'
const CardStyled = styled(Card)`
  width: 100%;
`
function SystemInfo() {
  return (
    <Row
      style={{ width: '100%', marginBottom: '20px', paddingBottom: '20px' }}
      justify="space-around"
    >
      <Col xs={21} xl={11} style={{ marginBottom: '20px' }}>
        {/* <Attendance /> */}
        <OrderPromotion />
        {/* <DailyTurn /> */}
      </Col>

      <Col xs={21} xl={11}>
        <Referral />
      </Col>
    </Row>
  )
}
export default SystemInfo
