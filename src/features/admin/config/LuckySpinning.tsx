import { Affix, message, PageHeader, Space, Table, Tag } from 'antd'
import React, { useEffect, useState } from 'react'
import reactotron from 'ReactotronConfig'
import moment from 'moment'
import LuckySpinningDetail from './LuckySpinningDetail'
import { AddEditGifts } from './components/AddEditGifts'
import { Header } from './components/Header'
import { getListGifts, createGifts, deleteGifts } from './configApi'
function LuckySpinning() {
  const [service, setService] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [showAddGift, setShowAddGift] = useState(false)
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [currentSelected, setCurrentSelected] = useState({ id: -1 })
  const [params, setParams] = useState<any>({
    search: '',
    page: 1,
    limit: 24,
    from_date: '',
    to_date: '',
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const getData = async () => {
    setIsLoading(true)
    try {
      const res = await getListGifts(params)
      const tableData = res.data.map((item: any, index: number) => {
        return { ...item, key: item.id, stt: index + 1 }
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setService(tableData)
      setPaging(formattedPaging)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  useEffect(() => {
    console.log(params)
    getData()
  }, [params])

  const createService = async (data: any, resetFields: any) => {
    setIsLoading(true)
    try {
      const res = await createGifts(data)
      if (res.code === 1) {
        message.success('Thêm quà tặng thành công')
        setShowAddGift(false)
        reactotron.log!(resetFields)
        resetFields()
      }
    } catch (error) {
      console.log(error)
      message.error('Thêm quà tặng thất bại')
    } finally {
      getData()
      setIsLoading(false)
    }
  }

  const contentView = () => {
    const columns = [
      {
        width: 70,
        title: 'STT',
        dataIndex: 'index',
        key: 'index',
        render: (text: any, record: any, index: any) => (
          <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
            {(paging.current - 1) * paging.pageSize + index + 1}
          </td>
        ),
        ellipsis: true,
      },
      { title: 'Tên quà tặng', dataIndex: 'name', key: 'name' },
      {
        title: 'Số điểm',
        dataIndex: 'value',
        key: 'value',
      },
      {
        title: 'Tỉ lệ trúng',
        dataIndex: 'percent',
        key: 'percent',
        render: (value: any) => {
          return <>{value ? value + ' %' : '0 %'}</>
        },
      },
      {
        title: 'Số lượt quay tối đa trong ngày',
        dataIndex: 'max_per_day',
        key: 'max_per_day',
        render: (value: any) => {
          return <>{value ? value : '0'}</>
        },
      },
      {
        title: 'Ngày tạo',
        dataIndex: 'create_at',
        key: 'create_at',
        render: (value: any) => {
          return <>{moment(value).format('DD-MM-YYYY')}</>
        },
      },
    ]

    return (
      <div
        style={{
          margin: '2px 5px 2px 5px',
        }}
      >
        <Affix>
          <PageHeader
            style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}
            extra={[<Space></Space>]}
          />
        </Affix>
        <div
          style={{
            backgroundColor: 'white',
            margin: '0px 10px 15px',
            padding: '10px 20px',
          }}
        >
          <Table
            scroll={{
              x: 800,
              scrollToFirstRowOnChange: true,
              y: 'calc(100vh - 280px)',
            }}
            bordered
            dataSource={service}
            loading={isLoading}
            columns={columns}
            expandedRowKeys={[currentSelected.id]}
            onRow={r => ({
              onClick: () => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            })}
            expandable={{
              expandedRowRender: (record: any) => (
                <LuckySpinningDetail
                  onDeleteGift={async (id: any) => {
                    try {
                      await deleteGifts({ id: [id] })
                      message.success('Xóa quà tặng thành công')
                      getData()
                    } catch (error) {
                      // message.error(error)
                      console.log(error)
                    }
                  }}
                  data={record}
                  getData={getData}
                />
              ),
              onExpand: (status: any, r: any) => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            }}
            pagination={{
              ...paging,
              onChange: async (page, pageSize) => {
                setParams({ ...params, page })
              },
            }}
          />
        </div>
        {showAddGift && (
          <AddEditGifts
            visible={showAddGift}
            onCancel={() => {
              setShowAddGift(false)
              getData()
            }}
            onCreateNewService={(newData: any, resetFields: any) => {
              createService(newData, resetFields)
            }}
            isLoading={isLoading}
          />
        )}
      </div>
    )
  }

  return contentView()
}
export default LuckySpinning
