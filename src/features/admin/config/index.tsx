import { Affix, Tabs, PageHeader } from 'antd'
import SystemInfo from './SystemInfo'
import ManageGoogleAccount from './ManageGoogleAccount'

const { TabPane } = Tabs

const Configs = () => {
  return (
    <div style={{ margin: '2px 5px 2px 5px' }}>
      <Affix>
        <PageHeader
          title="Cấu hình"
          style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}
        />
      </Affix>
      <Affix>
        <div
          style={{
            backgroundColor: 'white',
            margin: '5px 10px 15px',
            padding: 10,
          }}
        >
          <Tabs defaultActiveKey="1">
            <TabPane
              tab={
                <strong style={{ fontSize: 'large' }}>Thông số hệ thống</strong>
              }
              key="1"
            >
              <SystemInfo />
            </TabPane>
            {/* <TabPane
              tab={
                <strong style={{ fontSize: 'large' }}>
                  Quản lý tài khoản google
                </strong>
              }
              key="2"
            >
              <ManageGoogleAccount />
            </TabPane> */}
            {/* <TabPane
              tab={
                <strong style={{ fontSize: 'large' }}>Vòng quay may mắn</strong>
              }
              key="2"
            >
              <LuckySpinning />
            </TabPane> */}
          </Tabs>
        </div>
      </Affix>
    </div>
  )
}

export default Configs
