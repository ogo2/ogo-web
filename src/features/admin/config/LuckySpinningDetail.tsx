import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  DeleteFilled,
  EditOutlined,
  UndoOutlined,
} from '@ant-design/icons'
import { Button, Card, Descriptions, message, Popconfirm } from 'antd'
import moment from 'moment'
import React, { useState } from 'react'
import { CHANGE_STATUS, STATUS } from 'utils/constants'
import { AddEditGifts } from './components/AddEditGifts'
import { updateGifts, changeStatusGifts } from './configApi'

type Props = {
  data: any
  getData: any
  onDeleteGift: any
  //   onUpdateService: any
}

const ContentView = (data: any) => {
  return (
    <Descriptions size="default" column={2}>
      <Descriptions.Item label="Tên gói quà">{data.name}</Descriptions.Item>
      <Descriptions.Item label="Số điểm">{data.value}</Descriptions.Item>
      <Descriptions.Item label="Tỉ lệ trúng">
        {data.percent ? data.percent + ' %' : '0' + ' %'}
      </Descriptions.Item>
      <Descriptions.Item label="Lượt quay tối đa trong ngày">
        {data.max_per_day ? data.max_per_day : '0'}
      </Descriptions.Item>
      <Descriptions.Item label="Ngày tạo">
        {moment(data.create_at).format('DD-MM-YYYY')}
      </Descriptions.Item>
    </Descriptions>
  )
}

function LuckySpinningDetail({ data, getData, onDeleteGift }: Props) {
  const [showEditGift, setShowEditGift] = useState(false)

  const onChangeStatus = async () => {
    try {
      const payload = data.id
      await changeStatusGifts(payload)
      message.success(`Cập nhật trạng thái thành công`)
      getData()
    } catch (error) {
      console.log(error)
    }
  }

  const updateGift = async (value: any, resetFields: any) => {
    try {
      let dataPush = {
        id: data.id,
        data: {
          percent: value.percent,
          name: value.name,
          value: value.value,
          max_per_day: value.max_per_day,
        },
      }
      // console.log('dataPush', dataPush)
      await updateGifts(dataPush)
      message.success(`Cập nhật thành công`)
      setShowEditGift(false)
      getData()
      resetFields()
    } catch (error) {
      // message.error(`Đã có lỗi xảy ra, vui lòng thử lại`)
      console.log(error)
    }
  }

  return (
    <div>
      {showEditGift && (
        <AddEditGifts
          data={data}
          visible={showEditGift}
          onCancel={() => {
            setShowEditGift(false)
          }}
          onUpdateService={(newData: any, resetFields: any) => {
            updateGift(newData, resetFields)
          }}
          isLoading={false}
        />
      )}
      <Card
        style={{
          backgroundColor: '#f6f9ff',
          borderColor: '#1890ff',
          borderTop: 'none',
        }}
        actions={[
          <Popconfirm
            title={
              data.status === STATUS.ACTIVE
                ? 'Bạn chắc chắn muốn ngừng hoạt động gói quà tặng này'
                : 'Bạn chắc chắn muốn bật hoạt động gói quà tặng này'
            }
            onConfirm={() => {
              onChangeStatus()
            }}
            okText={
              data.status === STATUS.ACTIVE
                ? CHANGE_STATUS.INACTIVE
                : CHANGE_STATUS.ACTIVE
            }
            cancelText="Quay lại"
            okButtonProps={
              data.status === STATUS.ACTIVE
                ? {
                    danger: true,
                    type: 'primary',
                  }
                : {
                    danger: false,
                    type: 'primary',
                  }
            }
          >
            <Button
              type="text"
              size="large"
              style={{
                color: data.status === STATUS.ACTIVE ? 'red' : '#389e0d',
              }}
              icon={
                data.status === STATUS.ACTIVE ? (
                  <CloseCircleOutlined />
                ) : (
                  <CheckCircleOutlined />
                )
              }
            >
              {data.status === 1
                ? CHANGE_STATUS.INACTIVE
                : CHANGE_STATUS.ACTIVE}
            </Button>
          </Popconfirm>,

          <Button
            onClick={() => {
              setShowEditGift(true)
            }}
            type="text"
            size="large"
            icon={<EditOutlined />}
            style={{ color: '#1890ff' }}
          >
            Chỉnh sửa
          </Button>,

          <Popconfirm
            title={'Bạn chắc chắn muốn xoá gói quà tặng này'}
            onConfirm={async () => {
              try {
                onDeleteGift(data.id)
              } catch (error) {
              } finally {
              }
            }}
            okText="Xoá"
            cancelText="Quay lại"
            okButtonProps={{
              danger: true,
              type: 'primary',
            }}
          >
            <Button
              type="text"
              size="large"
              icon={<DeleteFilled />}
              style={{ color: 'red' }}
            >
              Xoá quà tặng
            </Button>
          </Popconfirm>,
        ]}
      >
        {ContentView(data)}
      </Card>
    </div>
  )
}

export default LuckySpinningDetail
