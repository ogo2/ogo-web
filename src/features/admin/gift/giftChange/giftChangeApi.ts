import { ApiClient } from 'services/ApiService'

export const getListGiftChanges = (payload: any) =>
  ApiClient.get('/admin/gift/purchase-gift', payload)
export const confirmGiftChanges = (payload: any) =>
  ApiClient.put(`/admin/gift/purchase-gift/confirm`, payload)
// export const cancelGiftChanges = (payload: any) =>
//   ApiClient.put(`/gift/purchase-gift/cancel`, payload)
