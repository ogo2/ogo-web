import {
  Button,
  Col,
  DatePicker,
  Input,
  PageHeader,
  Popconfirm,
  Row,
  Select,
} from 'antd'
import { GiftOutlined } from '@ant-design/icons'
import { confirmGiftChanges } from '../giftChangeApi'
import { notificationError, notificationSuccess } from 'utils/notification'
import { useState } from 'react'
import { GIFT_STATUS } from 'utils/constants'
import ColumnGroup from 'rc-table/lib/sugar/ColumnGroup'
type Props = {
  isSearchLoading: boolean
  onSelected: Array<number>
  getData: any
  onSearchSubmit: (searchKey: string) => void
  onStatusSubmit: (status: number) => void
  onTypeGift: (type_id: number) => void
  onDateSubmit: (from_date: string, to_date: string) => void
}

const { Search } = Input
const { RangePicker } = DatePicker
const { Option } = Select

function HeaderGiftChange({
  isSearchLoading,
  onSelected,
  getData,
  onSearchSubmit,
  onStatusSubmit,
  onTypeGift,
  onDateSubmit,
}: Props) {
  const [isLoading, setIsLoading] = useState({
    refuse: false,
    accept: false,
  })
  return (
    <div>
      <div
        style={{
          backgroundColor: 'white',
          margin: '10px 10px',
        }}
      >
        <PageHeader
          title="Yêu cầu đổi quà "
          extra={[
            <Popconfirm
              placement="bottomRight"
              title="Bạn có chắc chắn muốn gửi quà?"
              onConfirm={async () => {
                if (onSelected === []) {
                  notificationError('Bạn chưa có lựa chọn nào ')
                } else {
                  try {
                    setIsLoading({ ...isLoading, accept: true })
                    const res = await confirmGiftChanges({
                      purchase_gifts_id: onSelected,
                    })
                    if (res.code === 1) {
                      notificationSuccess('Đã hoàn thành gửi quà')
                      getData()
                    }
                  } catch (error) {
                    console.log(error)
                  } finally {
                    setIsLoading({ ...isLoading, accept: false })
                  }
                }
              }}
              okText="Đồng ý"
              cancelText="Huỷ"
              okButtonProps={{
                type: 'primary',
              }}
            >
              <Button
                style={{
                  backgroundColor: '#00abba',
                  borderColor: '#00abba',
                  color: 'white',
                }}
                loading={isLoading.accept}
              >
                <GiftOutlined /> Gửi quà
              </Button>
            </Popconfirm>,
          ]}
        />
      </div>
      <div
        style={{
          backgroundColor: 'white',
          margin: '10px 10px',
          padding: '15px 20px',
        }}
      >
        <Row gutter={[16, 16]} justify="end">
          <Col span={8}>
            <Search
              loading={isSearchLoading}
              // style={{ minWidth: '200px' }}
              placeholder="Tìm kiếm tên khách hàng"
              onChange={(e: any) => {
                onSearchSubmit(e.target.value)
              }}
            />
          </Col>
          <Col span={4}>
            <Select
              allowClear
              placeholder="Loại quà"
              style={{ minWidth: '120px' }}
              onChange={(value: number) => {
                onTypeGift(value)
              }}
            >
              <Option value={1}>Quà tặng</Option>
              <Option value={2}>Mã giảm giá</Option>
            </Select>
          </Col>
          <Col span={4}>
            <Select
              allowClear
              placeholder="Trạng thái"
              style={{ minWidth: '130px' }}
              onChange={(value: number) => {
                onStatusSubmit(value)
              }}
            >
              <Option value={GIFT_STATUS.PENDING}>Đã đổi quà</Option>
              <Option value={GIFT_STATUS.CONFIRMED}>Đã đổi mã</Option>
              <Option value={GIFT_STATUS.SENT_GIFT}>Đã hoàn thành</Option>
              <Option value={GIFT_STATUS.USED}>Đã sử dụng</Option>
            </Select>
          </Col>
          <Col span={5}>
            <RangePicker
              // style={{ minWidth: '150px' }}
              placeholder={['Từ ngày', 'đến ngày']}
              onChange={(value, dateString) => {
                onDateSubmit(dateString[0], dateString[1])
              }}
            />
          </Col>
        </Row>
      </div>
    </div>
  )
}
export default HeaderGiftChange
