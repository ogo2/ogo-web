import moment from 'moment'
import { useEffect, useRef, useState } from 'react'
import { Table, Tag } from 'antd'
import { getListGiftChanges } from '../giftChangeApi'
import { formatPrice } from 'utils/ruleForm'
import HeaderGiftChange from './HeaderGiftChange'
import { GIFT_STATUS } from 'utils/constants'
import reactotron from 'ReactotronConfig'
import DetailGiftChange from './DetailGiftChange'
import { useParams } from 'react-router'
import { useSelector } from 'react-redux'

function ListGiftChange() {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })
  const [listGiftChange, setListGiftChange] = useState<Array<any>>([])
  const [isLoading, setIsLoading] = useState(false)
  const [selectedRowKeys, setSelectedRowKeys] = useState<Array<number>>([])
  const [currentSelected, setCurrentSelected] = useState({ id: -1 })
  const [params, setParams] = useState<any>({
    search: '',
    status: undefined,
    df_type_gift_id: undefined,
    page: 1,
    limit: 24,
    from_date: '',
    to_date: '',
  })
  const [isTyping, setIsTyping] = useState(false)
  const timeOut = useRef<any>(null)
  const key: any = useParams()
  useEffect(() => {
    if (timeOut.current) {
      setIsTyping(true)
      clearTimeout(timeOut.current)
    }
    timeOut.current = setTimeout(() => {
      getData()
      setIsTyping(false)
    }, 300)
  }, [params])

  useEffect(() => {
    getData()

    if (key) {
      const selectedKey = parseInt(key.key)
      setSelectedRowKeys([selectedKey])
    }
  }, [])

  const getData = async () => {
    try {
      setIsLoading(true)
      setCurrentSelected({ id: -1 })
      const res = await getListGiftChanges(params)
      const data_list = res.data.map((item: any) => {
        return {
          ...item,
          key: item.id,
          receiver_name: item.address?.name,
          receiver_phone: item.address?.phone,
        }
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setPaging(formattedPaging)
      setListGiftChange(data_list)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }
  const rowSelection = {
    selectedRowKeys,
    onChange: async (selectedRowKeys: any, selectedRows: any) => {
      // const selectedRowID = selectedRows.map((item: any) => {
      //   return item.id
      // })
      setSelectedRowKeys(selectedRowKeys)
    },
    getCheckboxProps: (record: any) => ({
      disabled: record.status !== 0, // Column configuration not to be checked
    }),
  }
  const columns = [
    {
      width: 70,
      title: 'STT',
      render: (text: any, record: any, index: any) => (
        <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
          {(paging.current - 1) * paging.pageSize + index + 1}
        </td>
      ),
      ellipsis: true,
    },
    {
      title: 'Tên khách hàng',
      dataIndex: 'customer_name',
      key: 'customer_name',
      width: 120,
      render: (value: any) => {
        return <>{value ? value : '--'}</>
      },
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
      key: 'phone',
      width: 120,
      render: (value: any) => {
        return <>{value ? value : '--'}</>
      },
    },
    {
      title: 'Tên người nhận',
      dataIndex: 'receiver_name',
      key: 'receiver_name',
      width: 120,
      render: (value: any) => {
        return <>{value ? value : '--'}</>
      },
    },
    {
      title: 'SĐT người nhận',
      dataIndex: 'receiver_phone',
      key: 'receiver_phone',
      width: 120,
      render: (value: any) => {
        return <>{value ? value : '--'}</>
      },
    },
    {
      title: 'Tên quà tặng',
      dataIndex: 'gift_name',
      key: 'gift_name',
      width: 120,
    },
    {
      title: 'Số điểm',
      dataIndex: 'point',
      key: 'point',
      width: 100,
      render: (value: any) => {
        return <>{value === 0 ? 0 : formatPrice(value)}</>
      },
    },
    {
      title: 'Loại quà',
      dataIndex: 'type_gift_name',
      key: 'type_gift_name',
      width: 120,
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      key: 'status',
      width: 120,
      render: (value: any) => {
        switch (value) {
          case GIFT_STATUS.PENDING:
            return <Tag color="blue">Đã đổi quà</Tag>
          case GIFT_STATUS.CONFIRMED:
            return <Tag color="blue">Đã đổi mã</Tag>
          case GIFT_STATUS.SENT_GIFT:
            return <Tag color="green">Đã hoàn thành</Tag>
          case GIFT_STATUS.USED:
            return <Tag color="green">Đã sử dụng</Tag>
        }
      },
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'create_at',
      key: 'create_at',
      width: 120,
      render: (value: any) => {
        return <>{moment(value).format('DD-MM-YYYY')}</>
      },
    },
  ]
  return (
    <div>
      <HeaderGiftChange
        isSearchLoading={isLoading}
        onSelected={selectedRowKeys}
        getData={getData}
        onSearchSubmit={(searchKey: string) => {
          setParams({ ...params, search: searchKey.trim() })
        }}
        onStatusSubmit={(statusKey: number) => {
          setParams({ ...params, status: statusKey })
        }}
        onTypeGift={(type_id: number) => {
          setParams({ ...params, df_type_gift_id: type_id })
        }}
        onDateSubmit={(from_date: string, to_date: string) => {
          setParams({
            ...params,
            from_date: from_date,
            to_date: to_date,
          })
        }}
      />

      <div>
        <div
          style={{
            backgroundColor: 'white',
            margin: '0px 10px 15px',
            padding: '10px 20px',
          }}
        >
          <p>
            Kết quả lọc: <b>{paging.total}</b>
          </p>
          <Table
            className="table-expanded-custom-no-image"
            scroll={{
              scrollToFirstRowOnChange: true,
              // y: 'calc(100vh - 420px)',
              y: `calc(${heightWeb}px - 420px)`,
            }}
            rowSelection={{ ...rowSelection }}
            bordered
            dataSource={listGiftChange}
            loading={isLoading}
            columns={columns}
            expandedRowKeys={[currentSelected.id]}
            onRow={r => ({
              onClick: () => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            })}
            expandable={{
              expandedRowRender: (record: any) => (
                <DetailGiftChange data={record} getData={getData} />
              ),
              onExpand: (status: any, r: any) => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            }}
            pagination={{
              ...paging,
              showSizeChanger: false,
              onChange: async (page, pageSize) => {
                setParams({ ...params, page })
              },
            }}
          />
        </div>
      </div>
    </div>
  )
}
export default ListGiftChange
