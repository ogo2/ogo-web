import { useState } from 'react'
import { Card, Descriptions, Image } from 'antd'
import moment from 'moment'
import { formatPrice } from 'utils/ruleForm'

type Props = {
  data?: any
  getData: any
}

const ContentView = (data: any) => {
  console.log('data', data)
  return (
    <Descriptions
      size="default"
      column={2}
      title="Thông tin khách hàng nhận quà tặng"
    >
      {!data.address ? (
        <>
          <Descriptions.Item label="Tên khách hàng">
            {data.customer_name}
          </Descriptions.Item>
          <Descriptions.Item label="Số điện thoại">
            {data.phone}
          </Descriptions.Item>
        </>
      ) : (
        <></>
      )}

      {data.address ? (
        <>
          <Descriptions.Item label="Tên người nhận">
            {data.address.name}
          </Descriptions.Item>
          <Descriptions.Item label="SĐT người nhận">
            {data.address.phone}
          </Descriptions.Item>
          <Descriptions.Item label="Địa chỉ">
            {data.address.address ? data.address.address : 'Chưa có địa chỉ'}
          </Descriptions.Item>
          <Descriptions.Item label="Phường/Xã">
            {data.ward_name ? data.ward_name : 'Chưa có phường'}
          </Descriptions.Item>
          <Descriptions.Item label="Quận/Huyện">
            {data.district_name ? data.district_name : 'Chưa có quận'}
          </Descriptions.Item>
          <Descriptions.Item label="Tỉnh/Thành phố">
            {data.province_name ? data.province_name : 'Chưa có thành phố'}
          </Descriptions.Item>
        </>
      ) : (
        <></>
      )}
      {data?.status === 3 ? (
        <Descriptions.Item label="Mã đơn hàng">
          {data.order_code}
        </Descriptions.Item>
      ) : (
        <></>
      )}
    </Descriptions>
  )
}

function DetailGiftChange({ data }: Props) {
  return (
    <div>
      <Card
        style={{
          backgroundColor: '#f6f9ff',
          borderColor: '#1890ff',
          borderTop: 'none',
        }}
      >
        {ContentView(data)}
      </Card>
    </div>
  )
}
export default DetailGiftChange
