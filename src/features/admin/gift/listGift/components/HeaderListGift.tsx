import { Button, PageHeader } from 'antd'
import ButtonFixed from 'common/components/ButtonFixed'
type Props = {
  onShowAddGift: any
}
function HeaderListGift({ onShowAddGift }: Props) {
  return (
    <div>
      <div
        style={{
          backgroundColor: 'white',
          margin: '10px 10px',
        }}
      >
        <PageHeader
          title="Quà tặng "
          extra={[
            // <Button
            //   type="primary"
            //   onClick={() => {
            //     onShowAddGift()
            //   }}
            // >
            //   Thêm mới
            // </Button>,
            <ButtonFixed
              setIsCreate={onShowAddGift}
              text="Thêm mới"
              type="add"
            />,
          ]}
        />
      </div>
    </div>
  )
}
export default HeaderListGift
