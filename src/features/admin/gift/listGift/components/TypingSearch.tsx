import { Input, Space, Col, Select, DatePicker, Row } from 'antd'
import { useState, useRef } from 'react'
import { IS_GIFT_ACTIVE } from 'utils/constants'

const { Search } = Input
const { RangePicker } = DatePicker
const { Option } = Select

type HeaderProps = {
  onSearchSubmit: (searchKey: string) => void
  onStatusSubmit: (status: number) => void
  onTypeGift: (type_id: number) => void
  onDateSubmit: (from_date: string, to_date: string) => void
  isSearchLoading: boolean
}

export const TypingSearch = ({
  onSearchSubmit,
  onStatusSubmit,
  onTypeGift,
  onDateSubmit,
  isSearchLoading,
}: HeaderProps) => {
  return (
    <div
      style={{
        backgroundColor: 'white',
        margin: '10px 10px',
        padding: '15px 10px',
      }}
    >
      <Row gutter={[16, 16]} justify="end">
        <Col span={8}>
          <Search
            loading={isSearchLoading}
            // style={{ minWidth: '200px' }}
            placeholder="Tìm kiếm tên quà tặng"
            onChange={(e: any) => {
              onSearchSubmit(e.target.value)
            }}
          />
        </Col>
        <Col span={4}>
          <Select
            allowClear
            placeholder="Loại quà"
            style={{ minWidth: '110px' }}
            onChange={(value: number) => {
              onTypeGift(value)
            }}
          >
            <Option value={1}>Quà tặng</Option>
            <Option value={2}>Mã giảm giá</Option>
          </Select>
        </Col>
        <Col span={4}>
          <Select
            allowClear
            className="select-type_account"
            placeholder="Trạng thái"
            style={{ minWidth: '150px' }}
            onChange={(value: number) => {
              onStatusSubmit(value)
            }}
          >
            <Option value={IS_GIFT_ACTIVE.ACTIVE}>Đang hoạt động</Option>
            <Option value={IS_GIFT_ACTIVE.INACTIVE}>Ngừng hoạt động</Option>
          </Select>
        </Col>
        <Col span={5}>
          <RangePicker
            placeholder={['Từ ngày', 'đến ngày']}
            onChange={(value, dateString) => {
              onDateSubmit(dateString[0], dateString[1])
            }}
          />
        </Col>
      </Row>
    </div>
  )
}
