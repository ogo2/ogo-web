import { useState } from 'react'
import { Card, Descriptions, Popconfirm, Button, Image } from 'antd'
import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  DeleteFilled,
  EditOutlined,
} from '@ant-design/icons'
import { changeStatusGift, deleteGift } from '../listGiftApi'
import { CHANGE_STATUS, STATUS } from 'utils/constants'
import moment from 'moment'
import { formatPrice } from 'utils/ruleForm'
import { AddEditGift } from './AddEditGift'
import { notificationError, notificationSuccess } from 'utils/notification'

type Props = {
  data?: any
  getData: any
  isLoadingButton: boolean
}

const ContentView = (data: any) => {
  return (
    <Descriptions size="default" column={3} title="Thông tin quà tặng">
      <Descriptions.Item label="Tên quà tặng"> {data.name}</Descriptions.Item>
      <Descriptions.Item label="Loại quà tặng">
        {data.type_gift_name}
      </Descriptions.Item>
      <Descriptions.Item label="Số điểm">
        {data.price === 0 ? 0 : formatPrice(data.price)}
      </Descriptions.Item>
      {data.discount_percent ? (
        <>
          <Descriptions.Item label="Giảm giá">
            {data.discount_percent ? data.discount_percent + '%' : '0%'}
          </Descriptions.Item>
          <Descriptions.Item label="Giảm tối đa">
            {data.max_discount_money
              ? formatPrice(data.max_discount_money) + 'đ'
              : '0đ'}
          </Descriptions.Item>
        </>
      ) : (
        <></>
      )}

      <Descriptions.Item label="Số lượng quà">
        {data.quantity === 0 ? 0 : formatPrice(data.quantity)}
      </Descriptions.Item>
      <Descriptions.Item label="Đã đổi">
        {data.count_purchased_gift}
      </Descriptions.Item>
      <Descriptions.Item label="Ngày tạo">
        {moment(data.create_at).format('DD-MM-YYYY')}
      </Descriptions.Item>
      <Descriptions.Item label="Hình ảnh">
        <Image
          src={data.icon_url}
          style={{ width: '100px', height: '100px', overflow: 'hidden' }}
        />
      </Descriptions.Item>
    </Descriptions>
  )
}

function DetailGift({ data, getData, isLoadingButton }: Props) {
  const [showEditGift, setShowEditGift] = useState(false)

  const onChangeStatus = async () => {
    try {
      const payload = data.id
      await changeStatusGift(payload)
      notificationSuccess(`Cập nhật trạng thái thành công`)
      getData()
    } catch (error) {
      notificationError('Cập nhật trạng thái thất bại')
    }
  }

  return (
    <div>
      <Card
        style={{
          backgroundColor: '#f6f9ff',
          borderColor: '#1890ff',
          borderTop: 'none',
        }}
        actions={[
          <Popconfirm
            title={
              data.status === STATUS.ACTIVE
                ? 'Bạn chắc chắn muốn ngừng hoạt động gói quà tặng này'
                : 'Bạn chắc chắn muốn bật hoạt động gói quà tặng này'
            }
            onConfirm={() => {
              onChangeStatus()
            }}
            okText={
              data.status === STATUS.ACTIVE
                ? CHANGE_STATUS.INACTIVE
                : CHANGE_STATUS.ACTIVE
            }
            cancelText="Quay lại"
            okButtonProps={
              data.status === STATUS.ACTIVE
                ? {
                    danger: true,
                    type: 'primary',
                  }
                : {
                    danger: false,
                    type: 'primary',
                  }
            }
          >
            <Button
              type="text"
              size="large"
              style={{
                color: data.status === STATUS.ACTIVE ? 'red' : '#389e0d',
              }}
              icon={
                data.status === STATUS.ACTIVE ? (
                  <CloseCircleOutlined />
                ) : (
                  <CheckCircleOutlined />
                )
              }
            >
              {data.status === 1
                ? CHANGE_STATUS.INACTIVE
                : CHANGE_STATUS.ACTIVE}
            </Button>
          </Popconfirm>,

          <Button
            onClick={() => {
              setShowEditGift(true)
            }}
            type="text"
            size="large"
            icon={<EditOutlined />}
            style={{ color: '#1890ff' }}
          >
            Chỉnh sửa
          </Button>,

          <Popconfirm
            title={'Bạn chắc chắn muốn xoá gói quà tặng này'}
            onConfirm={async () => {
              try {
                if (data.count_purchased_gift === 0) {
                  await deleteGift({ id: [data.id] })
                  notificationSuccess('Xóa gói quà tặng thành công')
                  getData()
                } else {
                  notificationError('Quà tặng đã có người đổi không thể xóa')
                }
              } catch (error) {
                notificationError('Xóa gói quà tặng thất bại')
              }
            }}
            okText="Xoá"
            cancelText="Quay lại"
            okButtonProps={{
              danger: true,
              type: 'primary',
            }}
          >
            <Button
              type="text"
              size="large"
              icon={<DeleteFilled />}
              style={{ color: 'red' }}
            >
              Xoá gói quà tặng
            </Button>
          </Popconfirm>,
        ]}
      >
        {ContentView(data)}
      </Card>
      {showEditGift ? (
        <AddEditGift
          isLoadingButton={isLoadingButton}
          visible={showEditGift}
          onCancel={() => {
            setShowEditGift(false)
          }}
          data={data}
          getData={getData}
        />
      ) : (
        <></>
      )}
    </div>
  )
}
export default DetailGift
