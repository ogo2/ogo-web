import { ApiClient } from 'services/ApiService'

export const getListTypeGift = () => ApiClient.get('/default/type-gift')
export const getListGift = (payload: any) =>
  ApiClient.get('/admin/gift', payload)
export const createGifts = (payload: any) =>
  ApiClient.post('/admin/gift', payload)
export const updateGift = (payload: any) =>
  ApiClient.put(`/admin/gift/${payload.id}`, payload.data)
export const changeStatusGift = (payload: any) =>
  ApiClient.put(`/admin/gift/${payload}/status`)
export const deleteGift = (payload: any) =>
  ApiClient.delete('/admin/gift', payload)
export const uploadImage = (payload: any) =>
  ApiClient.post(`/files/upload/single/${payload.type}`, payload.data)
