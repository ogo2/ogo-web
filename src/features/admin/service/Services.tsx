/* eslint-disable react-hooks/exhaustive-deps */
import { Affix, Collapse, PageHeader, Space, Table, Tag } from 'antd'
import 'moment/locale/vi'
import React, { useEffect, useState } from 'react'
import reactotron from 'ReactotronConfig'
import styled from 'styled-components'
import moment from 'moment'
import { notificationSuccess, notificationError } from 'utils/notification'
import { getPakage, createPakage, deletePakage } from './ServiceApi'
import ServiceDetail from './ServiceDetail'
import { AddEditService } from './components/AddEditService'
import { Header } from './components/Header'
import { formatPrice } from 'utils/ruleForm'
import { useSelector } from 'react-redux'

const { Panel } = Collapse

const StyledPanel = styled(Panel)`
  .ant-collapse-content-box {
    /* padding: 0; */
  }
`

const Services = () => {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const [service, setService] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [showAddAccount, setShowAddAccount] = useState(false)
  // const [showEditAccount, setShowEditAccount] = useState(false)
  const [isLoadingModal, setIsLoadingModal] = useState(false)
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [currentSelected, setCurrentSelected] = useState({ id: -1 })
  const [params, setParams] = useState<any>({
    search: undefined,
    page: 1,
    limit: 24,
    status: undefined,
    pakage_category_id: undefined,
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const getData = async () => {
    setIsLoading(true)
    try {
      const res = await getPakage(params)
      const tableData = res.data.map((item: any, index: number) => {
        return { ...item, key: item.id, stt: index + 1 }
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setService(tableData)
      setPaging(formattedPaging)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  useEffect(() => {
    setTimeout(() => {
      setCurrentSelected({ id: -1 })
      getData()
    }, 300)
  }, [params])

  const createService = async (data: any, resetFields: any) => {
    setIsLoading(true)
    try {
      const res = await createPakage(data)
      if (res.code === 1) {
        notificationSuccess('Thêm gói dịch vụ thành công')
        setShowAddAccount(false)
        reactotron.log!(resetFields)
        resetFields()
      }
    } catch (error) {
      console.log(error)
      notificationError('Thêm gói dịch vụ thất bại')
    } finally {
      setIsLoading(false)
    }
    getData()
  }

  const contentView = () => {
    const columns = [
      {
        width: 70,
        title: 'STT',
        dataIndex: 'index',
        key: 'index',
        render: (text: any, record: any, index: any) => (
          <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
            {(paging.current - 1) * paging.pageSize + index + 1}
          </td>
        ),
        ellipsis: true,
      },
      {
        title: 'Tên gói',
        dataIndex: 'name',
        key: 'name',
        render: (value: any, record: any) => (
          <>{record.is_default ? value + '(Gói mặc định) ' : value}</>
        ),
      },
      {
        width: 110,
        title: 'Số phút livestream',
        dataIndex: 'minus',
        key: 'minus',
      },
      {
        title: 'Loại gói',
        dataIndex: 'pakage_category_name',
        key: 'pakage_category_name',
      },
      {
        title: 'Giá',
        dataIndex: 'price',
        key: 'price',
        render: (value: any) => (
          <>{value === 0 ? '0đ' : formatPrice(value) + ' đ'}</>
        ),
      },
      {
        title: 'Số gian hàng sử dụng',
        dataIndex: 'count_shop_used',
        key: 'count_shop_used',
      },
      {
        title: 'Đã bán',
        dataIndex: 'count_sold',
        key: 'count_sold',
      },
      {
        width: '160px',
        title: 'Trạng thái',
        dataIndex: 'status',
        key: 'status',
        render: (value: any) => (
          <Tag color={value === 1 ? 'green' : 'volcano'}>
            {value === 1 ? 'ĐANG HOẠT ĐỘNG' : 'TẠM DỪNG'}
          </Tag>
        ),
      },
      {
        title: 'Ngày tạo',
        dataIndex: 'create_at',
        key: 'create_at',
        render: (value: any) => {
          return <>{moment(value).format('DD-MM-YYYY')}</>
        },
      },
    ]

    return (
      <div
        style={{
          margin: '2px 5px 2px 5px',
        }}
      >
        <Affix>
          <PageHeader
            title="Gói dịch vụ "
            style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}
            extra={[
              <Space>
                <Header
                  setIsCreate={setShowAddAccount}
                  isSearchLoading={isSearchLoading}
                  onSearchSubmit={(searchKey: string) => {
                    setIsSearchLoading(true)
                    setParams({
                      ...params,
                      search: searchKey === '' ? undefined : searchKey,
                      page: 1,
                    })
                  }}
                  onStatusSubmit={(statusKey: string) => {
                    setParams({
                      ...params,
                      status: statusKey === '' ? undefined : statusKey,
                      page: 1,
                    })
                  }}
                  onPakageCategoryIdSubmit={(categoryId: number) => {
                    setParams({
                      ...params,
                      pakage_category_id:
                        categoryId === -1 ? undefined : categoryId,
                      page: 1,
                    })
                  }}
                />
              </Space>,
            ]}
          />
        </Affix>
        <div
          style={{
            backgroundColor: 'white',
            margin: '0px 10px 15px',
            padding: '10px 20px',
          }}
        >
          <p>
            Kết quả lọc: <b>{paging.total}</b>
          </p>
          <Table
            className="table-expanded-custom-no-image"
            scroll={{
              x: 800,
              scrollToFirstRowOnChange: true,
              // y: 'calc(100vh - 370px)',
              y: `calc(${heightWeb}px - 370px)`,
            }}
            bordered
            dataSource={service}
            loading={isLoading}
            columns={columns}
            expandedRowKeys={[currentSelected.id]}
            onRow={r => ({
              onClick: () => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            })}
            expandable={{
              expandedRowRender: (record: any) => (
                <ServiceDetail
                  //   onUpdateAccount={(newData: any, resetFields: any) => {
                  // updateAcc(newData, resetFields)
                  //   }}
                  onDeleteService={async (id: any) => {
                    try {
                      await deletePakage({ id: [id] })
                      notificationSuccess('Xóa gói dịch vụ thành công')
                      getData()
                    } catch (error) {
                      console.log(error)
                    }
                  }}
                  data={record}
                  getData={getData}
                />
              ),
              onExpand: (status: any, r: any) => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            }}
            pagination={{
              ...paging,
              onChange: async (page, pageSize) => {
                setParams({ ...params, page })
              },
            }}
          />
        </div>
        {showAddAccount && (
          <AddEditService
            visible={showAddAccount}
            onCancel={() => {
              setShowAddAccount(false)
              getData()
            }}
            onCreateNewService={(newData: any, resetFields: any) => {
              createService(newData, resetFields)
            }}
            isLoadingButton={isLoading}
          />
        )}
      </div>
    )
  }

  return contentView()
}

export default Services
