import { ApiClient } from 'services/ApiService'

// Gói dịch vụ
export const getPakage = (payload: any) => ApiClient.get('/pakage', payload)
export const createPakage = (payload: any) => ApiClient.post('/pakage', payload)
export const updatePakage = (payload: any) =>
  ApiClient.put(`/pakage/${payload.id}`, payload.data)
export const changeStatusPakage = (payload: any) =>
  ApiClient.put(`/pakage/${payload}/status`)
export const deletePakage = (payload: any) =>
  ApiClient.delete('/pakage', payload)

//Danh mục gói dịch vụ
export const getPakageCategory = (payload: any) =>
  ApiClient.get('/pakage-category', payload)
