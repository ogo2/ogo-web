import { Button, Checkbox, Col, Form, Input, Modal, Row, Select } from 'antd'
import { useEffect, useState } from 'react'
import { getPakageCategory } from '../ServiceApi'
import { notificationError } from 'utils/notification'
import { formatPrice } from 'utils/ruleForm'
import ButtonBottomModal from 'common/components/ButtonBottomModal'

type Props = {
  visible: boolean
  onCancel?: any
  data?: any
  onCreateNewService?: any
  onUpdateService?: any
  isLoadingButton: boolean
}

function convertDataToFrom(data: any) {
  if (!data) {
    return {
      is_default: null,
      minus: null,
      name: null,
      pakage_category_id: null,
      price: null,
    }
  } else {
    data.price = formatPrice(data.price)
    return {
      data,
    }
  }
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
}

export const AddEditService = ({
  visible,
  onCancel,
  data,
  onCreateNewService,
  onUpdateService,
  isLoadingButton,
}: Props) => {
  const [form] = Form.useForm()
  const initialValue = convertDataToFrom(data)
  const [listCategory, setListCategory] = useState<Array<any>>([])
  const [checkbox, setCheckBox] = useState(-1)

  useEffect(() => {
    setPakageCategory()
    if (data && data.pakage_category_id === 1) {
      setCheckBox(1)
    }
  }, [])

  const setPakageCategory = async () => {
    try {
      const search = undefined
      const res = await getPakageCategory({ search })
      setListCategory(res.data)
    } catch (error) {
      console.log(error)
    }
  }
  const onFinish = async (values: any) => {
    try {
      values.is_default = values.is_default ? 1 : 0
      values.price = parseInt(values?.price.replaceAll(',', ''))
      values.name = values.name.trim()
      if (!data) {
        if (values.is_default && values.pakage_category_id === 2) {
          notificationError('Chọn gói mặc định là cộng phút')
        } else {
          onCreateNewService(values, form.resetFields)
        }
      } else {
        onUpdateService(values, form.resetFields)
      }
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <Modal
      onCancel={() => {
        form.resetFields()
        onCancel()
      }}
      maskClosable={false}
      footer={null}
      title={data ? 'Sửa dịch vụ ' : 'Thêm dịch vụ '}
      visible={visible}
    >
      <Form
        {...formItemLayout}
        form={form}
        name="add"
        labelAlign="left"
        onFinish={(values: any) => onFinish(values)}
        initialValues={initialValue.data}
        scrollToFirstError
      >
        <Form.Item
          name="name"
          label="Tên gói"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập tên dịch vụ!',
            },
            {
              whitespace: true,
              message: 'Vui lòng không nhập khoảng trắng!',
            },
            {
              min: 3,
              max: 50,
              message: 'Vui lòng nhập từ 3 đến 50 ký tự!',
            },
            // {
            //   message: 'Vui lòng không nhập kí tự đặc biệt',
            //   validator: (_, value) => {
            //     const reg = /[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềếýểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]/u
            //     if (!reg.test(value)) {
            //       return Promise.resolve()
            //     }
            //     return Promise.reject()
            //   },
            // },
          ]}
        >
          <Input placeholder="Nhập tên dịch vụ" />
        </Form.Item>

        <Form.Item
          name="minus"
          label="Số phút livestream"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập số phút livestream',
            },
            {
              message: 'Vui lòng chỉ nhập số ',
              validator: (_, value) => {
                const reg = /^\d+$/
                if (!reg.test(value)) {
                  return Promise.reject()
                }
                return Promise.resolve()
              },
            },
            {
              message: 'Vui lòng nhập số phút livestream lớn hơn 0 ',
              validator: (_, value) => {
                if (parseInt(value) === 0) {
                  return Promise.reject()
                }
                return Promise.resolve()
              },
            },
            {
              message: 'Vui lòng nhập thời gian livestream dưới 50 000',
              validator: (_, value) => {
                if (value > 50000) {
                  return Promise.reject()
                }
                return Promise.resolve()
              },
            },
          ]}
        >
          <Input placeholder="Nhập số phút livestream" />
        </Form.Item>

        <Form.Item
          label="Loại gói"
          name="pakage_category_id"
          rules={[
            {
              required: true,
              message: 'Vui lòng chọn loại gói!',
            },
          ]}
        >
          <Select
            placeholder="Chọn loại gói"
            defaultValue={data?.pakage_category_id}
            onChange={() => {
              setCheckBox(form.getFieldValue('pakage_category_id'))
            }}
          >
            {listCategory.map((item: any) => {
              return <Select.Option value={item.id}>{item.name}</Select.Option>
            })}
          </Select>
        </Form.Item>

        <Form.Item
          name="price"
          label="Giá"
          rules={[
            {
              whitespace: true,
              message: 'Vui lòng không nhập khoảng trắng!',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (value) {
                  const value_price = parseInt(value.replaceAll(',', ''))
                  if (value_price > 500000000) {
                    return Promise.reject('Vui lòng nhập giá dưới 500,000,000')
                  }
                  return Promise.resolve()
                }
                return Promise.reject(new Error('Vui lòng nhập giá'))
              },
            }),
          ]}
        >
          <Input
            placeholder="Nhập giá"
            onChange={() => {
              form.setFieldsValue({
                price: formatPrice(form.getFieldValue('price')),
              })
            }}
          />
        </Form.Item>
        {checkbox === 1 ? (
          <Form.Item
            label="Gói mặc định"
            name="is_default"
            valuePropName="checked"
          >
            <Checkbox />
          </Form.Item>
        ) : (
          ''
        )}

        {/* <Form.Item>
          <Row style={{ width: '100%' }}>
            <Col span="6" offset="10">
              <Button
                danger
                onClick={() => {
                  form.resetFields()
                  onCancel()
                }}
              >
                Huỷ
              </Button>
            </Col>
            <Col span="6" offset="2">
              <Button loading={isLoading} type="primary" htmlType="submit">
                {data ? 'Cập nhật' : 'Thêm gói dịch vụ'}
              </Button>
            </Col>
          </Row>
        </Form.Item> */}
        <ButtonBottomModal
          isLoadingButton={isLoadingButton}
          onCancel={onCancel}
          text={data ? 'Cập nhật' : 'Thêm gói dịch vụ'}
        />
      </Form>
    </Modal>
  )
}
