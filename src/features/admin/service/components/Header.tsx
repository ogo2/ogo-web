import { Button, Col, Input, Row, Select } from 'antd'
import { useState, useRef, useEffect } from 'react'
import styled from 'styled-components'
import './css/Header.css'
import { getPakageCategory } from '../ServiceApi'
import { PlusCircleOutlined } from '@ant-design/icons'
import ButtonFixed from 'common/components/ButtonFixed'

const { Option } = Select
const { Search } = Input
const Container = styled.div`
  width: 100%;
  padding: 0.5rem;
  background-color: white;
  border-bottom: 1px solid #dddd;
`

type HeaderProps = {
  setIsCreate: any
  onSearchSubmit: (searchKey: string) => void
  onStatusSubmit: (statusKey: any) => void
  onPakageCategoryIdSubmit: (categoryId: number) => void
  isSearchLoading: boolean
}

export const Header = ({
  setIsCreate,
  onSearchSubmit,
  onStatusSubmit,
  onPakageCategoryIdSubmit,
  isSearchLoading,
}: HeaderProps) => {
  const [searchKey, setSearchKey] = useState('')
  const [isTyping, setIsTyping] = useState(false)
  const [listCategory, setListCategory] = useState<Array<any>>([])

  useEffect(() => {
    setPakageCategory()
  }, [])

  useEffect(() => {
    onSearchSubmit(searchKey?.trim())
    setIsTyping(false)
  }, [searchKey])

  const setPakageCategory = async () => {
    try {
      const res = await getPakageCategory({ search: undefined })
      setListCategory(res.data)
    } catch (error) {
      console.log(error)
    }
  }
  return (
    <Row gutter={[16, 16]} justify="end">
      <Col span={8}>
        <Search
          loading={isTyping}
          value={searchKey}
          placeholder="Tìm kiếm tên gói"
          onChange={(e: any) => {
            setSearchKey(e.target.value === '' ? undefined : e.target.value)
          }}
        />
      </Col>
      <Col span={5}>
        <Select
          allowClear
          style={{ minWidth: '100px' }}
          placeholder="Trạng thái"
          onChange={value => {
            onStatusSubmit(value)
          }}
        >
          <Option value={''}>Tất cả</Option>
          <Option value="1">Hoạt động</Option>
          <Option value="0">Tạm dừng</Option>
        </Select>
      </Col>
      <Col span={6}>
        <Select
          style={{ minWidth: '140px' }}
          allowClear
          placeholder="Loại gói"
          onChange={(value: number) => {
            onPakageCategoryIdSubmit(value)
          }}
        >
          <Option value={-1}>Tất cả</Option>
          {listCategory.map((item: any) => {
            return <Option value={item.id}>{item.name}</Option>
          })}
        </Select>
      </Col>
      <Col span={4}>
        {/* <Button
          style={{
            minWidth: '70px',
            backgroundColor: '#00abba',
            borderColor: '#00abba',
            color: 'white',
          }}
          onClick={() => {
            setIsCreate(true)
          }}
        >
          <PlusCircleOutlined />
          Thêm mới
        </Button> */}
        <ButtonFixed setIsCreate={setIsCreate} text="Thêm mới" type="add" />
      </Col>
    </Row>
  )
}
