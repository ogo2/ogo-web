import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  DeleteFilled,
  EditOutlined,
} from '@ant-design/icons'
import { Button, Card, Descriptions, Popconfirm } from 'antd'
import { notificationSuccess, notificationError } from 'utils/notification'
import moment from 'moment'
import React, { useState } from 'react'
import { CHANGE_STATUS, STATUS } from 'utils/constants'
import { AddEditService } from './components/AddEditService'
import { changeStatusPakage, updatePakage } from './ServiceApi'
import { formatPrice } from 'utils/ruleForm'

type Props = {
  data: any
  getData: any
  onDeleteService: any
  //   onUpdateService: any
}

const ContentView = (data: any) => {
  return (
    <Descriptions size="default" column={2}>
      <Descriptions.Item label="Tên gói">{data.name}</Descriptions.Item>
      <Descriptions.Item label="Số phút livestream">
        {data.minus}
      </Descriptions.Item>
      <Descriptions.Item label="Giá">
        {data.price == 0 ? 0 : formatPrice(data.price)}
      </Descriptions.Item>
      <Descriptions.Item label="Đã bán">{data.count_sold}</Descriptions.Item>
      <Descriptions.Item label="Số gian hàng sử dụng">
        {data.count_shop_used}
      </Descriptions.Item>
      <Descriptions.Item label="Ngày tạo">
        {moment(data.create_at).format('DD-MM-YYYY')}
      </Descriptions.Item>
    </Descriptions>
  )
}

function ServiceDetail({ data, getData, onDeleteService }: Props) {
  const [showEditService, setShowEditService] = useState(false)

  const onChangeStatus = async () => {
    try {
      const payload = data.id
      await changeStatusPakage(payload)
      notificationSuccess(`Cập nhật trạng thái thành công`)
      getData()
    } catch (error) {
      console.log(error)
    } finally {
      //setIsLoadingModal(false)
    }
  }

  const updateService = async (value: any, resetFields: any) => {
    try {
      let dataPush = {
        id: data.id,
        data: {
          is_default: value.is_default ? 1 : 0,
          minus: value.minus,
          name: value.name,
          pakage_category_id: value.pakage_category_id,
          price: value.price,
        },
      }
      await updatePakage(dataPush)
      notificationSuccess(`Cập nhật thành công`)
      setShowEditService(false)
      getData()
      resetFields()
    } catch (error) {
      console.log(error)
    }
  }

  return (
    <div>
      {showEditService && (
        <AddEditService
          data={data}
          visible={showEditService}
          onCancel={() => {
            setShowEditService(false)
          }}
          onUpdateService={(newData: any, resetFields: any) => {
            updateService(newData, resetFields)
          }}
          isLoadingButton={false}
        />
      )}
      <Card
        style={{
          backgroundColor: '#f6f9ff',
          borderColor: '#1890ff',
          borderTop: 'none',
        }}
        actions={[
          <Popconfirm
            title={
              data.status === STATUS.ACTIVE
                ? 'Bạn chắc chắn muốn ngừng hoạt động gói dịch vụ này'
                : 'Bạn chắc chắn muốn bật hoạt động gói dịch vụ này'
            }
            onConfirm={() => {
              onChangeStatus()
            }}
            okText={
              data.status === STATUS.ACTIVE
                ? CHANGE_STATUS.INACTIVE
                : CHANGE_STATUS.ACTIVE
            }
            cancelText="Quay lại"
            okButtonProps={
              data.status === STATUS.ACTIVE
                ? {
                    danger: true,
                    type: 'primary',
                  }
                : {
                    danger: false,
                    type: 'primary',
                  }
            }
          >
            <Button
              type="text"
              size="large"
              style={{
                color: data.status === STATUS.ACTIVE ? 'red' : '#389e0d',
              }}
              icon={
                data.status === STATUS.ACTIVE ? (
                  <CloseCircleOutlined />
                ) : (
                  <CheckCircleOutlined />
                )
              }
            >
              {data.status === 1
                ? CHANGE_STATUS.INACTIVE
                : CHANGE_STATUS.ACTIVE}
            </Button>
          </Popconfirm>,

          <Button
            onClick={() => {
              setShowEditService(true)
            }}
            type="text"
            size="large"
            icon={<EditOutlined />}
            style={{ color: '#1890ff' }}
          >
            Chỉnh sửa
          </Button>,

          <Popconfirm
            title={'Bạn chắc chắn muốn xoá gói dịch vụ này'}
            onConfirm={async () => {
              try {
                onDeleteService(data.id)
              } catch (error) {
              } finally {
              }
            }}
            okText="Xoá"
            cancelText="Quay lại"
            okButtonProps={{
              danger: true,
              type: 'primary',
            }}
          >
            <Button
              type="text"
              size="large"
              icon={<DeleteFilled />}
              style={{ color: 'red' }}
            >
              Xoá gói dịch vụ
            </Button>
          </Popconfirm>,
        ]}
      >
        {ContentView(data)}
      </Card>
    </div>
  )
}

export default ServiceDetail
