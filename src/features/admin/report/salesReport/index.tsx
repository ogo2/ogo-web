import { Button, message, PageHeader, Space, Tabs } from 'antd'
import React, { useState } from 'react'
import ExportCsv from 'utils/ExportCSV'
import { formatPrice } from 'utils/ruleForm'
import ByCustomer from './components/ByCustomer'
import ByProduct from './components/ByProduct'
import './components/style.css'
import { getReport } from './SalesReportApi'

const { TabPane } = Tabs

export default function SalesReport() {
  const [keyTabs, setKeyTabs] = useState(1)
  const [params, setParams] = useState<any>()
  const handleTabs = (key: any) => {
    setKeyTabs(parseInt(key))
  }

  // const [isLoadingBtnExportData, setLoadingBtnExportData] = useState<boolean>(
  //   false
  // )
  // const [productExport, setProductExport] = useState<any>([])
  // const [customerExport, setCustomerExport] = useState<any>([])
  // const onExportDataToExcel = async (fn: any) => {
  //   try {
  //     setLoadingBtnExportData(true)
  //     const dataListReportNotPagging: any = await getReport(params)
  //     const exportProduct = dataListReportNotPagging?.data?.map(
  //       (o: any, i: number) => {
  //         return {
  //           STT: i + 1,
  //           'Tên sản phẩm': o.name || '',
  //           'Nhóm sản phẩm': o.category_name || '',
  //           'Gian hàng': o.Shop == null ? '--' : o.Shop?.name || '',
  //           'SL đã bán':
  //             o.total_amount == null ? '0' : formatPrice(o.total_amount),
  //           'Số đơn hàng':
  //             o.total_order == null ? '0' : formatPrice(o.total_order),
  //           'Tổng tiền thực tế':
  //             o.total_price != '0' ? formatPrice(o.total_price) : 0 + 'đ',
  //         }
  //       }
  //     )

  //     setProductExport(exportProduct)
  //     fn()
  //   } catch (error) {
  //     message.error(
  //       'Đã có lỗi trong quá trình export dữ liệu, vui lòng thử lại sau!'
  //     )
  //   } finally {
  //     setLoadingBtnExportData(false)
  //   }
  // }
  return (
    <>
      <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
        <PageHeader title="Báo cáo bán hàng" />
      </div>

      <div style={{ margin: '5px 10px 15px' }}>
        <Tabs
          style={{
            padding: '0 10px',
            backgroundColor: 'white',
          }}
          onChange={handleTabs}
        >
          <TabPane tab="Báo cáo bán hàng theo sản phẩm" key="1">
            <ByProduct keyTabs={keyTabs} />
          </TabPane>
          <TabPane tab="Báo cáo bán hàng theo khách hàng" key="2">
            <ByCustomer keyTabs={keyTabs} />
          </TabPane>
        </Tabs>
      </div>
    </>
  )
}
