import { DownloadOutlined, SaveOutlined } from '@ant-design/icons'
import { Col, DatePicker, message, Row, Select } from 'antd'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import React, { useState } from 'react'
import { REPORT_TYPE } from 'utils/constants'
import ExportCsv from 'utils/ExportCSV'
import { formatPrice } from 'utils/ruleForm'
import { getReport } from '../SalesReportApi'

const { Option } = Select
const { RangePicker } = DatePicker

type Props = {
  keyTabs: number
  listShop: Object[]
  onShopSubmit: (shop_id: string) => void
  onSearchSubmit: (search: string) => void
  onDateSubmit: (from_dateKey: string, to_dateKey: string) => void
  paramsProduct: any
  paramsCustomer: any
}

export default function Filter({
  keyTabs,
  listShop,
  onShopSubmit,
  onSearchSubmit,
  onDateSubmit,
  paramsProduct,
  paramsCustomer,
}: Props) {
  const [dataTables, setDataTables] = useState([])
  const [listShops, setListShops] = useState([])

  const [isLoadingBtnExportData, setLoadingBtnExportData] = useState<boolean>(
    false
  )
  const [productExport, setProductExport] = useState<any>([])
  const onExportProduct = async (fn: any) => {
    try {
      setLoadingBtnExportData(true)
      const dataListReportNotPagging = await getReport(paramsProduct)
      console.log(dataListReportNotPagging)
      const exportProduct = dataListReportNotPagging.data.rows.map(
        (o: any, i: number) => {
          return {
            STT: i + 1,
            'Tên sản phẩm': o.name || '',
            'Nhóm sản phẩm': o.category_name || '',
            'Gian hàng': o.Shop == null ? '--' : o.Shop?.name || '',
            'SL đã bán':
              o.total_amount == null ? '0' : formatPrice(o.total_amount),
            'Số đơn hàng':
              o.total_order == null ? '0' : formatPrice(o.total_order),
            'Tổng tiền thực tế':
              o.total_price != '0' ? formatPrice(o.total_price) : 0 + 'đ',
          }
        }
      )
      setProductExport(exportProduct)
      fn()
    } catch (error) {
      message.error(
        'Đã có lỗi trong quá trình export dữ liệu, vui lòng thử lại sau!'
      )
    } finally {
      setLoadingBtnExportData(false)
    }
  }

  const [customerExport, setCustomerExport] = useState<any>([])
  const onExportCustomer = async (fn: any) => {
    try {
      setLoadingBtnExportData(true)
      const dataListReportNotPagging = await getReport(paramsCustomer)
      console.log(dataListReportNotPagging)
      const exportCustomer = dataListReportNotPagging.data.rows.map(
        (o: any, i: number) => {
          return {
            STT: i + 1,
            'Tên khách hàng': o.name || '',
            'Số điện thoại': o.phone || '',
            'SL đã bán':
              o.total_amount == null ? '0' : formatPrice(o.total_amount),
            'Số đơn hàng':
              o.total_order == null ? '0' : formatPrice(o.total_order),
            'Tổng tiền thực tế':
              o.total_price != '0' ? formatPrice(o.total_price) : 0 + 'đ',
          }
        }
      )
      console.log(exportCustomer)
      setCustomerExport(exportCustomer)
      fn()
    } catch (error) {
      console.log(error)
      message.error(
        'Đã có lỗi trong quá trình export dữ liệu, vui lòng thử lại sau!'
      )
    } finally {
      setLoadingBtnExportData(false)
    }
  }

  return (
    <Row gutter={[16, 16]} justify="end">
      <Col span={10}>
        {keyTabs === 1 ? (
          <TypingAutoSearch
            onSearchSubmit={(searchKey: string) => {
              onSearchSubmit(searchKey.trim())
            }}
            isSearchLoading={false}
            placeholder="Nhập tên sản phẩm ..."
          />
        ) : (
          <TypingAutoSearch
            onSearchSubmit={(searchKey: string) => {
              onSearchSubmit(searchKey)
            }}
            isSearchLoading={false}
            placeholder="Nhập tên hoặc số điện thoại khách hàng ..."
          />
        )}
      </Col>

      <Col span={4}>
        <Select
          allowClear
          style={{ minWidth: '200px' }}
          optionFilterProp="children"
          placeholder="Gian hàng"
          onChange={(value: any) => {
            onShopSubmit(value)
          }}
          showSearch
          // onSearch={(value: string) => setParams({ ...params, search: value })}
        >
          {listShop.map((item: any, index: number) => {
            return (
              <Option key={index} value={item.id}>
                {item.name}
              </Option>
            )
          })}
        </Select>
      </Col>

      <Col span={5}>
        <RangePicker
          onChange={(value, dateString) => {
            onDateSubmit(dateString[0], dateString[1])
          }}
        />
      </Col>

      <Col span={4}>
        {keyTabs === 1 ? (
          <ExportCsv
            style={{ minWidth: '110px' }}
            loading={isLoadingBtnExportData}
            onClick={fn => onExportProduct(fn)}
            sheetName={['productSaleReport']}
            sheets={{
              productSaleReport: ExportCsv.getSheets(productExport),
            }}
            fileName="Báo cáo bán hàng theo sản phẩm"
          >
            <DownloadOutlined />
            &nbsp;Export
          </ExportCsv>
        ) : (
          <ExportCsv
            style={{ minWidth: '110px' }}
            loading={isLoadingBtnExportData}
            onClick={fn => onExportCustomer(fn)}
            sheetName={['customerSaleReport']}
            sheets={{
              customerSaleReport: ExportCsv.getSheets(customerExport),
            }}
            fileName="Báo cáo bán hàng theo khách hàng"
          >
            <DownloadOutlined />
            &nbsp;Export
          </ExportCsv>
        )}
      </Col>
    </Row>
  )
}
