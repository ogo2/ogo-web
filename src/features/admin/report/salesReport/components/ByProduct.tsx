import { Table } from 'antd'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { REPORT_TYPE } from 'utils/constants'
import { formatPrice } from 'utils/ruleForm'
import { getReport } from '../SalesReportApi'
import Filter from './Filter'

type Props = {
  keyTabs: number
}

export default function ByProduct({ keyTabs }: Props) {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const [dataTable, setDataTable] = useState([])
  const [listShop, setListShop] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [paramsProduct, setParamsProduct] = useState({
    search: '',
    type: REPORT_TYPE.CATEGORY,
    from_date: '',
    to_date: '',
    shop_id: '',
    page: 1,
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const getData = async () => {
    setIsLoading(true)
    try {
      const response = await getReport(paramsProduct)
      console.log(response)
      setListShop(response.data.listShop)
      setPaging({
        total: response.paging.totalItemCount,
        current: response.paging.page,
        pageSize: response.paging.limit,
      })
      setDataTable(response.data.rows)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    getData()
  }, [paramsProduct])

  const columns = [
    {
      width: 70,
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
      render: (text: any, record: any, index: any) => (
        <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
          {(paging.current - 1) * paging.pageSize + index + 1}
        </td>
      ),
      ellipsis: true,
    },
    {
      title: 'Tên sản phẩm',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Nhóm sản phẩm',
      dataIndex: 'Category',
      key: 'Category',
      render: (value: {
        id: number
        name: string
        parent_category?: {
          icon_url?: string
          id?: number
          name?: string
        }
      }) => (
        <td>
          {value?.parent_category?.name} -- {value?.name}
        </td>
      ),
    },
    {
      title: 'Gian hàng',
      dataIndex: 'Shop',
      key: 'Shop',
      render: (value: any) => <td>{value == null ? '--' : value?.name}</td>,
    },
    {
      title: 'SL đã bán',
      width: 110,
      dataIndex: 'total_amount',
      key: 'total_amount',
    },
    {
      title: 'Số đơn hàng',
      width: 130,
      dataIndex: 'total_order',
      key: 'total_order',
    },
    {
      title: 'Tổng tiền thực tế',
      dataIndex: 'total_price',
      key: 'total_price',
      render: (value: string) => (
        <td>{value != '0' ? formatPrice(value) : 0} đ</td>
      ),
    },
  ]
  return (
    <div>
      <div style={{ margin: '0 10px 15px' }}>
        <Filter
          paramsProduct={paramsProduct}
          paramsCustomer={paramsProduct}
          keyTabs={keyTabs}
          listShop={listShop}
          onShopSubmit={(shop_id: string) => {
            setParamsProduct({ ...paramsProduct, shop_id: shop_id, page: 1 })
          }}
          onSearchSubmit={(search: string) => {
            setParamsProduct({ ...paramsProduct, search: search, page: 1 })
          }}
          onDateSubmit={(from_date: string, to_date: string) => {
            setParamsProduct({
              ...paramsProduct,
              from_date: from_date,
              to_date: to_date,
              page: 1,
            })
          }}
        />
      </div>
      <div>
        <p>
          Kết quả lọc: <b>{paging.total}</b>
        </p>
        <Table
          className="table-expanded-custom-no-image"
          columns={columns}
          bordered
          loading={isLoading}
          dataSource={dataTable}
          scroll={{
            x: 800,
            scrollToFirstRowOnChange: true,
            // y: 'calc(100vh - 300px)',
            y: `calc(${heightWeb}px - 440px)`,
          }}
          summary={data => {
            let count_amount = 0
            let count_order = 0
            let count_price = 0
            data.forEach(item => {
              count_amount += parseInt(item.total_amount)
              count_order += parseInt(item.total_order)
              count_price += parseInt(item.total_price)
            })
            return (
              <Table.Summary.Row style={{ fontWeight: 'bold' }}>
                <Table.Summary.Cell index={0} colSpan={4} align="center">
                  Tổng
                </Table.Summary.Cell>
                <Table.Summary.Cell index={4}>
                  {count_amount}
                </Table.Summary.Cell>
                <Table.Summary.Cell index={5}>{count_order}</Table.Summary.Cell>
                <Table.Summary.Cell index={6}>
                  {formatPrice(count_price) ? formatPrice(count_price) : 0} đ
                </Table.Summary.Cell>
              </Table.Summary.Row>
            )
          }}
          pagination={{
            ...paging,
            showSizeChanger: false,
            onChange: async (page, pageSize) => {
              setParamsProduct({ ...paramsProduct, page: page })
            },
          }}
        />
      </div>
    </div>
  )
}
