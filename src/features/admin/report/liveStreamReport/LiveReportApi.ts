import { ApiClient } from 'services/ApiService'

export const getLiveReport = (payload: any) =>
  ApiClient.get('/report/report-livestream-admin', payload)
