import { ApiClient } from 'services/ApiService'

export const getShopReport = (payload: any) =>
  ApiClient.get('/report/report-shop', payload)
