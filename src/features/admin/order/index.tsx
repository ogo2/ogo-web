import { Empty, PageHeader, Table, Tag } from 'antd'
import { ADMIN_ROUTER_PATH } from 'common/config'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
import { ORDER_STATUS } from 'utils/constants'
import { formatPrice } from 'utils/ruleForm'
import { Header } from './components/Header'
import { getOrder } from './OrderApi'

export default function Order() {
  const [isLoading, setIsLoading] = useState(false)
  const [selectedRowList, setSelectedRowList] = useState<Array<any>>([])
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [params, setParams] = useState<any>({
    search: '',
    page: 1,
    limit: '',
    status: '',
    from_date: '',
    to_date: '',
    order_status: '',
    shop_id: '',
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })
  const history = useHistory()
  const [listOrder, setListOrder] = useState([])

  const getListOrder = async () => {
    setIsLoading(true)
    try {
      const res = await getOrder(params)
      const tableData = res.data.map((item: any, index: number) => {
        return { ...item, key: item.id, stt: index + 1 }
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setListOrder(tableData)
      setPaging(formattedPaging)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  useEffect(() => {
    getListOrder()
  }, [params])

  const columns = [
    {
      width: 70,
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
      render: (text: any, record: any, index: any) => (
        <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
          {(paging.current - 1) * paging.pageSize + index + 1}
        </td>
      ),
      ellipsis: true,
    },
    {
      title: 'Mã đơn',
      dataIndex: 'code',
      key: 'code',
    },
    {
      title: 'Khách hàng',
      dataIndex: 'User',
      key: 'User',
      render: (value: { id: number; name: string; phone: string }) => {
        return (
          <>
            <tr>{value.name}</tr>
            <tr>({value.phone})</tr>
          </>
        )
      },
    },
    {
      title: 'Người nhận hàng',
      dataIndex: 'UserAddress',
      key: 'UserAddress',
      render: (value: { id: number; name: string; phone: string }) => {
        return (
          <>
            <tr>{value.name}</tr>
            <tr>({value.phone})</tr>
          </>
        )
      },
    },
    {
      title: 'Tên gian hàng',
      dataIndex: 'Shop',
      key: 'Shop',
      render: (value: { id: number; name: string; phone: string }) => {
        return (
          <>
            <tr>{value.name}</tr>
          </>
        )
      },
    },
    {
      title: 'Số sp',
      dataIndex: 'total_amount',
      key: 'total_amount',
    },
    {
      title: 'Tổng tiền',
      dataIndex: 'total_price',
      key: 'total_price',
      render: (value: number) => (
        <td>{value == null ? '0' : formatPrice(value)} đ</td>
      ),
    },
    {
      width: '160px',
      title: 'TT đơn hàng',
      dataIndex: 'status',
      key: 'status  ',
      render: (value: any) => {
        if (value === ORDER_STATUS.PENDING)
          return <Tag color="blue">Chờ xác nhận</Tag>
        if (value === ORDER_STATUS.CONFIRMED) return <Tag>Đã xác nhận</Tag>
        if (value === ORDER_STATUS.SHIP) return <Tag>Đang giao</Tag>
        if (value === ORDER_STATUS.SUCCCESS)
          return <Tag color="green">Hoàn thành</Tag>
        if (value === ORDER_STATUS.CANCELED) return <Tag color="red">Huỷ</Tag>
      },
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'create_at',
      key: 'create_at',
      render: (value: any) => {
        return <>{moment(value).format('DD-MM-YYYY')}</>
      },
    },
  ]

  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any) => {
      const selectedRowID = selectedRows.map((item: any) => {
        return item
      })
      setSelectedRowList(selectedRowID)
    },
    getCheckboxProps: (record: any) => ({
      id: record.id,
    }),
  }

  return (
    <>
      <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
        <PageHeader
          title="Đơn hàng"
          extra={[
            <Header
              params={params}
              isSearchLoading={isSearchLoading}
              selectedRowList={selectedRowList}
              onSearchSubmit={(searchKey: string) => {
                setIsSearchLoading(true)
                setParams({ ...params, search: searchKey, page: 1 })
              }}
              onStatusSubmit={(status: string) => {
                setParams({ ...params, order_status: status, page: 1 })
              }}
              onShopSubmit={(shop_id: string) => {
                setParams({ ...params, shop_id: shop_id, page: 1 })
              }}
              onDateSubmit={(from_date: string, to_date: string) => {
                setParams({
                  ...params,
                  from_date: from_date,
                  to_date: to_date,
                  page: 1,
                })
              }}
            />,
          ]}
        />
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <p>
          Kết quả lọc: <b>{paging.total}</b>
        </p>
        <Table
          className="table-expanded-custom-no-image"
          bordered
          columns={columns}
          dataSource={listOrder}
          loading={isLoading}
          rowSelection={{
            ...rowSelection,
          }}
          scroll={{
            x: 800,
            scrollToFirstRowOnChange: true,
            y: 'calc(100vh - 340px)',
          }}
          onRow={(record, rowIndex) => ({
            onDoubleClick: () => {
              history.push({
                pathname: `${ADMIN_ROUTER_PATH.ORDER_DETAIL}/${record.id}`,
                state: record,
              })
            },
            // onMouseEnter: event => {
            //   setItemSelected({ id: record.id, rowIndex: Number(rowIndex) })
            // },
          })}
          locale={{
            emptyText: (
              <Empty
                image={Empty.PRESENTED_IMAGE_SIMPLE}
                description={<span>Không có dữ liệu.</span>}
              />
            ),
          }}
          pagination={{
            ...paging,
            showSizeChanger: false,
            onChange: async (page, pageSize) => {
              setParams({ ...params, page })
            },
          }}
        />
      </div>
    </>
  )
}
