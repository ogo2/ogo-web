import { DownloadOutlined, SaveOutlined } from '@ant-design/icons'
import { Col, DatePicker, message, Row, Select } from 'antd'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { ORDER_STATUS } from 'utils/constants'
import ExportCsv from 'utils/ExportCSV'
import { formatPrice } from 'utils/ruleForm'
import { allOrder, getDataShop } from '../OrderApi'
import './css/header.css'
const { Option } = Select
const { RangePicker } = DatePicker

type HeaderProps = {
  selectedRowList: Array<{}>
  onSearchSubmit: (searchKey: string) => void
  onStatusSubmit: (status: string) => void
  onDateSubmit: (from_dateKey: string, to_dateKey: string) => void
  onShopSubmit: (shop_id: string) => void
  isSearchLoading: boolean
  params: any
}

export const Header = ({
  onSearchSubmit,
  onStatusSubmit,
  onDateSubmit,
  onShopSubmit,
  isSearchLoading,
  selectedRowList,
  params,
}: HeaderProps) => {
  const [listShop, setListShop] = useState([])
  const [param, setParam] = useState({ page: 1, search: '' })
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [dataExport, setDataExport] = useState<any>([])
  const [isLoadingBtnExportData, setLoadingBtnExportData] = useState<boolean>(
    false
  )
  const getListShop = async () => {
    setIsLoading(true)
    try {
      const res = await getDataShop(param)
      setListShop(res.data)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    getListShop()
  }, [param])

  const handleDataSelect = () => {
    const dataSelected = selectedRowList.map((o: any, i: number) => {
      function checkStatus() {
        switch (o.status) {
          case ORDER_STATUS.PENDING:
            return 'Chờ xác nhận'
          case ORDER_STATUS.CONFIRMED:
            return 'Đã xác nhận'
          case ORDER_STATUS.SHIP:
            return 'Đang giao'
          case ORDER_STATUS.SUCCCESS:
            return 'Hoàn thành'
          case ORDER_STATUS.CANCELED:
            return 'Hủy'
          default:
            return ''
        }
      }
      return {
        STT: i + 1,
        'Mã đơn': o.code || '',
        'Khách hàng': o.user_name || '',
        'Gian hàng': o.shop_name || '',
        'Số sp': o.total_amount || '',
        'Tổng tiền':
          o.total_price == null ? '0đ' : formatPrice(o.total_price) + 'đ',
        'TT đơn hàng': checkStatus(),
        'Ngày tạo': moment(o.create_at).format('DD-MM-YYYY'),
      }
    })
    setDataExport(dataSelected)
  }

  useEffect(() => {
    handleDataSelect()
  }, [selectedRowList])

  const onExportDataToExcel = async (fn: any) => {
    try {
      setLoadingBtnExportData(true)
      if (selectedRowList.length === 0) {
        const dataListOrderNotPaging: any = await allOrder(params)
        const dataExport = dataListOrderNotPaging?.data?.map(
          (o: any, i: number) => {
            function checkStatus() {
              switch (o.status) {
                case ORDER_STATUS.PENDING:
                  return 'Chờ xác nhận'
                case ORDER_STATUS.CONFIRMED:
                  return 'Đã xác nhận'
                case ORDER_STATUS.SHIP:
                  return 'Đang giao'
                case ORDER_STATUS.SUCCCESS:
                  return 'Hoàn thành'
                case ORDER_STATUS.CANCELED:
                  return 'Hủy'
                default:
                  return ''
              }
            }
            return {
              STT: i + 1,
              'Mã đơn': o.code || '',
              'Khách hàng': o.user_name || '',
              'Gian hàng': o.shop_name || '',
              'Số sp': o.total_amount || '',
              'Tổng tiền':
                o.total_price == null ? '0đ' : formatPrice(o.total_price) + 'đ',
              'TT đơn hàng': checkStatus(),
              'Ngày tạo': moment(o.create_at).format('DD-MM-YYYY'),
            }
          }
        )
        setDataExport(dataExport)
        fn()
      } else {
        fn()
      }
    } catch (error) {
      message.error(
        'Đã có lỗi trong quá trình export dữ liệu, vui lòng thử lại sau!'
      )
    } finally {
      setLoadingBtnExportData(false)
    }
  }

  return (
    <Row gutter={[16, 16]} justify="end">
      <Col span={6}>
        <TypingAutoSearch
          onSearchSubmit={(searchKey: string) => {
            onSearchSubmit(searchKey.trim())
          }}
          isSearchLoading={isSearchLoading}
          placeholder="Nhập mã đơn hàng, tên khách hàng, SĐT ..."
        />
      </Col>

      <Col span={4}>
        <Select
          style={{ minWidth: '120px' }}
          allowClear
          placeholder="TT đơn hàng"
          onChange={(value: any) => {
            onStatusSubmit(value)
          }}
        >
          <Option value="">Tất cả</Option>
          <Option value={ORDER_STATUS.PENDING}>Chờ xác nhận</Option>
          <Option value={ORDER_STATUS.CONFIRMED}>Đã xác nhận</Option>
          <Option value={ORDER_STATUS.SHIP}>Đang giao</Option>
          <Option value={ORDER_STATUS.SUCCCESS}>Hoàn thành</Option>
          <Option value={ORDER_STATUS.CANCELED}>Huỷ</Option>
        </Select>
      </Col>

      <Col span={5}>
        <Select
          allowClear
          style={{ minWidth: '200px' }}
          optionFilterProp="children"
          loading={isLoading}
          placeholder="Gian hàng"
          onChange={(value: any) => {
            console.log(value)
            onShopSubmit(value)
          }}
          showSearch
          onSearch={(value: string) => setParam({ ...param, search: value })}
        >
          {listShop.map((item: any, index: number) => {
            return (
              <Option key={index} value={item.id}>
                {item.name}
              </Option>
            )
          })}
        </Select>
      </Col>

      <Col span={5}>
        <RangePicker
          placeholder={['Từ ngày', 'đến ngày']}
          onChange={(value, dateString) => {
            onDateSubmit(dateString[0], dateString[1])
          }}
        />
      </Col>

      <Col span={4}>
        <ExportCsv
          loading={isLoadingBtnExportData}
          onClick={fn => onExportDataToExcel(fn)}
          sheetName={['OrderList']}
          sheets={{
            OrderList: ExportCsv.getSheets(dataExport),
          }}
          fileName="Danh sách đơn hàng"
        >
          <DownloadOutlined />
          &nbsp;Export
        </ExportCsv>
      </Col>
    </Row>
  )
}
