export interface PostMediaInterface {
  id: number
  media_url: string
  create_at: Date
}
export interface TopicInterface {
  icon_url: string
  name: string
  create_at: Date
}
export interface UserInterface {
  id: number
  name: string
  profile_picture_url: string
  user_id?: number
  shop_id?: number
  df_type_user_id?: number
}
export interface ShopInterface {
  id: number
  name: string
  profile_picture_url: string
  user_id?: number
  shop_id?: number
}
export interface DataPostInteface {
  id: number
  PostMedia: Array<PostMediaInterface>
  Reactions: Array<any>
  Topic: TopicInterface
  Shop?: ShopInterface
  User: UserInterface
  content?: string
  count_comment: number
  count_like: number
  is_reaction: 0 | 1
  create_at: Date
}

export interface CommentItem {
  id: number
  user_id: number
  shop_id: number
  parent_id: number
  Comments: Array<CommentItem>
  Shop?: ShopInterface | null
  User: UserInterface
  target_user?: UserInterface
  content: string
  count_like: number
  count_sub_comment: number
  is_reaction: 0 | 1
  create_at: Date
  update_at: Date
}
