import React, { useState, useEffect, useRef } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { postDetail } from './CommunityPostApi'
import PostContent from './components/PostContent'
import PostListComment from './components/PostListComment'
import DetailPostHeader from './components/DetailPostHeader'
import ContainerDetailPost from './components/ContainerDetailPost'
import { Row } from 'antd'
import { DataPostInteface } from './Model'

function DetailPost() {
  const history = useHistory()
  const params: any = useParams()
  const inputRef = useRef<any>(null)
  const [dataPost, setDataPost] = useState<DataPostInteface | null>(null)
  useEffect(() => {
    if (params.id) {
      getDataPost()
    }
  }, [params.id])

  const getDataPost = async () => {
    try {
      const res = await postDetail(parseInt(params.id))
      if (!res.data) {
        history.goBack()
      }
      setDataPost(res.data)
    } catch (error) {
      history.goBack()
    }
  }
  const onFocusInput = () => inputRef.current && inputRef.current.focus()

  const ContentComponent = () => {
    return (
      <div style={{ padding: 10, width: '100%' }}>
        <Row style={{ width: '100%' }}>
          <PostContent
            onFocusInput={onFocusInput}
            dataPost={dataPost}
            setDataPost={setDataPost}
          />
        </Row>
        <Row style={{ width: '100%' }}>
          <PostListComment
            dataPost={dataPost}
            setDataPost={setDataPost}
            onFocusInput={onFocusInput}
            inputRef={inputRef}
            post_id={params.id}
          />
        </Row>
      </div>
    )
  }
  return (
    <ContainerDetailPost
      headerComponent={DetailPostHeader(dataPost?.User.df_type_user_id)}
      contentComponent={ContentComponent}
    />
  )
}
export default DetailPost
