import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  CloseSquareOutlined,
  PlusOutlined,
} from '@ant-design/icons'
import {
  Button,
  Col,
  DatePicker,
  Input,
  PageHeader,
  Popconfirm,
  Row,
  Select,
  Upload,
} from 'antd'
import Checkbox from 'antd/lib/checkbox/Checkbox'
import Modal from 'antd/lib/modal/Modal'
import { ADMIN_ROUTER_PATH } from 'common/config'
import moment from 'moment'
import { useEffect, useRef, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { ADMIN_POST_TOPIC, POST_STATUS } from 'utils/constants'
import { notificationError, notificationSuccess } from 'utils/notification'
import {
  createCommunityPost,
  postDetail,
  updatePostDetail,
} from '../CommunityPostApi'

interface IDataPush {
  topic_id: string
  status: string
  schedule: string
  content: string
  post_media: Array<any>
  image_delete: Array<any>
  is_posted: boolean
}
const { TextArea } = Input

const getBase64 = (file: any) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result)
    reader.onerror = error => reject(error)
  })
}
const disabledDate = (current: any) => {
  // Can not select days before today
  return current && current.valueOf() < moment().subtract(1, 'day').endOf('day')
}
function range(start: any, end: any) {
  const result = []
  for (let i = start; i < end; i++) {
    result.push(i)
  }
  return result
}
const disabledTime = (date: any) => {
  const time = new Date()
  return {
    disabledHours: () => range(0, 24).splice(0, time.getHours()),
    disabledMinutes: () => range(0, 60).splice(0, time.getMinutes() + 3),
  }
}

export default function AddEditCommunityPost() {
  const history = useHistory()
  const param: any = useParams()
  const [isLoading, setIsLoading] = useState(false)
  const [previewImage, setPreviewImage] = useState<string>('')
  const [isShowModalPreview, setShowModalPreview] = useState<boolean>(false)
  const [postNow, setPostNow] = useState(false)
  const timeOut = useRef<any>(null)
  const [dataPush, setDataPush] = useState<IDataPush>({
    topic_id: '',
    status: '',
    schedule: '',
    content: '',
    post_media: [],
    image_delete: [],
    is_posted: false,
  })
  const [listImages, setListImages] = useState<Array<any>>(
    Array.from(Array(4).keys()).map(i => {
      return {
        id: i,
        fileList: [],
        buffer: null,
        url: '',
      }
    })
  )
  const [listVideo, setListVideo] = useState<Array<any>>(
    Array.from(Array(1).keys()).map(i => {
      return {
        id: i,
        fileList: [],
        buffer: null,
        url: '',
      }
    })
  )
  useEffect(() => {
    if (param.id) {
      getData(param.id)
    }
  }, [param])

  const getData = async (id: number) => {
    const res_data = await postDetail(id)
    let media_image: Array<any> = []
    let media_video: Array<any> = []
    res_data.data.PostMedia.map((item: any, index: number) => {
      if (item.media_url.indexOf('.mp4') === -1) {
        media_image.push({
          id: index,
          fileList: [{ ...item, url: item.media_url }],
          buffer: { ...item, url: item.media_url },
          url: '',
        })
      } else {
        media_video.push({
          id: index,
          fileList: [{ ...item, url: item.media_url }],
          buffer: { ...item, url: item.media_url },
          url: item.media_url,
        })
      }
    })
    setListImages(media_image)
    setListVideo(media_video)
    setDataPush({
      ...dataPush,
      content: res_data.data.content,
      status: res_data.data.status,
      topic_id: res_data.data.topic_id,
      schedule: res_data.data.schedule,
      is_posted: res_data.data.is_posted ? true : false,
    })
  }

  const validateBeforeConfirm = () => {
    if (dataPush.topic_id === '') {
      notificationError('Vui lòng chọn chủ đề cho bài đăng')
      return false
    }
    if (dataPush.status === '') {
      notificationError('Vui lòng chọn đối tượng xem bài đăng')
      return false
    }
    if (!postNow && dataPush.schedule === '') {
      notificationError('Vui lòng chọn thời gian đăng')
      return false
    }
    if (dataPush.content.trim() === '') {
      notificationError('Vui lòng nhập nội dung bài đăng')
      return false
    }
    return true
  }

  const onSave = async () => {
    if (validateBeforeConfirm() === true) {
      setIsLoading(true)
      const dataForm = new FormData()
      try {
        dataForm.append('topic_id', dataPush.topic_id)
        dataForm.append('content', dataPush.content)
        let newImages = listImages.filter(image => image.buffer)
        let newVideos = listVideo.filter(video => video.buffer)
        newImages.map((item: any) => {
          dataForm.append('post_image', item.buffer.originFileObj)
        })
        newVideos.map((item: any) => {
          dataForm.append('post_media', item.buffer.originFileObj)
        })
        if (!dataPush.is_posted) {
          if (
            Date.parse(dataPush.schedule) < Date.now() ||
            dataPush.schedule === null
          ) {
            notificationError('Thời điểm đăng bài không hợp lệ')
            return
          }
          dataForm.append('status', dataPush.status)
          if (!postNow) {
            dataForm.append('schedule', dataPush.schedule.toString())
          }
        }
        if (!param.id) {
          const res_create = await createCommunityPost(dataForm)
          if (res_create.code === 1) {
            timeOut.current = setTimeout(() => {
              notificationSuccess('Tạo bài đăng thành công')
              history.push({
                pathname: `${ADMIN_ROUTER_PATH.COMMUNITY_POST}`,
              })
            }, 1200)
          }
        } else {
          console.log('Sủa bài đang')
          dataPush.image_delete.some((item: any) => {
            dataForm.append('image_delete', item)
          })
          const res_update = await updatePostDetail(dataForm, param.id)
          console.log('Sủa bài đang')
          if (res_update.code === 1) {
            timeOut.current = setTimeout(() => {
              notificationSuccess('Sửa bài đăng thành công')
              history.push({
                pathname: `${ADMIN_ROUTER_PATH.COMMUNITY_POST}`,
              })
            }, 1100)
          }
        }
      } catch (error) {
        notificationError('Xảy ra lỗi khi đăng bài')
        console.log(error)
      } finally {
        setTimeout(() => {
          setIsLoading(false)
        }, 1300)
      }
    }
  }
  useEffect(() => {
    return timeOut.current && clearTimeout(timeOut.current)
  })
  const onAppendNewLine = (e: any) => {
    const data = dataPush.content + '\n '
    setDataPush({
      ...dataPush,
      content: data,
    })
    e.preventDefault()
  }
  const handlePreviewImage = async (file: any) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj)
    }
    setPreviewImage(file.url || file.preview)
    setShowModalPreview(true)
  }

  const handleChangeImage = async (file: any, index: number) => {
    const isLt3M = file.file.type ? file.file.size / 1024 / 1024 < 3 : true
    const isJpgOrPng = file.file.type
      ? file.file.type === 'image/jpeg' || file.file.type === 'image/png'
      : true

    if (!isJpgOrPng) {
      notificationError('Bạn chỉ có thể upload ảnh có định dạng JPG/PNG!')
      return
    } else if (!isLt3M) {
      notificationError('Dung lượng ảnh tối đa là 3MB!')
      return
    }

    // case uploading image
    if (file?.file?.status === 'uploading') {
      listImages[index].fileList = file.fileList || []
      listImages[index].buffer = file.fileList[0] || ''
      listImages[index].fileList[0].status = 'done'
      setListImages([...listImages])
    }

    if (file.file?.status === 'error') {
      delete file.file.error
      file.file.status = 'done'
    }
    if (file.file.status === 'removed') {
      listImages[index].fileList = []
      listImages[index].buffer = ''
      setListImages([...listImages])
      if (param.id && file.file.id) dataPush.image_delete.push(file.file.id)
    }
  }

  const handleChangeVideo = async (file: any, index: number) => {
    if (file.fileList[0]?.type !== 'video/mp4') {
      notificationError(
        'Định dạng video không được hỗ trợ. Vui lòng chọn video khác.'
      )
    }

    try {
      if (file.file?.status === 'error') {
        delete file.file.error
        file.file.status = 'done'
      }
      if (file?.file?.status === 'uploading') {
        listVideo[index].fileList = file.fileList || []
        listVideo[index].buffer = file.fileList[0] || ''
        listVideo[index].fileList[0].status = 'done'
        listVideo[index].url = await getBase64(file.file.originFileObj)
        setListVideo([...listVideo])
      }
    } catch (error) {
      notificationError(
        'Tải video thất bại, vui lòng kiểm tra kết nối và thử lại.'
      )
    } finally {
    }
  }

  return (
    <div>
      <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
        <PageHeader
          title={!param.id ? 'Thêm bài đăng' : 'Sửa bài đăng'}
          onBack={() => {
            history.push(ADMIN_ROUTER_PATH.COMMUNITY_POST)
          }}
          extra={[
            <Popconfirm
              placement="bottomRight"
              title="Bạn có chắc chắn muốn hủy?"
              onConfirm={() => {
                history.push(ADMIN_ROUTER_PATH.COMMUNITY_POST)
              }}
              okText="Đồng ý"
              cancelText="Huỷ"
              okButtonProps={{
                danger: true,
                type: 'primary',
              }}
            >
              <Button danger style={{ height: '35px', fontWeight: 800 }}>
                <CloseCircleOutlined /> Hủy
              </Button>
            </Popconfirm>,
            <Button
              loading={isLoading}
              type="primary"
              onClick={onSave}
              style={{
                fontWeight: 800,
                backgroundColor: '#00abba',
                borderColor: '#00abba',
                height: '35px',
              }}
            >
              <CheckCircleOutlined />
              Lưu
            </Button>,
          ]}
        />
      </div>
      <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
        <div>
          {/* <Col span={11} style={{ margin: '15px' }}> */}
          <Row style={{ padding: '20px 20px 10px' }}>
            <Row style={{ paddingRight: '120px', paddingBottom: '20px' }}>
              <div style={{ width: '140px' }}>
                <span
                  style={{
                    color: 'red',
                    marginRight: '5px',
                    fontSize: '20px',
                    textAlign: 'center',
                  }}
                >
                  *
                </span>
                <b>Chủ đề:</b>
              </div>

              <Select
                placeholder="Chọn chủ đề"
                style={{ width: '300px' }}
                onChange={(value: string) => {
                  setDataPush({ ...dataPush, topic_id: value })
                }}
                value={dataPush.topic_id === '' ? undefined : dataPush.topic_id}
              >
                <Select.Option value={ADMIN_POST_TOPIC.PROMOTION}>
                  Khuyến mãi
                </Select.Option>
                <Select.Option value={ADMIN_POST_TOPIC.NOTIFICATION}>
                  Thông báo
                </Select.Option>
              </Select>
            </Row>

            {/* </Col> */}

            {/* <Col span="11" style={{ margin: '15px' }}> */}
            {/* <Row> */}
            <Row>
              <div style={{ width: '140px' }}>
                <span
                  style={{
                    color: 'red',
                    marginRight: '5px',
                    fontSize: '20px',
                    textAlign: 'center',
                  }}
                >
                  *
                </span>
                <b>Đối tượng:</b>
              </div>

              <Select
                placeholder="Đối tượng"
                style={{ width: '300px' }}
                onChange={(value: string) => {
                  setDataPush({ ...dataPush, status: value })
                }}
                value={dataPush.status === '' ? undefined : dataPush.status}
                disabled={dataPush.is_posted}
              >
                <Select.Option value={POST_STATUS.ALL}>Tất cả</Select.Option>
                <Select.Option value={POST_STATUS.SHOP}>
                  Gian hàng
                </Select.Option>
                <Select.Option value={POST_STATUS.CUSTOMER}>
                  Khách hàng
                </Select.Option>
              </Select>
            </Row>
          </Row>
          {/* </Col> */}

          {/* <Col span={16} style={{ margin: '15px' }}> */}
          {!dataPush.is_posted ? (
            <Row gutter={[16, 16]} style={{ padding: '25px 25px 15px' }}>
              <div style={{ width: '140px' }}>
                <span
                  style={{
                    color: 'red',
                    marginRight: '5px',
                    fontSize: '20px',
                    textAlign: 'center',
                  }}
                >
                  *
                </span>
                <b>Hẹn giờ đăng bài:</b>
              </div>
              <Checkbox
                style={{ paddingRight: '50px' }}
                onChange={(e: any) => {
                  setPostNow(e.target.checked)
                }}
              >
                Đăng ngay
              </Checkbox>
              {!postNow ? (
                <DatePicker
                  style={{ width: '280px' }}
                  format="HH:mm DD-MM-YYYY "
                  disabledDate={disabledDate}
                  // disabledTime={disabledTime}
                  showTime={{ format: 'HH:mm' }}
                  onChange={(value: any, dataString: any) => {
                    setDataPush({ ...dataPush, schedule: value })
                  }}
                  value={param.id ? moment(dataPush.schedule) : undefined}
                />
              ) : (
                <></>
              )}
            </Row>
          ) : (
            ''
          )}
          {/* </Col> */}
        </div>
        <Row style={{ marginBottom: '15px', marginTop: '15px' }}>
          <Col span="3" style={{ marginLeft: '15px' }}>
            <b>Ảnh:</b>
          </Col>

          <Col span={20} style={{ display: 'flex', flexWrap: 'wrap' }}>
            {listImages.map((item: any, index: number) => {
              return (
                <Upload
                  listType="picture-card"
                  accept="image/jpeg,image/png,image/jpg"
                  fileList={item?.fileList}
                  onPreview={handlePreviewImage}
                  onChange={(value: any) => {
                    handleChangeImage(value, index)
                  }}
                >
                  {item?.fileList.length >= 1 ? null : (
                    <div>
                      <PlusOutlined />
                      Tải ảnh
                    </div>
                  )}
                </Upload>
              )
            })}
            {listImages.length < 20 ? (
              <Button
                style={{
                  width: '104px',
                  height: '104px',
                  border: '1px dashed #d9d9d9 ',
                  backgroundColor: '#fafafa',
                }}
                onClick={() => {
                  setListImages([
                    ...listImages,
                    {
                      id: listImages.length,
                      fileList: [],
                      buffer: null,
                      url: '',
                    },
                  ])
                }}
              >
                <div>
                  <PlusOutlined />
                  <div>Thêm ảnh</div>
                </div>
              </Button>
            ) : (
              ''
            )}
          </Col>
        </Row>
        {/* <Row style={{ marginBottom: '15px', marginTop: '15px' }}>
          <Col span="3" style={{ marginLeft: '15px' }}>
            <b>Video:</b>
          </Col>

          <Col span={20} style={{ display: 'flex', flexWrap: 'wrap' }}>
            {listVideo.map((item: any, index: number) => {
              return (
                <div>
                  {item.fileList.length === 0 ? (
                    <Upload
                      listType="picture-card"
                      accept="video/mp4"
                      fileList={item?.fileList}
                      onPreview={handlePreviewImage}
                      onChange={(value: any) => {
                        handleChangeVideo(value, index)
                      }}
                    >
                      {item?.fileList.length >= 1 ? null : (
                        <div>
                          <PlusOutlined />
                          Tải video
                        </div>
                      )}
                    </Upload>
                  ) : (
                    <div
                      style={{
                        border: '1px solid dotted',
                        position: 'relative',
                        marginRight: '30px',
                      }}
                    >
                      <video
                        controls
                        src={listVideo[index].url}
                        className="uploaded-pic img-thumbnail "
                        style={{ marginRight: '10px' }}
                        width={310}
                        height={217.5}
                      />
                      <CloseSquareOutlined
                        style={{
                          position: 'absolute',
                          fontSize: 20,
                          cursor: 'pointer',
                          color: 'red',
                        }}
                        onClick={() => {
                          if (param.id && item.buffer.id) {
                            dataPush.image_delete.push(item.buffer.id)
                          }
                          listVideo[index].fileList = []
                          listVideo[index].buffer = ''
                          setListVideo([...listVideo])
                        }}
                      />
                    </div>
                  )}
                </div>
              )
            })}
            {listVideo.length < 5 ? (
              <Button
                style={{
                  width: '104px',
                  height: '104px',
                  border: '1px dashed #d9d9d9 ',
                  backgroundColor: '#fafafa',
                  marginLeft: '10px',
                }}
                onClick={() => {
                  setListVideo([
                    ...listVideo,
                    {
                      id: listVideo.length,
                      fileList: [],
                      buffer: null,
                      url: '',
                    },
                  ])
                }}
              >
                <div>
                  <PlusOutlined />
                  <div>Thêm video</div>
                </div>
              </Button>
            ) : (
              ''
            )}
          </Col>
        </Row> */}
        <Row>
          <Col span="3" style={{ marginLeft: '15px' }}>
            <span
              style={{
                color: 'red',
                marginRight: '5px',
                fontSize: '20px',
                textAlign: 'center',
              }}
            >
              *
            </span>
            <b>Nội dung:</b>
          </Col>
          <Col span="18">
            <TextArea
              style={{ height: '300px' }}
              placeholder="Nhập nội dung bài đăng"
              value={dataPush.content}
              onChange={e => {
                setDataPush({ ...dataPush, content: e.target.value })
              }}
              onKeyPress={e =>
                e.shiftKey && e.key === 'Enter' ? onAppendNewLine(e) : ''
              }
            />
          </Col>
        </Row>

        <Row
          gutter={16}
          justify="end"
          style={{
            marginTop: '20px',
            paddingBottom: '30px',
            paddingRight: '120px',
          }}
        >
          <Col>
            <Popconfirm
              placement="bottomRight"
              title="Bạn có chắc chắn muốn hủy?"
              style={{ margin: 'auto' }}
              onConfirm={() => {
                history.push(ADMIN_ROUTER_PATH.COMMUNITY_POST)
              }}
              okText="Đồng ý"
              cancelText="Huỷ"
              okButtonProps={{
                danger: true,
                type: 'primary',
              }}
            >
              <Button danger style={{ height: '35px', fontWeight: 800 }}>
                <CloseCircleOutlined /> Hủy
              </Button>
            </Popconfirm>
          </Col>
          <Col>
            <Button
              loading={isLoading}
              type="primary"
              onClick={onSave}
              style={{
                backgroundColor: '#00abba',
                borderColor: '#00abba',
                height: '35px',
                fontWeight: 800,
              }}
            >
              <CheckCircleOutlined />
              Lưu
            </Button>
          </Col>
          <Modal
            visible={isShowModalPreview}
            footer={null}
            onCancel={() => setShowModalPreview(false)}
          >
            <img alt="example" style={{ width: '100%' }} src={previewImage} />
          </Modal>
        </Row>
      </div>
    </div>
  )
}
