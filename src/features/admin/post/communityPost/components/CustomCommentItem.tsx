import React from 'react'
import { Comment, Avatar, Button } from 'antd'
import { splitTextEndLine } from 'utils/funcHelper'
import { CommentItem } from '../Model'
import moment from 'moment'
import { requestReactionComment } from '../CommunityPostApi'

import { UserOutlined, HeartFilled, MessageOutlined } from '@ant-design/icons'

interface PropsCustomCommentItem {
  children?: any
  commentItem: CommentItem
  post_id: number
  onCommentFocusInput: (commentItem: CommentItem) => void
}

const CustomCommentItem = ({
  children,
  commentItem,
  post_id,
  onCommentFocusInput,
}: PropsCustomCommentItem) => {
  const reactionRequest = async () => {
    if (commentItem?.id) {
      try {
        const res = await requestReactionComment(post_id, commentItem.id)
        console.log('res', res.data)
      } catch (error) {
        console.log(error)
      }
    }
  }
  return (
    <Comment
      style={{ padding: 0, margin: 0 }}
      actions={[
        <span>
          {moment(commentItem?.create_at).format('h:mm DD/MM/YYYY ')}
        </span>,
        <Button
          type="text"
          danger={commentItem?.is_reaction ? true : false}
          icon={<HeartFilled style={{ fontSize: 12 }} />}
          onClick={() => reactionRequest()}
        >
          <span style={{ fontSize: 12 }}>{commentItem?.count_like}</span>
        </Button>,
        <Button
          type="text"
          onClick={() => onCommentFocusInput(commentItem)}
          icon={<MessageOutlined style={{ fontSize: 12 }} />}
        >
          <span style={{ fontSize: 12 }}>{commentItem?.count_sub_comment}</span>
        </Button>,
      ]}
      author={
        <a>
          <strong style={{ fontSize: 15 }}>
            {commentItem?.Shop
              ? commentItem?.Shop?.name
              : commentItem?.User?.name}
          </strong>
        </a>
      }
      avatar={
        <Avatar
          size={42}
          icon={<UserOutlined />}
          src={
            commentItem?.Shop
              ? commentItem?.Shop?.profile_picture_url
              : commentItem?.User?.profile_picture_url
          }
          alt={
            commentItem?.Shop
              ? commentItem?.Shop?.name
              : commentItem?.User?.name
          }
        />
      }
      content={
        <>
          {commentItem?.target_user && (
            <a style={{ color: '#484848' }}>
              <strong>{commentItem?.target_user?.name} </strong>
            </a>
          )}
          {splitTextEndLine(
            commentItem?.content
          ).map((text: string, index: number) =>
            index === 0 ? (
              <p style={{ display: 'inline-block' }}>{text}</p>
            ) : (
              <p>{text}</p>
            )
          )}
        </>
      }
    >
      {children}
    </Comment>
  )
}

export default CustomCommentItem
