import {
  Button,
  Col,
  DatePicker,
  Input,
  PageHeader,
  Pagination,
  Row,
  Select,
  Image,
  Tag,
  Divider,
  Typography,
} from 'antd'
import moment from 'moment'
import {
  HeartFilled,
  MessageOutlined,
  PlusCircleOutlined,
} from '@ant-design/icons'
import { useState, useEffect } from 'react'
import { ADMIN_POST_TOPIC } from 'utils/constants'
import { ADMIN_ROUTER_PATH } from 'common/config'
import Avatar from 'antd/lib/avatar/avatar'
import { UserOutlined } from '@ant-design/icons'
import { listCommunityPost } from './CommunityPostApi'
import { useHistory } from 'react-router-dom'
import { Empty } from 'antd'
import { lineBreakPost, splitTextEndLine } from 'utils/funcHelper'
import { useSelector } from 'react-redux'

const { Option } = Select
const { RangePicker } = DatePicker
const { Search } = Input
const { Paragraph, Text } = Typography

export default function CommunityPost() {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [dataPost, setDataPost] = useState<Array<any>>([])

  const [paramSearch, setParamSearch] = useState({
    search: '',
    user_id: undefined,
    shop_id: undefined,
    topic_id: undefined,
    from_date: '',
    to_date: '',
    is_posted: undefined,
    page: 1,
  })
  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })
  const history = useHistory()
  useEffect(() => {
    getListCommunityPost()
  }, [])

  useEffect(() => {
    setTimeout(() => {
      getListCommunityPost()
    }, 400)
  }, [paramSearch])

  const getListCommunityPost = async () => {
    setIsSearchLoading(true)
    try {
      const res = await listCommunityPost(paramSearch)
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setPaging(formattedPaging)
      setDataPost(res.data)
    } catch (error) {
      console.log(error)
    } finally {
      setIsSearchLoading(false)
    }
  }
  return (
    <div>
      <div style={{ backgroundColor: 'white', margin: '5px 10px' }}>
        <PageHeader
          title="Bài đăng cộng đồng"
          extra={[
            <Button
              style={{
                backgroundColor: '#00abba',
                borderColor: '#00abba',
                color: 'white',
              }}
              onClick={() => {
                history.push({
                  pathname: `${ADMIN_ROUTER_PATH.ADD_POST}`,
                })
              }}
            >
              <PlusCircleOutlined />
              Thêm bài đăng
            </Button>,
          ]}
        />
      </div>
      <div
        style={{
          backgroundColor: 'white',
          padding: '20px 20px 20px',
          margin: '10px 10px',
        }}
      >
        <Row gutter={[16, 16]} justify="end">
          <Col span={6}>
            <Search
              allowClear
              placeholder="Người đăng, nội dung bài đăng ..."
              loading={isSearchLoading}
              onChange={e => {
                setParamSearch({
                  ...paramSearch,
                  search: e.target.value.trim(),
                })
              }}
            />
          </Col>
          <Col span={4}>
            <Select
              placeholder="Chọn chủ đề"
              allowClear
              // style={{ minWidth: '120px' }}
              onChange={(value: any) => {
                setParamSearch({
                  ...paramSearch,
                  topic_id: value === '' ? undefined : value,
                })
              }}
            >
              <Option value="">Tất cả</Option>
              <Option value={ADMIN_POST_TOPIC.PROMOTION}>Khuyến mãi</Option>
              <Option value={ADMIN_POST_TOPIC.NOTIFICATION}>Thông báo</Option>
            </Select>
          </Col>
          <Col span={4}>
            <Select
              allowClear
              placeholder="Trạng thái hoạt động"
              // style={{ minWidth: '120px' }}
              onChange={(value: any) => {
                setParamSearch({
                  ...paramSearch,
                  is_posted: value,
                })
              }}
            >
              <Option value={0}>Chưa đăng</Option>
              <Option value={1}>Đã đăng</Option>
            </Select>
          </Col>
          <Col span={5}>
            <RangePicker
              placeholder={['Từ ngày', 'đến ngày']}
              onChange={(value, dateString) => {
                setParamSearch({
                  ...paramSearch,
                  from_date: dateString[0],
                  to_date: dateString[1],
                })
              }}
            />
          </Col>
        </Row>
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '10px 10px',
          padding: '15px 15px',
        }}
      >
        <p>
          {/* Kết quả lọc: <b>{dataPost.length}</b> */}
          Kết quả lọc: <b>{paging.total}</b>
        </p>
        {dataPost.length === 0 ? (
          <Empty
            description={
              <span>Chưa có bài đăng. Hãy là người đầu tiên đăng bài</span>
            }
          />
        ) : (
          <>
            <div
              style={{
                overflowY: 'scroll',
                height: `calc(${heightWeb}px - 340px)`,
              }}
            >
              {dataPost.map((item: any) => {
                return (
                  <div
                    style={{ marginBottom: '25px' }}
                    onClick={() => {
                      history.push({
                        pathname: `${ADMIN_ROUTER_PATH.COMMUNITY_POST}/${item.id}`,
                      })
                    }}
                  >
                    <Row style={{ paddingBottom: '15px' }}>
                      <Col>
                        <Avatar
                          size={64}
                          icon={<UserOutlined />}
                          src={
                            item.Shop?.profile_picture_url
                              ? item.Shop?.profile_picture_url
                              : item.User?.profile_picture_url
                          }
                        />
                      </Col>
                      <Col style={{ marginLeft: '15px' }}>
                        {item.Shop ? (
                          <b>{item.Shop.name + '( ' + item.User.name + ') '}</b>
                        ) : (
                          <b>{item.User.name}</b>
                        )}

                        <Row>
                          <p>
                            {' ' +
                              moment(
                                item.schedule ? item.schedule : item.create_at
                              ).fromNow() +
                              ' '}
                          </p>
                          &nbsp;
                          <p>
                            {item.is_posted ? '( Đã đăng )' : '( Chưa đăng )'}
                          </p>
                          <Tag
                            color="#B47EDE"
                            style={{ marginLeft: '10px', height: '25px' }}
                          >
                            {item.Topic.name}
                          </Tag>
                        </Row>
                      </Col>
                      <p style={{ paddingLeft: '43%' }}>
                        Ngày viết:
                        <b>
                          {' ' +
                            moment(item.create_at).format('HH:mm DD/MM/YYYY ')}
                        </b>
                        <br></br>
                        Ngày đăng:
                        <b>
                          {item.schedule
                            ? ' ' +
                              moment(item.schedule).format('HH:mm DD/MM/YYYY ')
                            : ' ' +
                              moment(item.create_at).format(
                                'HH:mm DD/MM/YYYY '
                              )}
                        </b>
                      </p>
                    </Row>
                    <div style={{ paddingBottom: '5px' }}>
                      {item.content &&
                        lineBreakPost(item.content).map((text: string) => (
                          <Text>
                            <span>
                              {text} <br />
                            </span>
                          </Text>
                        ))}
                      <Row>
                        {item.PostMedia.map((itemA: any) => {
                          return (
                            <Col
                              span={5}
                              style={{
                                maxHeight: '145px',
                                maxWidth: '200px',
                                display: 'flex',
                                justifyContent: 'center',
                                alignItems: 'center',
                                marginRight: '10px',
                                overflow: 'hidden',
                                marginBottom: '10px',
                                marginTop: '15px',
                              }}
                            >
                              {itemA.media_url.indexOf('.mp4') === -1 ? (
                                <Image
                                  src={itemA.media_url}
                                  onClick={e => e.stopPropagation()}
                                />
                              ) : (
                                <video
                                  controls
                                  src={itemA.media_url}
                                  className="uploaded-pic img-thumbnail "
                                  width={200}
                                  height={145}
                                />
                              )}
                            </Col>
                          )
                        })}
                      </Row>
                      <Row style={{ marginBottom: 5 }}>
                        <Col>
                          <Button
                            type="text"
                            danger={item?.is_reaction ? true : false}
                            icon={<HeartFilled style={{ fontSize: 18 }} />}
                          >
                            <span style={{ fontSize: 18 }}>
                              {item?.count_like}
                            </span>
                          </Button>
                        </Col>
                        <Col>
                          <Button
                            type="text"
                            // danger={item?.is_reaction ? true : false}
                            icon={<MessageOutlined style={{ fontSize: 18 }} />}
                          >
                            <span style={{ fontSize: 18 }}>
                              {item?.count_comment}
                            </span>
                          </Button>
                        </Col>
                      </Row>
                    </div>
                    {dataPost.length === 1 ? (
                      <></>
                    ) : (
                      <Divider style={{ backgroundColor: '#A4A4A4' }} />
                    )}
                  </div>
                )
              })}
            </div>
            <div>
              <Divider style={{ margin: '5px 0px' }} />
              <Pagination
                size="small"
                showSizeChanger={false}
                defaultCurrent={paging.current}
                total={paging.total}
                pageSize={paging.pageSize}
                onChange={async (page, pageSize) => {
                  setParamSearch({ ...paramSearch, page })
                }}
              />
            </div>
          </>
        )}
      </div>
    </div>
  )
}
