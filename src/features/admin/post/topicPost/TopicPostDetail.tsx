import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  DeleteFilled,
  EditOutlined,
} from '@ant-design/icons'
import { Button, Card, Descriptions, Popconfirm, Spin } from 'antd'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { CHANGE_STATUS, STATUS } from 'utils/constants'
import { AddEditTopicPost } from './components/AddEditTopicPost'
import { changeStatusTopic, topicDetail, updateTopic } from './TopicPostApi'
import { notificationSuccess } from 'utils/notification'

type Props = {
  data: any
  getData: any
  onDeleteTopicPost: any
}

function TopicPostDetail({ data, getData, onDeleteTopicPost }: Props) {
  const [showEditAccount, setShowEditAccount] = useState(false)
  const [detail, setDetail] = useState({})
  const [isLoading, setIsLoading] = useState<boolean>(false)

  const onChangeStatus = async () => {
    try {
      const payload = data.id
      await changeStatusTopic(payload)
      notificationSuccess(`Cập nhật trạng thái thành công`)
      getData()
    } catch (error) {
      console.log(error)
    } finally {
      //setIsLoadingModal(false)
    }
  }

  const updateService = async (value: any, resetFields: any) => {
    try {
      let dataPush = {
        id: data.id,
        data: {
          name: value.name,
          order: value.order,
          description: value.description,
          icon_url: value.icon_url,
          status: data.status,
        },
      }
      console.log('dataPush', dataPush)
      await updateTopic(dataPush)
      notificationSuccess(`Cập nhật thành công`)
      setShowEditAccount(false)
      getData()
      // setShowEditAccount(false)
      resetFields()
    } catch (error) {
      // message.error(`Đã có lỗi xảy ra, vui lòng thử lại`)
      console.log(error)
    }
  }

  const getDetail = async () => {
    setIsLoading(true)
    try {
      const response = await topicDetail(data.id)
      setDetail(response.data)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    getDetail()
  }, [])

  const ContentView = (data: any) => {
    return (
      <Spin spinning={isLoading}>
        <Descriptions size="default" column={3}>
          <Descriptions.Item label="Tên chủ đề">{data?.name}</Descriptions.Item>
          <Descriptions.Item label="Trạng thái">
            {data?.status ? 'Đang hoạt động' : 'Ngừng hoạt động'}
          </Descriptions.Item>
          <Descriptions.Item label="Thứ tự hiển thị">
            {data?.order}
          </Descriptions.Item>
          <Descriptions.Item label="Số người quan tâm">
            {data?.count_care}
          </Descriptions.Item>
          <Descriptions.Item label="Số bài viết">
            {data?.count_post}
          </Descriptions.Item>
          <Descriptions.Item label="Ngày tạo">
            {moment(data?.create_at).format('DD-MM-YYYY')}
          </Descriptions.Item>
          <Descriptions.Item label="Mô tả">
            {data?.description}
          </Descriptions.Item>
        </Descriptions>
      </Spin>
    )
  }

  return (
    <div>
      {showEditAccount && (
        <AddEditTopicPost
          data={data}
          visible={showEditAccount}
          onCancel={() => {
            setShowEditAccount(false)
          }}
          onUpdateTopicPost={(newData: any, resetFields: any) => {
            updateService(newData, resetFields)
          }}
          isLoadingButton={false}
        />
      )}
      <Card
        style={{
          backgroundColor: '#f6f9ff',
          borderColor: '#1890ff',
          borderTop: 'none',
        }}
        actions={[
          <Popconfirm
            title={
              data.status === STATUS.ACTIVE
                ? 'Bạn chắc chắn muốn ngừng hoạt động chủ đề này'
                : 'Bạn chắc chắn muốn bật hoạt động chủ đề này'
            }
            onConfirm={() => {
              onChangeStatus()
            }}
            okText={
              data.status === STATUS.ACTIVE
                ? CHANGE_STATUS.INACTIVE
                : CHANGE_STATUS.ACTIVE
            }
            cancelText="Quay lại"
            okButtonProps={
              data.status === STATUS.ACTIVE
                ? {
                    danger: true,
                    type: 'primary',
                  }
                : {
                    danger: false,
                    type: 'primary',
                  }
            }
          >
            <Button
              type="text"
              size="large"
              style={{
                color: data.status === STATUS.ACTIVE ? 'red' : '#389e0d',
              }}
              icon={
                data.status === STATUS.ACTIVE ? (
                  <CloseCircleOutlined />
                ) : (
                  <CheckCircleOutlined />
                )
              }
            >
              {data.status === 1
                ? CHANGE_STATUS.INACTIVE
                : CHANGE_STATUS.ACTIVE}
            </Button>
          </Popconfirm>,

          <Button
            onClick={() => {
              setShowEditAccount(true)
            }}
            type="text"
            size="large"
            icon={<EditOutlined />}
            style={{ color: '#1890ff' }}
          >
            Chỉnh sửa
          </Button>,

          <Popconfirm
            title={'Bạn chắc chắn muốn xoá chủ đề này'}
            onConfirm={async () => {
              try {
                onDeleteTopicPost(data.id)
              } catch (error) {
              } finally {
              }
            }}
            okText="Xoá"
            cancelText="Quay lại"
            okButtonProps={{
              danger: true,
              type: 'primary',
            }}
          >
            <Button
              type="text"
              size="large"
              icon={<DeleteFilled />}
              style={{ color: 'red' }}
            >
              Xoá chủ đề
            </Button>
          </Popconfirm>,
        ]}
      >
        {ContentView(detail)}
      </Card>
    </div>
  )
}

export default TopicPostDetail
