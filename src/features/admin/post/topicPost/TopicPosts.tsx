/* eslint-disable react-hooks/exhaustive-deps */
import { Affix, Collapse, PageHeader, Space, Table, Tag } from 'antd'
import moment from 'moment'
import 'moment/locale/vi'
import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import reactotron from 'ReactotronConfig'
import styled from 'styled-components'
import { notificationError, notificationSuccess } from 'utils/notification'
import { AddEditTopicPost } from './components/AddEditTopicPost'
import { Header } from './components/Header'
import { createTopic, deleteTopic, getTopic } from './TopicPostApi'
import TopicPostDetail from './TopicPostDetail'

const { Panel } = Collapse

const StyledPanel = styled(Panel)`
  .ant-collapse-content-box {
    /* padding: 0; */
  }
`

const TopicPost = () => {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const [service, setService] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [showAddAccount, setShowAddAccount] = useState(false)
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [currentSelected, setCurrentSelected] = useState({ id: -1 })
  const [params, setParams] = useState<any>({
    search: undefined,
    page: 1,
    limit: 24,
    status: undefined,
    from_date: '',
    to_date: '',
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const getData = async () => {
    setIsLoading(true)
    try {
      const res = await getTopic(params)
      const tableData = res.data.map((item: any, index: number) => {
        return { ...item, key: item.id, stt: index + 1 }
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setService(tableData)
      setPaging(formattedPaging)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  useEffect(() => {
    getData()
    setCurrentSelected({ id: -1 })
  }, [params])

  const createTopicPost = async (data: any, resetFields: any) => {
    setIsLoading(true)
    try {
      const res = await createTopic(data)
      if (res.code === 1) {
        notificationSuccess('Thêm gói chủ đề thành công')
        setShowAddAccount(false)
        setTimeout(() => {
          getData()
        }, 300)
        reactotron.log!(resetFields)
        resetFields()
      }
    } catch (error) {
      console.log(error)
      notificationError('Thêm gói chủ đề thất bại')
    } finally {
      setIsLoading(false)
    }
  }

  const contentView = () => {
    const columns = [
      {
        width: 70,
        title: 'STT',
        dataIndex: 'index',
        key: 'index',
        render: (text: any, record: any, index: any) => (
          <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
            {(paging.current - 1) * paging.pageSize + index + 1}
          </td>
        ),
        ellipsis: true,
      },
      { title: 'Tên chủ đề', dataIndex: 'name', key: 'name' },
      {
        title: 'Thứ tự hiển thị ',
        dataIndex: 'order',
        key: 'order',
      },

      {
        width: '160px',
        title: 'Trạng thái',
        dataIndex: 'status',
        key: 'status',
        render: (value: any) => (
          <Tag color={value === 1 ? 'green' : 'volcano'}>
            {value === 1 ? 'ĐANG HOẠT ĐỘNG' : 'TẠM DỪNG'}
          </Tag>
        ),
      },
      {
        title: 'Ngày tạo',
        dataIndex: 'create_at',
        key: 'create_at',
        render: (value: any) => {
          return <>{moment(value).format('DD-MM-YYYY')}</>
        },
      },
    ]

    return (
      <div
        style={{
          margin: '2px 5px 2px 5px',
        }}
      >
        <Affix>
          <PageHeader
            title="Chủ đề"
            style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}
            extra={[
              <Space>
                <Header
                  setIsCreate={setShowAddAccount}
                  isSearchLoading={isSearchLoading}
                  onSearchSubmit={(searchKey: string) => {
                    setIsSearchLoading(true)
                    setParams({ ...params, search: searchKey.trim(), page: 1 })
                  }}
                  onStatusSubmit={(statusKey: string) => {
                    setParams({
                      ...params,
                      status: statusKey === '' ? undefined : statusKey,
                      page: 1,
                    })
                  }}
                  onDateSubmit={(from_date: string, to_date: string) => {
                    setParams({
                      ...params,
                      from_date: from_date,
                      to_date: to_date,
                      page: 1,
                    })
                  }}
                />
              </Space>,
            ]}
          />
        </Affix>
        <div
          style={{
            backgroundColor: 'white',
            margin: '0px 10px 15px',
            padding: '10px 20px',
          }}
        >
          <p>
            Kết quả lọc: <b>{paging.total}</b>
          </p>
          <Table
            className="table-expanded-custom-no-image"
            scroll={{
              x: 800,
              scrollToFirstRowOnChange: true,
              // y: 'calc(100vh - 340px)',
              y: `calc(${heightWeb}px - 340px)`,
            }}
            bordered
            dataSource={service}
            loading={isLoading}
            columns={columns}
            expandedRowKeys={[currentSelected.id]}
            onRow={r => ({
              onClick: () => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            })}
            expandable={{
              expandedRowRender: (record: any) => (
                <TopicPostDetail
                  //   onUpdateAccount={(newData: any, resetFields: any) => {
                  // updateAcc(newData, resetFields)
                  //   }}
                  onDeleteTopicPost={async (id: any) => {
                    try {
                      await deleteTopic({ id: [id] })
                      notificationSuccess('Xóa chủ đề thành công')
                      getData()
                    } catch (error) {
                      console.log(error)
                    }
                  }}
                  data={record}
                  getData={getData}
                />
              ),
              onExpand: (status: any, r: any) => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            }}
            pagination={{
              ...paging,
              onChange: async (page, pageSize) => {
                setParams({ ...params, page })
              },
            }}
          />
        </div>
        {showAddAccount && (
          <AddEditTopicPost
            visible={showAddAccount}
            onCancel={() => {
              setShowAddAccount(false)
              getData()
            }}
            onCreateNewTopicPost={(newData: any, resetFields: any) => {
              createTopicPost(newData, resetFields)
            }}
            isLoadingButton={isLoading}
          />
        )}
      </div>
    )
  }

  return contentView()
}

export default TopicPost
