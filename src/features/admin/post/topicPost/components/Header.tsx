import { Button, Col, DatePicker, Input, Row, Select } from 'antd'
import ButtonFixed from 'common/components/ButtonFixed'
import React, { useState, useRef } from 'react'
import styled from 'styled-components'
import './css/Header.css'
const { Option } = Select
const { Search } = Input
const { RangePicker } = DatePicker
const Container = styled.div`
  width: 100%;
  padding: 0.5rem;
  background-color: white;
  border-bottom: 1px solid #dddd;
`

type HeaderProps = {
  setIsCreate: any
  onSearchSubmit: (searchKey: string) => void
  onStatusSubmit: (statusKey: any) => void
  onDateSubmit: (from_dateKey: string, to_dateKey: string) => void
  isSearchLoading: boolean
}

export const Header = ({
  setIsCreate,
  onSearchSubmit,
  onStatusSubmit,
  onDateSubmit,
  isSearchLoading,
}: HeaderProps) => {
  const [searchKey, setSearchKey] = useState('')
  const [isTyping, setIsTyping] = useState(false)
  const timeOut = useRef<any>(null)

  return (
    <Row gutter={[16, 16]} justify="end">
      <Col span={7}>
        <Search
          loading={isTyping}
          value={searchKey}
          placeholder="Tìm kiếm tên chủ đề"
          onChange={(e: any) => {
            setSearchKey(e.target.value)
            if (timeOut.current) {
              setIsTyping(true)
              clearTimeout(timeOut.current)
            }
            timeOut.current = setTimeout(() => {
              onSearchSubmit(searchKey)
              setIsTyping(false)
            }, 300)
          }}
        />
      </Col>
      <Col span={5}>
        <Select
          style={{ width: '120px' }}
          placeholder="Trạng thái"
          onChange={value => {
            onStatusSubmit(value)
          }}
        >
          <Option value={''}>Tất cả</Option>
          <Option value="1">Hoạt động</Option>
          <Option value="0">Tạm dừng</Option>
        </Select>
      </Col>
      <Col span={7}>
        <RangePicker
          placeholder={['Từ ngày', 'đến ngày']}
          onChange={(value, dateString) => {
            onDateSubmit(dateString[0], dateString[1])
          }}
        />
      </Col>
      <Col span={4}>
        {/* <Button
          type="primary"
          style={{ minWidth: '70px' }}
          onClick={() => {
            setIsCreate(true)
          }}
        >
          Thêm mới
        </Button> */}
        <ButtonFixed setIsCreate={setIsCreate} text="Thêm mới" type="add" />
      </Col>
    </Row>
  )
}
