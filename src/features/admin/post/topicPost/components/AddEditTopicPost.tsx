import {
  LoadingOutlined,
  PlusOutlined,
  UploadOutlined,
} from '@ant-design/icons'
import { Button, Col, Form, Input, message, Modal, Row } from 'antd'
import ButtonBottomModal from 'common/components/ButtonBottomModal'
import { requestUploadImageCategory } from 'features/admin/product/categoryProduct/CategoryProductApi'
import { useState } from 'react'
import createFormDataImage from 'utils/createFormDataImage'
import './css/AddEditTopicPost.css'
import { Upload } from 'antd'
// import ImgCrop from 'antd-img-crop'

type Props = {
  visible: boolean
  onCancel?: any
  data?: any
  onCreateNewTopicPost?: any
  onUpdateTopicPost?: any
  isLoadingButton: boolean
}

function convertDataToFrom(data: any) {
  if (!data) {
    return {
      is_default: null,
      minus: null,
      name: null,
      pakage_category_id: null,
      price: null,
    }
  } else {
    return {
      data,
    }
  }
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
}

export const AddEditTopicPost = ({
  visible,
  onCancel,
  data,
  onCreateNewTopicPost,
  onUpdateTopicPost,
  isLoadingButton,
}: Props) => {
  const [form] = Form.useForm()
  const { TextArea } = Input
  const initialValue = convertDataToFrom(data)
  const onFinish = async (values: any) => {
    if (values?.icon_url?.fileList) {
      var resUploadImage = []
      var last_element = []

      if (values.icon_url.fileList.length > 1) {
        last_element = [values.icon_url.fileList.slice(-1)[0]]
      } else {
        last_element = values.icon_url.fileList
      }
      const dataImage = await createFormDataImage(last_element)
      const payloadImage = {
        type: 0,
        data: dataImage,
      }
      resUploadImage = await requestUploadImageCategory(payloadImage)
      // console.log('resUploadImage', resUploadImage)
    }
    let newData

    try {
      values.description = values.description.trim()
      if (!data) {
        console.log('old', resUploadImage.data.path)
        newData = {
          ...values,
          icon_url: resUploadImage.data.path,
        }
        onCreateNewTopicPost(newData, form.resetFields)
      } else {
        newData = {
          ...values,
          id: data.id,
          icon_url: resUploadImage.data.path,
        }
        console.log('new', newData, form.resetFields)

        onUpdateTopicPost(newData, form.resetFields)
      }
    } catch (error) {
      console.log(error)
    }
  }

  const createFormData = (dataObject: any) => {
    let formdata = new FormData()
    const keys = Object.keys(dataObject)
    if (!dataObject.file) {
      message.error('Bạn chưa chọn file!')
      return null
    } else {
      keys.forEach(key => {
        formdata.append(key, dataObject[key])
      })
      return formdata
    }
  }
  // const handleImportFile = async () => {
  //   try {
  //     const form = createFormData({
  //       file: fileSelected?.originFileObj,
  //     })
  //     if (form) {
  //       // const rest = await ImportFilePath(form)
  //       // if (rest.status === 1) {
  //       //   message.success('Import file thành công')
  //       //   await setFileSelected({})
  //       //   window.location.reload()
  //       // } else {
  //       //   message.error('Import file thất bại!')
  //       // }
  //     }
  //   } catch (error) {
  //     message.error(`${error}`)
  //   }
  // }
  const [upload, setUpload] = useState({
    loading: false,
    imageUrl: data?.icon_url || '',
  })

  function getBase64(img: any, callback: any) {
    const reader = new FileReader()
    reader.addEventListener('load', () => callback(reader.result))
    reader.readAsDataURL(img)
  }
  function beforeUpload(file: any) {
    const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png'
    if (!isJpgOrPng) {
      message.error('Xảy ra lỗi! Bạn chỉ có thể upload ảnh có dạng JPG/PNG!')
    }
    const isLt2M = file.size / 1024 / 1024 < 2
    if (!isLt2M) {
      message.error('Cho phép ảnh có dung lượng tối đa là 2MB')
    }
    return isJpgOrPng && isLt2M
  }
  const handleChange = (info: any) => {
    if (info.file.status === 'uploading') {
      setUpload({
        imageUrl: '',
        loading: true,
      })
      return
    }

    if (info.file.status === 'done' || info.file.status === 'error') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, (imageUrl: any) =>
        setUpload({
          imageUrl: imageUrl,
          loading: false,
        })
      )
    }
  }

  const uploadButton = (
    <div>
      {upload.loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  )
  console.log('data', data)

  return (
    <Modal
      onCancel={() => {
        form.resetFields()
        onCancel()
      }}
      maskClosable={false}
      footer={null}
      title={data ? 'Sửa chủ đề ' : 'Thêm chủ đề '}
      visible={visible}
    >
      <Form
        {...formItemLayout}
        form={form}
        name="add"
        labelAlign="left"
        onFinish={(values: any) => onFinish(values)}
        initialValues={initialValue.data}
        scrollToFirstError
      >
        <Form.Item
          name="name"
          label="Tên chủ đề"
          rules={[
            {
              type: 'string',
              message: 'Nhập tên chủ đề',
            },
            {
              required: true,
              message: 'Vui lòng nhập tên chủ đề!',
            },
            {
              whitespace: true,
              message: 'Vui lòng không nhập khoảng trắng!',
            },
            {
              min: 3,
              max: 50,
              message: 'Vui lòng nhập từ 3 đến 50 ký tự!',
            },
            // {
            //   message: 'Vui lòng không nhập kí tự đặc biệt',
            //   validator: (_, value) => {
            //     const reg = /[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêếìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵýỷỹ\s]/u
            //     if (!reg.test(value)) {
            //       return Promise.resolve()
            //     }
            //     return Promise.reject()
            //   },
            // },
          ]}
        >
          <Input
            placeholder="Nhập tên chủ đề"
            onBlur={() => {
              form.setFieldsValue({
                name: form.getFieldValue('name')?.trim(),
              })
            }}
          />
        </Form.Item>

        <Form.Item
          name="order"
          label="STT hiển thị"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập STT hiển thị',
            },
            {
              message: 'Vui lòng nhập chỉ nhập số',
              validator: (_, value) => {
                const reg = /^\d+$/
                if (reg.test(value)) {
                  return Promise.resolve()
                }
                return Promise.reject()
              },
            },
          ]}
        >
          <Input placeholder="Nhập STT hiển thị" />
        </Form.Item>
        <Form.Item
          name="description"
          label="Mô tả"
          rules={[
            {
              whitespace: true,
              message: 'Vui lòng không nhập khoảng trắng!',
            },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (value) {
                  if (value.length < 256) {
                    return Promise.resolve()
                  }
                  return Promise.reject('Nhập mô tả dưới 256 ký tự')
                }
                return Promise.reject(new Error('Vui lòng nhập mô tả'))
              },
            }),
          ]}
        >
          <TextArea
            placeholder="Nhập mô tả chủ đề"
            id="text-area-topicpost"
            style={{ width: '100%' }}
            showCount
            maxLength={256}
          />
        </Form.Item>
        <Form.Item
          label="Icon danh mục"
          name="icon_url"
          rules={[
            {
              required: true,
              message: 'Vui lòng chọn icon!',
            },
          ]}
        >
          <Upload
            name="icon_url"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            beforeUpload={beforeUpload}
            onChange={handleChange}
          >
            {upload.imageUrl ? (
              <img
                src={upload.imageUrl === '' ? data?.icon_url : upload.imageUrl}
                alt="avatar"
                style={{ width: '100%', objectFit: 'contain', height: '100%' }}
              />
            ) : (
              uploadButton
            )}
          </Upload>
        </Form.Item>
        <ButtonBottomModal
          isLoadingButton={isLoadingButton}
          onCancel={onCancel}
          text={data ? 'Cập nhật' : 'Thêm chủ đề'}
        />
      </Form>
    </Modal>
  )
}
