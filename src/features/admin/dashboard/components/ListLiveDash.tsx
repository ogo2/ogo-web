import { LikeOutlined, MessageOutlined, EyeOutlined } from '@ant-design/icons'
import { message, List, Avatar, Space, Spin, Typography } from 'antd'
import { ADMIN_ROUTER_PATH } from 'common/config'
import moment from 'moment'
import React, { useState } from 'react'
import history from 'utils/history'
// import InfiniteScroll from 'react-infinite-scroller'
import './css/Dashboash.css'

type Props = {
  list: Array<any> | undefined
}

export const ListLive = ({ list }: Props) => {
  const { Text } = Typography
  const EllipsisMiddle = ({ suffixCount, children }: any) => {
    const start = children.slice(0, children.length - suffixCount).trim()
    const suffix = children.slice(-suffixCount).trim()
    return (
      <Text style={{ maxWidth: '100%' }} ellipsis={{ suffix }}>
        {start}
      </Text>
    )
  }

  const IconText = ({ icon, text }: any) => (
    <Space>
      {React.createElement(icon)}
      {text}
    </Space>
  )
  return (
    <div className="list-live">
      <span style={{ fontWeight: 600, fontSize: '18px' }}>Live stream</span>
      <div className="divider"></div>
      <div
        style={{
          height: '60vh',
          overflowX: 'hidden',
          overflowY: 'scroll',
          display: 'block',
        }}
      >
        <List
          dataSource={list}
          renderItem={item => (
            <List.Item
              onDoubleClick={() => {
                history.push({
                  pathname: `${ADMIN_ROUTER_PATH.LIVE_STREAM_DETAIL}/${item.id}`,
                  state: item,
                })
              }}
              key={item.id}
              actions={[
                <IconText
                  icon={LikeOutlined}
                  text={item.count_reaction}
                  key="like"
                />,
                <IconText
                  icon={EyeOutlined}
                  text={item.count_viewed}
                  key="viewed"
                />,
                <IconText
                  icon={MessageOutlined}
                  text={item.count_comment}
                  key="comment"
                />,
              ]}
            >
              <List.Item.Meta
                avatar={<Avatar src={item.cover_image_url} size={100} />}
                title={
                  <EllipsisMiddle suffixCount={12}>{item.title}</EllipsisMiddle>
                }
                description={moment(item.update_at).format('HH:mm DD/MM/YYYY')}
                // description={moment(item.update_at).format('HH:mm DD/MM/YYYY')}
              />
            </List.Item>
          )}
        >
          {/* {loading && hasMore && (
            <div className="demo-loading-container">
              <Spin />
            </div>
          )} */}
        </List>
      </div>
    </div>
  )
}
