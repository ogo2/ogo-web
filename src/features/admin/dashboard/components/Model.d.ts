export interface ListLiveStreamInterface {
  cover_image_url: string
  id: number
  user_id: number
  shop_id: number
  title: string
  token_publisher?: null
  record_uid?: null
  record_sid?: null
  record_resourceid?: null
  minutes_used_before_livestream: null
  broadcast_id: string
  stream_id: string
  cdn: CdnInterface
  livestream_record_url?: null
  count_viewed: number
  count_reaction: number
  count_comment: number
  start_at: Date
  finish_at: Date
  expire_at: Date
  ping_at: string
  type: number
  status: number
  is_active: number
  create_at: Date
  update_at: Date
  Shop: ShopInterface
  LivestreamLayouts: Array
}
export interface CdnInterface {
  frameRate: string
  resolution: string
  ingestionInfo: {
    streamName: string
    ingestionAddress: string
    rtmpsIngestionAddress: string
    backupIngestionAddress: string
    rtmpsBackupIngestionAddress: string
  }
  ingestionType: string
}
export interface ShopInterface {
  profile_picture_url: sring
  id: number
  name: string
  phone: string
  pancake_shop_key?: null
  pancake_shop_id?: null
  stream_minutes_available: number
  google_tokens: GoogleTokenInterface
  agora_project_id?: null
  agora_app_id?: null
  agora_app_certificate?: null
  email: string
  livestream_enable: number
  status: number
  is_active: number
  create_at: string
  update_at: string
}

export interface GoogleTokenInterface {
     email: string
     scope: string
     id_token: string
     token_type: string
     expiry_date: number
     access_token: string
     refresh_token: string
   }
