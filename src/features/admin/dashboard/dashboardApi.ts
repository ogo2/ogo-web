import { ApiClient } from 'services/ApiService'

export const getDataOverviews = (payload: any) =>
  ApiClient.get('admin/overview', payload)
