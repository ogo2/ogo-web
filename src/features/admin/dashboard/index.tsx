import { PageHeader, Row, Space, Divider, Card, Col, Spin } from 'antd'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import Cards from './components/Cards'
import { ChartLive } from './components/ChartLive'
import { Header } from './components/Header'
import { ListLive } from './components/ListLiveDash'
import { OrderDashboard } from './components/OrderDashboard'
import moment from 'moment'
import { getDataOverviews } from './dashboardApi'

export default function DashBoard() {
  const userState = useSelector((state: any) => state.authReducer)?.userInfo
  const [fromDaytoDay, setFromDaytoDay] = useState([
    moment().subtract(14, 'days').format('YYYY-MM-DD'),
    moment().format('YYYY-MM-DD'),
  ])
  const [dataCard, setDataCard] = useState({
    count_package: 0,
    count_turnover: '',
    count_purchased_gift: 0,
    count_livestream: 0,
    count_shop: 0,
  })
  const [order, setOrder] = useState({
    count_pending: 0,
    count_ship: 0,
    count_cancel: 0,
    count_success: 0,
    count_confirmed: 0,
  })
  const [listLiveStream, setListLiveStream] = useState<Array<any>>([])
  const [dataDashboardOrder, setDataDashboardOrder] = useState<any>({})
  const [sumLiveStream, setSumLiveStream] = useState<any>({})
  const [dataDashboardLive, setDataDashboardLive] = useState()
  const [view, setView] = useState<number>(0)
  const [stateLoading, setStateLoading]= useState(false)
  // const [test, setTest] = useState<any>(0)


  useEffect(() => {
    getData()
  }, [fromDaytoDay])

  const getData = async () => {
    setStateLoading(true)
    try {
      const res = await getDataOverviews({
        from_date: fromDaytoDay[0],
        to_date: fromDaytoDay[1],
      })
      setDataCard({
        count_package: res.data.count_package,
        count_turnover: res.data.count_turnover,
        count_purchased_gift: res.data.count_purchased_gift,
        count_livestream: res.data.count_livestream,
        count_shop: res.data.count_shop,
      })
      setOrder(res.data.order)
      setListLiveStream(res.data.list_livestream)
      setDataDashboardOrder(res.data.chart_order)
      setSumLiveStream(res.data.livestream[0])
      setDataDashboardLive(res.data.chart_livestream)
      setView(parseInt(res.data.livestream_view))
      // setTest(res.data.chart_livestream.datasets[0].label)
    } catch (error) {
      console.log(error)
    }finally{
      setStateLoading(false)
    }
  }
  return (
      <div>
        {userState?.df_type_user_id === 1 && (
          <div style={{ margin: '5px 10px 15px' }}>
            <PageHeader
              style={{ backgroundColor: 'white', marginBottom: '15px' }}
              title="Tổng quan"
              extra={[
                <Space>
                  <Header
                    fromDaytoDay={fromDaytoDay}
                    dateOnSubmit={(x: string, y: string) => {
                      setFromDaytoDay([x, y])
                    }}
                  />
                </Space>,
              ]}
            />
            {/* test:{test} */}
            <Spin spinning={stateLoading}>
            <div style={{ marginBottom: '15px' }}>
              <Cards
                count_package={dataCard.count_package}
                count_turnover={dataCard.count_turnover}
                count_purchased_gift={dataCard.count_purchased_gift}
                count_livestream={dataCard.count_livestream}
                count_shop={dataCard.count_shop}
              />
            </div>
              <Row style={{ marginBottom: '15px' }}>
                <OrderDashboard data={dataDashboardOrder} order={order} />
                <ListLive list={listLiveStream} />
              </Row>
            </Spin>
            <div
              style={{
                marginBottom: '15px',
                backgroundColor: 'white',
                borderRadius: '5px',
              }}
            >
              <ChartLive data={dataDashboardLive} sumLiveStream={sumLiveStream} view={view}/>
              {/* <ChartLives /> */}
            </div>
          </div>
        )}
      </div>
  )
}
