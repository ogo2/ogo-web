import { DownloadOutlined } from '@ant-design/icons'
import {
  Button,
  PageHeader,
  Space,
  Row,
  Col,
  Select,
  Input,
  DatePicker,
  message,
} from 'antd'
import moment from 'moment'
import { useEffect, useRef, useState } from 'react'
import ExportCsv from 'utils/ExportCSV'
import { formatPrice } from 'utils/ruleForm'
import { allCustomer, getCustomerList, getListProvince } from '../CustomerApi'
const { Option } = Select
const { Search } = Input
const { RangePicker } = DatePicker

type Props = {
  onSearchSubmit: (searchKey: string) => void
  onProvinceSubmit: (statusKey: any) => void
  onDateSubmit: (from_dateKey: string, to_dateKey: string) => void
  // onExportDataToExcel: () => void
  // isLoadingBtnExportData: boolean
  params: any
}
export default function HeaderCustomer({
  onSearchSubmit,
  onProvinceSubmit,
  onDateSubmit,
  params,
}: // onExportDataToExcel,
// isLoadingBtnExportData,
Props) {
  const [listProvince, setListProvince] = useState<Array<any>>([])
  const [paramsExport, setParamsExport] = useState({
    search: '',
    province_id: undefined,
    page: 1,
    from_date: '',
    to_date: '',
  })
  const [isLoadingBtnExportData, setLoadingBtnExportData] = useState<boolean>(
    false
  )
  useEffect(() => {
    getProvince()
  }, [])

  const getProvince = async () => {
    try {
      const res_province = await getListProvince()
      setListProvince(res_province.data.rows)
    } catch (error) {}
  }
  const [dataExport, setDataExport] = useState<any>([])
  const onExportDataToExcel = async (fn: any) => {
    try {
      setLoadingBtnExportData(true)
      const dataListCustomerNotPagging: any = await allCustomer(params)
      const dataExport = dataListCustomerNotPagging?.data?.map(
        (o: any, i: number) => {
          return {
            STT: i + 1,
            'Tên khách hàng': o.name || '',
            'Điện thoại': o.user_name || '',
            'Tỉnh thành phố':
              o.UserAddresses[0] == null
                ? '0đ'
                : o.UserAddresses[0]?.province_name,
            'Doanh số bán hàng':
              o.total_price_success == 0
                ? '0đ'
                : formatPrice(o.total_price_success) + 'đ',
            'Doanh số thực tế':
              o.total_price_pending == 0
                ? '0đ'
                : formatPrice(o.total_price_pending) + 'đ',
            'Ngày tạo': moment(o.create_at).format('DD-MM-YYYY'),
          }
        }
      )
      // const data: any = JSON.parse(JSON.stringify(dataExport))
      // const fileName: string = 'Danh sách khách hàng'
      // const exportType: any = 'csv'
      // const fields: any = {}
      // exportFromJSON({ data, fileName, exportType, fields })
      setDataExport(dataExport)
      fn()
    } catch (error) {
      message.error(
        'Đã có lỗi trong quá trình export dữ liệu, vui lòng thử lại sau!'
      )
    } finally {
      setLoadingBtnExportData(false)
    }
  }
  return (
    <div>
      <div
        style={{
          backgroundColor: 'white',
          margin: '10px 10px',
        }}
      >
        <PageHeader
          title="Danh sách khách hàng "
          extra={[
            <Space style={{ width: '100%' }}>
              {/* <Button
                type="primary"
                loading={isLoadingBtnExportData}
                onClick={() => {
                  onExportDataToExcel()
                }}
              >
                Export
              </Button> */}
              <ExportCsv
                loading={isLoadingBtnExportData}
                onClick={fn => onExportDataToExcel(fn)}
                sheetName={['CustomertList']}
                sheets={{
                  CustomertList: ExportCsv.getSheets(dataExport),
                }}
                fileName="Danh sách khách hàng"
              >
                <DownloadOutlined /> &nbsp; Export
              </ExportCsv>
            </Space>,
          ]}
        />
      </div>
      <div
        style={{
          backgroundColor: 'white',
          margin: '10px 10px',
          padding: '15px 10px',
        }}
      >
        <Row gutter={16}>
          <Col span="12">
            <Search
              className="input-search_account"
              style={{ width: '100%' }}
              placeholder="Tìm kiếm tên, số điện thoại"
              onSearch={(e: any) => {
                onSearchSubmit(e.target.value)
              }}
              onChange={(e: any) => {
                onSearchSubmit(e.target.value)
                setParamsExport({ ...paramsExport, search: e.target.value })
              }}
            />
          </Col>
          {/* <Col span="6">
            <Select
              allowClear
              showSearch
              optionFilterProp="children"
              className="select-type_account"
              placeholder="Tỉnh thành phố"
              style={{ width: '100%' }}
              onChange={(value: number) => {
                onProvinceSubmit(value)
                setParamsExport({ ...paramsExport })
              }}
            >
              {listProvince.map((item: any) => {
                return <Option value={item.id}>{item.name}</Option>
              })}
            </Select>
          </Col> */}
          <Col span="5">
            <RangePicker
              // className="rangerpicker_topic"
              style={{ width: '100%' }}
              placeholder={['Từ ngày', 'đến ngày']}
              onChange={(value, dateString) => {
                onDateSubmit(dateString[0], dateString[1])
                setParamsExport({
                  ...paramsExport,
                  to_date: dateString[0],
                  from_date: dateString[1],
                })
              }}
            />
          </Col>
        </Row>
      </div>
    </div>
  )
}
