import { message } from 'antd'
import React, { useState } from 'react'
import { formatPrice } from 'utils/ruleForm'
import HeaderCustomer from './components/HeaderCustomer'
import ListCustomer from './components/ListCustomer'
import { getCustomerList } from './CustomerApi'
import exportFromJSON from 'export-from-json'
import moment from 'moment'

export default function Customer() {
  return (
    <div>
      {/* <HeaderCustomer
        // isLoadingBtnExportData={isLoadingBtnExportData}
        // onExportDataToExcel={onExportDataToExcel}
      /> */}
      <ListCustomer />
    </div>
  )
}
