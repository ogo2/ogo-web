import { ApiClient } from 'services/ApiService'

export const getCustomerList = (payload: any) =>
  ApiClient.get('/customer', payload)
export const getListProvince = () => ApiClient.get('/default/province')
export const getListOrder = (payload: any) =>
  ApiClient.get('/customer/list-order', payload)
export const getListPointHistory = (payload: any) =>
  ApiClient.get('/customer/list-point-history', payload)
export const deleteCustomer = (payload: any) =>
  ApiClient.delete(`/customer/${payload}`)
export const allCustomer = (payload: any) =>
  ApiClient.get(`/customer/all`, payload)
