import { Image, PageHeader, Space, Table, Typography } from 'antd'
import { ADMIN_ROUTER_PATH } from 'common/config'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import { msToTime } from 'utils/funcHelper'
import Filter from './components/Filter'
import { getListLiveStream, getShopList } from './LiveStreamApi'

const { Paragraph } = Typography

export default function LiveStream() {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const history = useHistory()
  const [listLive, setListLive] = useState<Array<any>>([])
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [params, setParams] = useState({
    search: '',
    status: '',
    user_id: '',
    shop_id: '',
    from_date: '',
    to_date: '',
    page: 1,
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const getData = async () => {
    setIsLoading(true)
    try {
      const response = await getListLiveStream(params)
      const data = response.data.map((item: any) => {
        return { ...item, key: item.id }
      })
      setListLive(data)
      setPaging({
        total: response.paging.totalItemCount,
        current: response.paging.page,
        pageSize: response.paging.limit,
      })
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  const getShop = async () => {
    try {
      const response = await getShopList({})
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getShop()
  }, [])

  useEffect(() => {
    getData()
  }, [params])

  const columns = [
    {
      width: 70,
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
      render: (text: any, record: any, index: any) => (
        <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
          {(paging.current - 1) * paging.pageSize + index + 1}
        </td>
      ),
      ellipsis: true,
    },
    {
      title: 'Ảnh',
      dataIndex: 'cover_image_url',
      key: 'cover_image_url',
      width: 70,
      render: (value: any) => {
        return (
          <td className="icon-eye-antd-custom">
            <Image
              style={{ objectFit: 'cover', width: '35px', height: '35px' }}
              src={value}
              onClick={e => {
                e.stopPropagation()
              }}
            />
          </td>
        )
      },
    },
    {
      title: 'Tên gian hàng',
      dataIndex: '',
      render: (value: any) => <td>{value.Shop.name}</td>,
    },
    {
      title: 'Mô tả',
      // width: 500,
      dataIndex: 'title',
      render: (value: string) => <td>{value}</td>,
    },
    {
      title: 'Lượt xem',
      width: 100,
      dataIndex: 'count_viewed',
    },
    {
      title: 'Lượt thích',
      width: 110,
      dataIndex: 'count_reaction',
    },
    {
      title: 'Bình luận',
      width: 100,
      dataIndex: 'count_comment',
    },
    {
      title: 'Thời lượng',
      dataIndex: '',
      width: 130,
      render: (value: any) => (
        <td>
          {msToTime(
            moment(value.finish_at).unix() - moment(value.start_at).unix()
          )}
        </td>
      ),
    },
    {
      title: 'Ngày livestream',
      dataIndex: 'create_at',
      width: 150,
      render: (value: string) => <td>{moment(value).format('DD-MM-YYYY')}</td>,
    },
    // {
    //   title: 'Ngày kết thúc',
    //   dataIndex: 'finish_at',
    //   render: (value: string) => <td>{moment(value).format('DD-MM-YYYY')}</td>,
    // },
  ]

  return (
    <>
      <div
        style={{
          backgroundColor: 'white',
          margin: '5px 10px 15px',
          // padding: '10px',
        }}
      >
        <PageHeader
          className="site-page-header"
          title="Livestream"
          extra={[
            <Space>
              <Filter
                isSearchLoading={isSearchLoading}
                onSearchSubmit={(searchKey: string) => {
                  setIsSearchLoading(true)
                  setParams({ ...params, search: searchKey, page: 1 })
                }}
                onDateSubmit={(from_date: string, to_date: string) => {
                  setParams({
                    ...params,
                    from_date: from_date,
                    to_date: to_date,
                    page: 1,
                  })
                }}
                onShopSubmit={(shopKey: string) => {
                  setParams({ ...params, shop_id: shopKey, page: 1 })
                }}
              />
            </Space>,
          ]}
        />
      </div>
      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <p>
          Kết quả lọc: <b>{paging.total}</b>
        </p>
        <Table
          className="table-expanded-custom"
          scroll={{
            x: 900,
            scrollToFirstRowOnChange: true,
            // y: 'calc(100vh - 340px)',
            y: `calc(${heightWeb}px - 340px)`,
          }}
          bordered
          dataSource={listLive}
          loading={isLoading}
          columns={columns}
          onRow={(record, rowIndex) => ({
            onDoubleClick: () => {
              history.push({
                pathname: `${ADMIN_ROUTER_PATH.LIVE_STREAM_DETAIL}/${record.id}`,
                state: record,
              })
            },
          })}
          pagination={{
            ...paging,
            showSizeChanger: false,
            onChange: async (page, pageSize) => {
              setParams({ ...params, page })
            },
          }}
        />
      </div>
    </>
  )
}
