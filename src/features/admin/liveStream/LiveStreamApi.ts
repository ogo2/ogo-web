import { ApiClient } from 'services/ApiService'

export const getListLiveStream = (payload: {
  search?: string
  status?: string
  user_id?: string
  shop_id?: string
  from_date?: string
  to_date?: string
  page: number
}) => {
  let param: any = { page: payload.page }
  if (payload.search) {
    param.search = payload.search
  }
  if (payload.status) {
    param.status = parseInt(payload.status)
  }
  if (payload.user_id) {
    param.user_id = parseInt(payload.user_id)
  }
  if (payload.shop_id) {
    param.shop_id = parseInt(payload.shop_id)
  }
  if (payload.from_date) {
    param.from_date = payload.from_date
  }
  if (payload.to_date) {
    param.to_date = payload.to_date
  }
  return ApiClient.get('/admin/livestream', param)
}

export const getDetail = (payload: number) =>
  ApiClient.get(`/admin/livestream/${payload}/detail`)

export const deleteLive = (payload: number) =>
  ApiClient.delete(`/admin/livestream/${payload}`)

export const getShopList = (payload: {}) =>
  ApiClient.get('/admin/shop', payload)

export const getCommentList = (payload: any) =>
  ApiClient.get('/comment', payload)

export const getAllComment = (payload: any) =>
  ApiClient.get('/comment/all', payload)
