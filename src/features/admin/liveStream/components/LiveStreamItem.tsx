import {
  ClockCircleOutlined,
  CommentOutlined,
  EyeOutlined,
  LikeFilled,
} from '@ant-design/icons'
import { Col, Row, Typography } from 'antd'
import { ADMIN_ROUTER_PATH } from 'common/config'
import moment from 'moment'
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import { msToTime } from 'utils/funcHelper'
import '../css/LiveStreamItem.css'

type Props = {
  dataLive: any
}

export default function LiveStreamItem({ dataLive }: Props) {
  const history = useHistory()

  return (
    <Col
      xxl={{ span: 8 }}
      md={{ span: 12 }}
      xs={{ span: 24 }}
      style={{ cursor: 'pointer' }}
      onClick={() => {
        history.push({
          pathname: `${ADMIN_ROUTER_PATH.LIVE_STREAM_DETAIL}/${dataLive.id}`,
        })
      }}
    >
      <div className="container-livestream-item">
        <div className="screen-livestream">
          <img
            style={{
              // minWidth: '100%',
              // maxHeight: '100%',
              width: '100%',
              height: '100%',
              // display: 'block',
              objectFit: 'cover',
            }}
            src={dataLive.cover_image_url}
            alt="Thumbnail live stream"
          />
          {/* <div style={{ backgroundImage: url(dataLive.cover_image_url) }}></div> */}
        </div>
        <div className="infor-livestream">
          {/* <div className="title-livestram">{dataLive.title}</div> */}
          <div className="title-livestram">
            <Typography.Paragraph
              ellipsis={
                true
                  ? {
                      rows: 1,
                      expandable: true,
                      symbol: ' ',
                    }
                  : false
              }
            >
              {dataLive.title}
            </Typography.Paragraph>
          </div>
          <div className="create-at-livestream">
            <ClockCircleOutlined style={{ marginRight: '5px' }} />
            {moment(dataLive.create_at).format('HH:mm DD-MM-YYYY')}
          </div>
          <div className="infor-detail-livestream">
            <Row>
              <Col
                span={12}
                className="item-small-livestream"
                style={{ color: '#1276E0 ' }}
              >
                <LikeFilled className="item-icon" /> {dataLive.count_reaction}
              </Col>
              <Col
                span={12}
                className="item-small-livestream"
                style={{ color: '#E38E20 ' }}
              >
                <EyeOutlined className="item-icon" /> {dataLive.count_viewed}
              </Col>
              <Col
                span={12}
                className="item-small-livestream"
                style={{ color: '#7A7A7A ' }}
              >
                <ClockCircleOutlined className="item-icon" />
                {msToTime(
                  moment(dataLive.finish_at).unix() -
                    moment(dataLive.start_at).unix()
                )}
              </Col>
              <Col
                span={12}
                className="item-small-livestream"
                style={{ color: '#E33720 ' }}
              >
                <CommentOutlined className="item-icon" />
                {dataLive.count_comment}
              </Col>
            </Row>
          </div>
        </div>
      </div>
    </Col>
  )
}
