import React, { useEffect, useState } from 'react'
import { Col, Row, DatePicker, Select } from 'antd'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import { getShopList } from '../LiveStreamApi'

const { RangePicker } = DatePicker
const { Option } = Select

type Props = {
  isSearchLoading: boolean
  onSearchSubmit: (key: string) => void
  onDateSubmit: (from_dateKey: string, to_dateKey: string) => void
  onShopSubmit: (key: string) => void
}

export default function Filter({
  isSearchLoading,
  onSearchSubmit,
  onDateSubmit,
  onShopSubmit,
}: Props) {
  const [listShop, setListShop] = useState([])
  const [params, setParams] = useState({ page: 1, search: '' })
  const [isLoading, setIsLoading] = useState<boolean>(false)

  const getListShop = async () => {
    setIsLoading(true)
    try {
      const res = await getShopList(params)
      setListShop(res.data)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    getListShop()
  }, [params])

  return (
    <Row justify="end" gutter={[16, 16]}>
      <Col span={8}>
        <TypingAutoSearch
          onSearchSubmit={(key: string) => {
            onSearchSubmit(key.trim())
          }}
          isSearchLoading={isSearchLoading}
          placeholder="Mô tả livestream ..."
        />
      </Col>
      <Col span={7}>
        <Select
          allowClear
          style={{ minWidth: '200px' }}
          optionFilterProp="children"
          loading={isLoading}
          placeholder="Gian hàng"
          onChange={(value: any) => {
            onShopSubmit(value)
          }}
          showSearch
          onSearch={(value: string) => setParams({ ...params, search: value })}
        >
          {listShop.map((item: any, index: number) => {
            return (
              <Option key={index} value={item.id}>
                {item.name}
              </Option>
            )
          })}
        </Select>
      </Col>
      <Col span={8}>
        <RangePicker
          placeholder={['Từ ngày', 'Đến ngày']}
          onChange={(value, dateString) => {
            onDateSubmit(dateString[0], dateString[1])
          }}
        />
      </Col>
    </Row>
  )
}
