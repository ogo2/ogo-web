import {
  CloseCircleOutlined,
  DownloadOutlined,
  SaveOutlined,
  UserOutlined,
} from '@ant-design/icons'
import {
  Avatar,
  Button,
  Col,
  Comment,
  Descriptions,
  Empty,
  message,
  PageHeader,
  Pagination,
  Popconfirm,
  Row,
  Space,
  Spin,
} from 'antd'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import { ADMIN_ROUTER_PATH } from 'common/config'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { msToTime } from 'utils/funcHelper'
import './css/Comment.css'
import {
  deleteLive,
  getAllComment,
  getCommentList,
  getDetail,
} from './LiveStreamApi'
import exportFromJSON from 'export-from-json'
import ExportCsv from 'utils/ExportCSV'

export default function LiveStreamDetail() {
  const params: any = useParams()
  console.log('params', params.id)

  const history = useHistory()
  const [data, setData] = useState<any>()
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [isLoadingComment, setIsLoadingComment] = useState<boolean>(false)
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [commentList, setCommentList] = useState([])
  // const [paramExport, setParamExport] = useState({
  //   search: '',
  //       livestream_id: params.id,
  // })
  const [paramComment, setParamComment] = useState({
    search: '',
    livestream_id: params.id,
    page: 1,
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const getLiveStreamDetail = async () => {
    setIsLoading(true)
    try {
      const res = await getDetail(params.id)
      setData(res.data)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  const getComment = async () => {
    setIsLoadingComment(true)
    try {
      const response = await getCommentList(paramComment)
      setCommentList(response.data)
      setPaging({
        total: response.paging.totalItemCount,
        current: response.paging.page,
        pageSize: response.paging.limit,
      })
    } catch (error) {
      console.log(error)
    } finally {
      setIsSearchLoading(false)
      setIsLoadingComment(false)
    }
  }

  useEffect(() => {
    getComment()
  }, [paramComment])

  useEffect(() => {
    getLiveStreamDetail()
  }, [])

  // const exportComment = () => {
  //   const dataExport = commentList.map((item: any, index: number) => ({
  //     STT: index + 1,
  //     'Người bình luận': item.User.name,
  //     'Nội dung bình luận': item.content,
  //     'Thời gian bình luận': msToTime(
  //       moment(item.create_at).unix() - moment(data?.start_at).unix()
  //     ),
  //   }))
  //   const fileName: string = 'Danh sách bình luận'
  //   const exportType: any = exportFromJSON.types.csv
  //   exportFromJSON({ data: dataExport, fileName, exportType })
  // }

  const [dataExport, setDataExport] = useState<any>([])
  const [loadingExport, setLoadingExport] = useState<boolean>(false)

  const onExport = async (fn: any) => {
    console.log('data export')
    try {
      setLoadingExport(true)
      const response = await getAllComment(
        //   {
        //   search: '',
        //   livestream_id: params.id,
        // }
        paramComment
      )
      console.log('data export:', response)
      const dataExport = response.data.map((item: any, index: number) => ({
        STT: index + 1,
        'Người bình luận': item.User.name,
        'Nội dung bình luận': item.content,
        'Thời gian bình luận': msToTime(
          moment(item.create_at).unix() - moment(data?.start_at).unix()
        ),
        'Số điện thoại:': item.User.phone,
        'Địa chỉ:':
          item.User?.UserAddresses[0]?.DFWard?.name +
          ' / ' +
          item.User?.UserAddresses[0]?.DFDistrict?.name +
          ' / ' +
          item.User?.UserAddresses[0]?.DFProvince?.name,
      }))
      setDataExport(dataExport)
      fn()
    } catch (error) {
      console.log(error)
    } finally {
      setLoadingExport(false)
    }
  }

  return (
    <>
      <PageHeader
        style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}
        title="Chi tiết livestream"
        onBack={() => {
          history.goBack()
        }}
        extra={[
          <Space>
            <Popconfirm
              placement="bottomRight"
              title="Bạn chắc chắn muốn xoá livestream này!"
              onConfirm={async () => {
                try {
                  await deleteLive(params.id)
                  history.push(ADMIN_ROUTER_PATH.LIVE_STREAM)
                  message.success('Xoá thành công livestream')
                } catch (error) {
                  console.log(error)
                }
              }}
              okText="Xoá"
              cancelText="Quay lại"
            >
              <Button type="primary" danger>
                <CloseCircleOutlined />
                Xoá
              </Button>
            </Popconfirm>

            <ExportCsv
              loading={loadingExport}
              onClick={fn => onExport(fn)}
              sheetName={['CommentList']}
              sheets={{
                CommentList: ExportCsv.getSheets(dataExport),
              }}
              fileName="CommentListAdmin"
            >
              <DownloadOutlined /> &nbsp; Export bình luận
            </ExportCsv>
          </Space>,
        ]}
      />

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '20px 20px 0',
        }}
      >
        <Spin spinning={isLoading}>
          <Descriptions title="THÔNG TIN LIVESTREAM" column={4}>
            <Descriptions.Item label="Gian hàng">
              {data?.Shop.name}
            </Descriptions.Item>
            <Descriptions.Item label="Lượt xem">
              {data?.count_viewed}
            </Descriptions.Item>
            <Descriptions.Item label="Thời lượng">
              {msToTime(
                moment(data?.finish_at).unix() - moment(data?.start_at).unix()
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Lượt thích">
              {data?.count_reaction}
            </Descriptions.Item>
            <Descriptions.Item label="Bình luận">
              {data?.count_comment}
            </Descriptions.Item>
            <Descriptions.Item label="Ngày livestream">
              {moment(data?.create_at).format('DD-MM-YYYY')}
            </Descriptions.Item>
            <Descriptions.Item label="Kết thúc">
              {moment(data?.finish_at).format('DD-MM-YYYY')}
            </Descriptions.Item>
          </Descriptions>
          <Descriptions>
            <Descriptions.Item label="Mô tả">{data?.title}</Descriptions.Item>
          </Descriptions>
        </Spin>
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
        }}
      >
        <Row gutter={[24, 24]}>
          <Col xxl={7} xl={10}>
            {data?.livestream_record_url ? (
              <video
                // style={{ height: 'calc(100vh - 370px)', width: 'auto' }}
                style={{
                  paddingTop: '10px',
                  paddingLeft: '10px',
                  width: '100%',
                }}
                loop
                // autoPlay
                controls
                src={data?.livestream_record_url}
              />
            ) : (
              <Empty
                image={Empty.PRESENTED_IMAGE_SIMPLE}
                description={<span>Không có record livestream</span>}
              />
            )}
          </Col>
          {/* <Col span={1}>
            <Divider type="vertical" style={{ height: '100%' }} />
          </Col> */}
          <Col xxl={17} xl={14}>
            <Descriptions title="BÌNH LUẬN" style={{ paddingTop: '10px' }} />

            <Row gutter={[24, 24]}>
              <Col span={15}>
                <TypingAutoSearch
                  onSearchSubmit={(key: string) => {
                    setIsSearchLoading(true)
                    setParamComment({ ...paramComment, search: key, page: 1 })
                  }}
                  isSearchLoading={isSearchLoading}
                  placeholder="Tên khách hàng hoặc nội dung bình luận ..."
                />
              </Col>
            </Row>

            <Spin spinning={isLoadingComment}>
              <div
                style={{
                  marginTop: '20px',
                  height: 'calc(100vh - 230px)',
                  overflowY: 'scroll',
                }}
              >
                {commentList.length > 0 ? (
                  commentList.map((item: any, index: number) => (
                    <Comment
                      className="fe-comment"
                      key={index}
                      author={item.User.name}
                      avatar={
                        item.User.profile_picture_url ? (
                          item.User.profile_picture_url
                        ) : (
                          <Avatar icon={<UserOutlined />} />
                        )
                      }
                      content={item.content}
                      datetime={msToTime(
                        moment(item.create_at).unix() -
                          moment(data?.start_at).unix()
                      )}
                    />
                  ))
                ) : (
                  <Empty
                    image={Empty.PRESENTED_IMAGE_SIMPLE}
                    description={<span>Không có bình luận.</span>}
                  />
                )}
              </div>
            </Spin>

            <Row style={{ marginBottom: '15px' }}>
              <Col span={24} offset={16} style={{ marginLeft: '200px' }}>
                {paging.total < paging.pageSize ? (
                  <></>
                ) : (
                  <Pagination
                    defaultCurrent={1}
                    total={paging.total}
                    pageSizeOptions={['24']}
                    defaultPageSize={24}
                    onChange={async page => {
                      setParamComment({ ...paramComment, page: page })
                    }}
                    showLessItems={true}
                    showSizeChanger={false}
                  />
                )}
              </Col>
            </Row>
          </Col>
        </Row>
      </div>
    </>
  )
}
