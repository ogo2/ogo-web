/* eslint-disable react-hooks/exhaustive-deps */
import {
  Affix,
  Collapse,
  Image,
  message,
  PageHeader,
  Radio,
  Select,
  Space,
  Table,
  Tag,
} from 'antd'
import 'moment/locale/vi'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import reactotron from 'ReactotronConfig'
import styled from 'styled-components'
import { ADMIN_ROLE } from 'utils/constants'
import R from 'utils/R'
import {
  createAccount,
  deleteAccount,
  getAccounts,
  resetPassword,
} from './AccountApi'
import AccountDetail from './AccountDetail'
import { AddEditAccount } from './components/AddEditAccount'
import { Header } from './components/Header'
import { AccountModel } from './Model'

//

const { Panel } = Collapse

const StyledPanel = styled(Panel)`
  .ant-collapse-content-box {
    /* padding: 0; */
  }
`

const Accounts = () => {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const [accounts, setAccounts] = useState([])
  const [isLoading, setIsLoading] = useState(true)
  const [showAddAccount, setShowAddAccount] = useState(false)
  // const [showEditAccount, setShowEditAccount] = useState(false)
  const [isLoadingModal, setIsLoadingModal] = useState(false)
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [currentSelected, setCurrentSelected] = useState({ id: -1 })
  const [params, setParams] = useState<any>({
    search: '',
    df_type_user_id: '',
    page: 1,
    limit: 24,
    status: null,
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const getData = async () => {
    setIsLoading(true)
    try {
      const res = await getAccounts(params)
      console.log(res)
      const tableData = res.data.map((item: AccountModel, index: number) => {
        return { ...item, key: item.id, stt: index + 1 }
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setAccounts(tableData)
      setPaging(formattedPaging)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  useEffect(() => {
    getData()
  }, [params])

  const createAcc = async (data: any, resetFields: any) => {
    setIsLoading(true)
    try {
      await createAccount(data)
      setShowAddAccount(false)
      reactotron.log!(resetFields)
      resetFields()
    } catch (error) {
      console.log(error)
      // message.error(`${error.message}`)
    } finally {
      setIsLoading(false)
    }
    getData()
  }

  const contentView = () => {
    const columns = [
      {
        width: 70,
        title: 'STT',
        dataIndex: 'index',
        key: 'index',
        render: (text: any, record: any, index: any) => (
          <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
            {(paging.current - 1) * paging.pageSize + index + 1}
          </td>
        ),
        ellipsis: true,
      },
      {
        title: 'Ảnh',
        dataIndex: 'profile_picture_url',
        key: 'profile_picture_url',
        width: 70,
        render: (value: any) => {
          return (
            <td className="icon-eye-antd-custom">
              <Image
                style={{ objectFit: 'cover', width: '35px', height: '35px' }}
                src={value}
                onClick={e => {
                  e.stopPropagation()
                }}
              />
            </td>
          )
        },
      },
      { title: 'Họ tên', dataIndex: 'name', key: 'name' },
      {
        title: 'Số điện thoại',
        dataIndex: 'phone',
        key: 'phone',
      },
      {
        title: 'Email',
        dataIndex: 'email',
        key: 'email',
      },
      {
        title: 'Loại tài khoản',
        dataIndex: 'df_type_user_id',
        key: 'df_type_user_id',
        render: (value: any) => (
          <>
            {value === ADMIN_ROLE.ADMIN
              ? 'Quản trị viên'
              : value === ADMIN_ROLE.ADMIN_EDITOR
              ? 'Biên tập'
              : value === ADMIN_ROLE.ADMIN_SELL_MANAGER
              ? 'Quản lý bán hàng'
              : value === ADMIN_ROLE.ADMIN_SERVICE_MANAGER
              ? ' Quản lý dịch vụ'
              : ''}
          </>
        ),
      },
      {
        title: 'Trạng thái',
        dataIndex: 'status',
        key: 'status',
        render: (value: any) => (
          <Tag color={value === 1 ? 'green' : 'volcano'}>
            {value === 1 ? 'HOẠT ĐỘNG' : 'TẠM DỪNG'}
          </Tag>
        ),
      },
    ]

    return (
      <div
      // style={{
      //   margin: '2px 5px 2px 5px',
      // }}
      >
        {/* <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
            <Header
              setIsCreate={setShowAddAccount}
              onSearchSubmit={(searchKey: string) => {
                setParams({ ...params, search: searchKey, page: 1 })
              }}
            />
          </div> */}

        <PageHeader
          title="Tài khoản"
          style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}
          extra={[
            <Header
              setIsCreate={setShowAddAccount}
              isSearchLoading={isSearchLoading}
              onSearchSubmit={(searchKey: string) => {
                setIsSearchLoading(true)
                setParams({ ...params, search: searchKey, page: 1 })
              }}
              onStatusSubmit={(statusKey: string) => {
                setParams({ ...params, status: statusKey, page: 1 })
              }}
              onRoleSubmit={(roleKey: string) => {
                setParams({ ...params, df_type_user_id: roleKey, page: 1 })
              }}
            />,
          ]}
        />

        <div
          style={{
            backgroundColor: 'white',
            margin: '0px 10px 15px',
            padding: '10px 20px',
          }}
        >
          <p>
            Kết quả lọc: <b>{paging.total}</b>
          </p>
          <Table
            className="table-expanded-custom"
            scroll={{
              x: 800,
              scrollToFirstRowOnChange: true,
              // y: 'calc(100vh - 340px)',
              y: `calc(${heightWeb}px - 340px)`,
            }}
            bordered
            dataSource={accounts}
            loading={isLoading}
            columns={columns}
            expandedRowKeys={[currentSelected.id]}
            onRow={r => ({
              onClick: () => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            })}
            expandable={{
              expandedRowRender: (record: any) => (
                <AccountDetail
                  // onUpdateAccount={(newData: any, resetFields: any) => {
                  //   updateAcc(newData, resetFields)
                  // }}
                  onDeleteAccount={async (id: any) => {
                    try {
                      await deleteAccount({ id: [id] })
                      message.success(
                        `Đã xoá người dùng: ${record.name}(${record.phone})`
                      )
                      getData()
                    } catch (error) {
                      // message.error(error)
                      console.log(error)
                    }
                  }}
                  onResetPassword={async (id: any) => {
                    try {
                      await resetPassword({ id })
                      message.success(`Mật khẩu mới là: ${record.phone}`)
                      getData()
                    } catch (error) {
                      // message.error(error)
                      console.log(error)
                    }
                  }}
                  // isShowEditAccount={showEditAccount}
                  // setShowEditAccount={(isShow: any) => {
                  //   setShowEditAccount(isShow)
                  // }}
                  data={record}
                  getData={getData}
                />
              ),
              onExpand: (status: any, r: any) => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            }}
            pagination={{
              ...paging,
              showSizeChanger: false,
              onChange: async (page, pageSize) => {
                setParams({ ...params, page })
              },
            }}
          />
        </div>
        {showAddAccount && (
          <AddEditAccount
            visible={showAddAccount}
            onCancel={() => {
              setShowAddAccount(false)
              // getData()
            }}
            onCreateNewAccount={(newData: any, resetFields: any) => {
              createAcc(newData, resetFields)
            }}
            isLoadingButton={isLoading}
          />
        )}
      </div>
    )
  }

  return (
    // <Container filterComponent={FilterView} contentComponent={contentView} />
    contentView()
  )
}

export default Accounts
