import { ApiClient } from 'services/ApiService'

export const getAccounts = (payload: any) => {
  const param: any = {}
  if (payload.search) {
    param.search = payload.search
  }
  if (payload.status) {
    param.status = parseInt(payload.status)
  }
  if (payload.page) {
    param.page = payload.page
  }
  if (payload.limit) {
    param.limit = payload.limit
  }
  if (payload.df_type_user_id) {
    param.df_type_user_id = payload.df_type_user_id
  }
  return ApiClient.get('/admin/users', param)
}

export const createAccount = (payload: any) =>
  ApiClient.post('/admin/users', payload)

export const updateAccount = (payload: any) =>
  ApiClient.put(`/admin/users/${payload.id}`, payload.data)

export const resetPassword = (payload: any) =>
  ApiClient.put(`/admin/users/${payload.id}/reset-password`, payload)

export const deleteAccount = (payload: any) =>
  ApiClient.delete('/admin/users', payload)

export const requestUploadImageAccount = (payload: any) =>
  ApiClient.post(`/files/upload/single/${payload.type}`, payload.data)

export const accountChangeStatus = (payload: any) =>
  ApiClient.put(`/admin/users/${payload.id}/status`, payload.status)
