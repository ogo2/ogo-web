import { PlusCircleOutlined } from '@ant-design/icons'
import { Button, Col, Row, Select } from 'antd'
import ButtonFixed from 'common/components/ButtonFixed'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import React from 'react'
import styled from 'styled-components'
import { ADMIN_ROLE, STATUS } from 'utils/constants'
import './css/Header.css'
const { Option } = Select

const Container = styled.div`
  width: 100%;
  padding: 0.5rem;
  background-color: white;
  border-bottom: 1px solid #dddd;
`

type HeaderProps = {
  setIsCreate: any
  onSearchSubmit: (searchKey: string) => void
  onStatusSubmit: (statusKey: any) => void
  onRoleSubmit: (roleKey: any) => void
  isSearchLoading: boolean
}

export const Header = ({
  setIsCreate,
  onSearchSubmit,
  onStatusSubmit,
  onRoleSubmit,
  isSearchLoading,
}: HeaderProps) => {
  return (
    <Row gutter={[16, 16]} justify="end">
      <Col span={8} style={{ minWidth: '260px' }}>
        <TypingAutoSearch
          onSearchSubmit={(key: string) => {
            onSearchSubmit(key.trim())
          }}
          isSearchLoading={isSearchLoading}
          placeholder="Tên hoặc số điện thoại ..."
        />
      </Col>

      <Col span={4}>
        <Select
          allowClear
          placeholder="Trạng thái"
          style={{ minWidth: '110px' }}
          onChange={value => {
            onStatusSubmit(value)
          }}
        >
          <Option value={''}>Tất cả</Option>
          <Option value={`${STATUS.ACTIVE}`}>Hoạt động</Option>
          <Option value={`${STATUS.INACTIVE}`}>Tạm dừng</Option>
        </Select>
      </Col>

      <Col span={4}>
        <Select
          allowClear
          placeholder="Loại tài khoản"
          style={{ minWidth: '160px' }}
          onChange={value => {
            onRoleSubmit(value)
          }}
        >
          <Option value={''}>Tất cả</Option>
          <Option value={ADMIN_ROLE.ADMIN}>Quản trị viên</Option>
          <Option value={ADMIN_ROLE.ADMIN_SELL_MANAGER}>
            Quản lý bán hàng
          </Option>
          <Option value={ADMIN_ROLE.ADMIN_SERVICE_MANAGER}>
            Quản lý dịch vụ
          </Option>
          <Option value={ADMIN_ROLE.ADMIN_EDITOR}>Biên tập</Option>
        </Select>
      </Col>
      <Col span={4}>
        {/* <Button
          type="primary"
          style={{ minWidth: '70px' }}
          onClick={() => {
            setIsCreate(true)
          }}
        >
          <PlusCircleOutlined />
          Thêm mới
        </Button> */}
        <ButtonFixed setIsCreate={setIsCreate} text="Thêm mới" type="add" />
      </Col>
    </Row>
  )
}
