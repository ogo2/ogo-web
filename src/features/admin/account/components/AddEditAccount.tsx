import { LoadingOutlined, PlusOutlined } from '@ant-design/icons'
import {
  Button,
  Col,
  Form,
  Input,
  message,
  Modal,
  Row,
  Select,
  Upload,
} from 'antd'
import ButtonBottomModal from 'common/components/ButtonBottomModal'
import React, { useEffect, useState } from 'react'
import { ADMIN_ROLE } from 'utils/constants'
import createFormDataImage from 'utils/createFormDataImage'
import { validHeadPhoneNumber } from 'utils/funcHelper'
import { requestUploadImageAccount } from '../AccountApi'

type Props = {
  visible: boolean
  onCancel?: any
  data?: any
  onCreateNewAccount?: any
  onUpdateAccount?: any
  isLoadingButton: boolean
}

function getBase64(img: any, callback: any) {
  const reader = new FileReader()
  reader.addEventListener('load', () => callback(reader.result))
  reader.readAsDataURL(img)
}

function beforeUpload(file: any) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png'
  if (!isJpgOrPng) {
    message.error('Xảy ra lỗi! Bạn chỉ có thể upload ảnh có dạng JPG/PNG!')
  }
  const isLt2M = file.size / 1024 / 1024 < 2
  if (!isLt2M) {
    message.error('Cho phép ảnh có dung lượng tối đa là 2MB')
  }
  return isJpgOrPng && isLt2M
}

function convertDataToFrom(data: any) {
  // reactotron.log!(data)
  if (!data) {
    return {
      name: null,
      phone: null,
      password: null,
      email: null,
      df_type_user_id: null,
      profile_picture_url: null,
      // role_id: null,
      // address: null,
      // date_of_birth: null,
      // expired_at: null,
      // gender: null,
      // province_id: null,
    }
  } else {
    return {
      ...data,
      // name: data.name,
      // date_of_birth: moment.unix(1616620499),
      // expired_at: moment.unix(data.expired_at),
    }
  }
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
}

export const AddEditAccount = ({
  visible,
  onCancel,
  data,
  onCreateNewAccount,
  onUpdateAccount,
  isLoadingButton,
}: Props) => {
  const [upload, setUpload] = useState({
    loading: false,
    imageUrl: '',
  })

  const [form] = Form.useForm()

  const initialValues = convertDataToFrom(data)

  const handleChange = (info: any) => {
    if (info.file.status === 'uploading') {
      setUpload({
        imageUrl: '',
        loading: true,
      })
      return
    }

    if (info.file.status === 'done' || info.file.status === 'error') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, (imageUrl: any) =>
        setUpload({
          imageUrl: imageUrl,
          loading: false,
        })
      )
    }
  }

  const uploadButton = (
    <div>
      {upload.loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  )

  const onFinish = async (values: any, onCancel: any) => {
    try {
      // Upload image lên Server
      if (values.profile_picture_url.fileList) {
        var resUploadImage = []
        var last_element = []

        if (values.profile_picture_url.fileList.length > 1) {
          last_element = [values.profile_picture_url.fileList.slice(-1)[0]]
        } else {
          last_element = values.profile_picture_url.fileList
        }
        const dataImage = await createFormDataImage(last_element)
        const payloadImage = {
          type: 0,
          data: dataImage,
        }
        resUploadImage = await requestUploadImageAccount(payloadImage)
        console.log(resUploadImage)
      }

      delete values.confirm
      let newData
      if (!data) {
        newData = {
          ...values,
          name: values.name.trim(),
          email: values.email.trim(),
          profile_picture_url: resUploadImage.data.path,
        }
        console.log(newData)
        onCreateNewAccount(newData, form.resetFields)
      } else {
        if (data.profile_picture_url === values.profile_picture_url) {
          delete values.profile_picture_url
          newData = {
            ...values,
            name: values.name.trim(),
            email: values.email.trim(),
            id: data.id,
          }
        } else {
          newData = {
            ...values,
            name: values.name.trim(),
            email: values.email.trim(),
            id: data.id,
            profile_picture_url: resUploadImage.data.path,
          }
        }

        console.log(data)
        console.log(newData)
        onUpdateAccount(newData, form.resetFields)
      }
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    if (data) {
      setUpload({
        ...upload,
        imageUrl: data.profile_picture_url,
      })
    }
  }, [data])

  return (
    <Modal
      onCancel={() => {
        form.resetFields()
        // setUploadImage({
        //   profile_picture_url: '',
        //   profile_picture: '',
        // })
        onCancel()
      }}
      maskClosable={false}
      footer={null}
      title={data ? 'Sửa tài khoản' : 'Thêm tài khoản'}
      visible={visible}
    >
      <Form
        {...formItemLayout}
        form={form}
        name="register"
        labelAlign="left"
        onFinish={(values: any) => onFinish(values, onCancel)}
        initialValues={initialValues}
        scrollToFirstError
      >
        <Form.Item
          name="name"
          label="Họ tên"
          rules={[
            {
              type: 'string',
              message: 'Nhập họ tên',
            },
            {
              required: true,
              message: 'Vui lòng nhập họ tên!',
            },
            {
              whitespace: true,
              message: 'Vui lòng không nhập khoảng trắng!',
            },
            {
              min: 3,
              max: 50,
              message: 'Vui lòng nhập từ 3 đến 50 ký tự!',
            },
            // {
            //   message: 'Vui lòng không nhập kí tự đặc biệt',
            //   validator: (_, value) => {
            //     // const reg = /^[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/
            //     const reg = /[^a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩếýũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s]/u
            //     if (!reg.test(value)) {
            //       return Promise.resolve()
            //     }
            //     return Promise.reject()
            //   },
            // },
          ]}
        >
          <Input placeholder="Nhập họ tên" />
        </Form.Item>

        <Form.Item
          name="phone"
          label="Số điện thoại"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập số điện thoại',
            },
            {
              message: 'Số điện thoại không hợp lệ',
              validator: (_, value) => {
                let trimValue: string = ''
                if (value) {
                  trimValue = value.trim()
                }
                const reg = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{3,6}$/im // validate phone number
                let validPhone: boolean = true
                if (
                  trimValue &&
                  trimValue.length < 10 &&
                  trimValue.includes('84')
                ) {
                  validPhone = false
                } else if (
                  trimValue &&
                  trimValue.length < 11 &&
                  trimValue.includes('+84')
                ) {
                  validPhone = false
                }

                if (
                  trimValue === '' ||
                  trimValue === null ||
                  (reg.test(trimValue) &&
                    validHeadPhoneNumber(trimValue) &&
                    validPhone)
                ) {
                  return Promise.resolve()
                }
                return Promise.reject()
              },
            },
          ]}
        >
          <Input disabled={data} placeholder="Nhập số điện thoại" />
        </Form.Item>

        <Form.Item
          name="email"
          label="E-mail"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập email!',
            },
            {
              message: 'Định dạng email không hợp lệ!',
              validator: (_, value) => {
                let trimValue: string = ''
                let subString: Array<string> = []
                if (value) {
                  trimValue = value.trim()
                  subString = value.split('@')
                }
                const reg = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/ // validate email
                const reg2 = /^[\x00-\x7F]*$/ // just ascii

                const reg3 = /([a-zA-Z])+([ -~])*/ // contains a letter
                let checkMail = false
                if (subString.length === 2 && reg3.test(subString[0])) {
                  checkMail = true
                }
                if (
                  !value ||
                  value === '' ||
                  // value.length > 20 ||
                  (reg.test(trimValue) && reg2.test(trimValue) && checkMail)
                ) {
                  return Promise.resolve()
                }
                return Promise.reject()
              },
            },
          ]}
        >
          <Input placeholder="Nhập email" />
        </Form.Item>

        <Form.Item
          label="Loại tài khoản"
          name="df_type_user_id"
          rules={[
            {
              required: true,
              message: 'Vui lòng chọn loại tài khoản!',
            },
          ]}
        >
          <Select
            placeholder="Chọn loại tài khoản"
            defaultValue={data?.df_type_user_id}
          >
            <Select.Option value={ADMIN_ROLE.ADMIN}>
              Quản trị viên
            </Select.Option>
            <Select.Option value={ADMIN_ROLE.ADMIN_SELL_MANAGER}>
              Quản lý bán hàng
            </Select.Option>
            <Select.Option value={ADMIN_ROLE.ADMIN_SERVICE_MANAGER}>
              Quản lý dịch vụ
            </Select.Option>
            <Select.Option value={ADMIN_ROLE.ADMIN_EDITOR}>
              Biên tập viên
            </Select.Option>
          </Select>
        </Form.Item>

        {!data && (
          <Form.Item
            name="password"
            label="Mật khẩu"
            rules={[
              {
                required: true,
                message: 'Vui lòng nhập mật khẩu!',
              },
              {
                min: 6,
                max: 20,
                message: 'Vui lòng nhập từ 6 đến 20 ký tự!',
              },
            ]}
            hasFeedback
          >
            <Input.Password placeholder="Nhập mật khẩu" />
          </Form.Item>
        )}
        {!data && (
          <Form.Item
            name="confirm"
            label="Xác nhận mật khẩu"
            dependencies={['password']}
            hasFeedback
            rules={[
              {
                required: true,
                message: 'Nhập lại mật khẩu!',
              },
              ({ getFieldValue }) => ({
                validator(_, value) {
                  if (!value || getFieldValue('password') === value) {
                    return Promise.resolve()
                  }
                  return Promise.reject(new Error('Mật khẩu không khớp!'))
                },
              }),
            ]}
          >
            <Input.Password placeholder="Nhập lại mật khẩu" />
          </Form.Item>
        )}

        <Form.Item
          label="Ảnh đại diện"
          name="profile_picture_url"
          rules={[
            {
              required: true,
              message: 'Vui lòng chọn ảnh đại diện!',
            },
          ]}
        >
          <Upload
            name="profile_picture_url"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            beforeUpload={beforeUpload}
            onChange={handleChange}
          >
            {upload.imageUrl ? (
              <img
                src={upload.imageUrl}
                alt="avatar"
                style={{ width: '100%', objectFit: 'contain', height: '100%' }}
              />
            ) : (
              uploadButton
            )}
          </Upload>
          {/* <div style={{ fontSize: '12px' }}>
            <p></p>
            <p>1. Định dạng: JPG/JPEG/PNG</p>
            <p>2. Kích thước: Tối đa 2MB</p>
          </div> */}
        </Form.Item>

        {/* <Form.Item>
          <Row style={{ width: '100%' }}>
            <Col span={6} offset={10}>
              <Button
                danger
                onClick={() => {
                  form.resetFields()
                  onCancel()
                }}
              >
                Huỷ
              </Button>
            </Col>
            <Col span={6} offset={2}>
              <Button loading={isLoading} type="primary" htmlType="submit">
                {data ? 'Cập nhật' : 'Tạo tài khoản'}
              </Button>
            </Col>
          </Row>
        </Form.Item> */}

        <ButtonBottomModal
          isLoadingButton={isLoadingButton}
          onCancel={onCancel}
          text={data ? 'Cập nhật' : 'Tạo tài khoản'}
        />
      </Form>
    </Modal>
  )
}
