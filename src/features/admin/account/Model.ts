export type AccountModel = {
  expired_at: string
  created_at: string
  id: number
  name: string
  phone: string
  password: string
  token: string
  email: string
  status: number
  role_id: number
  is_active: number
  role: string
  profile_picture_url: string
  profile_picture: string
}
