import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  DeleteFilled,
  EditOutlined,
  UndoOutlined,
} from '@ant-design/icons'
import { Button, Card, Descriptions, message, Popconfirm } from 'antd'
import moment from 'moment'
import React, { useState } from 'react'
import { CHANGE_STATUS, STATUS } from 'utils/constants'
import { accountChangeStatus, updateAccount } from './AccountApi'
import { AddEditAccount } from './components/AddEditAccount'

type Props = {
  data: any
  getData: any
  onDeleteAccount: any
  onResetPassword: any
  // onUpdateAccount: any
  // isShowEditAccount: boolean
  // setShowEditAccount: any
}

const ContentView = (data: any) => {
  return (
    <Descriptions size="default" column={3}>
      <Descriptions.Item label="Tên người dùng">{data.name}</Descriptions.Item>
      <Descriptions.Item label="Số điện thoại">{data.phone}</Descriptions.Item>
      <Descriptions.Item label="Email">{data.email}</Descriptions.Item>
      <Descriptions.Item label="Loại tài khoản">
        {data.df_type_user_id === 1 ? 'Quản trị viên' : ''}
      </Descriptions.Item>
      <Descriptions.Item label="Ngày tạo">
        {moment(data.create_at).format('DD-MM-YYYY')}
      </Descriptions.Item>
    </Descriptions>
  )
}

function AccountDetail({
  data,
  getData,
  // onUpdateAccount,
  // isShowEditAccount,
  // setShowEditAccount,
  onDeleteAccount,
  onResetPassword,
}: Props) {
  const [showEditAccount, setShowEditAccount] = useState(false)

  const onChangeStatus = async () => {
    try {
      const payload = {
        id: data.id,
        status: {
          status: data.status === 0 ? 1 : 0,
        },
      }
      await accountChangeStatus(payload)
      message.success(`Cập nhật trạng thái thành công`)
      getData()
    } catch (error) {
      console.log(error)
    } finally {
      //setIsLoadingModal(false)
    }
  }

  const updateAcc = async (data: any, resetFields: any) => {
    try {
      let dataPush = {
        id: data.id,
        data: {
          name: data.name,
          email: data.email,
          profile_picture_url: data.profile_picture_url,
          role: data.df_type_user_id,
          status: data.status,
        },
      }
      await updateAccount(dataPush)
      message.success(`Cập nhật thành công`)
      setShowEditAccount(false)
      getData()
      // setShowEditAccount(false)
      resetFields()
    } catch (error) {
      // message.error(`Đã có lỗi xảy ra, vui lòng thử lại`)
      console.log(error)
    }
  }

  return (
    <div>
      {showEditAccount && (
        <AddEditAccount
          data={data}
          visible={showEditAccount}
          onCancel={() => {
            setShowEditAccount(false)
          }}
          onUpdateAccount={(newData: any, resetFields: any) => {
            updateAcc(newData, resetFields)
          }}
          isLoadingButton={false}
        />
      )}
      <Card
        style={{
          backgroundColor: '#f6f9ff',
          borderColor: '#1890ff',
          borderTop: 'none',
        }}
        actions={[
          <Popconfirm
            title={
              data.status === STATUS.ACTIVE
                ? 'Bạn chắc chắn muốn tạm dừng hoạt động tài khoản này'
                : 'Bạn chắc chắn muốn bật hoạt động tài khoản này'
            }
            onConfirm={() => {
              onChangeStatus()
            }}
            okText={
              data.status === STATUS.ACTIVE
                ? CHANGE_STATUS.INACTIVE
                : CHANGE_STATUS.ACTIVE
            }
            cancelText="Quay lại"
            okButtonProps={
              data.status === STATUS.ACTIVE
                ? {
                    danger: true,
                    type: 'primary',
                  }
                : {
                    danger: false,
                    type: 'primary',
                  }
            }
          >
            <Button
              type="text"
              size="large"
              // onClick={onChangeStatus}
              style={{
                color: data.status === STATUS.ACTIVE ? 'red' : '#389e0d',
              }}
              icon={
                data.status === STATUS.ACTIVE ? (
                  <CloseCircleOutlined />
                ) : (
                  <CheckCircleOutlined />
                )
              }
            >
              {data.status === 1
                ? CHANGE_STATUS.INACTIVE
                : CHANGE_STATUS.ACTIVE}
            </Button>
          </Popconfirm>,

          <Button
            onClick={() => {
              setShowEditAccount(true)
            }}
            type="text"
            size="large"
            icon={<EditOutlined />}
            style={{ color: '#1890ff' }}
          >
            Chỉnh sửa
          </Button>,

          <Popconfirm
            title={'Bạn chắc chắn muốn đặt lại mật khẩu tài khoản này'}
            onConfirm={async () => {
              try {
                onResetPassword(data.id)
              } catch (error) {
              } finally {
              }
            }}
            okText="Đặt lại mật khẩu"
            cancelText="Quay lại"
            okButtonProps={{
              type: 'primary',
            }}
          >
            <Button type="text" size="large" icon={<UndoOutlined />}>
              Đặt lại mật khẩu
            </Button>
          </Popconfirm>,

          <Popconfirm
            title={'Bạn chắc chắn muốn xoá tài khoản này'}
            onConfirm={async () => {
              try {
                onDeleteAccount(data.id)
              } catch (error) {
              } finally {
              }
            }}
            okText="Xoá"
            cancelText="Quay lại"
            okButtonProps={{
              danger: true,
              type: 'primary',
            }}
          >
            <Button
              type="text"
              size="large"
              icon={<DeleteFilled />}
              style={{ color: 'red' }}
            >
              Xoá tài khoản
            </Button>
          </Popconfirm>,
        ]}
      >
        {ContentView(data)}
      </Card>
    </div>
  )
}

export default AccountDetail
