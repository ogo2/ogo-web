import { Descriptions, Row, Table } from 'antd'
import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { formatPrice } from 'utils/ruleForm'
import { detailProducts } from '../ProductApi'

type Props = {
  //   id_shop: any
  //   onChageStatus: any
  //   saleInfor: any
}

export default function SaleInfo() {
  const [isLoading, setIsLoading] = useState(false)
  const [dataSource, setDataSource] = useState<Array<any>>([])
  const param: { id: string } = useParams()
  useEffect(() => {
    if (param) {
      getProductDetail(parseInt(param.id))
    }
  }, [param])

  const getProductDetail = async (id: number) => {
    setIsLoading(true)
    try {
      const payload = { id: id, type: 2 }
      const orderList = await detailProducts(payload)
      const data = orderList.data.detailProduct.data.map(
        (item: any, index: any) => {
          return {
            key: item.id,
            stt: index + 1,
            name: item.name,
            phone: item.phone,
            total_amount_order: item.total_amount_order,
            total_price: item.total_price,
          }
        }
      )
      setDataSource(data)
    } catch (error) {
    } finally {
      setIsLoading(false)
    }
  }

  const columnsCustomer = [
    {
      title: 'STT',
      dataIndex: 'stt',
      render: (stt: any) => {
        if (stt > 0) {
          return <div>{stt}</div>
        }
        return <div style={{ fontWeight: 'bold' }}>Tổng</div>
      },
    },
    {
      title: 'Tên khách hàng',
      dataIndex: 'name',
    },
    {
      title: 'Số điện thoại',
      dataIndex: 'phone',
    },
    {
      title: 'Số lượng',
      dataIndex: 'total_amount_order',
    },
    {
      title: 'Doanh thu',
      dataIndex: 'total_price',
      render: (value: any) => {
        return <td>{value ? `${formatPrice(value)} đ ` : '0 đ'}</td>
      },
    },
  ]

  return (
    <div style={{ backgroundColor: 'white', paddingBottom: '20px' }}>
      <Row>
        <Descriptions
          title={<p style={{ color: '#CC0000' }}>KHÁCH HÀNG MUA SẢN PHẨM</p>}
          style={{ padding: '10px 10px' }}
        ></Descriptions>

        <div
          style={{
            padding: '0px 10px',
          }}
        >
          <Table
            className="table-expanded-custom-no-image"
            scroll={{
              x: 1020,
              scrollToFirstRowOnChange: true,
              y: 'calc(100vh - 280px)',
            }}
            bordered
            loading={isLoading}
            columns={columnsCustomer}
            dataSource={dataSource}
            summary={data => {
              let total_amount_order = 0
              let total_price = 0
              data.forEach(item => {
                total_amount_order += parseInt(item.total_amount_order)
                total_price += parseInt(item.total_price)
              })
              return (
                <Table.Summary.Row style={{ fontWeight: 'bold' }}>
                  <Table.Summary.Cell index={0} colSpan={3} align="center">
                    Tổng
                  </Table.Summary.Cell>
                  <Table.Summary.Cell index={3}>
                    {total_amount_order}
                  </Table.Summary.Cell>
                  <Table.Summary.Cell index={4}>
                    {formatPrice(total_price) ? formatPrice(total_price) : 0} đ
                  </Table.Summary.Cell>
                </Table.Summary.Row>
              )
            }}
          />
        </div>
      </Row>
    </div>
  )
}
