import { Col, Collapse, Empty, Row, Select } from 'antd'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import React, { useEffect, useState } from 'react'
import { STATUS } from 'utils/constants'
import { getCategory, getShop } from '../ProductApi'
import './css/header.css'

const { Panel } = Collapse
const { Option } = Select

type Props = {
  listStock: Array<{}>
  checkedTable: Array<{}>
  isSearchLoading: boolean
  onSearchSubmit: (searchKey: string) => void
  onStatusSubmit: (statusKey: string) => void
  onCategorySubmit: (category_id: string) => void
  onChildCategorySubmit: (children_category_id: string) => void
  onShopSubmit: (shop_id: string) => void
  onStockSubmit: (stock_id: any) => void
}

export default function Filter({
  listStock,
  checkedTable,
  isSearchLoading,
  onSearchSubmit,
  onStatusSubmit,
  onCategorySubmit,
  onChildCategorySubmit,
  onShopSubmit,
  onStockSubmit,
}: Props) {
  const [shopList, setShopList] = useState([])
  const [categoryList, setCategoryList] = useState([])
  const [selectedStockId, setSelectedStockId] = useState<any>()
  const [param, setParam] = useState({ page: 1, search: '' })
  const [paramsCategory, setParamsCategory] = useState({ page: 1, search: '' })
  const [paramsExport, setParamsExport] = useState({
    search: '',
    status: '',
    shop_id: '',
    category_id: '',
    stock_id: '',
  })
  const [isLoading, setIsLoading] = useState<boolean>(false)

  const [categorySubList, setCategorySubList] = useState([])
  const [paramsCategorySub, setParamsCategorySub] = useState({
    page: 1,
    search: '',
    parent_id: '',
  })
  const [selectedCategorySubId, setSelectedCategorySubId] = useState<string>()

  //lấy danh sách danh mục cha
  const getCategoryList = async () => {
    setIsLoading(true)
    try {
      const response = await getCategory(paramsCategory)

      setCategoryList(response.data)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    getCategoryList()
  }, [paramsCategory])

  //lấy danh sách danh mục con
  const getCategorySubList = async () => {
    try {
      if (paramsCategorySub.parent_id) {
        const response = await getCategory(paramsCategorySub)
        setCategorySubList(response.data)
      }
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getCategorySubList()
  }, [paramsCategorySub])

  const getShopList = async () => {
    setIsLoading(true)
    try {
      const response = await getShop(param)
      setShopList(response.data)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    getShopList()
  }, [param])

  return (
    <Row gutter={[16, 16]} justify="end">
      <Col>
        <TypingAutoSearch
          onSearchSubmit={(searchKey: string) => {
            onSearchSubmit(searchKey.trim())
            setParamsExport({ ...paramsExport, search: searchKey.trim() })
          }}
          isSearchLoading={isSearchLoading}
          placeholder="Tên hoặc mã sản phẩm ..."
        />
      </Col>

      <Col>
        <Select
          allowClear
          optionFilterProp="children"
          loading={isLoading}
          placeholder="DM sản phẩm"
          className="select-shop-category"
          showSearch
          onSearch={(value: string) =>
            setParamsCategory({ ...paramsCategory, search: value, page: 1 })
          }
          onClear={() => {
            setCategorySubList([])
            setSelectedCategorySubId(undefined)
            // onChildCategorySubmit('0')
            onCategorySubmit('0')
          }}
          onChange={(value: string) => {
            setParamsCategorySub({
              ...paramsCategorySub,
              parent_id: value,
              page: 1,
            })
            onCategorySubmit(value)
            setSelectedCategorySubId(undefined)
          }}
        >
          {categoryList.map((item: any, index: number) => {
            return (
              <Option value={item.id} key={index}>
                {item.name}
              </Option>
            )
          })}
        </Select>
      </Col>

      <Col>
        <Select
          allowClear
          optionFilterProp="children"
          loading={isLoading}
          placeholder="DM con sản phẩm"
          className="select-shop-category"
          showSearch
          value={selectedCategorySubId}
          onChange={(value: string) => {
            setSelectedCategorySubId(value)
            onChildCategorySubmit(value)
          }}
          onClear={() => {
            onChildCategorySubmit('0')
          }}
          notFoundContent={
            <Empty
              image={Empty.PRESENTED_IMAGE_SIMPLE}
              description={<span>Vui lòng chọn danh mục sản phẩm!</span>}
            />
          }
        >
          {categorySubList.map((item: any, index: number) => {
            return (
              <Option value={item.id} key={index}>
                {item.name}
              </Option>
            )
          })}
        </Select>
      </Col>

      <Col>
        <Select
          allowClear
          className="select-status-category"
          placeholder="Trạng thái"
          onChange={(value: any) => {
            onStatusSubmit(value)
            setParamsExport({ ...paramsExport, status: value })
          }}
        >
          <Option value="">Tất cả</Option>
          <Option value={`${STATUS.ACTIVE}`}>Đang hoạt động</Option>
          <Option value={`${STATUS.INACTIVE}`}>Ngừng hoạt động</Option>
        </Select>
      </Col>

      <Col>
        <Select
          allowClear
          optionFilterProp="children"
          loading={isLoading}
          placeholder="Gian hàng"
          className="select-shop-category"
          onChange={(value: string) => {
            onShopSubmit(value)
            setSelectedStockId(undefined)
          }}
          onClear={() => {
            console.log('clear shop_id')
            setSelectedStockId(undefined)
          }}
          showSearch
          onSearch={(value: string) => {
            setParam({ ...param, search: value })
            setParamsExport({ ...paramsExport, shop_id: value })
          }}
        >
          {shopList.map((item: any) => {
            return (
              <Option value={item.id} key={item.id}>
                {item.name}
              </Option>
            )
          })}
        </Select>
      </Col>

      <Col>
        <Select
          allowClear
          placeholder="Kho hàng"
          onClear={() => {
            console.log('clear stock_id')
          }}
          className="select-store-category"
          value={selectedStockId}
          onChange={(value: string) => {
            setSelectedStockId(value)
            onStockSubmit(value)
            setParamsExport({ ...paramsExport, search: value })
          }}
        >
          {listStock.map((item: any) => {
            return (
              <Option value={item.id} key={item.id}>
                {item.name}
              </Option>
            )
          })}
        </Select>
      </Col>
    </Row>
  )
}
