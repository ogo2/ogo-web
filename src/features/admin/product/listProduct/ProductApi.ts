import { ApiClient } from 'services/ApiService'
//Category
export const getCategory = (payload: any) => ApiClient.get('/category', payload)

//List shop
export const getShop = (payload: any) => {
  return ApiClient.get('/admin/shop', payload)
}

//Product
export const listProducts = (payload: any) => {
  const param: any = {}
  param.category_id = parseInt(payload.category_id) || 0
  if (param.category_id) {
    param.children_category_id = parseInt(payload.children_category_id) || 0
  } else {
    param.children_category_id = 0
  }
  if (payload.search) {
    param.search = payload.search
  }
  if (payload.status) {
    param.status = parseInt(payload.status)
  }
  if (payload.page) {
    param.page = payload.page
  }
  if (payload.shop_id) {
    param.shop_id = parseInt(payload.shop_id)
    if (payload.stock_id) {
      param.stock_id = parseInt(payload.stock_id)
    }
  }
  // console.log('payload: ', payload)
  // console.log('param: ', param)
  return ApiClient.get('/admin/product', param)
}
export const allProducts = (payload: any) => {
  const paramExport: any = {}
  paramExport.category_id = parseInt(payload.category_id) || 0
  if (paramExport.category_id) {
    paramExport.children_category_id =
      parseInt(payload.children_category_id) || 0
  } else {
    paramExport.children_category_id = 0
  }
  if (payload.search) {
    paramExport.search = payload.search
  }
  if (payload.status) {
    paramExport.status = parseInt(payload.status)
  }
  if (payload.shop_id) {
    paramExport.shop_id = parseInt(payload.shop_id)
    if (payload.stock_id) {
      paramExport.stock_id = parseInt(payload.stock_id)
    }
  }
  return ApiClient.get('/admin/product/all', paramExport)
}
export const detailProducts = (payload: { id: number; type: number }) =>
  ApiClient.get(`/admin/product/${payload.id}?type=${payload.type}`)
export const deleteProducts = (payload: any) =>
  ApiClient.delete('/product', payload)

export const changeStatusProduct = (payload: {
  id: number
  body: {
    status: number
  }
}) => ApiClient.put(`/product/change-status/${payload.id}`, payload.body)
