import { DownloadOutlined } from '@ant-design/icons'
import { message, PageHeader, Switch, Table, Tag } from 'antd'
import { ADMIN_ROUTER_PATH } from 'common/config'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { PRODUCT_STOCK_STATUS } from 'utils/constants'
import ExportCsv from 'utils/ExportCSV'
import history from 'utils/history'
import { formatPrice } from 'utils/ruleForm'
import Filter from './components/Filter'
import { allProducts, changeStatusProduct, listProducts } from './ProductApi'

interface DataType {
  id: React.Key
  stt: React.Key
  product_code: string
  product_name: string
  category: string
  status_goods: string
  sales: string
  status: number
  sold: string
}

interface dataChangeStatus {
  id: number
  body: {
    status: number
  }
}

export default function ListProduct() {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )

  const [dataTable, setDataTable] = useState<any>([])
  const [listStock, setListStock] = useState<any>([])
  const [checkedTable, setCheckedTable] = useState<Array<any>>([])
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [itemSelected, setItemSelected] = useState({ id: 0, rowIndex: 0 })
  const [selectedRowKey, setSelectedRowKey] = useState<Array<any>>([])
  const [params, setParams] = useState({
    search: '',
    status: '',
    shop_id: '',
    category_id: 0,
    children_category_id: 0,
    stock_id: '',
    page: 1,
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const getListProduct = async () => {
    setIsLoading(true)
    try {
      const response = await listProducts(params)
      // console.log('list product: ', response)
      const data = response.data.rows.map((item: any, index: any) => {
        return { ...item, key: item.id }
      })
      setDataTable(data)
      setListStock(response.data.listStock)
      setPaging({
        total: response.paging.totalItemCount,
        current: response.paging.page,
        pageSize: response.paging.limit,
      })
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  useEffect(() => {
    getListProduct()
  }, [params])

  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any) => {
      const selectedRowID = selectedRows.map((item: any) => {
        return item
      })
      console.log(selectedRowKey)
      setCheckedTable(selectedRowID)
      setSelectedRowKey(selectedRowID)
    },
    getCheckboxProps: (record: any) => ({
      id: record.id,
    }),
  }

  const onChangeProductStatus = async (checked: any, e: any) => {
    e.stopPropagation()
    setIsLoading(true)
    try {
      let param: dataChangeStatus
      if (checked) {
        param = {
          id: itemSelected.id,
          body: {
            status: 1,
          },
        }
      } else {
        param = {
          id: itemSelected.id,
          body: {
            status: 0,
          },
        }
      }
      let res = await changeStatusProduct(param)
      if (res.code) {
        dataTable[itemSelected.rowIndex].status = !dataTable[
          itemSelected.rowIndex
        ].status
      }
    } catch (error) {
      console.log('error', error)
    } finally {
      setIsLoading(false)
    }
  }

  const [dataExport, setDataExport] = useState<any>([])
  const [isLoadingBtnExportData, setLoadingBtnExportData] = useState<boolean>(
    false
  )
  const handleDataSelect = () => {
    const dataSelected = selectedRowKey?.map((o: any, i: number) => {
      return {
        STT: i + 1,
        'Mã sảm phẩm': o.code || '',
        'Tên sảm phẩm': o.name || '',
        'Gian hàng': o.shop_name || '',
        'Danh mục':
          o.Category.parent_category.name + '--' + o.Category.name || '',
        'Trạng thái hàng':
          o.stock_status === PRODUCT_STOCK_STATUS.AVAILABLE
            ? 'CÒN HÀNG'
            : 'HẾT HÀNG',
        'Trạng thái': o.status === 1 ? 'Đang hoạt động' : 'Ngừng hoạt động',
        'Đã bán': o.total_amount == null ? '0' : o.total_amount,
        'Doanh số':
          o.total_price == null ? '0đ' : formatPrice(o.total_price) + 'đ',
      }
    })
    setDataExport(dataSelected)
  }

  useEffect(() => {
    handleDataSelect()
  }, [selectedRowKey])

  const onExportDataToExcel = async (fn: any) => {
    try {
      if (selectedRowKey.length === 0) {
        setLoadingBtnExportData(true)
        const dataListProductNotPaging: any = await allProducts(params)
        console.log(dataListProductNotPaging)
        const data = dataListProductNotPaging?.data?.map(
          (o: any, i: number) => {
            return {
              STT: i + 1,
              'Mã sảm phẩm': o.code || '',
              'Tên sảm phẩm': o.name || '',
              'Gian hàng': o.shop_name || '',
              'Danh mục':
                o.Category.parent_category.name + '--' + o.Category.name || '',
              'Trạng thái hàng':
                o.stock_status === PRODUCT_STOCK_STATUS.AVAILABLE
                  ? 'CÒN HÀNG'
                  : 'HẾT HÀNG',
              'Trạng thái':
                o.status === 1 ? 'Đang hoạt động' : 'Ngừng hoạt động',
              'Đã bán': o.total_amount == null ? '0' : o.total_amount,
              'Doanh số':
                o.total_price == null ? '0đ' : formatPrice(o.total_price) + 'đ',
            }
          }
        )
        setDataExport(data)
        fn()
      } else {
        fn()
      }
    } catch (error) {
      message.error(
        'Đã có lỗi trong quá trình export dữ liệu, vui lòng thử lại sau!'
      )
    } finally {
      // setLoadingBtnExportData(false)
      setLoadingBtnExportData(false)
    }
  }

  const columns = [
    {
      width: 70,
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
      render: (text: any, record: any, index: any) => (
        <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
          {(paging.current - 1) * paging.pageSize + index + 1}
        </td>
      ),
      ellipsis: true,
    },
    {
      title: 'Mã sản phẩm',
      dataIndex: 'code',
    },
    {
      title: 'Tên sản phẩm',
      dataIndex: 'name',
    },
    {
      title: 'Gian hàng',
      dataIndex: 'Shop',
      render: (value: any) => {
        return <td>{value?.name}</td>
      },
    },
    {
      title: 'Danh mục',
      // dataIndex: 'Category',
      render: (text: any, record: any, index: any) => {
        return (
          <td>
            {record.Category?.parent_category.name} -- {record.Category?.name}
          </td>
        )
      },
    },
    {
      title: 'Trạng thái hàng',
      dataIndex: 'stock_status',
      render: (value: any) => {
        return (
          <Tag color={value === 1 ? 'green' : 'volcano'}>
            {value === PRODUCT_STOCK_STATUS.AVAILABLE ? 'CÒN HÀNG' : 'HẾT HÀNG'}
          </Tag>
        )
      },
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: (value: any) => {
        return (
          <td>
            {value === 1 ? (
              <Switch checked onChange={onChangeProductStatus} />
            ) : (
              <Switch onChange={onChangeProductStatus} />
            )}
          </td>
        )
      },
    },
    {
      width: 100,
      title: 'Đã bán',
      dataIndex: 'total_amount',
    },
    {
      title: 'Doanh số',
      dataIndex: 'total_price',
      render: (value: any) => {
        return (
          <td>{value === '0' ? '0đ' : `${formatPrice(parseInt(value))} đ`}</td>
        )
      },
    },
    {
      title: 'Số lượng',
      dataIndex: 'amount',
      render: (value: any) => {
        return (
          <td>
            {Number(value) <= 0 ? '0' : `${formatPrice(parseInt(value))}`}
          </td>
        )
      },
    },
  ]

  return (
    <div>
      <PageHeader
        style={{ backgroundColor: 'white', margin: '5px 10px 10px' }}
        title="Sản phẩm"
        extra={[
          <ExportCsv
            loading={isLoadingBtnExportData}
            onClick={fn => onExportDataToExcel(fn)}
            sheetName={['ProductList']}
            sheets={{
              ProductList: ExportCsv.getSheets(dataExport),
            }}
            fileName="Dánh sách sản phẩm"
          >
            <DownloadOutlined />
            &nbsp; Export
          </ExportCsv>,
        ]}
      />

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <Filter
          listStock={listStock}
          checkedTable={checkedTable}
          isSearchLoading={isSearchLoading}
          onSearchSubmit={(searchKey: string) => {
            setIsSearchLoading(true)
            setParams({ ...params, search: searchKey, page: 1 })
          }}
          onStatusSubmit={(statusKey: string) => {
            setParams({ ...params, status: statusKey, page: 1 })
          }}
          onCategorySubmit={(category_id: string) => {
            setParams({
              ...params,
              category_id: parseInt(category_id),
              children_category_id: 0,
              page: 1,
            })
          }}
          onChildCategorySubmit={(children_category_id: string) => {
            setParams({
              ...params,
              children_category_id: parseInt(children_category_id),
              page: 1,
            })
          }}
          onShopSubmit={(shop_id: string) => {
            if (shop_id) {
              setParams({
                ...params,
                shop_id: shop_id,
                stock_id: '',
                page: 1,
              })
            } else {
              setParams({
                ...params,
                shop_id: shop_id,
                stock_id: shop_id,
                page: 1,
              })
            }
          }}
          onStockSubmit={(stock_id: string) => {
            setParams({ ...params, stock_id: stock_id, page: 1 })
          }}
        />
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '5px 10px 15px',
        }}
      >
        <p style={{ padding: '20px 20px 0px' }}>
          Kết quả lọc: <b>{paging.total}</b>
        </p>
        <Table
          className="table-expanded-custom-no-image"
          rowSelection={{
            ...rowSelection,
          }}
          scroll={{
            x: 800,
            scrollToFirstRowOnChange: true,
            // y: 'calc(100vh - 430px)',
            y: `calc(${heightWeb}px - 410px)`,
          }}
          // expandedRowKeys={[currentSelected.id]}
          onRow={(record, rowIndex) => ({
            onDoubleClick: () => {
              history.push({
                pathname: `${ADMIN_ROUTER_PATH.PRODUCT_DETAIL}/${record.id}`,
                state: record,
              })
            },
            onMouseEnter: event => {
              setItemSelected({ id: record.id, rowIndex: Number(rowIndex) })
            },
          })}
          style={{ margin: '10px 20px' }}
          dataSource={dataTable}
          bordered
          columns={columns}
          loading={isLoading}
          pagination={{
            ...paging,
            showSizeChanger: false,
            onChange: async (page, pageSize) => {
              setParams({ ...params, page })
            },
          }}
        />
      </div>
    </div>
  )
}
