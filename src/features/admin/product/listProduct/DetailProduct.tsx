import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  PauseCircleOutlined,
} from '@ant-design/icons'
import { Button, message, PageHeader, Popconfirm, Spin, Tabs } from 'antd'
import { ADMIN_ROUTER_PATH } from 'common/config'
import React, { useState, useEffect } from 'react'
import { useHistory, useParams, useLocation } from 'react-router-dom'
import OrderList from './components/OrderList'
import ProductInfo from './components/ProductInfo'
import SaleInfo from './components/SaleInfo'
import { changeStatusProduct, deleteProducts } from './ProductApi'

const { TabPane } = Tabs

type Props = { location: any }

export default function DetailProduct() {
  const history = useHistory()
  const params: any = useParams()
  const location: any = useLocation()
  const [statusProduct, setStatusProduct] = useState(location.state.status)
  const [isLoading, setIsLoading] = useState(false)
  const [statusShop, setStatusShop] = useState(0)
  const [productInfor, setProductInfor] = useState({
    create_at: '',
    delete_at: null,
    id: 0,
    is_active: 1,
    livestream_enable: 1,
    name: '',
    phone: '',
    profile_picture_url: '',
    status: 1,
    stream_minutes_available: 0,
    update_at: '',
    email: '',
  })

  // useEffect(() => {
  //   setStatusProduct(location.state.status)
  // }, [])

  const onDeleteProduct = async (id: any) => {
    try {
      const payload = { id }
      const data = await deleteProducts(payload)
      message.success(data?.message)
      history.push({
        pathname: `${ADMIN_ROUTER_PATH.PRODUCT}`,
      })
    } catch (error) {
      console.log(error)
    }
  }

  const onChangeStatus = async () => {
    const payload = {
      id: params.id,
      body: {
        status: statusProduct ? 0 : 1,
      },
    }
    try {
      setIsLoading(true)
      const res = await changeStatusProduct(payload)
      if (res.code === 1) {
        setStatusProduct(payload.body.status)
      }
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  return (
    // <Spin size="large" spinning={isLoading}>
    <>
      <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
        <PageHeader
          onBack={() => {
            history.goBack()
          }}
          title=" Chi tiết sản phẩm"
          extra={[
            <Popconfirm
              placement="bottomRight"
              title="Bạn có chắc chắn muốn xoá?"
              onConfirm={() => {
                onDeleteProduct(params.id)
              }}
              okText="Xoá"
              cancelText="Huỷ"
              okButtonProps={{
                danger: true,
                type: 'primary',
              }}
            >
              <Button danger>
                <CloseCircleOutlined />
                Xoá sản phẩm
              </Button>
            </Popconfirm>,
            statusProduct ? (
              <Button
                type="primary"
                danger
                loading={isLoading}
                onClick={() => {
                  onChangeStatus()
                }}
              >
                <PauseCircleOutlined />
                Ngừng hoạt động
              </Button>
            ) : (
              <Button
                style={{
                  backgroundColor: '#00abba',
                  borderColor: '#00abba',
                  color: 'white',
                }}
                loading={isLoading}
                onClick={() => {
                  onChangeStatus()
                }}
              >
                <CheckCircleOutlined />
                Hoạt động
              </Button>
            ),
          ]}
        />
      </div>
      <div style={{ margin: '5px 10px 15px' }}>
        <Tabs
          style={{
            padding: '0 10px',
            fontWeight: 'bold',
            backgroundColor: 'white',
          }}
        >
          <TabPane tab="Thông tin sản phẩm" key="1">
            <ProductInfo
              statusProduct={statusProduct}
              // id_product={params.id}
              // onChageStatus={getStatus}
              // productInfor={productInfor}
            />
          </TabPane>
          <TabPane tab="Khách hàng mua sản phẩm" key="2">
            <SaleInfo
            // id_sale={params.id}
            // onChageStatus={getStatus}
            // saleInfor={saleInfor}
            />
          </TabPane>
          <TabPane tab="Danh sách đơn hàng" key="3">
            <OrderList
            // id_sale={params.id}
            // onChageStatus={getStatus}
            // saleInfor={saleInfor}
            />
          </TabPane>
        </Tabs>
      </div>
    </>
    // </Spin>
  )
}
