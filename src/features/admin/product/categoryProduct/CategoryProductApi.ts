import { ApiClient } from 'services/ApiService'

export const getCategory = (payload: any) => ApiClient.get('/category', payload)

export const createCategory = (payload: any) =>
  ApiClient.post(`/category`, payload)

export const updateCategory = (payload: any) =>
  ApiClient.put(`/category/${payload.id}`, payload.data)

export const deleteCategory = (payload: any) =>
  ApiClient.delete(`/category/${payload.id}`, payload.delete)

export const categoryChangeStatus = (payload: any) =>
  ApiClient.put(`/category/${payload.id}/change-status`, payload.status)

export const requestUploadImageCategory = (payload: any) =>
  ApiClient.post(`/files/upload/single/${payload.type}`, payload.data)
