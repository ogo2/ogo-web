import { LoadingOutlined, PlusOutlined } from '@ant-design/icons'
import {
  Modal,
  Input,
  Form,
  InputNumber,
  Button,
  message,
  Upload,
  Row,
  Col,
} from 'antd'
import ButtonBottomModal from 'common/components/ButtonBottomModal'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import createFormDataImage from 'utils/createFormDataImage'
import { requestUploadImageCategory } from '../CategoryProductApi'

type Props = {
  visible: boolean
  onCancel?: any
  data?: any
  onCreateNewCategory?: any
  onUpdateCategory?: any
  // isLoading: boolean
  isLoadingButton: boolean
}

function getBase64(img: any, callback: any) {
  const reader = new FileReader()
  reader.addEventListener('load', () => callback(reader.result))
  reader.readAsDataURL(img)
}

function beforeUpload(file: any) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png'
  if (!isJpgOrPng) {
    message.error('Xảy ra lỗi! Bạn chỉ có thể upload ảnh có dạng JPG/PNG!')
  }
  const isLt2M = file.size / 1024 / 1024 < 2
  if (!isLt2M) {
    message.error('Cho phép ảnh có dung lượng tối đa là 2MB')
  }
  return isJpgOrPng && isLt2M
}

function convertDataToFrom(data: any) {
  if (!data) {
    return {
      name: null,
      order: null,
      expired_at: null,
      icon_url: null,
    }
  } else {
    return {
      ...data,
      name: data.name,
      date_of_birth: moment.unix(1616620499),
      expired_at: moment.unix(data.expired_at),
    }
  }
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
}

export const AddEditCategory = ({
  visible,
  onCancel,
  data,
  onCreateNewCategory,
  onUpdateCategory,
  // isLoading,
  isLoadingButton,
}: Props) => {
  const [upload, setUpload] = useState({
    loading: false,
    imageUrl: '',
  })

  const [form] = Form.useForm()

  const initialValues = convertDataToFrom(data)

  const handleChange = (info: any) => {
    if (info.file.status === 'uploading') {
      setUpload({
        imageUrl: '',
        loading: true,
      })
      return
    }

    if (info.file.status === 'done' || info.file.status === 'error') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, (imageUrl: any) =>
        setUpload({
          imageUrl: imageUrl,
          loading: false,
        })
      )
    }
  }

  const uploadButton = (
    <div>
      {upload.loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  )

  const onFinish = async (values: any, onCancel: any) => {
    try {
      if (values.icon_url.fileList) {
        var resUploadImage = []
        var last_element = []

        if (values.icon_url.fileList.length > 1) {
          last_element = [values.icon_url.fileList.slice(-1)[0]]
        } else {
          last_element = values.icon_url.fileList
        }
        const dataImage = await createFormDataImage(last_element)
        console.log('dataImage', dataImage)

        const payloadImage = {
          type: 0,
          data: dataImage,
        }
        resUploadImage = await requestUploadImageCategory(payloadImage)
        console.log('resUploadImage', resUploadImage)
      }

      let newData

      if (!data) {
        newData = {
          ...values,
          icon_url: resUploadImage.data.path,
        }
        onCreateNewCategory(newData, form.resetFields, setUpload)
      } else {
        if (data.icon_url === values.icon_url) {
          newData = {
            ...values,
            id: data.id,
          }
        } else {
          newData = {
            ...values,
            id: data.id,
            icon_url: resUploadImage.data.path,
          }
        }
        console.log(newData)
        console.log(data)
        onUpdateCategory(newData, form.resetFields)
      }
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    if (data) {
      setUpload({
        ...upload,
        imageUrl: data.icon_url,
      })
      console.log(data)
    }
  }, [data])

  return (
    <Modal
      onCancel={() => {
        form.resetFields()
        onCancel()
      }}
      maskClosable={false}
      footer={null}
      title={data ? 'Sửa danh mục' : 'Thêm danh mục cha'}
      visible={visible}
    >
      <Form
        {...formItemLayout}
        form={form}
        name="register"
        labelAlign="left"
        onFinish={(values: any) => onFinish(values, onCancel)}
        initialValues={initialValues}
        scrollToFirstError
      >
        <Form.Item
          label="Tên danh mục"
          name="name"
          rules={[
            {
              required: true,
              message: 'Vui lòng điền tên danh mục',
            },
            {
              whitespace: true,
              message: 'Không được nhập khoảng trắng!',
            },
            {
              max: 50,
              message: 'Vui lòng không nhập quá 50 ký tự!',
            },
          ]}
        >
          <Input placeholder="Tên danh mục" />
        </Form.Item>
        <Form.Item
          name="order"
          label="STT hiển thị"
          rules={[
            {
              type: 'number',
              message: 'Vui lòng nhập số',
            },
            {
              required: true,
              message: 'Vui lòng nhập STT',
            },
          ]}
        >
          <InputNumber min={1} placeholder="Nhập STT hiển thị" />
        </Form.Item>
        <Form.Item
          label="Icon danh mục"
          name="icon_url"
          rules={[
            {
              required: true,
              message: 'Vui lòng chọn icon danh mục!',
            },
          ]}
        >
          <Upload
            name="icon_url"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            beforeUpload={beforeUpload}
            onChange={handleChange}
          >
            {upload.imageUrl ? (
              <img
                src={upload.imageUrl}
                alt="avatar"
                style={{ width: '100%', objectFit: 'contain', height: '100%' }}
              />
            ) : (
              uploadButton
            )}
          </Upload>
        </Form.Item>
        <ButtonBottomModal
          isLoadingButton={isLoadingButton}
          onCancel={onCancel}
          text={data ? 'Cập nhật' : 'Thêm danh mục'}
        />
      </Form>
    </Modal>
  )
}
