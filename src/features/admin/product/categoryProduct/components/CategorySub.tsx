import {
  Button,
  DatePicker,
  Col,
  Row,
  message,
  PageHeader,
  Popconfirm,
  Space,
  Switch,
  Table,
  Tag,
} from 'antd'
import React, { useEffect, useState } from 'react'
import reactotron from 'ReactotronConfig'
import { ProductModel } from '../../Model'
import {
  categoryChangeStatus,
  createCategory,
  deleteCategory,
  getCategory,
  updateCategory,
} from '../CategoryProductApi'
import 'moment/locale/vi'
import moment from 'moment'
import Icon, {
  DeleteFilled,
  EditOutlined,
  PlusCircleOutlined,
} from '@ant-design/icons'
import { CATEGORY_STATUS } from 'utils/constants'
import { FilterSub } from './FilterSub'
import AddEditCategorySub from './AddEditCategorySub'
import './css/Header.css'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import ButtonFixed from 'common/components/ButtonFixed'

type Props = {
  categoryId: number
  categoryName: string
  getDataCategory: any
}

const { RangePicker } = DatePicker

const CategorySub = ({ categoryId, categoryName, getDataCategory }: Props) => {
  console.log(categoryId, categoryName)
  const [categorySub, setCategorySub] = useState(false)
  const [itemSelected, setItemSelected] = useState({ id: 0, rowIndex: 0 })
  const [category, setCategory] = useState<any>([])
  const [isLoading, setIsLoading] = useState(true)
  const [showAddCategorySub, setShowAddCategorySub] = useState(false)
  const [isLoadingModal, setIsLoadingModal] = useState(false)
  const [currentSelected, setCurrentSelected] = useState({ id: -1 })
  const [value, setValue] = useState({})
  const [isSubModalVisible, setIsSubModalVisible] = useState(false)
  const [searchKey, setSearchKey] = useState('')
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [categorySubList, setCategorySubList] = useState([
    {
      key: 1,
      id: 0,
      name: '',
      order: 0,
      create_at: 0,
      status: '',
    },
  ])
  const [params, setParams] = useState({
    search: '',
    page: 1,
    status: '',
    parent_id: categoryId,
    from_date: '',
    to_date: '',
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const getData = async () => {
    setIsLoading(true)
    try {
      const res = await getCategory(params)
      console.log(res.data)
      const tableData = res.data.map((item: ProductModel) => {
        return { ...item, key: item.id }
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setCategory(tableData)
      setPaging(formattedPaging)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  useEffect(() => {
    getData()
  }, [params])

  const onUpdateSubCategory = (record: any) => {
    setValue(record)
    setIsSubModalVisible(true)
  }

  const onDeleteCategorySub = async (record: any) => {
    try {
      await deleteCategory(record)
      message.success(`Đã xoá danh mục`)
      getData()
    } catch (error) {
      // message.error(`Đã có lỗi xảy ra, vui lòng thử lại`)
      console.log(error)
    } finally {
      getDataCategory()
    }
  }

  const editCategorySub = async (data: any, resetFields: any) => {
    setIsLoadingModal(true)
    try {
      delete data.parent_category

      const payload = {
        id: data.id,
        data: data,
      }
      delete payload.data.id
      await updateCategory(payload)
      message.success(`Cập nhật thành công`)

      resetFields()
      setIsSubModalVisible(false)
      getData()
    } catch (error) {
      message.error(message)
    } finally {
      //setIsLoadingModal(false)
      getDataCategory()
    }
  }

  const createCategorySub = async (data: any, resetFields: any) => {
    console.log('data', data)
    setIsLoadingModal(true)
    try {
      delete data.parent_category

      const res = await createCategory({
        ...data,
        parent_id: data.parent_id,
      })
      console.log(res)
      setCategory([res.data, ...category])
      message.success(`Thêm mới thành công`)
      setShowAddCategorySub(false)
      reactotron.log!(resetFields)
      resetFields()
    } catch (error) {
      console.log(error)
      // message.error(error.message)
    } finally {
      setIsLoadingModal(false)
      setIsLoading(false)
      getDataCategory()
    }
  }

  const onChangeStatus = async (record: any) => {
    try {
      const payload = {
        id: record.id,
        status: {
          status: record.status === 0 ? 1 : 0,
        },
      }
      await categoryChangeStatus(payload)
      message.success(`Cập nhật trạng thái thành công`)
      getData()
    } catch (error) {
      message.error(`Đã có lỗi xảy ra, vui lòng thử lại`)
    } finally {
      //setIsLoadingModal(false)
    }
  }

  const changeStatus = async (checked: any, e: any) => {
    e.stopPropagation()
    setIsLoading(true)
    try {
      let data = await categoryChangeStatus(itemSelected.id)
      categorySubList[itemSelected.rowIndex].status = data.data.status
    } catch (error) {
      console.log('error', error)
    } finally {
      setIsLoading(false)
    }
  }

  const contentView = () => {
    const columns = [
      {
        title: 'Tên danh mục',
        dataIndex: 'name',
        key: 'name',
      },
      // {
      //   title: 'Trạng thái',
      //   dataIndex: 'status',
      //   key: 'status',
      //   render: (value: any, record: any) => (
      //     <div style={{ textAlign: 'center' }}>
      //       <Switch
      //         defaultChecked={record.status === 1 ? true : false}
      //         size="small"
      //         onChange={() => onChangeStatus(record)}
      //       />
      //     </div>
      //   ),
      // },
      {
        title: 'Thứ tự hiện thị',
        dataIndex: 'order',
        key: 'order',
      },
      {
        title: 'Ngày tạo',
        dataIndex: 'create_at',
        key: 'create_at',
        render: (data: any) => <div>{moment(data).format('DD-MM-YYYY')}</div>,
      },
      {
        title: 'Trạng thái',
        dataIndex: 'status',
        key: 'status',
        render: (value: any) => (
          <Tag
            style={{ textAlign: 'center' }}
            color={value === 1 ? 'green' : 'volcano'}
          >
            {value === CATEGORY_STATUS.ACTIVE ? 'HOẠT ĐỘNG' : 'TẠM DỪNG'}
          </Tag>
        ),
      },
      {
        title: 'Chức năng',
        dataIndex: 'status',
        // key: 'status',
        render: (text: any, record: any) => (
          <div style={{ textAlign: 'center', cursor: 'pointer' }}>
            <Space size="middle">
              {/* {record === 1 ? (
                <Switch
                  size="small"
                  onChange={() => changeStatus(record.sta)}
                  style={{ backgroundColor: '#00abba' }}
                />
              ) : (
                <Switch size="small" onChange={() => changeStatus(record)} />
              )} */}
              <Switch
                // style={{ backgroundColor: '#00abba' }}
                defaultChecked={record.status === 1 ? true : false}
                size="small"
                onChange={() => onChangeStatus(record)}
              />
              <Button
                onClick={() => {
                  onUpdateSubCategory(record)
                }}
                type="text"
                size="large"
                icon={<EditOutlined />}
                style={{ color: '#1890ff' }}
              />
              <Popconfirm
                title={'Bạn chắc chắn muốn xoá danh mục này'}
                onConfirm={async () => {
                  try {
                    onDeleteCategorySub(record)
                  } catch (error) {
                    console.log(error)
                  } finally {
                  }
                }}
                okText="Xoá"
                cancelText="Quay lại"
                okButtonProps={{
                  danger: true,
                  type: 'primary',
                }}
              >
                <Button
                  type="text"
                  size="large"
                  icon={<DeleteFilled />}
                  style={{ color: 'red' }}
                />
              </Popconfirm>
            </Space>
          </div>
        ),
      },
    ]

    const handleChangeTime = (dates: any, dateStrings: any[]) => {
      setParams({
        ...params,
        to_date: dateStrings[1],
        from_date: dateStrings[0],
        page: 1,
      })
    }

    const onSearchSubmit = (searchKey: string) => {
      setIsSearchLoading(true)
      setParams({ ...params, search: searchKey, page: 1 })
    }

    return (
      <div>
        {isSubModalVisible && (
          <AddEditCategorySub
            isLoadingButton={isLoading}
            visible={isSubModalVisible}
            // isLoading={isLoading}
            categoryName={categoryName}
            categoryId={categoryId}
            data={value}
            onCancel={() => {
              setIsSubModalVisible(false)
              getData()
            }}
            onUpdateCategory={(newData: any, resetFields: any) => {
              editCategorySub(newData, resetFields)
              setIsLoading(true)
            }}
          />
        )}

        {showAddCategorySub && (
          <AddEditCategorySub
            visible={showAddCategorySub}
            // isLoading={isLoadingModal}
            isLoadingButton={isLoadingModal}
            categoryName={categoryName}
            categoryId={categoryId}
            onCancel={() => {
              setShowAddCategorySub(false)
              getData()
            }}
            onCreateNewCategory={(newData: any, resetFields: any) => {
              createCategorySub(newData, resetFields)
              setIsLoading(false)
            }}
          />
        )}
        <div>
          <PageHeader
            style={{ backgroundColor: 'white', margin: '0px 0px 5px' }}
            extra={[
              <Row gutter={8}>
                {/* <FilterSub
                  setIsCreate={setShowAddCategorySub}
                  onSearchSubmit={(searchKey: string) => {
                    setParams({ ...params, search: searchKey, page: 1 })
                  }}
                /> */}
                <Col>
                  <TypingAutoSearch
                    onSearchSubmit={(key: string) => {
                      onSearchSubmit(key.trim())
                    }}
                    isSearchLoading={isSearchLoading}
                    placeholder="Tên danh mục con ..."
                  />
                </Col>
                <Col>
                  <Space>
                    <RangePicker
                      className="rangerpicker-categorySub"
                      onChange={(dates, dateStrings) => {
                        handleChangeTime(dates, dateStrings)
                      }}
                      placeholder={['Từ ngày', 'Đến ngày']}
                      // onChange={handleChangeTime}
                    />
                  </Space>
                </Col>
                <Col>
                  {/* <Button
                    style={{
                      backgroundColor: '#00abba',
                      borderColor: '#00abba',
                      color: 'white',
                    }}
                    onClick={() => {
                      setShowAddCategorySub(true)
                      // setValue('')
                    }}
                  >
                    <PlusCircleOutlined />
                    Thêm danh mục con
                  </Button> */}
                  <ButtonFixed
                    setIsCreate={setShowAddCategorySub}
                    text="Thêm danh mục con"
                    type="add"
                  />
                </Col>
              </Row>,
            ]}
          />

          <Table
            size="small"
            style={{
              padding: '10px',
              backgroundColor: 'white',
            }}
            columns={columns}
            onRow={r => ({
              onClick: () => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
              // onMouseEnter: event => {
              //   setItemSelected({ id: record.id, rowIndex: Number(rowIndex) })
              // },
            })}
            scroll={{
              x: 800,
              scrollToFirstRowOnChange: true,
              y: 'calc(100vh - 280px)',
            }}
            loading={isLoading}
            dataSource={category}
            expandedRowKeys={[currentSelected.id]}
            bordered
            pagination={{
              ...paging,
              showSizeChanger: false,
              onChange: async (page, pageSize) => {
                setParams({ ...params, page })
              },
            }}
          />
        </div>
      </div>
    )
  }
  return contentView()
}
export default CategorySub
