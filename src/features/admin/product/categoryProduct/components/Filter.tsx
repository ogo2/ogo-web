import Icon from '@ant-design/icons'
import { Button, Collapse, DatePicker, Input, Row, Select, Space } from 'antd'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import React, { useEffect, useState } from 'react'
import { IS_ACTIVE } from 'utils/constants'
import './css/Header.css'

const { Panel } = Collapse
const { RangePicker } = DatePicker
const { Option } = Select

type Props = {
  setIsCreate: any
  onSearchSubmit: any
  onChangeStatus?: any
  isSearchLoading: boolean
}

function Filter({
  onChangeStatus,
  setIsCreate,
  onSearchSubmit,
  isSearchLoading,
}: Props) {
  const [searchKey, setSearchKey] = useState('')
  const [params, setParams] = useState({
    search: '',
    page: 1,
    limit: '',
    // id: categoryId,
    type: 0,
    // status: 1,
    // province_id: '',
  })

  function callback(key: any) {
    console.log(key)
  }

  // useEffect(() => {
  //   setValue('')
  //   onChangeStatus('')
  // }, [])

  // const onChange = (e: any) => {
  //   setValue(e.target.value)
  //   onChangeStatus(e.target.value)
  // }

  function handleChangeType(value: any) {
    setParams({
      ...params,
      type: value,
    })
  }

  return (
    <Row gutter={16}>
      {/* <Collapse defaultActiveKey={['1']} onChange={callback}>
        <StyledPanel header="Danh mục sản phẩm" key="1"> */}
      {/* <Input.Search
        className="input-search_category"
        allowClear
        placeholder="Nhập tên danh mục ..."
        addonAfter={
          <Icon
            type="close-circle-o"
            onClick={() => {
              onSearchSubmit('')
            }}
          />
        }
        onKeyDown={e => {
          if (e.keyCode == 13) {
            onSearchSubmit(searchKey)
          }
        }}
        onSearch={(value, event) => {
          onSearchSubmit(value.trim())
        }}
        onChange={e => {
          setSearchKey(e.target.value)
        }}
      /> */}

      <TypingAutoSearch
        onSearchSubmit={(searchKey: string) => {
          onSearchSubmit(searchKey.trim())
        }}
        isSearchLoading={isSearchLoading}
        placeholder="Nhập tên danh mục ..."
      />
      {/* </StyledPanel>
      </Collapse> */}

      {/* <Collapse
        style={{
          marginTop: '10px',
        }}
        defaultActiveKey={['1']}
        onChange={callback}
      >
        <StyledPanel header="Trạng thái" key="1"> */}
      <Select
        allowClear
        placeholder="Trạng thái"
        className="select-status_category"
        onChange={handleChangeType}
      >
        <Option value={IS_ACTIVE.ACTIVE}>Hoạt động</Option>
        <Option value={IS_ACTIVE.INACTIVE}>Tạm dừng</Option>
      </Select>
      {/* </StyledPanel>
      </Collapse> */}

      {/* <Space direction="vertical" className="rangePicker_category"> */}
      <RangePicker />
      {/* </Space> */}
      <Button
        type="primary"
        onClick={() => {
          setIsCreate(true)
        }}
      >
        Thêm mới
      </Button>
    </Row>
  )
}
export default Filter
