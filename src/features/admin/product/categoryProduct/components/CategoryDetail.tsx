import {
  CheckCircleOutlined,
  CloseCircleOutlined,
  DeleteFilled,
  EditOutlined,
} from '@ant-design/icons'
import {
  Button,
  Descriptions,
  Tabs,
  Popconfirm,
  Card,
  message,
  Image,
  Row,
  Col,
} from 'antd'
import moment from 'moment'
import { useState } from 'react'
import { CHANGE_STATUS, STATUS } from 'utils/constants'
import CategorySub from './CategorySub'
import { categoryChangeStatus, updateCategory } from '../CategoryProductApi'
import { AddEditCategory } from './AddEditCategory'

type Props = {
  data: any
  getData: any
  onDeleteCategory: any
}

const { TabPane } = Tabs

function callback(key: any) {
  console.log(key)
}

const ContentView = (data: any) => {
  return (
    <Row gutter={[16, 16]}>
      <Col sm={6}>
        <Descriptions size="default" column={3}>
          <Descriptions.Item label="Icon danh mục">
            <Image
              style={{ objectFit: 'cover', width: '100px', height: '100px' }}
              src={data.icon_url}
            />
          </Descriptions.Item>
        </Descriptions>
      </Col>
      <Col sm={18}>
        <Descriptions size="default" column={3}>
          <Descriptions.Item label="Tên danh mục">
            {data.name}
          </Descriptions.Item>
          <Descriptions.Item label="Thứ tự hiển thị">
            {data.order}
          </Descriptions.Item>
          <Descriptions.Item label="Ngày tạo">
            {moment(data.create_at).format('DD-MM-YYYY')}
          </Descriptions.Item>
        </Descriptions>
      </Col>
    </Row>
  )
}

function CategoryDetail({ data, getData, onDeleteCategory }: Props) {
  const [isLoading, setIsLoading] = useState(true)
  const [showEditCategory, setShowEditCategory] = useState(false)
  const onChangeStatus = async () => {
    try {
      const payload = {
        id: data.id,
        status: {
          status: data.status === 0 ? 1 : 0,
        },
      }
      await categoryChangeStatus(payload)
      message.success(`Cập nhật trạng thái thành công`)
      getData()
    } catch (error) {
      console.log(error)
    } finally {
      //setIsLoadingModal(false)
    }
  }

  const updateCategorys = async (data: any, resetFields: any) => {
    try {
      let dataPush = {
        id: data.id,
        data: {
          name: data.name,
          order: data.order,
          icon_url: data.icon_url,
        },
      }
      console.log(dataPush)
      await updateCategory(dataPush)
      message.success(`Cập nhật thành công`)
      setShowEditCategory(false)
      getData()
      // setShowEditAccount(false)
      resetFields()
    } catch (error) {
      console.log(error)
      // message.error(`Đã có lỗi xảy ra, vui lòng thử lại`)
    }
  }

  return (
    <div>
      {showEditCategory && (
        <AddEditCategory
          data={data}
          visible={showEditCategory}
          isLoadingButton={false}
          onCancel={() => {
            setShowEditCategory(false)
            getData()
          }}
          onUpdateCategory={(newData: any, resetFields: any) => {
            updateCategorys(newData, resetFields)
            setIsLoading(true)
            getData()
          }}
        />
      )}
      <Tabs
        defaultActiveKey="1"
        onChange={callback}
        style={{ padding: '0px 10px' }}
      >
        <TabPane tab="Thông tin danh mục" key="1">
          <Card
            style={{
              backgroundColor: '#f6f9ff',
              borderColor: '#1890ff',
              borderTop: 'none',
            }}
            actions={[
              <Popconfirm
                title={
                  data.status === STATUS.ACTIVE
                    ? 'Bạn chắc chắn muốn tạm dừng hoạt động danh mục này'
                    : 'Bạn chắc chắn muốn bật hoạt động danh mục này'
                }
                onConfirm={() => {
                  onChangeStatus()
                }}
                okText={
                  data.status === STATUS.ACTIVE
                    ? CHANGE_STATUS.INACTIVE
                    : CHANGE_STATUS.ACTIVE
                }
                cancelText="Quay lại"
                okButtonProps={
                  data.status === STATUS.ACTIVE
                    ? {
                        danger: true,
                        type: 'primary',
                      }
                    : {
                        danger: false,
                        type: 'primary',
                      }
                }
              >
                <Button
                  type="text"
                  size="large"
                  // onClick={onChangeStatus}
                  style={{
                    color: data.status === STATUS.ACTIVE ? 'red' : '#389e0d',
                  }}
                  icon={
                    data.status === STATUS.ACTIVE ? (
                      <CloseCircleOutlined />
                    ) : (
                      <CheckCircleOutlined />
                    )
                  }
                >
                  {data.status === 1
                    ? CHANGE_STATUS.INACTIVE
                    : CHANGE_STATUS.ACTIVE}
                </Button>
              </Popconfirm>,

              <Button
                onClick={() => {
                  setShowEditCategory(true)
                }}
                type="text"
                size="large"
                icon={<EditOutlined />}
                style={{ color: '#1890ff' }}
              >
                Chỉnh sửa
              </Button>,

              <Popconfirm
                title={'Bạn chắc chắn muốn xoá danh mục này'}
                onConfirm={async () => {
                  try {
                    onDeleteCategory(data.id)
                  } catch (error) {
                    console.log(error)
                  }
                }}
                okText="Xoá"
                cancelText="Quay lại"
                okButtonProps={{
                  danger: true,
                  type: 'primary',
                }}
              >
                <Button
                  type="text"
                  size="large"
                  icon={<DeleteFilled />}
                  style={{ color: 'red' }}
                >
                  Xoá danh mục
                </Button>
              </Popconfirm>,
            ]}
          >
            {ContentView(data)}
          </Card>
        </TabPane>
        <TabPane tab="Danh mục con" key="2">
          <div>
            <CategorySub
              getDataCategory={getData}
              categoryId={data.id} // Dùng để lấy parentID dùng cho thêm danh mục con
              categoryName={data.name} //Dùng để lấy name dùng để hiển thị tên danh mục cha
            />
          </div>
        </TabPane>
      </Tabs>
    </div>
  )
}

export default CategoryDetail
