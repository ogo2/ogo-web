import {
  Col,
  DatePicker,
  message,
  PageHeader,
  Row,
  Select,
  Table,
  Tag,
} from 'antd'
import ButtonFixed from 'common/components/ButtonFixed'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import moment from 'moment'
import 'moment/locale/vi'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import reactotron from 'ReactotronConfig'
import { IS_ACTIVE } from 'utils/constants'
import {
  createCategory,
  deleteCategory,
  getCategory,
} from '../CategoryProductApi'
import { AddEditCategory } from './AddEditCategory'
import CategoryDetail from './CategoryDetail'
import './css/Header.css'

const { Option } = Select
const { RangePicker } = DatePicker

const Category = () => {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const [category, setCategory] = useState<any>([])
  const [isLoading, setIsLoading] = useState(true)
  const [showAddCategory, setShowAddCategory] = useState(false)
  const [isLoadingModal, setIsLoadingModal] = useState(false)
  const [currentSelected, setCurrentSelected] = useState({ id: -1 })
  // const [searchKey, setSearchKey] = useState('')
  // const [value, setValue] = useState(0)
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [params, setParams] = useState({
    search: '',
    page: 1,
    status: '',
    // parent_id: '',
    from_date: '',
    to_date: '',
    type: 0,
    limit: 24,
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const getData = async () => {
    setIsLoading(true)
    try {
      const res = await getCategory(params)
      console.log(res.data)
      const tableData = res.data.map((item: any) => {
        return { ...item, key: item.id }
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setPaging(formattedPaging)
      setCategory(tableData)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  useEffect(() => {
    getData()
  }, [params])

  const createCategorys = async (
    data: any,
    resetFields: any,
    setUpload: any
  ) => {
    setIsLoadingModal(true)
    try {
      const res = await createCategory(data)
      res.data = { ...res.data, key: res.data.id }
      console.log(res.data)
      setCategory([{ ...res.data, total_children_category: 0 }, ...category])
      message.success('Thêm thành công')
      setShowAddCategory(false)
      reactotron.log!(resetFields)
      resetFields()
      setUpload({
        loading: false,
        imageUrl: '',
      })
    } catch (error) {
      console.log(error)
      // message.error(`Đã có lỗi xảy ra, vui lòng thử lại`)
    } finally {
      setIsLoadingModal(false)
    }
    // getData()
  }
  const contentView = () => {
    const columns = [
      {
        title: 'Tên danh mục',
        dataIndex: 'name',
        key: 'name',
      },
      {
        title: 'Danh mục con',
        dataIndex: '',
        key: '',
        render: (record: any) => (
          <td>
            {record.children_category ? record?.children_category.length : 0}
          </td>
        ),
      },
      {
        title: 'Trạng thái',
        dataIndex: 'status',
        key: 'status',
        render: (value: any) => (
          <Tag color={value === 1 ? 'green' : 'volcano'}>
            {value === 1 ? 'HOẠT ĐỘNG' : 'TẠM DỪNG'}
          </Tag>
        ),
      },
      {
        title: 'Thứ tự hiển thị',
        dataIndex: 'order',
        key: 'order',
      },
      {
        title: 'Ngày tạo',
        dataIndex: 'create_at',
        key: 'create_at',
        render: (data: any) => <div>{moment(data).format('DD-MM-YYYY')}</div>,
      },
    ]

    function handleChangeStatus(value: any) {
      setParams({
        ...params,
        status: value,
        page: 1,
      })
      console.log(value)
    }

    // function handleChangeTime(dates: any[], dateStrings: any[]) {
    //   setParams({
    //     ...params,
    //     to_date: dateStrings[1],
    //     from_date: dateStrings[0],
    //   })
    //   console.log(dateStrings)
    // }

    const handleChangeTime = (dates: any, dateStrings: any[]) => {
      setParams({
        ...params,
        to_date: dateStrings[1],
        from_date: dateStrings[0],
        page: 1,
      })
    }

    const onSearchSubmit = (searchKey: string) => {
      setIsSearchLoading(true)
      setParams({ ...params, search: searchKey, page: 1 })
    }

    return (
      <div>
        <PageHeader
          title="Danh mục sản phẩm"
          style={{ backgroundColor: 'white', margin: '5px 10px 10px' }}
          extra={[
            <Row
              gutter={[16, 16]}
              justify="end"
              style={{
                marginRight: '30px',
                // width: '90%',
              }}
            >
              <Col
                span={8}
                // className="input-search_category"
              >
                <TypingAutoSearch
                  onSearchSubmit={(searchKey: string) => {
                    onSearchSubmit(searchKey.trim())
                  }}
                  isSearchLoading={isSearchLoading}
                  placeholder="Tên danh mục ..."
                />
              </Col>

              <Col span={5}>
                <Select
                  allowClear
                  placeholder="Trạng thái"
                  // className="select-status_category"
                  onChange={handleChangeStatus}
                >
                  <Option value="">Tất cả</Option>
                  <Option value={IS_ACTIVE.ACTIVE}>Hoạt động</Option>
                  <Option value={IS_ACTIVE.INACTIVE}>Tạm dừng</Option>
                </Select>
              </Col>

              <Col span={7}>
                <RangePicker
                  // className="rangerpicker-category"
                  onChange={(dates, dateStrings) => {
                    handleChangeTime(dates, dateStrings)
                  }}
                  placeholder={['Từ ngày', 'Đến ngày']}
                />
              </Col>

              <Col span={3}>
                <ButtonFixed
                  setIsCreate={setShowAddCategory}
                  text="Thêm mới"
                  type="add"
                />
              </Col>
            </Row>,
          ]}
        />
        <div
          style={{
            backgroundColor: 'white',
            margin: '0px 10px 15px',
            padding: '10px 20px',
          }}
        >
          {/* <p style={{ margin: '10px' }}>
            Kết quả lọc: <b>{paging.total}</b>
          </p> */}
          <Table
            className="table-expanded-custom-no-image"
            scroll={{
              x: 800,
              scrollToFirstRowOnChange: true,
              y: `calc(${heightWeb} - 300px)`,
              // y: 'calc(100vh - 300px)',
            }}
            bordered
            dataSource={category}
            loading={isLoading}
            columns={columns}
            expandedRowKeys={[currentSelected.id]}
            onRow={r => ({
              onClick: () => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            })}
            expandable={{
              expandedRowRender: (record: any) => (
                <CategoryDetail
                  // onUpdateCategory={(newData: any, resetFields: any) => {
                  //   updateCategorys(newData, resetFields)
                  // }}
                  onDeleteCategory={async (id: any) => {
                    try {
                      await deleteCategory({ id: [id] })
                      message.success(`Đã xoá danh mục: ${record.name}`)
                      getData()
                    } catch (error) {
                      console.log(error)
                      // message.error(`Đã có lỗi xảy ra, vui lòng thử lại`)
                    }
                  }}
                  // onChangeStatusCategory={async (id: any) => {
                  //   try {
                  //     await changeStatusCategory({ id: [id] })
                  //     message.success(
                  //       `Đã ngừng hoạt động danh mục: ${record.status}`
                  //     )
                  //     getData()
                  //   } catch (error) {
                  //     message.error(`Đã có lỗi xảy ra, vui lòng thử lại`)
                  //   }
                  // }}
                  // isShowEditCategory={showEditCategory}
                  // setShowEditCategory={(isShow: any) => {
                  //   setShowEditCategory(isShow)
                  // }}
                  data={record}
                  getData={getData}
                />
              ),
              onExpand: (status: any, r: any) => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            }}
            pagination={{
              ...paging,
              showSizeChanger: false,
              onChange: async (page, pageSize) => {
                setParams({ ...params, page })
              },
            }}
          />
        </div>
        <AddEditCategory
          visible={showAddCategory}
          onCancel={() => {
            setShowAddCategory(false)
            getData()
          }}
          onCreateNewCategory={(
            newData: any,
            resetFields: any,
            setUpload: any
          ) => {
            createCategorys(newData, resetFields, setUpload)
          }}
          // isLoading={isLoadingModal}
          isLoadingButton={isLoadingModal}
        />
      </div>
    )
  }
  return contentView()
}
export default Category
