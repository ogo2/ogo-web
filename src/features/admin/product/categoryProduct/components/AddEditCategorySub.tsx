import { LoadingOutlined, PlusOutlined } from '@ant-design/icons'
import {
  Input,
  Form,
  Button,
  InputNumber,
  Upload,
  Modal,
  message,
  Row,
  Col,
} from 'antd'
import { useEffect, useMemo, useState } from 'react'
import createFormDataImage from 'utils/createFormDataImage'
import R from 'utils/R'
import { Spin } from 'antd'
import { requestUploadImageCategory } from '../CategoryProductApi'
import moment from 'moment'
import ButtonBottomModal from 'common/components/ButtonBottomModal'

type Props = {
  // isModalVisible: boolean
  visible: boolean
  // isLoading: boolean
  onCancel?: any
  data?: any
  onCreateNewCategory?: any
  onUpdateCategory?: any
  categoryName?: any
  isErrorAdd?: boolean
  categoryId: any
  isLoadingButton: boolean
}

function getBase64(img: any, callback: any) {
  const reader = new FileReader()
  reader.addEventListener('load', () => callback(reader.result))
  reader.readAsDataURL(img)
}

function beforeUpload(file: any) {
  const isJpgOrPng = file.type === 'image/jpeg' || file.type === 'image/png'
  if (!isJpgOrPng) {
    message.error('Xảy ra lỗi! Bạn chỉ có thể upload ảnh có dạng JPG/PNG!')
  }
  const isLt2M = file.size / 1024 / 1024 < 2
  if (!isLt2M) {
    message.error('Cho phép ảnh có dung lượng tối đa là 2MB')
  }
  return isJpgOrPng && isLt2M
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
}

function AddEditCategorySub({
  visible,
  // isModalVisible,
  // isLoading,
  onCancel,
  data,
  onCreateNewCategory,
  onUpdateCategory,
  categoryName,
  isErrorAdd,
  categoryId,
  isLoadingButton,
}: Props) {
  const [form] = Form.useForm()

  const [upload, setUpload] = useState({
    loading: false,
    imageUrl: '',
  })

  const uploadButton = (
    <div>
      {upload.loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  )

  const handleChange = (info: any) => {
    if (info.file.status === 'uploading') {
      setUpload({
        ...upload,
        loading: true,
      })
      return
    }

    if (info.file.status === 'done' || info.file.status === 'error') {
      // Get this url from response in real world.
      getBase64(info.file.originFileObj, (imageUrl: any) =>
        setUpload({
          imageUrl: imageUrl,
          loading: false,
        })
      )
    }
  }

  const handleErrorAdd = () => {
    if (isErrorAdd === false) {
      setUpload({
        ...upload,
        imageUrl: '',
      })
    }
  }
  useEffect(() => {
    handleErrorAdd()
  }, [isErrorAdd])

  const onFinish = async (values: any) => {
    try {
      console.log(values)
      // Upload image lên Server
      if (values.icon_url) {
        if (values.icon_url.fileList) {
          var resUploadImage = []
          var last_element = []

          if (values.icon_url.fileList.length > 1) {
            last_element = [values.icon_url.fileList.slice(-1)[0]]
          } else {
            last_element = values.icon_url.fileList
          }
          const dataImage = await createFormDataImage(last_element)
          const payloadImage = {
            type: 0,
            data: dataImage,
          }
          resUploadImage = await requestUploadImageCategory(payloadImage)
          console.log(resUploadImage)
        }
      }

      let newData

      if (!data) {
        if (values.icon_url) {
          newData = {
            ...values,
            parent_id: categoryId,
            icon_url: resUploadImage.data.path,
          }
        } else {
          newData = {
            ...values,
            parent_id: categoryId,
            icon_url: '',
          }
        }
        onCreateNewCategory(newData, form.resetFields)
      } else {
        if (values.icon_url) {
          if (data.icon_url === values.icon_url) {
            newData = {
              ...values,
              id: data.id,
            }
            console.log(newData)
          } else {
            newData = {
              ...values,
              id: data.id,
              icon_url: resUploadImage.data.path,
            }
            console.log(newData)
          }
        } else {
          newData = {
            ...values,
            id: data.id,
            icon_url: '',
          }
          console.log(newData)
        }
        onUpdateCategory(newData, form.resetFields)
      }
    } catch (error) {
      console.log(error)
    }
  }
  console.log(categoryName)

  const convertDataToFrom = (data: any, categoryName: any) => {
    // Nếu tên danh mục cha tồn tại => đây là thêm danh mục con
    if (!data) {
      return {
        name: null,
        order: null,
        icon_url: null,
        parent_category: categoryName,
      }
    } else {
      return {
        ...data,
        name: data.name,
        expired_at: moment.unix(data.expired_at),
        parent_category: categoryName,
      }
    }
  }

  useEffect(() => {
    if (data) {
      setUpload({
        ...upload,
        imageUrl: data.icon_url,
      })
      console.log(data)
    }
  }, [data])

  const initialValues = convertDataToFrom(data, categoryName)

  return (
    <Modal
      onCancel={() => {
        form.resetFields()
        onCancel()
      }}
      maskClosable={false}
      footer={null}
      title={data ? 'Sửa danh mục con' : 'Thêm danh mục con'}
      visible={visible}
    >
      <Form
        {...formItemLayout}
        form={form}
        name="register"
        onFinish={onFinish}
        initialValues={initialValues}
        scrollToFirstError
      >
        <Form.Item
          name="parent_category"
          label="Danh mục cha"
          rules={[
            {
              required: true,
              message: 'Vui lòng nhập tên danh mục!',
            },
          ]}
        >
          <Input placeholder="Nhập tên danh mục cha" disabled />
        </Form.Item>

        <Form.Item
          name="name"
          label="Tên danh mục con"
          rules={[
            {
              type: 'string',
              message: 'Nhập tên danh mục',
            },
            {
              required: true,
              message: 'Vui lòng nhập tên danh mục!',
            },

            {
              whitespace: true,
              message: 'Không được nhập khoảng trắng!',
            },
            {
              max: 50,
              message: 'Vui lòng không nhập quá 50 ký tự!',
            },
          ]}
        >
          <Input placeholder="Nhập tên danh mục con" />
        </Form.Item>

        <Form.Item
          name="order"
          label="STT hiển thị"
          rules={[
            {
              type: 'number',
              message: 'Vui lòng nhập số',
            },
            {
              required: true,
              message: 'Vui lòng nhập STT',
            },
          ]}
        >
          <InputNumber min={1} placeholder="Nhập STT hiển thị" />
        </Form.Item>
        <Form.Item
          label="Icon danh mục"
          name="icon_url"
          rules={
            [
              // {
              //   required: true,
              //   message: 'Vui lòng chọn Icon danh mục!',
              // },
            ]
          }
        >
          <Upload
            name="icon_url"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={false}
            action="https://www.mocky.io/v2/5cc8019d300000980a055e76"
            beforeUpload={beforeUpload}
            onChange={handleChange}
          >
            {upload.imageUrl ? (
              <img
                src={upload.imageUrl}
                alt="avatar"
                style={{ width: '100%', objectFit: 'contain', height: '100%' }}
              />
            ) : (
              uploadButton
            )}
          </Upload>
          {/* <div style={{ fontSize: '12px' }}>
            <p></p>
            <p>1. Dana dạng: JPG/JPEG/PNG</p>
            <p>2. Kích thước: Tối đa 2MB</p>
          </div> */}
        </Form.Item>
        {/* <Form.Item>
          <Row style={{ width: '100%' }}>
            <Col span={6} offset={10}>
              <Button
                danger
                onClick={() => {
                  form.resetFields()
                  onCancel()
                }}
              >
                Huỷ
              </Button>
            </Col>
            <Col span={6} offset={2}>
              <Button type="primary" loading={isLoading} htmlType="submit">
                {data ? 'Cập nhật' : 'Thêm danh mục'}
              </Button>
            </Col>
          </Row>
        </Form.Item> */}
        <ButtonBottomModal
          isLoadingButton={isLoadingButton}
          onCancel={onCancel}
          text={data ? 'Cập nhật' : 'Thêm danh mục'}
        />
      </Form>
    </Modal>
  )
}

export default AddEditCategorySub
