import Icon from '@ant-design/icons'
import {
  Button,
  Collapse,
  DatePicker,
  Input,
  PageHeader,
  Select,
  Space,
} from 'antd'
import React, { useState } from 'react'
import './css/Header.css'

const { Panel } = Collapse
const { RangePicker } = DatePicker
const { Option } = Select

type FilterProps = {
  setIsCreate: any
  onSearchSubmit: any
}

export const FilterSub = (props: FilterProps) => {
  const [searchKey, setSearchKey] = useState('')
  function callback(key: any) {
    console.log(key)
  }
  return (
    <Space>
      <Input.Search
        className="input-search_categorySub"
        allowClear
        placeholder="Nhập tên danh mục con"
        addonAfter={
          <Icon
            type="close-circle-o"
            onClick={() => {
              props.onSearchSubmit('')
            }}
          />
        }
        onKeyDown={e => {
          if (e.keyCode == 13) {
            props.onSearchSubmit(searchKey)
          }
        }}
        onSearch={(value, event) => {
          props.onSearchSubmit(value)
        }}
        onChange={e => {
          setSearchKey(e.target.value)
        }}
      />
      <Space direction="vertical" size={12}>
        <RangePicker />
      </Space>
      <Button
        style={{
          backgroundColor: '#00abba',
          borderColor: '#00abba',
          color: 'white',
        }}
        onClick={() => {
          props.setIsCreate(true)
        }}
      >
        Thêm danh mục
      </Button>
    </Space>
  )
}
