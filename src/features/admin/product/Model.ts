export type ProductModel = {
  expired_at: string
  created_at: string
  id: number
  name: string
  order: number
  status: number

  phone_number: string
  address: string
  password: string
  date_of_birth: string
  gender: number
  token: string
  email: string
  role_id: number
  province_id: number
  modified_at: string
  is_active: number
  role: string
  province_name: string
}
