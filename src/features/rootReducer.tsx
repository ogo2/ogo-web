import AuthReducer from './auth/AuthSlice'
import NotificationReducer from 'common/components/header/NotificationSlice'
import ConfigReducer from 'common/ConfigSlice'
import MessageNotReadReducer from 'features/enterprise/chat/slices/MessageNotReadSlice'

const rootReducer = {
  authReducer: AuthReducer,
  notificationReducer: NotificationReducer,
  configReducer: ConfigReducer,
  messageNotReadReducer: MessageNotReadReducer,
}

export default rootReducer
