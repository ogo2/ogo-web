import { ApiClient } from 'services/ApiService'

export const getBlogPostDetail = (post_id: string) =>
  ApiClient.get(`/client/post/${post_id}`)

export const getToppicBlogs = () => ApiClient.get(`/topic`)

export const getPostByIdTopic = (url: string, payload: any) =>
  ApiClient.get(url, payload)
