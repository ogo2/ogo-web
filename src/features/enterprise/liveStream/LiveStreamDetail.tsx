import {
  CloseCircleOutlined,
  DownloadOutlined,
  SaveOutlined,
  UserOutlined,
} from '@ant-design/icons'
import {
  Button,
  Col,
  Descriptions,
  Divider,
  Input,
  PageHeader,
  Row,
  Select,
  Comment,
  Avatar,
  Pagination,
  Empty,
  Spin,
  Space,
  Popconfirm,
  message,
  Modal,
} from 'antd'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { msToTime } from 'utils/funcHelper'
import {
  deleteLive,
  getCommentList,
  getDetail,
  updateBroadcastLive,
} from './LiveStreamApi'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import './css/Comment.css'
import ExportCsv from 'utils/ExportCSV'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import { LIVESTREAM_STATUS } from 'utils/constants'
import ButtonBottomModal from 'common/components/ButtonBottomModal'

const { Option } = Select

export default function LiveStreamDetail() {
  const params: any = useParams()
  const history = useHistory()
  const [data, setData] = useState<any>()
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [isLoadingComment, setIsLoadingComment] = useState<boolean>(false)
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [commentList, setCommentList] = useState([])
  const [paramComment, setParamComment] = useState({
    search: '',
    livestream_id: params.id,
    page: 1,
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const [broadcastId, setBroadcastId] = useState<string>('')
  const [
    showUpdateBroadCastIdModal,
    setShowUpdateBroadCastIdModal,
  ] = useState<boolean>(false)

  const getLiveStreamDetail = async () => {
    setIsLoading(true)
    try {
      const res = await getDetail(params.id)
      console.log('detail: ', res.data)
      setData(res.data)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  const getComment = async () => {
    setIsLoadingComment(true)
    try {
      const response = await getCommentList(paramComment)
      setCommentList(response.data)
      setPaging({
        total: response.paging.totalItemCount,
        current: response.paging.page,
        pageSize: response.paging.limit,
      })
    } catch (error) {
      console.log(error)
    } finally {
      setIsSearchLoading(false)
      setIsLoadingComment(false)
    }
  }

  useEffect(() => {
    getComment()
  }, [paramComment])

  useEffect(() => {
    getLiveStreamDetail()
  }, [])

  const [dataExport, setDataExport] = useState<any>([])
  const [loadingExport, setLoadingExport] = useState<boolean>(false)

  const onExport = async (fn: any) => {
    console.log('data export')
    try {
      setLoadingExport(true)
      const response = await getCommentList({
        search: '',
        livestream_id: params.id,
        page: 1,
        limit: 10000,
      })
      console.log(response)
      const dataExport = response.data.map((item: any, index: number) => ({
        STT: index + 1,
        'Người bình luận': item.User.name,
        'Nội dung bình luận': item.content,
        'Thời gian bình luận': msToTime(
          moment(item.create_at).unix() - moment(data?.start_at).unix()
        ),
        'Số điện thoại:': item.User.phone,
        'Địa chỉ:':
          item.User?.UserAddresses[0]?.DFWard?.name +
          ' / ' +
          item.User?.UserAddresses[0]?.DFDistrict?.name +
          ' / ' +
          item.User?.UserAddresses[0]?.DFProvince?.name,
      }))
      setDataExport(dataExport)
      fn()
    } catch (error) {
      console.log(error)
    } finally {
      setLoadingExport(false)
    }
  }

  return (
    <>
      <PageHeader
        style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}
        title="Chi tiết livestream"
        onBack={() => {
          history.goBack()
        }}
        extra={[
          <Space>
            {data?.status === LIVESTREAM_STATUS.INITIAL && (
              <Button
                type="primary"
                onClick={() => setShowUpdateBroadCastIdModal(true)}
              >
                Update broadcast ID
              </Button>
            )}
            <Popconfirm
              placement="bottomRight"
              title="Bạn chắc chắn muốn xoá livestream này!"
              onConfirm={async () => {
                try {
                  await deleteLive(params.id)
                  history.push(ENTERPRISE_ROUTER_PATH.LIVE_STREAM)
                  message.success('Xoá thành công livestream')
                } catch (error) {
                  console.log(error)
                }
              }}
              okText="Xoá"
              cancelText="Quay lại"
            >
              <Button type="primary" danger>
                <CloseCircleOutlined />
                Xóa
              </Button>
            </Popconfirm>

            <ExportCsv
              loading={loadingExport}
              onClick={fn => onExport(fn)}
              sheetName={['CommentList']}
              sheets={{
                CommentList: ExportCsv.getSheets(dataExport),
              }}
              fileName="CommentList"
            >
              <DownloadOutlined />
              &nbsp;Export bình luận
            </ExportCsv>
          </Space>,
        ]}
      />

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '20px 20px 0',
        }}
      >
        <Spin spinning={isLoading}>
          <Descriptions title="THÔNG TIN LIVESTREAM" column={4}>
            <Descriptions.Item label="Lượt xem">
              {data?.count_viewed}
            </Descriptions.Item>
            <Descriptions.Item label="Thời lượng">
              {msToTime(
                moment(data?.finish_at).unix() - moment(data?.start_at).unix()
              )}
            </Descriptions.Item>
            <Descriptions.Item label="Lượt thích">
              {data?.count_reaction}
            </Descriptions.Item>
            <Descriptions.Item label="Bình luận">
              {data?.count_comment}
            </Descriptions.Item>
            <Descriptions.Item label="Ngày livestream">
              {moment(data?.create_at).format('DD-MM-YYYY')}
            </Descriptions.Item>
            <Descriptions.Item label="Kết thúc">
              {moment(data?.finish_at).format('DD-MM-YYYY')}
            </Descriptions.Item>
            <Descriptions.Item label="Broadcast ID">
              {data?.broadcast_id ? data.broadcast_id : '--'}
            </Descriptions.Item>
          </Descriptions>
          <Descriptions>
            <Descriptions.Item label="Mô tả">{data?.title}</Descriptions.Item>
          </Descriptions>
        </Spin>
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
        }}
      >
        <Row gutter={[24, 24]}>
          <Col xxl={7} xl={10}>
            {data?.livestream_record_url ? (
              <video
                // style={{ height: 'calc(100vh - 370px)', width: 'auto' }}
                style={{
                  paddingTop: '10px',
                  paddingLeft: '10px',
                  width: '100%',
                }}
                loop
                // autoPlay
                controls
                src={data?.livestream_record_url}
              />
            ) : (
              <Empty
                image={Empty.PRESENTED_IMAGE_SIMPLE}
                description={<span>Không có record livestream</span>}
              />
            )}
          </Col>
          {/* <Col span={1}>
            <Divider type="vertical" style={{ height: '100%' }} />
          </Col> */}
          <Col xxl={17} xl={14}>
            <Descriptions title="BÌNH LUẬN" style={{ paddingTop: '10px' }} />

            <Row gutter={[24, 24]}>
              <Col span={15}>
                <TypingAutoSearch
                  onSearchSubmit={(key: string) => {
                    setIsSearchLoading(true)
                    setParamComment({ ...paramComment, search: key, page: 1 })
                  }}
                  isSearchLoading={isSearchLoading}
                  placeholder="Tên khách hàng hoặc nội dung bình luận ..."
                />
              </Col>
            </Row>

            <Spin spinning={isLoadingComment}>
              <div
                style={{
                  marginTop: '20px',
                  height: 'calc(100vh - 515px)',
                  overflowY: 'scroll',
                }}
              >
                {commentList.length > 0 ? (
                  commentList.map((item: any, index: number) => (
                    <Comment
                      className="fe-comment"
                      key={index}
                      author={item.User.name}
                      avatar={
                        item.User.profile_picture_url ? (
                          item.User.profile_picture_url
                        ) : (
                          <Avatar icon={<UserOutlined />} />
                        )
                      }
                      content={item.content}
                      datetime={moment(item.create_at).format('HH:mm')}
                    />
                  ))
                ) : (
                  <Empty image={Empty.PRESENTED_IMAGE_SIMPLE} />
                )}
              </div>
            </Spin>

            <Row style={{ marginBottom: '15px' }}>
              <Col span={24} offset={16} style={{ marginLeft: '200px' }}>
                <Pagination
                  defaultCurrent={1}
                  total={paging.total}
                  pageSizeOptions={['24']}
                  defaultPageSize={24}
                  onChange={async page => {
                    setParamComment({ ...paramComment, page: page })
                  }}
                  showLessItems={true}
                />
              </Col>
            </Row>
          </Col>
        </Row>
      </div>

      {/* Modal update broadcast live */}
      <Modal
        title="Update broadcast ID"
        visible={showUpdateBroadCastIdModal}
        okText={'Lưu'}
        onOk={async () => {
          if (broadcastId) {
            try {
              const payload = {
                livestream_id: parseInt(params.id),
                broadcast_id: broadcastId,
              }
              await updateBroadcastLive(payload)
              setBroadcastId('')
              setShowUpdateBroadCastIdModal(false)
              message.success('Cập nhật thành công')
            } catch (error) {
              console.log('error', error)
            }
          } else {
            message.warning('Vui lòng nhập broadcast ID')
          }
        }}
        onCancel={() => {
          setBroadcastId('')
          setShowUpdateBroadCastIdModal(false)
        }}
      >
        <label>Broadcast ID</label>
        <Input
          value={broadcastId}
          onChange={event => setBroadcastId(event.target.value)}
        />
      </Modal>
    </>
  )
}
