import {
  CloseCircleOutlined,
  DeleteOutlined,
  EditOutlined,
  PlusCircleOutlined,
  PlusOutlined,
} from '@ant-design/icons'
import {
  Button,
  Col,
  Image,
  PageHeader,
  Row,
  Space,
  Table,
  Divider,
  Popconfirm,
  message,
} from 'antd'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router'
import { msToTime } from 'utils/funcHelper'
import Filter from './components/Filter'
import {
  getListLiveStream,
  getShopList,
  requestCreateGoogleAccount,
  getConfigLivestream,
  deleteGoogleAccount,
} from './LiveStreamApi'

export default function LiveStream() {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const history = useHistory()
  const authState = useSelector((state: any) => state.authReducer)
  const [listLive, setListLive] = useState<Array<any>>([])
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [dataConfigLivestream, setDataConfigLivestream] = useState<any>({})
  const [params, setParams] = useState({
    search: '',
    status: '',
    user_id: '',
    shop_id: authState.userInfo?.Shop?.id,
    from_date: '',
    to_date: '',
    page: 1,
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const getData = async () => {
    setIsLoading(true)
    try {
      const response = await getListLiveStream(params)
      const data = response.data.map((item: any) => {
        return { ...item, key: item.id }
      })
      setListLive(data)
      setPaging({
        total: response.paging.totalItemCount,
        current: response.paging.page,
        pageSize: response.paging.limit,
      })
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  const getDataConfig = async () => {
    try {
      const data_config = await getConfigLivestream()
      setDataConfigLivestream(data_config.data)
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getData()
    getDataConfig()
  }, [params])

  const columns = [
    {
      width: 70,
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
      render: (text: any, record: any, index: any) => (
        <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
          {(paging.current - 1) * paging.pageSize + index + 1}
        </td>
      ),
      ellipsis: true,
    },
    {
      title: 'Ảnh',
      dataIndex: 'cover_image_url',
      key: 'cover_image_url',
      width: 70,
      render: (value: any) => {
        return (
          <td className="icon-eye-antd-custom">
            <Image
              style={{ objectFit: 'cover', width: '35px', height: '35px' }}
              src={value}
              onClick={e => {
                e.stopPropagation()
              }}
            />
          </td>
        )
      },
    },
    {
      title: 'Tên gian hàng',
      dataIndex: '',
      render: (value: any) => <td>{value.Shop.name}</td>,
    },
    {
      title: 'Mô tả',
      // width: 500,
      dataIndex: 'title',
      render: (value: string) => <td>{value}</td>,
    },
    {
      title: 'Lượt xem',
      width: 100,
      dataIndex: 'count_viewed',
    },
    {
      title: 'Lượt thích',
      width: 120,
      dataIndex: 'count_reaction',
    },
    {
      title: 'Bình luận',
      width: 100,
      dataIndex: 'count_comment',
    },
    {
      title: 'Thời lượng',
      dataIndex: '',
      width: 130,
      render: (value: any) => (
        <td>
          {msToTime(
            moment(value.finish_at).unix() - moment(value.start_at).unix()
          )}
        </td>
      ),
    },
    {
      title: 'Ngày livestream',
      dataIndex: 'create_at',
      width: 150,
      render: (value: string) => <td>{moment(value).format('DD-MM-YYYY')}</td>,
    },
    // {
    //   title: 'Ngày kết thúc',
    //   dataIndex: 'finish_at',
    //   render: (value: string) => <td>{moment(value).format('DD-MM-YYYY')}</td>,
    // },
  ]

  return (
    <>
      <div
        style={{
          backgroundColor: 'white',
          margin: '5px 10px 15px',
          // padding: '10px',
        }}
      >
        <PageHeader
          className="site-page-header"
          title="Livestream"
          extra={[
            <Space>
              <Filter
                isSearchLoading={isSearchLoading}
                onSearchSubmit={(searchKey: string) => {
                  setIsSearchLoading(true)
                  setParams({ ...params, search: searchKey, page: 1 })
                }}
                onDateSubmit={(from_date: string, to_date: string) => {
                  setParams({
                    ...params,
                    from_date: from_date,
                    to_date: to_date,
                    page: 1,
                  })
                }}
                onShopSubmit={(shopKey: string) => {
                  setParams({ ...params, shop_id: shopKey, page: 1 })
                }}
              />
            </Space>,
          ]}
        />
      </div>
      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <Row style={{ width: '100%', marginBottom: '10px' }}>
          <span
            style={{
              fontSize: '18px',
              fontWeight: 'bold',
            }}
          >
            Thiết lập tài khoản livestream
          </span>
          <div style={{ position: 'absolute', right: '30px' }}>
            <Button
              style={{
                marginLeft: '8px',
                backgroundColor: '#00abba',
                borderColor: '#00abba',
                color: 'white',
              }}
              onClick={async () => {
                const res = await requestCreateGoogleAccount()
                window.open(res.data.url)
                window.focus()
              }}
            >
              <PlusCircleOutlined />
              Thêm tài khoản
            </Button>
            {/* <Button
              style={{
                marginLeft: '8px',
                backgroundColor: '#389e0d',
                color: 'white',
              }}
              onClick={async () => {}}
            >
              <EditOutlined />
              Sửa
            </Button> */}
            <Popconfirm
              placement="bottomRight"
              title="Bạn có chắc chắn muốn xoá!"
              onConfirm={async () => {
                try {
                  await deleteGoogleAccount()
                  message.success('Xoá thành công!')
                } catch (error) {
                  console.log(error)
                } finally {
                  getDataConfig()
                }
              }}
              okText="Xoá"
              cancelText="Quay lại"
            >
              <Button style={{ marginLeft: '8px' }} type="primary" danger>
                <CloseCircleOutlined />
                Xóa
              </Button>
            </Popconfirm>
          </div>
        </Row>
        <Divider />
        <Row style={{ width: '100%' }}>
          <Col span={8}>
            <label style={{ fontWeight: 'bold' }}>Email: </label>
            {dataConfigLivestream?.google_tokens?.email
              ? dataConfigLivestream?.google_tokens?.email
              : ' ---'}
          </Col>
          <Col span={8}>
            <label style={{ fontWeight: 'bold' }}>Trạng thái: </label>{' '}
            {dataConfigLivestream?.google_tokens
              ? dataConfigLivestream?.livestream_enable
                ? 'Đang hoạt động'
                : 'Ngưng hoạt động'
              : ' ---'}
          </Col>
          <Col span={8}>
            <label style={{ fontWeight: 'bold' }}>Ngày tạo: </label>
            {dataConfigLivestream?.google_tokens?.expiry_date
              ? moment(dataConfigLivestream?.google_tokens?.expiry_date).format(
                  'DD/MM/YYYY'
                )
              : ' ---'}
          </Col>
        </Row>
      </div>
      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <p>
          Kết quả lọc: <b>{paging.total}</b>
        </p>
        <Table
          className="table-expanded-custom"
          scroll={{
            x: 800,
            scrollToFirstRowOnChange: true,
            // y: 'calc(100vh - 340px)',
            // y: `calc(${heightWeb}px - 600px)`,
          }}
          bordered
          dataSource={listLive}
          loading={isLoading}
          columns={columns}
          onRow={(record, rowIndex) => ({
            onDoubleClick: () => {
              history.push({
                pathname: `${ENTERPRISE_ROUTER_PATH.LIVE_STREAM_DETAIL}/${record.id}`,
                state: record,
              })
            },
          })}
          pagination={{
            ...paging,
            showSizeChanger: false,
            onChange: async (page, pageSize) => {
              setParams({ ...params, page })
            },
          }}
        />
      </div>
    </>
  )
}
