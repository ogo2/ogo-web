import { ApiClient } from 'services/ApiService'

export const getAccounts = (payload: any) => {
  const param: any = {}
  if (payload.search) {
    param.search = payload.search
  }
  if (payload.status) {
    param.status = parseInt(payload.status)
  }
  if (payload.page) {
    param.page = payload.page
  }
  return ApiClient.get('/shop/shop/account', param)
}
export const createAccount = (payload: any) =>
  ApiClient.post('/shop/shop/account', payload)

export const updateAccount = (payload: any) =>
  ApiClient.put(`/shop/shop/${payload.id}/account`, payload.data)

export const resetPassword = (payload: any) =>
  ApiClient.put(`/shop/shop/${payload.id}/account/reset-password`, payload)

export const deleteAccount = (payload: any) =>
  ApiClient.delete('/shop/shop/account', payload)

export const requestUploadImageAccount = (payload: any) =>
  ApiClient.post(`/files/upload/single/${payload.type}`, payload.data)

export const accountChangeStatus = (payload: any) =>
  ApiClient.put(`/shop/shop/${payload.id}/account/status`, payload.status)
