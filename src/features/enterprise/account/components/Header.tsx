import { Button, Select, Space, Row, Col } from 'antd'
import ButtonFixed from 'common/components/ButtonFixed'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import React from 'react'
import styled from 'styled-components'
import { STATUS } from 'utils/constants'
const { Option } = Select

const Container = styled.div`
  width: 100%;
  padding: 0.5rem;
  background-color: white;
  border-bottom: 1px solid #dddd;
`
type HeaderProps = {
  setIsCreate: any
  onSearchSubmit: (searchKey: string) => void
  onStatusSubmit: (statusKey: any) => void
  isSearchLoading: boolean
}

export const Header = ({
  setIsCreate,
  onSearchSubmit,
  onStatusSubmit,
  isSearchLoading,
}: HeaderProps) => {
  return (
    <Row gutter={[16, 16]} justify="end">
      <Col style={{ width: '300px' }}>
        <TypingAutoSearch
          onSearchSubmit={(key: string) => {
            onSearchSubmit(key.trim())
          }}
          isSearchLoading={isSearchLoading}
          placeholder="Tên hoặc số điện thoại"
        />
      </Col>
      <Col>
        <Select
          allowClear
          placeholder="Trạng thái"
          style={{ minWidth: '130px' }}
          onChange={value => {
            onStatusSubmit(value)
          }}
        >
          <Option value="">Tất cả</Option>
          <Option value={`${STATUS.ACTIVE}`}>Hoạt động</Option>
          <Option value={`${STATUS.INACTIVE}`}>Tạm dừng</Option>
        </Select>
      </Col>
      <Col>
        {/* <Button
        type="primary"
        style={{ minWidth: '70px' }}
        onClick={() => {
          setIsCreate(true)
        }}
      >
        Thêm mới
      </Button> */}
        <ButtonFixed setIsCreate={setIsCreate} text="Thêm mới" type="add" />
      </Col>
    </Row>
  )
}
