import React, { useRef } from 'react'
import {
  Header,
  Container,
  SlideBar,
  ChatArea,
  EmptyChatArea,
} from './components'

function Chat() {
  return (
    <Container
      headerComponent={Header}
      filterComponent={SlideBar}
      contentComponent={ChatArea}
      emptyContentComponent={EmptyChatArea}
    />
  )
}
export default Chat
