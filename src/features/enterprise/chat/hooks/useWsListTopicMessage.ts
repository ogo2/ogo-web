import { useState, useEffect } from 'react'
import { useSelector } from 'react-redux'
import { WebSocket } from 'services/WebSocket'
import {
  ResponseSocketModel,
  USER_EVENT,
  withShopChannel,
  withUserChannel,
} from 'utils/socketConstants'
import { requestGetListTopicMessage } from '../ChatService'

export default function useWsListTopicMessage() {
  const { userInfo: UserInstance } = useSelector(
    (state: any) => state.authReducer
  )
  const [isLoadingFirstTime, setIsLoadingFirstTime] = useState<boolean>(false)
  const [listTopicMessage, setListTopicMessage] = useState<Array<any>>([])
  const [pagingTopicMessage, setPagingTopicMessage] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  useEffect(() => {
    if (UserInstance?.id) {
      onUserHasNewMessage()
      getListTopic()
    }
    if (UserInstance?.shop_id) {
      onShopHasNewMessage()
    }
  }, [UserInstance])

  const onUserHasNewMessage = () => {
    WebSocket.socketClient?.on(
      withUserChannel(UserInstance?.id),
      (res: ResponseSocketModel) => {
        // new channel event
        if (res.type_action === USER_EVENT.NEW_CHANNEL_MESSAGE) {
          setListTopicMessage(prevState => {
            const topicMessage = prevState.find(
              item => item.id === res?.data?.id
            )
            if (!topicMessage) return [res.data, ...prevState]
            else {
              const newListTopicMessage = prevState.filter(
                item => item.id !== res.data.id
              )
              res.data.Messages[0].create_at = new Date()
              return [res.data, ...newListTopicMessage]
            }
          })
        }

        // new message event
        else if (res.type_action === USER_EVENT.NEW_MESSAGE) {
          setListTopicMessage(prevState => {
            const newListMesage = prevState.filter(
              item => item.id !== res?.data?.id
            )
            res.data.Messages[0].create_at = new Date()
            return [res.data, ...newListMesage]
          })
        }
      }
    )
  }
  const onShopHasNewMessage = () => {
    WebSocket.socketClient?.on(
      withShopChannel(UserInstance?.shop_id),
      (res: ResponseSocketModel) => {
        if (res.type_action === USER_EVENT.NEW_CHANNEL_MESSAGE) {
          setListTopicMessage(prevState => {
            const topicMessage = prevState.find(
              item => item.id === res?.data?.id
            )
            // channel message not existed in listTopicMessage
            if (!topicMessage) return [res.data, ...prevState]
            // channel message existed in listTopicMessage
            else {
              const newListTopicMessage = prevState.filter(
                item => item.id !== res.data.id
              )
              res.data.Messages[0].create_at = new Date()
              return [res.data, ...newListTopicMessage]
            }
          })
        }
        // new message event
        else if (res.type_action === USER_EVENT.NEW_MESSAGE) {
          setListTopicMessage(prevState => {
            const newListMesage = prevState.filter(
              item => item.id !== res?.data?.id
            )
            const message = res.data.Messages[0]
            message.create_at = new Date()
            // console.log('newListTopicMessage', newListMesage)
            // console.log('res.data', res.data)
            return [res.data, ...newListMesage]
          })
        }
      }
    )
  }

  const getListTopic = async () => {
    setIsLoadingFirstTime(true)
    try {
      const res = await requestGetListTopicMessage({ page: 1 })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setPagingTopicMessage(formattedPaging)
      setListTopicMessage(res.data)
    } catch (error) {
      console.log('error', error)
    } finally {
      setIsLoadingFirstTime(false)
    }
  }

  return {
    isLoadingFirstTime,
    setIsLoadingFirstTime,
    listTopicMessage,
    setListTopicMessage,
    pagingTopicMessage,
    setPagingTopicMessage,
  }
}
