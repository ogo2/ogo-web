import { requestCountMessageNotRead } from 'features/enterprise/chat/ChatService'
import { useEffect, useState } from 'react'
import { WebSocket } from 'services/WebSocket'
import {
  ResponseSocketModel,
  USER_EVENT,
  withShopChannel,
  withUserChannel,
} from 'utils/socketConstants'
import { useDispatch, useSelector } from 'react-redux'
import { IS_READ } from 'utils/constants'
import { getMessageNotReadAction } from 'features/enterprise/chat/slices/MessageNotReadSlice'

export const useListMessageNotRead = () => {
  const dispatch = useDispatch()
  const { listTopicMessageNotRead } = useSelector(
    (state: any) => state.messageNotReadReducer
  )
  const { userInfo: UserInstance } = useSelector(
    (state: any) => state.authReducer
  )
  useEffect(() => {
    getCountMessageNotRead()
    if (UserInstance?.id) {
      onUserHasNewMessage()
    }
    if (UserInstance?.shop_id) {
      onShopHasNewMessage()
    }
  }, [UserInstance])

  const onUserHasNewMessage = () => {
    WebSocket.socketClient?.on(
      withUserChannel(UserInstance?.id),
      (res: ResponseSocketModel) => {
        getCountMessageNotRead()
        // setCountMessageNotRead(prevState => prevState + 1)
        // console.log('res sk user', res)
        // new channel event
        if (res.type_action === USER_EVENT.NEW_CHANNEL_MESSAGE) {
        }

        // new message event
        else if (res.type_action === USER_EVENT.NEW_MESSAGE) {
        }
      }
    )
  }
  const onShopHasNewMessage = () => {
    WebSocket.socketClient?.on(
      withShopChannel(UserInstance?.shop_id),
      (res: ResponseSocketModel) => {
        getCountMessageNotRead()
        // setCountMessageNotRead(prevState => prevState + 1)
        // console.log('res sk shop', res)
        // new channel event
        if (res.type_action === USER_EVENT.NEW_CHANNEL_MESSAGE) {
        }
        // new message event
        else if (res.type_action === USER_EVENT.NEW_MESSAGE) {
        }
      }
    )
  }

  const getCountMessageNotRead = async () => {
    dispatch(getMessageNotReadAction())
  }
  return { listTopicMessageNotRead }
}
