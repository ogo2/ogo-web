import { useEffect, useState, useMemo } from 'react'
import './css/styles.css'
import { Col, Row, Avatar } from 'antd'
import { getFromDateToNow, get_ss_hh_dd_mm } from 'utils/TimerHelper'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import { useHistory } from 'react-router-dom'
import { useDispatch, useSelector } from 'react-redux'
import { splitTextEndLine } from 'utils/funcHelper'
import { IS_READ } from 'utils/constants'
import { UserInfoState } from '../types'
import R from 'utils/R'
import { deleteNotificationMessenger } from 'common/components/header/NotificationSlice'
import { readATopicMessage } from '../slices/MessageNotReadSlice'

export interface TopicChatItemProps {
  id: number
  Message: any
  Shop: any
  User: any
  time_last_send: Date
  index: number
  count_message_not_read: number
  mode: number
  onReadMessage: (index: number, mode: number) => void
}

function TopicChatItem({
  id,
  Message,
  Shop,
  User,
  time_last_send,
  count_message_not_read,
  index,
  mode,
  onReadMessage,
}: TopicChatItemProps) {
  const dispatch = useDispatch()
  const history = useHistory()
  const { userInfo } = useSelector((state: any) => state.authReducer)
  const [userDisplay, setUserDisplay] = useState<UserInfoState | null>(null)
  useEffect(() => {
    if (userInfo && User && Shop) {
      if (userInfo.id === User.id) setUserDisplay(Shop)
      else setUserDisplay(User)
    }
  }, [userInfo, User, Shop])

  const split_pathname: Array<string> = history?.location?.pathname?.split('/')
  const topic_message_id: string | undefined =
    split_pathname[split_pathname.length - 1]
  let message: string = Message?.content || ''
  if (message) {
    const arrMgs: Array<string> = splitTextEndLine(message)
    message = ''
    arrMgs.forEach(
      (e: string, i) => (message += i !== arrMgs.length - 1 ? e + ' ' : e)
    )
  }
  const mgs = useMemo(() => {
    let message: string = Message?.content || ''
    if (message) {
      const arrMgs: Array<string> = splitTextEndLine(message)
      message = ''
      arrMgs.forEach(
        (e: string, i) => (message += i !== arrMgs.length - 1 ? e + ' ' : e)
      )
    }
    return userInfo &&
      Message &&
      (userInfo?.id === Message?.user_id ||
        userInfo.shop_id === Message.shop_id)
      ? message
        ? `Bạn: ${message}`
        : 'Bạn: Đã gửi một media.'
      : message
      ? message
      : 'Đã gửi một media.'
  }, [userInfo, Message])

  const is_read_mgs =
    Message?.is_read === IS_READ.READ ||
    userInfo?.id === Message.user_id ||
    userInfo?.shop_id === Message.shop_id
  return (
    <Row
      className={
        topic_message_id && parseInt(topic_message_id) === id
          ? 'chat-item active-chat-item'
          : 'chat-item'
      }
      onClick={() => {
        onReadMessage(index, mode)
        dispatch(deleteNotificationMessenger(id))
        dispatch(readATopicMessage({ topic_message_id: id }))
        history.replace(`${ENTERPRISE_ROUTER_PATH.CHAT}/${id}`)
        //
      }}
    >
      <Col lg={5} md={8} xs={24}>
        <Row justify="center">
          <div
            style={{
              position: 'relative',
            }}
          >
            {count_message_not_read !== 0 && !is_read_mgs && (
              <div
                style={{
                  position: 'absolute',
                  top: 0,
                  left: 0,
                  width: 10,
                  height: 10,
                  zIndex: 1,
                  borderRadius: '50%',
                  border: 2,
                  backgroundColor: '#be0000',
                  color: 'white',
                  fontSize: 'x-small',
                  fontWeight: 'bold',
                  textAlign: 'center',
                  verticalAlign: 'top',
                }}
              >
                {/* {count_message_not_read} */}
              </div>
            )}

            <Avatar
              size={48}
              src={userDisplay?.profile_picture_url}
              icon={<img src={R.images.img_ogo} />} // <UserOutlined />
            />
          </div>
        </Row>
      </Col>
      <Col lg={15} md={14} xs={0} style={{ height: '100%' }}>
        <Row align="middle" style={{ height: '100%' }}>
          <Col span={20} style={{ height: '80%' }}>
            <Row>
              <strong
                style={{
                  fontSize: 'large',
                  whiteSpace: 'nowrap',
                  width: '100%',
                  overflow: 'hidden',
                  textOverflow: 'ellipsis',
                }}
              >
                {userDisplay?.name}{' '}
                <span style={{ fontSize: 'small' }}>
                  ({userDisplay?.phone})
                </span>
              </strong>
            </Row>
            <Row>
              {is_read_mgs ? (
                <p
                  style={{
                    whiteSpace: 'nowrap',
                    width: '100%',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    color: '#515151',
                  }}
                >
                  {mgs}
                </p>
              ) : (
                <p
                  style={{
                    whiteSpace: 'nowrap',
                    width: '100%',
                    overflow: 'hidden',
                    textOverflow: 'ellipsis',
                    color: '#515151',
                  }}
                >
                  <strong>{mgs}</strong>
                </p>
              )}
            </Row>
          </Col>
        </Row>
      </Col>
      <Col lg={3} md={0} xs={0} style={{ height: '80%' }}>
        <Row align="top" justify="end" style={{ height: '100%' }}>
          <span style={{ fontSize: 11, color: 'gray' }}>
            {Message?.create_at
              ? get_ss_hh_dd_mm(Message?.create_at)
              : get_ss_hh_dd_mm(time_last_send)}
          </span>
        </Row>
      </Col>
    </Row>
  )
}
export default TopicChatItem
