import './css/styles.css'
import React, { useState, useEffect, useRef } from 'react'
import { Spin, Skeleton } from 'antd'
import TopicChatItem from './TopicChatItem'
import { requestGetListTopicMessage } from '../ChatService'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import { useHistory } from 'react-router-dom'
import AutoSearch from './AutoSearch'
import { IS_READ, MODE_MESSAGE } from 'utils/constants'
import useWsListTopicMessage from '../hooks/useWsListTopicMessage'

function SlideBar() {
  const history = useHistory()
  const split_pathname: Array<string> = history?.location?.pathname?.split('/')
  const topic_message_id: string | undefined =
    split_pathname[split_pathname.length - 1]

  const {
    isLoadingFirstTime,
    setIsLoadingFirstTime,
    listTopicMessage,
    setListTopicMessage,
    pagingTopicMessage,
    setPagingTopicMessage,
  } = useWsListTopicMessage()
  const [isSearchMode, setIsSearchMode] = useState<boolean>(false)
  const [listSearchTopicMessage, setListSearchTopicMessage] = useState<
    Array<any>
  >([])
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [isLoadingMore, setIsLoadingMore] = useState<boolean>(false)
  const [canLoading, setCanLoading] = useState<boolean>(false)

  const refFirstTimeRender = useRef<boolean>(false)

  useEffect(() => {
    if (
      listTopicMessage.length !== 0 &&
      !topic_message_id &&
      !refFirstTimeRender.current
    ) {
      refFirstTimeRender.current = true
      history.replace(
        `${ENTERPRISE_ROUTER_PATH.CHAT}/${listTopicMessage[0].id}`
      )
    }
  }, [listTopicMessage])

  useEffect(() => {
    const infiniteListTopic: any = document.getElementById('infiniteListTopic')
    infiniteListTopic.addEventListener('scroll', (e: any) => {
      const el = e.target
      if (
        el.clientHeight + el.scrollTop + 10 > el.scrollHeight &&
        !isLoadingMore
      ) {
        setCanLoading(true)
      }
    })
    getListTopic()
  }, [])

  useEffect(() => {
    if (canLoading) {
      getMoreTopic()
    }
  }, [canLoading])

  const getListTopic = async () => {
    setIsLoadingFirstTime(true)
    try {
      const res = await requestGetListTopicMessage({ page: 1 })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setPagingTopicMessage(formattedPaging)
      setListTopicMessage(res.data)
    } catch (error) {
      console.log('error', error)
    } finally {
      setIsLoadingFirstTime(false)
    }
  }

  const searchListTopicMessage = async (search: string) => {
    setIsSearchLoading(true)
    try {
      const res = await requestGetListTopicMessage({ page: 1, search })
      setListSearchTopicMessage(res.data)
    } catch (error) {
      console.log('error', error)
    } finally {
      setIsSearchLoading(false)
    }
  }

  const getMoreTopic = async () => {
    if (!isLoadingMore && listTopicMessage.length < pagingTopicMessage.total) {
      try {
        setIsLoadingMore(true)
        const res = await requestGetListTopicMessage({
          page: pagingTopicMessage.current + 1,
        })
        const formattedPaging = {
          total: res.paging.totalItemCount,
          current: res.paging.page,
          pageSize: res.paging.limit,
        }
        setPagingTopicMessage(formattedPaging)
        setListTopicMessage([...listTopicMessage, ...res.data])
        if (formattedPaging.total > listTopicMessage.length + res.data.length) {
          setCanLoading(false)
        } else {
          setCanLoading(true)
        }
      } catch (error) {
        console.log('err', error)
        setCanLoading(false)
      } finally {
        setIsLoadingMore(false)
      }
    }
  }
  const onReadMessage = (index: number, mode: number) => {
    if (mode === MODE_MESSAGE.DEFAULT) {
      const item = listTopicMessage[index]
      const message = item?.Messages[0]
      message.is_read = IS_READ.READ
      setListTopicMessage([...listTopicMessage])
    } else if ((mode = MODE_MESSAGE.SEARCH)) {
    } else if (mode === MODE_MESSAGE.NOT_READ) {
    } else {
    }
  }

  return (
    <div className="slidebar-chat">
      <div
        style={{
          width: '100%',
          height: 50,
          padding: 5,
        }}
      >
        <AutoSearch
          onSearchSubmit={(key: string) => {
            if (key) searchListTopicMessage(key)
            else setListSearchTopicMessage([])
          }}
          isSearchMode={isSearchMode}
          isSearchLoading={isSearchLoading}
          placeholder="Tìm kiếm..."
          onFocus={() => setIsSearchMode(true)}
          onBlur={() => setIsSearchMode(false)}
        />
        <hr style={{ width: '100%', color: 'gainsboro' }} />
      </div>
      {isSearchMode ? (
        // search mode
        <div className="scroll-list">
          {isSearchLoading ? (
            <Spin
              style={{
                marginTop: '50%',
                width: '100%',
                textAlign: 'center',
              }}
              tip="Đang tải..."
              size="large"
            />
          ) : (
            listSearchTopicMessage.map((item, index: number) => (
              <TopicChatItem
                key={item.id}
                id={item.id}
                count_message_not_read={item.count_message_not_read}
                Message={item?.Messages[0] || null}
                Shop={item.Shop}
                User={item.User}
                time_last_send={item.time_last_send}
                index={index}
                mode={MODE_MESSAGE.SEARCH}
                onReadMessage={onReadMessage}
              />
            ))
          )}
        </div>
      ) : (
        // default mode
        <div className="scroll-list" id="infiniteListTopic">
          {isLoadingFirstTime ? (
            <Spin
              style={{
                marginTop: '50%',
                width: '100%',
                textAlign: 'center',
              }}
              tip="Đang tải..."
              size="large"
            />
          ) : listTopicMessage.length > 0 ? (
            listTopicMessage.map((item, index: number) => (
              <TopicChatItem
                key={item.id}
                id={item.id}
                Message={item?.Messages[0] || null}
                count_message_not_read={item.count_message_not_read}
                Shop={item.Shop}
                User={item.User}
                time_last_send={item.time_last_send}
                index={index}
                mode={MODE_MESSAGE.DEFAULT}
                onReadMessage={onReadMessage}
              />
            ))
          ) : (
            <p
              style={{
                width: '100%',
                textAlign: 'center',
                marginTop: 25,
                color: 'gray',
                fontWeight: 'bold',
                userSelect: 'none',
              }}
            >
              Chưa có cuộc hội thoại nào
            </p>
          )}
          {isLoadingMore && (
            <div style={{ width: '90%', marginLeft: '5%', marginTop: 0 }}>
              <Skeleton
                loading={true}
                active
                avatar
                paragraph={{ rows: 1, width: '100%' }}
                title={{ width: '100%' }}
              />
            </div>
          )}
        </div>
      )}
    </div>
  )
}
export default SlideBar
