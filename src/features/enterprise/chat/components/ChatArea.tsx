import './css/styles.css'
import React, { useState, useEffect, useRef, useCallback } from 'react'
import { useParams } from 'react-router-dom'
import { useHistory } from 'react-router-dom'
import { useSelector } from 'react-redux'
import ChatHeader from './ChatHeader'
import { requestGetDetailTopicMessage } from '../ChatService'
import TypingChatArea from './TypingChatArea'
import ChatBody from './ChatBody'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import { ChatItemParam, UserInfoState, TopicMessageState } from '../types'

function ChatArea() {
  const history = useHistory()
  const { id } = useParams<ChatItemParam>()
  const { userInfo: UserInstance } = useSelector(
    (state: any) => state.authReducer
  )
  const [topicInfo, setTopicInfo] = useState<TopicMessageState | null>(null)
  const [selfInfo, setSelfInfo] = useState<UserInfoState | null>(null)
  const [otherInfo, setotherInfo] = useState<UserInfoState | null>(null)
  const [seltIsUser, setSeltIsUser] = useState<boolean>(false)
  const [isLoadingFirstTime, setIsLoadingFirstTime] = useState<boolean>(false)
  useEffect(() => {
    if (id) {
      getInfoTopicMessage()
    }
  }, [id])
  useEffect(() => {
    setUserChatInfo()
  }, [UserInstance, topicInfo])

  const getInfoTopicMessage = async () => {
    if (id) {
      try {
        setIsLoadingFirstTime(true)
        const res = await requestGetDetailTopicMessage(id)
        setTopicInfo(res.data)
      } catch (error) {
        console.log('err', error)
        history.replace(`${ENTERPRISE_ROUTER_PATH.CHAT}`)
      } finally {
        setIsLoadingFirstTime(false)
      }
    }
  }

  const setUserChatInfo = () => {
    if (UserInstance && topicInfo) {
      if (UserInstance.id === topicInfo?.User?.id) {
        setSelfInfo(topicInfo?.User)
        setotherInfo(topicInfo?.Shop)
        setSeltIsUser(true)
      } else if (UserInstance?.Shop?.id === topicInfo.Shop.id) {
        setSelfInfo(topicInfo?.Shop)
        setotherInfo(topicInfo?.User)
        setSeltIsUser(false)
      } else {
        alert('else')
      }
    }
  }
  return (
    <div style={{ height: '100%' }}>
      <ChatHeader
        name={otherInfo?.name}
        phone={otherInfo?.phone}
        lastSendMessage={topicInfo?.time_last_send}
        avatar={otherInfo?.profile_picture_url}
        isLoading={isLoadingFirstTime}
      />
      <ChatBody
        id={id}
        seltIsUser={seltIsUser}
        selfInfo={selfInfo}
        otherInfo={otherInfo}
        isLoading={isLoadingFirstTime}
      />
      <TypingChatArea id={id} otherInfo={otherInfo} />
    </div>
  )
}
export default ChatArea
