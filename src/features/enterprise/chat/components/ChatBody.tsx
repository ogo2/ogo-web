import './css/bootstrap.css'
import { useState, useEffect } from 'react'
import { Spin, Skeleton } from 'antd'
import { requestGetListMessage } from '../ChatService'
import MessageItem from './MessageItem'
import useWsListMessage from '../hooks/useWsListMessage'
import { MessageItem as IMessageItem, UserInfoState } from '../types'

interface ChatBodyProps {
  seltIsUser: boolean
  selfInfo: UserInfoState | null
  otherInfo: UserInfoState | null
  id?: string
  isLoading: boolean
}

function ChatBody({
  id,
  seltIsUser,
  selfInfo,
  otherInfo,
  isLoading,
}: ChatBodyProps) {
  const {
    listMessage,
    setListMessage,
    pagingMessage,
    setPagingMessage,
  } = useWsListMessage(id)
  const [isLoadingMore, setIsLoadingMore] = useState<boolean>(false)
  const [canLoading, setCanLoading] = useState<boolean>(false)
  useEffect(() => {
    const infiniteListBody: any = document.getElementById('infiniteListBody')
    infiniteListBody.addEventListener('scroll', (e: any) => {
      const el = e.target
      if (
        el.clientHeight - el.scrollHeight > el.scrollTop - 50 &&
        !isLoadingMore
      ) {
        setCanLoading(true)
      }
    })
    setPagingMessage({
      total: 0,
      current: 1,
      pageSize: 0,
    })
    setCanLoading(false)
  }, [id])
  useEffect(() => {
    if (canLoading) {
      getMoreMessage()
    }
  }, [canLoading])
  const getMoreMessage = async () => {
    if (id && !isLoadingMore && listMessage.length < pagingMessage.total) {
      try {
        setIsLoadingMore(true)
        const res = await requestGetListMessage(id, {
          page: pagingMessage.current + 1,
        })
        const formattedPaging = {
          total: res.paging.totalItemCount,
          current: res.paging.page,
          pageSize: res.paging.limit,
        }
        setPagingMessage(formattedPaging)
        setListMessage([...listMessage, ...res.data])
        if (formattedPaging.total > listMessage.length + res.data.length) {
          setCanLoading(false)
        } else {
          setCanLoading(true)
        }
      } catch (error) {
        console.log('err', error)
        setCanLoading(false)
      } finally {
        setIsLoadingMore(false)
      }
    }
  }
  return (
    <>
      <ol className="chat-body-scroll" id="infiniteListBody">
        {isLoading ? (
          <Spin
            style={{
              marginBottom: '20%',
              width: '100%',
              textAlign: 'center',
            }}
            tip="Đang tải..."
            size="large"
          />
        ) : (
          listMessage.map((item: IMessageItem, index: number) => (
            <MessageItem
              key={index}
              is_self={
                (seltIsUser && selfInfo?.id === item?.user_id) ||
                (!seltIsUser && selfInfo?.id === item?.shop_id) ||
                (!item?.user_id && selfInfo?.shop_id === item?.shop_id)
              }
              avatar={
                (seltIsUser && selfInfo?.id === item.user_id) ||
                (!seltIsUser && selfInfo?.id === item.shop_id)
                  ? selfInfo?.profile_picture_url
                  : otherInfo?.profile_picture_url
              }
              content={item.content}
              message_media_url={item.message_media_url}
              create_at={item.create_at}
              type_message_media={item.type_message_media}
            />
          ))
        )}
        {isLoadingMore && (
          <li style={{ width: '90%', marginLeft: '5%', marginTop: 0 }}>
            <Skeleton
              loading={true}
              active
              paragraph={{ rows: 1, width: '100%' }}
              title={{ width: '100%' }}
            />
          </li>
        )}
      </ol>
    </>
  )
}
export default ChatBody
