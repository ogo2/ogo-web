import { Avatar, Image } from 'antd'
import { MEDIA_TYPE } from 'utils/constants'
import { splitTextEndLine } from 'utils/funcHelper'
import { getFromDateToNow } from 'utils/TimerHelper'
import R from 'utils/R'

interface MessageProps {
  is_self: boolean
  avatar?: string | ''
  content?: string | ''
  message_media_url?: string | '' | null
  type_message_media?: number
  create_at: string | number | Date
}

function MessageItem({
  is_self,
  avatar,
  content,
  message_media_url,
  create_at,
  type_message_media,
}: MessageProps) {
  // console.log('is_self', is_self)
  return (
    <li className={is_self ? 'self' : 'other'}>
      <div className="avatar">
        <Avatar
          size={40}
          src={avatar}
          icon={<img src={R.images.img_ogo} />} //<UserOutlined /> ||
          draggable={false}
        />
      </div>
      <div className="msg">
        {message_media_url &&
          (type_message_media === MEDIA_TYPE.VIDEO ? (
            <video controls>
              <source src={message_media_url} type="video/mp4" />
            </video>
          ) : (
            // <img src={message_media_url} draggable="false" />
            <Image
              className="img"
              src={message_media_url}
              fallback={message_media_url}
              draggable="false"
            />
          ))}
        {content &&
          splitTextEndLine(content).map((text: string) => (
            <p
              style={{ fontWeight: 500, fontSize: 16, wordWrap: 'break-word' }}
            >
              {text}
            </p>
          ))}
        <time>{getFromDateToNow(create_at)}</time>
      </div>
    </li>
  )
}
export default MessageItem
