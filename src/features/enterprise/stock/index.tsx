import { message, PageHeader, Space, Table } from 'antd'
import { truncateSync } from 'fs'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import reactotron from 'ReactotronConfig'
import AddEditStock from './components/AddEditStock'
import Header from './components/Header'
import { StockModel } from './Model'
import { createStock, deleteStock, getStocks } from './StockApi'
import StockDetail from './StockDetail'

export default function Stock() {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const [showAddStock, setShowAddStock] = useState(false)
  const [showEditStock, setShowEditStock] = useState(false)
  const [currentSelected, setCurrentSelected] = useState({ id: -1 })
  const [data, setData] = useState<Array<any>>([])
  const [isLoading, setIsLoading] = useState(false)
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [params, setParams] = useState({
    search: '',
    page: 1,
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  useEffect(() => {
    getListStock()
  }, [params])

  const getListStock = async () => {
    try {
      setIsLoading(true)
      const res = await getStocks(params)
      const tableData = res.data.map((item: StockModel, index: number) => {
        return { ...item, key: item.id, stt: index + 1 }
      })
      console.log(tableData)
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setData(tableData)
      setPaging(formattedPaging)
    } catch (error) {
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  const onCreateStock = async (value: any) => {
    console.log(value)
    setIsLoading(true)
    try {
      const res = await createStock(value)
      setData([{ ...res.data, key: res.data.id }, ...data])
      message.success(`Thêm kho thành công`)
      setShowAddStock(false)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  const onDeleteStock = async (id: number, name: string) => {
    try {
      await deleteStock(id)
      message.success(`Đã xoá kho ${name}`)
      getListStock()
    } catch (error) {
      console.log(error)
    }
  }

  const columns = [
    {
      width: 70,
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
      render: (text: any, record: any, index: any) => (
        <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
          {(paging.current - 1) * paging.pageSize + index + 1}
        </td>
      ),
      ellipsis: true,
    },
    {
      title: 'Tên kho',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'create_at',
      key: 'create_at',
      render: (record: any) => {
        return <div>{moment(record).format('DD-MM-YYYY')}</div>
      },
    },
  ]

  return (
    <>
      <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
        <PageHeader
          title="Kho hàng"
          extra={[
            <Space>
              <Header
                CreateStock={setShowAddStock}
                // setShowAddStock={setShowAddStock}
                isSearchLoading={isSearchLoading}
                onSearchSubmit={(searchKey: string) => {
                  setIsSearchLoading(true)
                  setParams({ ...params, search: searchKey, page: 1 })
                }}
              />
            </Space>,
          ]}
        />
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <p>
          Kết quả lọc: <b>{paging.total}</b>
        </p>
        <Table
          className="table-expanded-custom-no-image"
          scroll={{
            x: 800,
            scrollToFirstRowOnChange: true,
            // y: 'calc(100vh - 340px)',
            y: `calc(${heightWeb}px - 340px)`,
          }}
          bordered
          dataSource={data}
          loading={isLoading}
          columns={columns}
          expandedRowKeys={[currentSelected.id]}
          onRow={r => ({
            onClick: () => {
              // console.log('r', r)
              if (currentSelected !== r) setCurrentSelected(r)
              else setCurrentSelected({ id: -1 })
              reactotron.log!(r)
            },
          })}
          expandable={{
            expandedRowRender: (record: any) => (
              <StockDetail
                isLoadingButton={isLoading}
                data={record}
                onDeleteStock={onDeleteStock}
                getListStock={getListStock}
              />
            ),
            onExpand: (status: any, r: any) => {
              if (currentSelected !== r) setCurrentSelected(r)
              else setCurrentSelected({ id: -1 })
              reactotron.log!(r)
            },
          }}
          pagination={{
            ...paging,
            onChange: async (page, pageSize) => {
              setParams({ ...params, page })
            },
          }}
        />
      </div>

      {showAddStock && (
        <AddEditStock
          isLoadingButton={isLoading}
          visible={showAddStock}
          onCancel={() => {
            setShowAddStock(false)
          }}
          onCreateStock={onCreateStock}
        />
      )}
    </>
  )
}
