import { Form, Input, Modal, Select } from 'antd'
import { Option } from 'antd/lib/mentions'
import ButtonBottomModal from 'common/components/ButtonBottomModal'
import { useEffect, useState } from 'react'
import { getAddress, getPlace } from '../StockApi'

type Props = {
  visible: boolean
  onCancel: any
  onCreateStock?: (newData: any) => void
  onUpdateStock?: any
  data?: any
  isLoadingButton: boolean
}

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
}

function convertDataToFrom(data: any) {
  if (!data) {
    return {
      name: null,
      address: null,
    }
  } else {
    return {
      ...data,
    }
  }
}

export default function AddEditStock({
  visible,
  onCancel,
  onCreateStock,
  onUpdateStock,
  data,
  isLoadingButton,
}: Props) {
  const [listOptionAddress, setListOptionAddress] = useState<Array<any>>([])
  const [form] = Form.useForm()
  const initialValues = convertDataToFrom(data)
  const [address, setAddress] = useState<string>('')

  useEffect(() => {
    if (address) {
      const run = setTimeout(() => {
        getDataAddress()
      }, 500)
      return () => {
        clearTimeout(run)
      }
    }
  }, [address])

  const getDataAddress = async () => {
    try {
      const res = await getAddress({ address: address })
      setListOptionAddress(res.data.predictions)
    } catch (error) {}
  }
  const onFinish = async (values: any) => {
    console.log('values', values.name)
    try {
      const res =
        values.place_id && (await getPlace({ placeId: values?.place_id }))
      console.log(values.palce_id)
      console.log('res', res)
      const addressByPlaceId = listOptionAddress.find(e => {
        return e.place_id == values.place_id
      })
      if (!data) {
        const newData = {
          df_ward_id: 0,
          df_district_id: 0,
          df_province_id: 0,
          address: addressByPlaceId.description.trim(),
          name: values?.name?.trim(),
          lat: res?.data?.lat || 1,
          long: res?.data?.long || 1,
        }
        onCreateStock && onCreateStock(newData)
        form.resetFields()
      } else {
        const newData = {
          long: res?.data?.long || data?.long,
          lat: res?.data?.lat || data?.lat,
          df_ward_id: 0,
          df_district_id: 0,
          df_province_id: 0,
          address:
            addressByPlaceId?.description?.trim() || data?.address?.trim(),
          name: values?.name?.trim() || data.name,
          id: data?.id,
        }
        onUpdateStock(newData, form.resetFields)
        form.resetFields()
      }
    } catch (error) {
      console.log(error)
    }
  }
  return (
    <Modal
      title={data ? 'Sửa kho hàng' : 'Thêm kho hàng'}
      visible={visible}
      maskClosable={false}
      footer={null}
      onCancel={() => {
        form.resetFields()
        onCancel()
      }}
    >
      <Form
        {...formItemLayout}
        labelAlign="left"
        form={form}
        onFinish={(values: any) => onFinish(values)}
        initialValues={initialValues}
        scrollToFirstError
      >
        <Form.Item
          label="Tên kho"
          name="name"
          rules={[
            {
              type: 'string',
              message: 'Nhập tên kho',
            },
            {
              required: true,
              message: 'Vui lòng nhập tên kho!',
            },
            {
              min: 1,
              max: 50,
              message: 'Vui lòng nhập từ 1 đến 50 ký tự!',
            },
            {
              whitespace: true,
              message: 'Vui lòng không nhập khoảng trắng',
            },
            {
              message: 'Vui lòng không nhập kí tự đặc biệt',
              validator: (_, value) => {
                const reg = /^[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/
                if (!reg.test(value) || value === '') {
                  return Promise.resolve()
                }
                return Promise.reject()
              },
            },
          ]}
        >
          <Input placeholder="Nhập tên kho" />
        </Form.Item>

        <Form.Item
          label="Địa chỉ"
          name="place_id"
          rules={[
            {
              type: 'string',
              message: 'Nhập địa chỉ kho',
            },
            {
              required: true,
              message: 'Vui lòng nhập địa chỉ kho!',
            },
          ]}
        >
          <Select
            showSearch
            placeholder="Nhập địa chỉ kho"
            optionFilterProp="children"
            onSearch={(value: string) => setAddress(value)}
          >
            {listOptionAddress.map((item, index) => {
              if (index > 5) {
              }
              return <Option value={item.place_id}>{item.description}</Option>
            })}
          </Select>
        </Form.Item>

        <ButtonBottomModal
          isLoadingButton={isLoadingButton}
          onCancel={() => {
            form.resetFields()
            onCancel()
          }}
          text={data ? 'Lưu' : 'Thêm mới'}
        />
      </Form>
    </Modal>
  )
}
