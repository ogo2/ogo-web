import { Button, Space, Row, Col } from 'antd'
import ButtonFixed from 'common/components/ButtonFixed'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import Icon, { PlusCircleOutlined } from '@ant-design/icons'
import React from 'react'
import R from 'utils/R'

type Props = { CreateStock: any; onSearchSubmit: any; isSearchLoading: boolean }

export default function Header({
  CreateStock,
  onSearchSubmit,
  isSearchLoading,
}: Props) {
  return (
    <Row gutter={16}>
      <Col className=" input-search_account">
        <TypingAutoSearch
          onSearchSubmit={(key: string) => {
            onSearchSubmit(key.trim())
          }}
          isSearchLoading={isSearchLoading}
          placeholder="Tên kho ..."
        />
      </Col>
      <Col>
        {/* <Button
          type="primary"
          onClick={() => {
            CreateStock()
          }}
        >
          {R.strings().action_create}
        </Button> */}
        <ButtonFixed
          // style={{
          //   minWidth: '70px',
          //   backgroundColor: '#00abba',
          //   borderColor: '#00abba',
          //   color: 'white',
          // }}
          setIsCreate={CreateStock}
          text="Thêm mới"
          // icon={<PlusCircleOutlined />}
          type="add"
        />
      </Col>
    </Row>
  )
}
