import { DeleteFilled, EditOutlined } from '@ant-design/icons'
import { Button, Card, Descriptions, message, Popconfirm } from 'antd'
import moment from 'moment'
import React, { useState } from 'react'
import AddEditStock from './components/AddEditStock'
import { updateStock } from './StockApi'

type Props = {
  data: any
  onDeleteStock: (id: number, name: string) => void
  getListStock?: any
  isLoadingButton: boolean
}

const ContentView = (data: any) => {
  return (
    <Descriptions title="Thông tin kho">
      <Descriptions.Item label="Tên kho">{data.name}</Descriptions.Item>
      <Descriptions.Item label="Địa chỉ">{data.address}</Descriptions.Item>
      <Descriptions.Item label="Ngày tạo">
        {moment(data.create_at).format('DD-MM-YYYY')}
      </Descriptions.Item>
    </Descriptions>
  )
}

export default function StockDetail({
  data,
  onDeleteStock,
  getListStock,
  isLoadingButton,
}: Props) {
  const [showEditStock, setShowEditStock] = useState(false)

  const onUpdateStock = async (newData: any, resetFields: any) => {
    console.log('newData', newData)
    try {
      const payload = {
        id: newData.id,
        data: {
          long: newData.long,
          lat: newData.lat,
          df_ward_id: newData.df_ward_id,
          df_district_id: newData.df_district_id,
          df_province_id: newData.df_province_id,
          address: newData.address,
          name: newData.name,
        },
      }
      const res = await updateStock(payload)
      console.log(res)
      message.success(`Cập nhật thành công`)
      resetFields()
      setShowEditStock(false)
    } catch (error) {
      console.log(error)
    }
    getListStock()
  }

  return (
    <>
      {showEditStock && (
        <AddEditStock
          isLoadingButton={isLoadingButton}
          data={data}
          visible={showEditStock}
          onCancel={() => {
            setShowEditStock(false)
          }}
          onUpdateStock={(newData: any, resetFields: any) => {
            onUpdateStock(newData, resetFields)
          }}
        />
      )}
      <Card
        style={{ width: '100%', backgroundColor: '#f6f9ff' }}
        actions={[
          <Button
            onClick={() => {
              setShowEditStock(true)
            }}
            type="text"
            size="large"
            icon={<EditOutlined color="red" />}
            style={{ color: '#1890ff' }}
          >
            Chỉnh sửa
          </Button>,

          <Popconfirm
            title={'Bạn chắc chắn muốn xoá kho này'}
            onConfirm={() => {
              onDeleteStock(data.id, data.name)
            }}
            okText="Xoá"
            cancelText="Quay lại"
            okButtonProps={{
              danger: true,
              type: 'primary',
              // loading: isShowDeleteConfirm,
            }}
          >
            <Button type="text" size="large" danger icon={<DeleteFilled />}>
              Xoá kho
            </Button>
          </Popconfirm>,
        ]}
      >
        {ContentView(data)}
      </Card>
    </>
  )
}
