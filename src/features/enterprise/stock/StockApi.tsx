import { ApiClient } from 'services/ApiService'
//Stock
export const getStocks = (payload: any) => ApiClient.get('/stock', payload)
export const createStock = (payload: any) => ApiClient.post('/stock', payload)
export const deleteStock = (payload: any) =>
  ApiClient.delete(`/stock/${payload}`)
export const updateStock = (payload: any) =>
  ApiClient.put(`/stock/${payload.id}`, payload.data)

//Upload Image
export const requestUploadImageProduct = (payload: any) =>
  ApiClient.post(`/files/upload/single/${payload.type}`, payload.data)

export const getAddress = (payload: any) =>
  ApiClient.get(`/google-address/place-auto`, payload)

export const getPlace = (payload: any) =>
  ApiClient.get(`/google-address/place`, payload)
