export type StockModel = {
  address: string
  create_at: string
  create_by: number
  delete_at: string
  delete_by: string
  df_district_id: number
  df_province_id: number
  df_provine_id: number
  df_ward_id: number
  id: number
  is_active: number
  lat: number
  long: number
  name: string
  shop_id: number
  update_at: string
  update_by: string
  version: number
}
