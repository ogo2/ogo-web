import React from 'react'
import { Button, PageHeader, Popconfirm } from 'antd'
import { ADMIN_ROUTER_PATH } from 'common/config'
import { useHistory, useParams } from 'react-router-dom'
import { deletePost } from '../CommunityPostApi'
import { useSelector } from 'react-redux'
import { notificationSuccess } from 'utils/notification'
import { ADMIN_ROLE } from 'utils/constants'
interface Post {
  shop_id?: number
}
function DetailPostHeader(shop_id: any) {
  const history = useHistory()
  const param: any = useParams()
  const userState = useSelector((state: any) => state.authReducer)?.userInfo
  console.log('userInfo', userState)
  return (
    <PageHeader
      style={{ width: '100%' }}
      title="Chi tiết bài đăng"
      onBack={() => {
        history.goBack()
      }}
      extra={[
        <Popconfirm
          placement="bottomRight"
          title="Bạn có chắc chắn muốn xoá?"
          onConfirm={async () => {
            const res = await deletePost(param.id)
            if (res.code === 1) {
              history.push({
                pathname: `${ADMIN_ROUTER_PATH.COMMUNITY_POST}`,
              })
              notificationSuccess('Đã xóa bài đăng')
            }
          }}
          okText="Xoá"
          cancelText="Huỷ"
          okButtonProps={{
            danger: true,
            type: 'primary',
          }}
          disabled={
            userState?.df_type_user_id === ADMIN_ROLE.ADMIN ||
            userState?.shop_id === shop_id
              ? false
              : true
          }
        >
          <Button
            danger
            disabled={
              userState?.df_type_user_id === ADMIN_ROLE.ADMIN ||
              userState?.shop_id === shop_id
                ? false
                : true
            }
          >
            Xoá
          </Button>
        </Popconfirm>,
      ]}
    />
  )
}
export default DetailPostHeader
