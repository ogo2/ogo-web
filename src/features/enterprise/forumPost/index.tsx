import {
  Button,
  Col,
  DatePicker,
  Input,
  PageHeader,
  Pagination,
  Row,
  Select,
  Space,
  Image,
  Tag,
  Divider,
} from 'antd'
import moment from 'moment'
import { HeartFilled, MessageOutlined } from '@ant-design/icons'
import { useState, useEffect } from 'react'
import { ADMIN_POST_TOPIC } from 'utils/constants'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import Avatar from 'antd/lib/avatar/avatar'
import { UserOutlined } from '@ant-design/icons'
import { listCommunityPost } from './CommunityPostApi'
import { useHistory } from 'react-router-dom'
import { Empty } from 'antd'
import { useSelector } from 'react-redux'
import { lineBreakPost } from 'utils/funcHelper'

const { Option } = Select
const { RangePicker } = DatePicker
const { Search } = Input

export default function CommunityPost() {
  const userInfo = useSelector((state: any) => state.authReducer)?.userInfo
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [dataPost, setDataPost] = useState<Array<any>>([])
  const history = useHistory()
  const [paramSearch, setParamSearch] = useState({
    search: '',
    user_id: undefined,
    shop_id: userInfo?.shop_id,
    topic_id: undefined,
    from_date: '',
    to_date: '',
    is_posted: undefined,
    page: 1,
  })
  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  useEffect(() => {
    paramSearch.shop_id = userInfo?.shop_id
    setParamSearch({ ...paramSearch, shop_id: userInfo?.shop_id })
  }, [userInfo])

  useEffect(() => {
    setTimeout(() => {
      getListCommunityPost()
    }, 300)
  }, [paramSearch])

  const getListCommunityPost = async () => {
    setIsSearchLoading(true)
    try {
      const res = await listCommunityPost(paramSearch)
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setPaging(formattedPaging)
      setDataPost(res.data)
    } catch (error) {
      console.log(error)
    } finally {
      setIsSearchLoading(false)
    }
  }
  return (
    <div>
      <div style={{ backgroundColor: 'white', margin: '5px 10px' }}>
        <PageHeader
          title="Bài đăng cộng đồng"
          extra={
            [
              // <Button
              //   type="primary"
              //   onClick={() => {
              //     history.push({
              //       pathname: `${ADMIN_ROUTER_PATH.ADD_POST}`,
              //     })
              //   }}
              // >
              //   Thêm bài đăng
              // </Button>,
            ]
          }
        />
      </div>
      <div
        style={{
          backgroundColor: 'white',
          margin: '10px 10px 10px',
          padding: '20px 20px 20px',
        }}
      >
        <Row gutter={[16, 16]} justify="end">
          <Col span={10}>
            <Search
              allowClear
              placeholder="Người đăng, nội dung bài đăng ..."
              loading={isSearchLoading}
              onChange={e => {
                setParamSearch({
                  ...paramSearch,
                  search: e.target.value.trim(),
                })
              }}
            />
          </Col>
          <Col span={5}>
            <Select
              placeholder="Chọn chủ đề"
              allowClear
              // style={{ minWidth: '120px' }}
              onChange={(value: any) => {
                setParamSearch({
                  ...paramSearch,
                  topic_id: value === '' ? undefined : value,
                })
              }}
            >
              <Option value="">Tất cả</Option>
              <Option value={ADMIN_POST_TOPIC.PROMOTION}>Khuyến mãi</Option>
              <Option value={ADMIN_POST_TOPIC.NOTIFICATION}>Thông báo</Option>
            </Select>
          </Col>
          {/* <Select
                allowClear
                placeholder="Trạng thái hoạt động"
                style={{ minWidth: '180px' }}
                onChange={(value: any) => {
                  setParamSearch({
                    ...paramSearch,
                    is_posted: value,
                  })
                }}
              >
                <Option value={0}>Chưa đăng</Option>
                <Option value={1}>Đã đăng</Option>
              </Select> */}
          <Col span={6}>
            <RangePicker
              placeholder={['Từ ngày', 'đến ngày']}
              onChange={(value, dateString) => {
                setParamSearch({
                  ...paramSearch,
                  from_date: dateString[0],
                  to_date: dateString[1],
                })
              }}
            />
          </Col>
        </Row>
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '10px 10px',
          padding: '15px 15px',
        }}
      >
        <p>
          Kết quả lọc: <b>{dataPost.length}</b>
        </p>
        {dataPost.length === 0 ? (
          <Empty
            description={
              <span>Chưa có bài đăng. Hãy là người đầu tiên đăng bài</span>
            }
          />
        ) : (
          <div style={{ overflowY: 'scroll', height: 'calc(100vh - 170px)' }}>
            {dataPost.map((item: any) => {
              return (
                <div
                  style={{ marginBottom: '25px' }}
                  onClick={() => {
                    history.push({
                      pathname: `${ENTERPRISE_ROUTER_PATH.FORUM_POST}/${item.id}`,
                    })
                  }}
                >
                  <Row>
                    <Col>
                      <Avatar
                        size={64}
                        icon={<UserOutlined />}
                        src={
                          item?.Shop?.profile_picture_url
                            ? item.Shop?.profile_picture_url
                            : item.User?.profile_picture_url
                        }
                      />
                      <Avatar
                        size={30}
                        icon={<UserOutlined />}
                        style={{
                          bottom: '-27px',
                          left: '-25px',
                          border: '1px #2E64FE solid',
                        }}
                        src={item?.Shop?.profile_picture_url}
                      />
                    </Col>
                    <Col style={{ marginLeft: '15px' }}>
                      {item.Shop ? (
                        <b>{item.Shop.name + '( ' + item.User.name + ') '}</b>
                      ) : (
                        <b>{item.User.name}</b>
                      )}

                      <Row>
                        <p>{' ' + moment(item.create_at).fromNow() + ' '}</p>
                        <p>
                          {item.is_posted ? ' ( Đã đăng )' : ' ( Chưa đăng )'}
                        </p>
                        <Tag
                          color="#B47EDE"
                          style={{ marginLeft: '10px', height: '25px' }}
                        >
                          {item.Topic.name}
                        </Tag>
                      </Row>
                    </Col>
                    <p style={{ paddingLeft: '43%' }}>
                      Ngày viết:
                      <b>
                        {' ' +
                          moment(item.create_at).format('HH:mm DD/MM/YYYY ')}
                      </b>
                      <br></br>
                      Ngày đăng:
                      <b>
                        {item.schedule
                          ? ' ' +
                            moment(item.schedule).format('HH:mm DD/MM/YYYY ')
                          : ' ' +
                            moment(item.create_at).format('HH:mm DD/MM/YYYY ')}
                      </b>
                    </p>
                  </Row>
                  <div style={{ marginTop: '10px' }}>
                    {item.content &&
                      lineBreakPost(item.content).map((text: string) => (
                        <p style={{ marginBottom: '1px' }}>{text}</p>
                      ))}
                    <Row>
                      {item.PostMedia.map((itemA: any) => {
                        return (
                          <Col
                            span={5}
                            style={{
                              maxHeight: '145px',
                              maxWidth: '200px',
                              display: 'flex',
                              justifyContent: 'center',
                              alignItems: 'center',
                              marginRight: '10px',
                              overflow: 'hidden',
                              marginBottom: '10px',
                            }}
                          >
                            {itemA.media_url.indexOf('.mp4') === -1 ? (
                              <Image
                                src={itemA.media_url}
                                onClick={e => e.stopPropagation()}
                              />
                            ) : (
                              <video
                                controls
                                src={itemA.media_url}
                                className="uploaded-pic img-thumbnail "
                                width={200}
                                height={145}
                              />
                            )}
                          </Col>
                        )
                      })}
                    </Row>
                    <Row style={{ marginBottom: 5 }}>
                      <Col>
                        <Button
                          type="text"
                          danger={item?.is_reaction ? true : false}
                          icon={<HeartFilled style={{ fontSize: 18 }} />}
                        >
                          <span style={{ fontSize: 18 }}>
                            {item?.count_like}
                          </span>
                        </Button>
                      </Col>
                      <Col>
                        <Button
                          type="text"
                          danger={item?.is_reaction ? true : false}
                          icon={<MessageOutlined style={{ fontSize: 18 }} />}
                        >
                          <span style={{ fontSize: 18 }}>
                            {item?.count_comment}
                          </span>
                        </Button>
                      </Col>
                    </Row>
                  </div>
                  {dataPost.length === 1 ? (
                    <></>
                  ) : (
                    <Divider style={{ backgroundColor: '#A4A4A4' }} />
                  )}
                </div>
              )
            })}
            <Pagination
              showSizeChanger={false}
              defaultCurrent={paging.current}
              total={paging.total}
              pageSize={paging.pageSize}
              onChange={async (page, pageSize) => {
                setParamSearch({ ...paramSearch, page })
              }}
            />
          </div>
        )}
      </div>
    </div>
  )
}
