import { ApiClient } from 'services/ApiService'

export const getDataOverviews = (payload: any) =>
  ApiClient.get('/shop/overview', payload)

export const getDash = (payload: any) => ApiClient.get('/client/home', payload)
