import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { PageHeader, Row, Space, Divider, Card, Col, Spin } from 'antd'
import Cards from './components/Cards'
import { Header } from './components/Header'
import { OrderDashboard } from './components/OrderDashboard'
import { ListLive } from './components/ListLiveDash'
import { ChartLive } from './components/ChartLive'
import moment from 'moment'
import { getDataOverviews } from './DashBoardApi'

export default function DashBoard() {
  const userState = useSelector((state: any) => state.authReducer)?.userInfo
  const [params, setParams] = useState<any>({
    search: '',
  })
  const [fromDaytoDay, setFromDaytoDay] = useState([
    moment().subtract(14, 'days').format('YYYY-MM-DD'),
    moment().format('YYYY-MM-DD'),
  ])
  const [dataCard, setDataCard] = useState({
    count_turnover: 0,
    count_post: 0,
    count_livestream: 0,
    count_product: 0,
  })
  const [order, setOrder] = useState({
    count_pending: 0,
    count_ship: 0, 
    count_cancel: 0,
    count_success: 0,
    count_confirmed: 0,
  })
  const [stateLoading, setStateLoading] = useState(false)
  const [listLiveStream, setListLiveStream] = useState<Array<any>>([])
  const [dataDashboardOrder, setDataDashboardOrder] = useState<any>({})
  const [sumLiveStream, setSumLiveStream] = useState<any>({})
  const [dataDashboardLive, setDataDashboardLive] = useState<any>({})
  const [view, setView] = useState<number>(0)

  useEffect(() => {
    getData()
  }, [fromDaytoDay])

  const getData = async () => {
    setStateLoading(true)
    try {
      const res = await getDataOverviews({
        from_date: fromDaytoDay[0],
        to_date: fromDaytoDay[1],
      })
      setDataCard({
        count_turnover: res.data.count_turnover,
        count_post: res.data.count_post,
        count_livestream: res.data.count_livestream,
        count_product: res.data.count_product,
      })
      setOrder(res.data.order)
      setListLiveStream(res.data.list_livestream)
      setDataDashboardOrder(res.data.chart_order)
      setSumLiveStream(res.data.livestream[0])
      setDataDashboardLive(res.data.chart_livestream)
      setView(parseInt(res.data.livestream_view))
    } catch (error) {
      console.log(error)
    }finally{
      setStateLoading(false)
    }
  }
  // const getData = async () => {
  //   try {
  //     setIsLoading(true)
  //     const res = await getDash(params)
  //     const data_dash = res.data.rows.map((item: any) => {
  //       return {
  //         ...item,
  //         key: item.id,
  //       }
  //     })
  //     setDash(data_dash)
  //   } catch (error) {
  //     console.log(error)
  //   } finally {
  //     setIsLoading(false)
  //   }
  // }

  // useEffect(() => {
  //   getData()
  // }, [params])

  // useEffect(() => {
  //   getData()
  // }, [])
  return (
    <div>
      {userState?.df_type_user_id === 2 && (
        <div style={{ margin: '5px 10px 15px' }}>
          <PageHeader
            style={{ backgroundColor: 'white', marginBottom: '15px' }}
            title="Tổng quan"
            extra={[
              <Space>
                <Header
                  fromDaytoDay={fromDaytoDay}
                  dateOnSubmit={(x: string, y: string) => {
                    setFromDaytoDay([x, y])
                  }}
                />
              </Space>,
            ]}
          />
            <Spin spinning={stateLoading}>
          <div style={{ marginBottom: '15px' }}>
            <Cards
              count_turnover={dataCard.count_turnover}
              count_post={dataCard.count_post}
              count_livestream={dataCard.count_livestream}
              count_product={dataCard.count_product}
            />
          </div>
          <Row style={{ marginBottom: '15px' }}>
            <OrderDashboard data={dataDashboardOrder} order={order} />
            <ListLive
              list={listLiveStream}
              // data_dash={dash}
            />
          </Row>
          <div
            style={{
              marginBottom: '15px',
              backgroundColor: 'white',
              borderRadius: '5px',
            }}
          >
            <ChartLive
              data={dataDashboardLive}
              sumLiveStream={sumLiveStream}
              view={view}
            />
          </div>
        </Spin>
        </div>
      )}
    </div>
  )
}
