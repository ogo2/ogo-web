import { LikeOutlined, MessageOutlined, EyeOutlined } from '@ant-design/icons'
import { message, List, Avatar, Space, Spin, Typography } from 'antd'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import history from 'utils/history'

type Props = {
  list: Array<any>
}

export const ListLive = ({ list }: Props) => {
  const data = []
  for (let i = 0; i < 50; i++) {
    data.push({
      key: i,
      name: `Live stream ${i}`,
    })
  }

  const { Text } = Typography
  const EllipsisMiddle = ({ suffixCount, children }: any) => {
    const start = children.slice(0, children.length - suffixCount).trim()
    const suffix = children.slice(-suffixCount).trim()
    return (
      <Text style={{ maxWidth: '100%' }} ellipsis={{ suffix }}>
        {start}
      </Text>
    )
  }

  const IconText = ({ icon, text }: any) => (
    <Space>
      {React.createElement(icon)}
      {text}
    </Space>
  )
  return (
    <div className="list-live">
      <span style={{ fontWeight: 600, fontSize: '18px' }}>Live stream</span>
      <div className="divider"></div>
      <div
        style={{
          height: '60vh',
          overflowX: 'hidden',
          overflowY: 'scroll',
          display: 'block',
        }}
      >
        <List
          dataSource={list}
          renderItem={item => (
            <List.Item
              onDoubleClick={() => {
                history.push({
                  pathname: `${ENTERPRISE_ROUTER_PATH.LIVE_STREAM_DETAIL}/${item?.id}`,
                })
              }}
              key={item.id}
              actions={[
                <IconText
                  icon={LikeOutlined}
                  text={item.count_reaction}
                  key="like"
                />,
                <IconText
                  icon={EyeOutlined}
                  text={item.count_viewed}
                  key="viewed"
                />,
                <IconText
                  icon={MessageOutlined}
                  text={item.count_comment}
                  key="comment"
                />,
              ]}
            >
              <List.Item.Meta
                avatar={<Avatar src={item.cover_image_url} size={100} />}
                title={
                  <EllipsisMiddle suffixCount={12}>{item.title}</EllipsisMiddle>
                }
                description={moment(item.update_at).format('HH:mm DD/MM/YYYY')}
              />
            </List.Item>
          )}
        ></List>
      </div>
    </div>
  )
}
