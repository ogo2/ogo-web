import { Col, DatePicker, Divider, Row, Select } from 'antd'
import '../../../admin/liveStream/css/Comment.css'
import { Line } from 'react-chartjs-2'
import { O_DIRECTORY } from 'constants'
type Props = {
  order: {
    count_pending: number
    count_ship: number
    count_cancel: number
    count_success: number
    count_confirmed: number
  }
  data: any
}
export const OrderDashboard = ({ order, data }: Props) => {
  // const data = {
  //   labels: [
  //     'Jans',
  //     'Feb',
  //     'Mar',
  //     'Apr',
  //     'May',
  //     'Jun',
  //     'Jul',
  //     'Aug',
  //     'Sep',
  //     'Oct',
  //     'Nov',
  //     'Dec',
  //   ],
  //   datasets: [
  //     {
  //       label: 'Hoàn thành',
  //       fill: false,
  //       lineTension: 0.1,
  //       backgroundColor: 'orange',
  //       borderColor: 'orange',
  //       borderCapStyle: 'butt',
  //       borderDash: [],
  //       borderDashOffset: 0.0,
  //       borderJoinStyle: 'miter',
  //       pointBorderColor: 'orange',
  //       pointBackgroundColor: 'yellow',
  //       pointBorderWidth: 1,
  //       pointHoverRadius: 5,
  //       pointHoverBackgroundColor: 'orange',
  //       pointHoverBorderColor: 'rgba(220,220,220,1)',
  //       pointHoverBorderWidth: 2,
  //       pointRadius: 1,
  //       pointHitRadius: 10,
  //       data: [15, 30, 50, 5, 120, 100, 200, 50, 10, 80, 55, 6],
  //     },
  //     {
  //       label: 'Hủy/ từ chối',
  //       fill: false,
  //       lineTension: 0.1,
  //       backgroundColor: 'purple',
  //       borderColor: 'purple',
  //       borderCapStyle: 'butt',
  //       borderDash: [],
  //       borderDashOffset: 0.0,
  //       borderJoinStyle: 'miter',
  //       pointBorderColor: 'purple',
  //       pointBackgroundColor: 'violet',
  //       pointBorderWidth: 1,
  //       pointHoverRadius: 5,
  //       pointHoverBackgroundColor: 'purple',
  //       pointHoverBorderColor: 'rgba(220,220,220,1)',
  //       pointHoverBorderWidth: 2,
  //       pointRadius: 1,
  //       pointHitRadius: 10,
  //       data: [150, 60, 90, 12, 150, 30, 134, 98, 122, 97, 21, 1],
  //     },
  //   ],
  // }
  return (
    <div className="order-dash">
      <span style={{ fontWeight: 600, fontSize: '18px' }}>Đơn hàng</span>
      <div className="divider"></div>
      <div>
        <Row justify="space-between" gutter={[6, 6]}>
          <Col span={5}>
            <div style={{ textAlign: 'center', fontSize: 14, fontWeight: 600 }}>
              <p style={{ minWidth: 100 }}>Chờ xác nhận</p>
              <div
                className="orderCard"
                style={{
                  borderColor: 'orange',
                  color: 'orange',
                }}
              >
                {order.count_pending}
              </div>
            </div>
          </Col>
          <Col span={5}>
            <div style={{ textAlign: 'center', fontSize: 14, fontWeight: 600 }}>
              <p style={{ minWidth: 100 }}>Hoàn thành</p>
              <div
                className="orderCard"
                style={{
                  borderColor: '#af7bd6',
                  color: '#af7bd6',
                }}
              >
                {order.count_success}
              </div>
            </div>
          </Col>
          <Col span={5}>
            <div style={{ textAlign: 'center', fontSize: 14, fontWeight: 600 }}>
              <p style={{ minWidth: 100 }}>Đang giao</p>
              <div
                className="orderCard"
                style={{
                  borderColor: 'rgb(90 90 241)',
                  color: 'rgb(90 90 241)',
                }}
              >
                {order.count_ship}
              </div>
            </div>
          </Col>
          <Col span={5}>
            <div style={{ textAlign: 'center', fontSize: 14, fontWeight: 600 }}>
              <p style={{ minWidth: 100 }}>Hủy</p>
              <div
                className="orderCard"
                style={{
                  borderColor: '#ea1212',
                  color: '#ea1212',
                }}
              >
                {order.count_cancel}
              </div>
            </div>
          </Col>
          <Col span={5}>
            <div style={{ textAlign: 'center', fontSize: 14, fontWeight: 600 }}>
              <p style={{ minWidth: 100 }}>Đã xác nhận</p>
              <div
                className="orderCard"
                style={{
                  borderColor: '#54e854',
                  color: '#54e854',
                }}
              >
                {order.count_cancel}
              </div>
            </div>
          </Col>
        </Row>
        <div style={{ paddingTop: '10px' }}>
          {/* <Spin style={{ marginTop: 30 }} size="large"> */}
          <Line
            //   type="line"
            data={data}
            height={150}
            //   options={options_line_chart}
          />
          {/* </Spin> */}
        </div>
      </div>
    </div>
  )
}
