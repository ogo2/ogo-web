import { Row, Button } from 'antd'
import {
  DollarCircleFilled,
  FileTextFilled,
  VideoCameraFilled,
  DropboxOutlined,
  TeamOutlined,
} from '@ant-design/icons'
import history from 'utils/history'
import '../../../admin/liveStream/css/Comment.css'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import NumberFormat from 'react-number-format'

type Props = {
  count_turnover: number
  count_post: number
  count_livestream: number
  count_product: number
}
export default function Cards({
  count_turnover,
  count_post,
  count_livestream,
  count_product,
}: Props) {
  return (
    <div>
      <Row>
        <Button
          style={{
            width: '24.4%',
            height: 'auto',
            borderTopColor: '#ea1212',
            borderTopWidth: '5px',
            borderRadius: '5%',
          }}
          onClick={() => {
            history.push({
              pathname: `${ENTERPRISE_ROUTER_PATH.ORDER}`,
            })
          }}
        >
          <p className="titleDash">Doanh thu đơn hàng</p>
          <Row>
            <div style={{ width: '70%' }}></div>
            <div>
              <DollarCircleFilled
                style={{
                  fontSize: '380%',
                  color: 'lightgray',
                }}
              />
            </div>
            <div
              style={{
                position: 'absolute',
                fontWeight: 500,
                fontSize: '16px',
              }}
            >
              {/* <p>{count_turnover}</p> */}
              <NumberFormat
                value={count_turnover}
                displayType={'text'}
                thousandSeparator={true}
                suffix={' đ'}
              />
            </div>
          </Row>
        </Button>
        <div style={{ width: '0.8%' }}></div>
        {/* <Button
          style={{
            width: '24.4%',
            height: 'auto',
            borderTopColor: 'orange',
            borderTopWidth: '5px',
            borderRadius: '4%',
          }}
          onClick={() => {
            history.push({
              pathname: `${ENTERPRISE_ROUTER_PATH.ORDER}`,
            })
          }}
        >
          <p className="titleDash">Doanh thu gói DV</p>
          <Row>
            <div style={{ width: '70%' }}></div>
            <div>
              <FileTextFilled
                style={{
                  fontSize: '380%',
                  color: 'lightgray',
                }}
              />
            </div>
            <div style={{ position: 'absolute' }}>
              <p>Card content</p>
            </div>
          </Row>
        </Button> */}
        {/* <div style={{ width: '0.8%' }}></div> */}
        <Button
          style={{
            width: '24.4%',
            height: 'auto',
            borderTopColor: 'rgb(90 90 241)',
            borderTopWidth: '5px',
            borderRadius: '5%',
          }}
          onClick={() => {
            history.push({
              pathname: `${ENTERPRISE_ROUTER_PATH.FORUM_POST}`,
            })
          }}
        >
          <p className="titleDash">Bài đăng diễn đàn</p>
          <Row>
            <div style={{ width: '70%' }}></div>
            <div>
              <TeamOutlined
                style={{
                  fontSize: '380%',
                  color: 'lightgray',
                  // paddingLeft: '24.40%',
                }}
              />
            </div>
            <div
              style={{
                position: 'absolute',
                fontWeight: 500,
                fontSize: '16px',
              }}
            >
              <p>{count_post}</p>
            </div>
          </Row>
        </Button>
        <div style={{ width: '0.8%' }}></div>
        <Button
          style={{
            width: '24.4%',
            height: 'auto',
            borderTopColor: '#05ae05',
            borderTopWidth: '5px',
            borderRadius: '5%',
          }}
          onClick={() => {
            history.push({
              pathname: `${ENTERPRISE_ROUTER_PATH.LIVE_STREAM}`,
            })
          }}
        >
          <p className="titleDash">Live stream</p>
          <Row>
            <div style={{ width: '70%' }}></div>
            <div>
              <VideoCameraFilled
                style={{
                  fontSize: '380%',
                  color: 'lightgray',
                  // paddingLeft: '24.40%',
                }}
              />
            </div>
            <div
              style={{
                position: 'absolute',
                fontWeight: 500,
                fontSize: '16px',
              }}
            >
              <p>{count_livestream}</p>
            </div>
          </Row>
        </Button>
        <div style={{ width: '0.8%' }}></div>
        <Button
          style={{
            width: '24.4%',
            height: 'auto',
            borderTopColor: '#0cca0c',
            borderTopWidth: '5px',
            borderRadius: '5%',
          }}
          onClick={() => {
            history.push({
              pathname: `${ENTERPRISE_ROUTER_PATH.PRODUCT}`,
            })
          }}
        >
          <p className="titleDash">Sản phẩm</p>
          <Row>
            <div style={{ width: '70%' }}></div>
            <div>
              <DropboxOutlined
                style={{
                  fontSize: '380%',
                  color: 'lightgray',
                  // paddingLeft: '24.40%',
                }}
              />
            </div>
            <div
              style={{
                position: 'absolute',
                fontWeight: 500,
                fontSize: '16px',
              }}
            >
              <p>{count_product}</p>
            </div>
          </Row>
        </Button>
      </Row>
    </div>
  )
}
