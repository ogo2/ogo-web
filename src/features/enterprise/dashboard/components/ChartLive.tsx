import { Row, Descriptions, Spin, Empty } from 'antd'
import '../../../admin/liveStream/css/Comment.css'
import { Line } from 'react-chartjs-2'
// import {  Chart, ChartType, ...} from 'react-chartjs-2'
import { useRef } from 'react'
import {
  EyeOutlined,
  FieldTimeOutlined,
  LikeOutlined,
  MessageOutlined,
  PlayCircleOutlined,
} from '@ant-design/icons'

type Props = {
  sumLiveStream: any
  data: any
  view: number
}
export const ChartLive = ({ data, sumLiveStream, view }: Props) => {
  return (
    <Row style={{ padding: '15px 20px', width: '100%' }}>
      <div style={{ width: '30.8%' }}>
        <span style={{ fontWeight: 600, fontSize: '18px' }}>
          Tổng hợp live stream
        </span>
        <div className="divider"></div>
        <Descriptions style={{ paddingTop: '20px', paddingRight: '50px' }}>
          <Descriptions.Item label={<b>Số live stream</b>} span={3}>
            <span className="descriptions-live">
              {sumLiveStream.count_livestream}
            </span>
            <div style={{ marginLeft: '20px' }}>
              <PlayCircleOutlined />
            </div>
          </Descriptions.Item>
          <Descriptions.Item label={<b>Lượt xem</b>} span={3}>
            <span className="descriptions-live">
              {sumLiveStream.count_viewed}
            </span>
            <div style={{ marginLeft: '20px' }}>
              <EyeOutlined />
            </div>
          </Descriptions.Item>
          <Descriptions.Item label={<b>Thời gian trung bình</b>} span={3}>
            <span className="descriptions-live">{view}</span>
            <div style={{ marginLeft: '20px' }}>
              <FieldTimeOutlined />
            </div>
          </Descriptions.Item>
          <Descriptions.Item label={<b>Lượt bình luận</b>} span={3}>
            <span className="descriptions-live">
              {sumLiveStream.count_comment}
            </span>
            <div style={{ marginLeft: '20px' }}>
              <MessageOutlined />
            </div>
          </Descriptions.Item>
          <Descriptions.Item label={<b>Lượt thích</b>} span={3}>
            <span className="descriptions-live">
              {sumLiveStream.count_reaction}
            </span>
            <div style={{ marginLeft: '20px' }}>
              <LikeOutlined />
            </div>
          </Descriptions.Item>
        </Descriptions>
      </div>

      <div
        style={{
          width: '0.2%',
        }}
      >
        <div
          style={{
            width: '100%',
            height: '15%',
            backgroundColor: 'white',
          }}
        ></div>
        <div
          style={{
            width: '100%',
            height: '85%',
            backgroundColor: 'rgb(220 223 228)',
          }}
        ></div>
      </div>

      <div style={{ width: '69%', paddingLeft: '50px' }}>
        <span style={{ fontWeight: 600, fontSize: '18px' }}>
          Biểu đồ số liệu
        </span>
        <div className="divider"></div>
        <div>
          {/* <Spin style={{ marginTop: 30 }} size="large"> */}
          <Line
            // type="line"
            data={data}
            height={150}
            // options={options_line_chart}
          />
          {/* </Spin> */}
        </div>
      </div>
    </Row>
  )
}
