import { ApiClient } from 'services/ApiService'

export const getReport = (payload: any) =>
  ApiClient.get('/report/report-sell', payload)
