import {
  Affix,
  Button,
  message,
  PageHeader,
  Space,
  Table,
  Typography,
} from 'antd'
import React, { useEffect, useState } from 'react'
import { formatPrice, number } from 'utils/ruleForm'
import 'antd/dist/antd.css'
import moment from 'moment'
import { msToTime } from 'utils/funcHelper'
import { Filter } from './components/Filter'
import { getAllLiveReport, getLiveReport } from './LiveReportApi'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import { useHistory } from 'react-router'
import ExportCsv from 'utils/ExportCSV'
import { DownloadOutlined, SaveOutlined } from '@ant-design/icons'
import { useSelector } from 'react-redux'

export default function ShopReport() {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const [isLoading, setIsLoading] = useState(false)
  const [listLiveReport, setListLiveReport] = useState([
    {
      key: 1,
      id: 0,
      // name: '',
      // total_livestream: 0,
      // count_reaction: 0,
      // count_comment: 0,
      // count_viewed: 0,
      // livestream_enable: 1,
      // avg_time_live: '',
      // status: 0,
    },
  ])
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [params, setParams] = useState<any>({
    search: '',
    df_type_user_id: '',
    page: 1,
    limit: 24,
    status: null,
  })

  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const [pagings, setPagings] = useState({
    pageSize: 1,
  })

  const [itemSelected, setItemSelected] = useState({ id: 0, rowIndex: 0 })
  const history = useHistory()

  const getData = async () => {
    setIsLoading(true)
    try {
      const res = await getLiveReport(params)
      console.log(res.data)
      const tableData = res.data.map((item: any, index: number) => {
        return { ...item, key: item.id, stt: index + 1 }
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setListLiveReport(tableData)
      setPaging(formattedPaging)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  useEffect(() => {
    getData()
  }, [params])

  const { Text } = Typography

  const columns = [
    {
      width: 70,
      title: 'Stt',
      dataIndex: 'index',
      key: 'index',
      render: (text: any, record: any, index: any) => (
        <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
          {(paging.current - 1) * paging.pageSize + index + 1}
        </td>
      ),
      ellipsis: true,
    },
    {
      title: 'Mô tả live',
      dataIndex: 'title',
      key: 'title',
    },
    {
      title: 'Thời gian bắt đầu',
      dataIndex: 'start_at',
      key: 'start_at',
      render: (value: string) => (
        <td>
          {moment(value).format('HH:mm:ss DD/MM/YYYY')}
          {/* {msToTime(moment(value).unix())} */}
          {/* <br />
          {value == null ? 'dd-mm-yyyy' : moment(value).format('DD-MM-YYYY')} */}
        </td>
      ),
    },
    {
      title: 'Thời gian kết thúc',
      dataIndex: 'finish_at',
      key: 'finish_at',
      // render: (value: string) => <td>{moment(value).format('DD-MM-YYYY')}</td>,
      render: (value: string) => (
        <td>
          {moment(value).format('HH:mm:ss DD/MM/YYYY')}
          {/* {msToTime(moment(value).unix())}
          <br />
          {value == null ? 'dd-mm-yyyy' : moment(value).format('DD-MM-YYYY')} */}
        </td>
      ),
    },
    {
      title: 'Thời lượng',
      dataIndex: '',
      render: (value: any) => (
        <td>
          {msToTime(
            moment(value.finish_at).unix() - moment(value.start_at).unix()
          )}
        </td>
      ),
    },
    {
      title: 'Lượt thích',
      dataIndex: 'count_reaction',
      key: 'count_reaction',
      render: (value: any) => <td>{value == 0 ? 0 : formatPrice(value)}</td>,
    },
    {
      title: 'Lượt bình luận',
      dataIndex: 'count_comment',
      key: 'count_comment',
      render: (value: any) => <td>{value == 0 ? 0 : formatPrice(value)}</td>,
    },
    {
      title: 'Lượt xem',
      dataIndex: 'count_viewed',
      key: 'count_viewed',
      render: (value: any) => <td>{value == 0 ? 0 : formatPrice(value)}</td>,
    },
  ]

  const [isLoadingBtnExportData, setLoadingBtnExportData] = useState<boolean>(
    false
  )
  const [dataExport, setDataExport] = useState<any>([])

  const onExportDataToExcel = async (fn: any) => {
    try {
      setLoadingBtnExportData(true)
      const dataListReportNotPagging: any = await getAllLiveReport()
      const dataExport = dataListReportNotPagging?.data?.rows?.map(
        (o: any, i: number) => {
          // Tên, Ngày Live, Thời gian, lượt thích bình luận
          return {
            STT: String(i + 1),
            'Tên gian hàng': o.Shop?.name || '-',
            'Mổ tả': o.title || '-',
            'Thời gian bắt đầu':
              moment(o.create_at).format('DD/MM/YYYY HH:mm:ss') || '-',
            'Thời lượng':
              String(
                msToTime(moment(o.finish_at).unix() - moment(o.start_at).unix())
              ) || '0',
            'Lượt thích':
              o.count_reaction == null
                ? '0'
                : o.count_reaction == 0
                ? '0'
                : String(o.count_reaction),
            'Lượt bình luận':
              o.count_comment == null
                ? '0'
                : o.count_comment == 0
                ? '0'
                : String(o.count_comment),
            'Lượt xem':
              o.count_viewed == null
                ? '0'
                : o.count_viewed == 0
                ? '0'
                : String(o.count_viewed),
          }
        }
      )

      // const data: any = JSON.parse(JSON.stringify(dataExport))
      // const fileName: string = 'Danh sách sản phẩm'
      // const exportType: any = 'csv'
      // const fields: any = {}
      // exportFromJSON({ data, fileName, exportType, fields })
      setDataExport(dataExport)
      fn()
    } catch (error) {
      message.error(
        'Đã có lỗi trong quá trình export dữ liệu, vui lòng thử lại sau!'
      )
    } finally {
      // setLoadingBtnExportData(false)
      setLoadingBtnExportData(false)
    }
  }

  return (
    <div>
      <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
        <PageHeader
          title="Báo cáo livestream"
          extra={[
            <Space>
              <ExportCsv
                loading={isLoadingBtnExportData}
                onClick={fn => onExportDataToExcel(fn)}
                sheetName={['LiveStreamReport']}
                sheets={{
                  LiveStreamReport: ExportCsv.getSheets(dataExport),
                }}
                fileName="Báo cáo Livestream"
              >
                <DownloadOutlined />
                &nbsp;Export
              </ExportCsv>
            </Space>,
          ]}
        />
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <Filter
          isSearchLoading={isSearchLoading}
          onSearchSubmit={(searchKey: string) => {
            setIsSearchLoading(true)
            setParams({ ...params, search: searchKey, page: 1 })
          }}
          onDateSubmit={(from_date: string, to_date: string) => {
            setParams({
              ...params,
              from_date: from_date,
              to_date: to_date,
              page: 1,
            })
          }}
        />
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <p>
          Kết quả lọc: <b>{paging.total}</b>
        </p>

        <Table
          className="table-expanded-custom-no-image"
          loading={isLoading}
          columns={columns}
          dataSource={listLiveReport}
          bordered
          scroll={{
            x: 800,
            scrollToFirstRowOnChange: true,
            // y: 'calc(105vh - 450px)',
            y: `calc(${heightWeb}px - 300px)`,
          }}
          onRow={(record, rowIndex) => ({
            onClick: () => {
              history.push({
                pathname: `${ENTERPRISE_ROUTER_PATH.LIVE_STREAM_DETAIL}/${record.id}`,
                state: record,
              })
            },
            onMouseEnter: event => {
              setItemSelected({ id: record.id, rowIndex: Number(rowIndex) })
            },
          })}
          pagination={{
            ...paging,
            showSizeChanger: false,
            onChange: async (page, pageSize) => {
              setParams({ ...params, page })
            },
          }}
        />
      </div>
    </div>
  )
}
