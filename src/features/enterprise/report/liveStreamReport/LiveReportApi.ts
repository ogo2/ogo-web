import { ApiClient } from 'services/ApiService'

export const getLiveReport = (payload: any) =>
  ApiClient.get('/report/report-livestream', payload)

export const getAllLiveReport = () =>
  ApiClient.get('/report/report-livestream-all')
