import { Button, PageHeader, Popconfirm, message, Spin, Tabs } from 'antd'
import { useHistory, useParams, useLocation } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import R from 'utils/R'
import { deleteProducts, changeStatusProduct } from './ProductApi'
import ProductInfo from './components/ProductInfo'
import SaleInfo from './components/SaleInfo'
import OrderList from './components/OrderList'
import {
  CloseCircleOutlined,
  EditOutlined,
  PauseCircleOutlined,
  CheckCircleOutlined,
} from '@ant-design/icons'

const { TabPane } = Tabs

export default function DetailProduct() {
  const history = useHistory()
  const params: any = useParams()
  const location: any = useLocation()
  const [statusProduct, setStatusProduct] = useState(location?.state?.status)
  const [isLoading, setIsLoading] = useState(false)

  const onDeleteProduct = async (id: any) => {
    try {
      const payload = { id }
      const data = await deleteProducts(payload)
      message.success(data?.message)
      history.push({
        pathname: `${ENTERPRISE_ROUTER_PATH.PRODUCT}`,
      })
    } catch (error) {
      console.log(error)
    }
  }
  const onChangeStatus = async () => {
    const payload = {
      product_id: params.id,
      data: {
        status: statusProduct ? 0 : 1,
      },
    }
    try {
      const res = await changeStatusProduct(payload)
      if (res.code === 1) {
        setStatusProduct(payload.data.status)
      }
    } catch (error) {}
  }
  return (
    <Spin size="large" spinning={isLoading}>
      <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
        <PageHeader
          onBack={() => {
            history.push(ENTERPRISE_ROUTER_PATH.PRODUCT)
          }}
          title="Chi tiết sản phẩm"
          extra={[
            <Popconfirm
              placement="bottomRight"
              title="Bạn có chắc chắn muốn xoá?"
              onConfirm={() => {
                onDeleteProduct(params.id)
              }}
              okText="Xoá"
              cancelText="Huỷ"
              okButtonProps={{
                danger: true,
                type: 'primary',
              }}
            >
              <Button style={{ borderWidth: '2px' }} danger>
                <CloseCircleOutlined />
                Xoá
              </Button>
            </Popconfirm>,
            <Button
              style={{
                borderWidth: '2px',
                borderColor: '#7c7c7c',
                color: 'black',
              }}
              onClick={() => {
                history.push({
                  pathname: `${ENTERPRISE_ROUTER_PATH.EDIT_PRODUCT}/${params.id}`,
                })
              }}
            >
              <EditOutlined />
              {R.strings().action_edit}
            </Button>,
            statusProduct ? (
              <Button
                type="primary"
                danger
                onClick={() => {
                  onChangeStatus()
                }}
              >
                <PauseCircleOutlined />
                Ngừng hoạt động
              </Button>
            ) : (
              <Button
                style={{
                  backgroundColor: '#00abba',
                  borderColor: '#00abba',
                  color: 'white',
                }}
                onClick={() => {
                  onChangeStatus()
                }}
              >
                <CheckCircleOutlined />
                Hoạt động
              </Button>
            ),
          ]}
        />
      </div>
      <div style={{ margin: '5px 10px 15px' }}>
        <Tabs
          style={{
            fontWeight: 'bold',
          }}
        >
          <TabPane
            tab="Thông tin sản phẩm"
            key="1"
            style={{ backgroundColor: '#fff', padding: '10px 10px' }}
          >
            <ProductInfo statusProduct={statusProduct} />
          </TabPane>
          <TabPane tab="Khách hàng mua sản phẩm" key="2">
            <SaleInfo />
          </TabPane>
          <TabPane tab="Danh sách đơn hàng" key="3">
            <OrderList />
          </TabPane>
        </Tabs>
      </div>
    </Spin>
  )
}
