import { ApiClient } from 'services/ApiService'
//Stock
export const getStocks = () => ApiClient.get('/stock')

export const getArrayStocks = (payload: number) =>
  ApiClient.get(`product/${payload}/lits-stock`)

export const createStock = (payload: any) => ApiClient.post('/stock', payload)
//Category
export const getCategory = (payload: any) => ApiClient.get('/category', payload)
//Product
export const listProducts = (payload: any) => ApiClient.get('/product', payload)

export const detailProducts = (payload: any) =>
  ApiClient.get(`/product/${payload.id}?type=${payload.type}`)

export const createProduct = (payload: any) =>
  ApiClient.post('/product', payload)
export const updateProduct = (payload: any) =>
  ApiClient.put(`/product/${payload.id}`, payload)
export const changeStatusProduct = (payload: any) =>
  ApiClient.put(`/product/change-status/${payload.product_id}`, payload.data)
export const deleteProducts = (payload: any) =>
  ApiClient.delete('/product', payload)
export const allProducts = (payload: any) => {
  const paramExport: any = {}
  paramExport.category_id = parseInt(payload.category_id) || 0
  if (paramExport.category_id) {
    paramExport.children_category_id =
      parseInt(payload.children_category_id) || 0
  } else {
    paramExport.children_category_id = 0
  }
  if (payload.search) {
    paramExport.search = payload.search
  }
  if (payload.status) {
    paramExport.status = parseInt(payload.status)
  }
  if (payload.category_id) {
    paramExport.category_id = parseInt(payload.category_id)
  }
  if (payload.children_category_id) {
    paramExport.children_category_id = parseInt(payload.children_category_id)
  }
  if (payload.stock_id) {
    paramExport.stock_id = parseInt(payload.stock_id)
  }

  return ApiClient.get('/shop/product/all', paramExport)
}

export const ImportFilePath = (payload: any) =>
  ApiClient.post('/product/excel/upload-product', payload)
