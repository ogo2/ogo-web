import { PlusOutlined } from '@ant-design/icons'
import {
  Button,
  Col,
  Descriptions,
  Divider,
  Image,
  message,
  Spin,
  Table,
  Upload,
} from 'antd'
import Checkbox from 'antd/lib/checkbox/Checkbox'
import Modal from 'antd/lib/modal/Modal'
import Column from 'antd/lib/table/Column'
import { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { lineBreakPost } from 'utils/funcHelper'
import { formatPrice } from 'utils/ruleForm'
import { detailProducts, getArrayStocks } from '../ProductApi'
interface IAttributes {
  name: string
  AttributesOption: Array<IAttributesOption>
}
interface IFireList {
  path: string
  url: string
}
interface IAttributesOption {
  id?: number | null
  name: string
  url?: string
  fileList: Array<IFireList>
}

const getBase64 = (file: any) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result)
    reader.onerror = error => reject(error)
  })
}
// Ghép dòng cho bảng dataTable
export const getRowSpans = (arr: any, key: any) => {
  let sameValueLength = 0
  const rowSpans = []
  for (let i = arr.length - 1; i >= 0; i--) {
    if (i === 0) {
      rowSpans[i] = sameValueLength + 1
      continue
    }
    if (arr[i][key] === arr[i - 1][key]) {
      rowSpans[i] = 0
      sameValueLength++
    } else {
      rowSpans[i] = sameValueLength + 1
      sameValueLength = 0
    }
  }
  return rowSpans
}
export default function ProductInfo({ statusProduct }: any) {
  const [isModalVisible, setIsModalVisible] = useState(false)
  const [isShowModalPreview, setShowModalPreview] = useState<boolean>(false)
  const [isLoading, setIsLoading] = useState(false)
  const [isLoadingTable, setIsLoadingTable] = useState(false)
  const [previewImage, setPreviewImage] = useState<string>('')

  const [dataTable, setDataTable] = useState<Array<any>>([])
  const [listAttributes, setListAttributes] = useState<Array<IAttributes>>([])
  const [stockList, setStockList] = useState<Array<any>>([])
  const [listImages, setListImages] = useState<Array<any>>(
    Array.from(Array(1).keys()).map(i => {
      return {
        id: i,
        fileList: [],
        buffer: null,
        url: '',
      }
    })
  )
  const [videoProduct, setVideoProduct] = useState<any>({
    id: 0,
    fileList: [],
    buffer: null,
    url: '',
  })
  const [generalData, setGeneralData] = useState({
    description: '',
    name: '',
    category_name: '',
    parent_name: '',
    code: '',
    status: -1,
    stock_status: -1,
    status_stock: -1,
  })

  const param: { id: string } = useParams()
  useEffect(() => {
    if (param) {
      getProductDetail(parseInt(param.id))
    }
  }, [param, statusProduct])

  const getProductDetail = async (id: number) => {
    try {
      setIsLoading(true)
      setIsLoadingTable(true)
      const res = await getArrayStocks(id)
      setStockList(res.data)
      const payload = { id: id, type: 1 }
      const productDetail = await detailProducts(payload)
      // phần thông tin chung
      generalData.description = productDetail.data?.description
      generalData.name = productDetail.data?.name
      generalData.category_name = productDetail.data?.Category?.name
      generalData.parent_name =
        productDetail.data?.Category?.parent_category?.name
      generalData.code = productDetail.data?.code
      generalData.status = productDetail.data?.status
      generalData.stock_status = productDetail.data?.stock_status
      generalData.status_stock = productDetail.data?.status_stock

      //phần dataTable
      const dataPrice: Array<any> = productDetail.data.ProductPrices.map(
        (item: any) => {
          return {
            attribute_groups_0: item?.tier_index[0]
              ? null
              : listAttributes[0]?.AttributesOption[0].name,
            attribute_groups_1: item.tier_index[1]
              ? null
              : listAttributes[1]?.AttributesOption[item.tier_index[1]].name,
            key: item?.id,
            stock_id: item?.stock_id,
            nameStock: item?.name,
            price: item.price,
            amount: item.amount,
            status: item.status,
            tier_index: item.tier_index,
          }
        }
      )
      setDataTable([...dataPrice])
      //phần thông tin bán hàng
      let new_record: Array<any> = productDetail.data.ProductCustomAttributes.map(
        (item: any, index: number) => {
          return {
            name: item.name,
            AttributesOption: item.ProductCustomAttributeOptions.map(
              (itemA: any) => {
                return {
                  id: itemA?.id,
                  name: itemA?.name,
                  url: itemA?.media_url ? itemA?.media_url : '',
                  fileList: itemA?.media_url
                    ? [
                        {
                          path: itemA?.media_url
                            ? itemA?.media_url?.replace(
                                'http://api.ogo.winds.vn/',
                                ''
                              )
                            : '',
                          url: itemA?.media_url ? itemA?.media_url : '',
                        },
                      ]
                    : [],
                }
              }
            ),
          }
        }
      )
      setListAttributes(new_record)
      // phần hình ảnh sản phẩm
      let new_record_images: any = []
      if (productDetail.data?.media_url) {
        new_record_images.push({
          url: productDetail.data?.media_url,
          fileList: [
            {
              url: productDetail.data?.media_url,
              path: productDetail.data.media_url
                ? productDetail?.data?.media_url.replace(
                    'http://api.ogo.winds.vn/',
                    ''
                  )
                : '',
            },
          ],
        })
      }

      if (productDetail.data?.ProductMedia.length > 0) {
        productDetail.data?.ProductMedia.map((item: any, index: number) => {
          if (item?.path_url?.indexOf('.mp4') === -1) {
            new_record_images.push({
              url: item?.media_url,
              fileList: [
                {
                  url: item?.media_url,
                  path: item?.path_url,
                },
              ],
            })
          }
        })
      }
      setListImages(new_record_images)
      // phần video
      if (productDetail.data.video_url) {
        setVideoProduct({
          url: productDetail?.data?.video_url,
          fileList: {
            url: productDetail?.data?.video_url,
            path: productDetail?.data?.video_url?.replace(
              'http://api.ogo.winds.vn/',
              ''
            ),
          },
        })
      }
    } catch (error) {
      message.error('Truy vấn lỗi.')
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsLoadingTable(false)
    }
  }
  useEffect(() => {
    setIsLoadingTable(true)
    const newDataTable: Array<any> = []
    // case has have attr
    if (listAttributes.length === 1) {
      stockList.forEach((stock_item: any, stock_index: any) => {
        listAttributes[0]?.AttributesOption.forEach(
          (attr_item_0: IAttributesOption, attr_index_0: number) => {
            newDataTable.push({
              attribute_groups_0: attr_item_0.name,
              attribute_groups_1: null,
              // key: id ? stock_item?.stock_id : stock_item?.id,
              stock_id: stock_item?.stock_id,
              nameStock: stock_item?.name,
              price: 0,
              amount: stock_item?.amount,
              status: 1,
              tier_index: [attr_index_0],
              // percent: 0,
            })
          }
        )
      })
      newDataTable.map((val: any) => {
        return dataTable.forEach((item: any) => {
          if (
            item.stock_id === val.stock_id &&
            item.tier_index[0] === val.tier_index[0]
          ) {
            val.price = item.price
            val.amount = item.amount
            val.status = item.status
            return val
          } else {
            return val
          }
        })
      })
    }
    // case have 2 attr
    else if (listAttributes.length === 2) {
      stockList.forEach((stock_item: any, stock_index: any) => {
        listAttributes[0]?.AttributesOption.forEach(
          (attr_item_0: IAttributesOption, attr_index_0: number) => {
            listAttributes[1]?.AttributesOption.forEach(
              (attr_item_1: IAttributesOption, attr_index_1: number) => {
                newDataTable.push({
                  attribute_groups_0: attr_item_0.name,
                  attribute_groups_1: attr_item_1.name,
                  // key: id ? stock_item?.stock_id : stock_item?.id,
                  stock_id: stock_item?.stock_id,
                  nameStock: stock_item?.name,
                  price: 0,
                  amount: stock_item?.amount,
                  status: 1,
                  tier_index: [attr_index_0, attr_index_1],
                  // percent: 0,
                })
              }
            )
          }
        )
      })
      newDataTable.map((val: any) => {
        return dataTable.forEach((item: any) => {
          if (
            item.stock_id === val.stock_id &&
            item.tier_index[0] === val.tier_index[0] &&
            item.tier_index[1] === val.tier_index[1]
          ) {
            val.price = item.price
            val.amount = item.amount
            val.status = item.status
            return val
          } else {
            return val
          }
        })
      })
    }
    // case haven't attr
    else {
      stockList.forEach((stock_item: any, index: any) => {
        newDataTable.push({
          // key: id ? stock_item?.stock_id : stock_item?.id,
          stock_id: stock_item?.stock_id,
          nameStock: stock_item?.name,
          price: 0,
          amount: stock_item?.amount,
          status: 1,
          tier_index: [],
          // percent: 0,
        })
      })
      newDataTable.map((val: any) => {
        return dataTable.forEach((item: any) => {
          if (item.stock_id === val.stock_id) {
            val.price = item.price
            val.amount = item.amount
            val.status = item.status
            return val
          } else {
            return val
          }
        })
      })
    }

    setDataTable(newDataTable)
    setIsLoadingTable(false)
  }, [listAttributes, stockList])
  const showModal = () => {
    setIsModalVisible(true)
  }

  const handleCancel = () => {
    setIsModalVisible(false)
  }

  const handlePreviewImage = async (file: any) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj)
    }
    setPreviewImage(file.url || file.preview)
    setShowModalPreview(true)
  }

  const rowSpans: any = getRowSpans(dataTable, 'nameStock')

  return (
    <>
      <div>
        <Spin spinning={isLoading}>
          <Descriptions
            title={<p style={{ color: '#CC0000' }}>THÔNG TIN CHUNG</p>}
          >
            <Descriptions.Item label="Mã sản phẩm">
              {generalData.code}
            </Descriptions.Item>
            <Descriptions.Item label="Danh mục">
              {generalData.parent_name + ' / ' + generalData.category_name}
            </Descriptions.Item>
            <Descriptions.Item label="Tên sản phẩm">
              {generalData.name}
            </Descriptions.Item>
            <Descriptions.Item label="Trạng thái sản phẩm">
              {generalData.status === 1 ? 'Đang hoạt động' : 'Ngưng hoạt động'}
            </Descriptions.Item>
            <Descriptions.Item label="Mô tả">
              <Col>
                <p
                  style={{
                    textOverflow: 'ellipsis',
                    whiteSpace: 'nowrap',
                    overflow: 'hidden',
                    width: '200px',
                  }}
                >
                  {generalData.description &&
                    lineBreakPost(
                      generalData.description
                    ).map((text: string, index: number) =>
                      index === 0 ? (
                        <p style={{ marginBottom: '1px' }}>{text}</p>
                      ) : (
                        ''
                      )
                    )}
                </p>
                <Button
                  type="text"
                  onClick={showModal}
                  style={{ color: '#CC0000' }}
                >
                  [Xem thêm]
                </Button>
                <Modal
                  visible={isModalVisible}
                  onCancel={handleCancel}
                  footer={null}
                >
                  <div style={{ textAlign: 'justify', marginTop: '20px' }}>
                    {generalData.description &&
                      lineBreakPost(
                        generalData.description
                      ).map((text: string) => (
                        <p style={{ marginBottom: '1px' }}>{text}</p>
                      ))}
                  </div>
                </Modal>
              </Col>
            </Descriptions.Item>
            <Descriptions.Item label="Trạng thái hàng">
              {Number(generalData.status_stock) >= 1 ? 'Còn hàng' : 'Hết hàng'}
            </Descriptions.Item>
          </Descriptions>
        </Spin>
      </div>
      <Divider />

      <div>
        <Spin spinning={isLoading}>
          <Descriptions
            title={<p style={{ color: '#CC0000' }}>THÔNG TIN HÌNH ẢNH</p>}
          >
            <Descriptions.Item
              label="Hình ảnh sản phẩm"
              span={3}
              labelStyle={{ fontWeight: 'bold' }}
            >
              {listImages.map((item: any, index: number) => {
                return (
                  // <Upload
                  //   listType="picture-card"
                  //   accept="image/jpeg,image/png,image/jpg"
                  //   fileList={item?.fileList}
                  //   onPreview={handlePreviewImage}
                  // >
                  //   {item?.fileList.length >= 1 ? null : (
                  //     <div>
                  //       <PlusOutlined />
                  //       <div>Ảnh bìa</div>
                  //     </div>
                  //   )}
                  // </Upload>
                  <div
                    style={{
                      width: '120px',
                      height: '120px',
                      borderColor: '#d9d9d9',
                      borderRadius: '2px',
                      borderStyle: 'solid',
                      borderWidth: '1px',
                      color: '#000000',
                      fontSize: '14px',
                      fontWeight: 700,
                      lineHeight: '22px',
                      padding: '10px',
                      alignContent: 'center',
                      alignSelf: 'center',
                      marginRight: '8px',
                      marginBottom: '8px',
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                      overflow: 'hidden',
                    }}
                  >
                    <Image
                      style={{
                        maxWidth: '100px',
                        maxHeight: '100px',
                        width: '100%',
                        height: '100%',
                        objectFit: 'contain',
                      }}
                      src={item.url}
                    />
                  </div>
                )
              })}
            </Descriptions.Item>
            {videoProduct.url !== '' ? (
              <Descriptions.Item
                label="Video sản phẩm"
                span={3}
                labelStyle={{ fontWeight: 'bold' }}
              >
                <video
                  controls
                  src={videoProduct.url}
                  className="uploaded-pic img-thumbnail "
                  style={{ marginLeft: '14px' }}
                  width={310}
                  height={217.5}
                />
              </Descriptions.Item>
            ) : (
              <Descriptions.Item
                label="Video sản phẩm"
                span={3}
                labelStyle={{ fontWeight: 'bold' }}
              >
                Sản phẩm chưa có video
              </Descriptions.Item>
            )}

            {listAttributes?.length > 0 && (
              <Descriptions.Item
                label={`Phân loại (${
                  listAttributes[0]?.name || 'Phân loại 1'
                })`}
                style={{ display: 'flex' }}
                labelStyle={{ fontWeight: 'bold' }}
              >
                {listAttributes[0].AttributesOption?.map(
                  (item: any, index: number) => {
                    if (item.url !== '') {
                      return (
                        <div>
                          {/* <Upload
                            key={index}
                            id={`upload-image-attribute-${index}`}
                            listType="picture-card"
                            accept="image/jpeg,image/png,image/jpg"
                            fileList={item?.fileList}
                            onPreview={handlePreviewImage}
                            beforeUpload={() => false}
                          >
                            {item?.fileList?.length >= 1 ? null : (
                              <div>
                                <PlusOutlined />
                              </div>
                            )}
                          </Upload> */}
                          <div
                            style={{
                              width: '120px',
                              height: '120px',
                              borderColor: '#d9d9d9',
                              borderRadius: '2px',
                              borderStyle: 'solid',
                              borderWidth: '1px',
                              color: '#000000',
                              fontSize: '14px',
                              fontWeight: 700,
                              lineHeight: '22px',
                              padding: '10px',
                              alignContent: 'center',
                              alignSelf: 'center',
                              marginRight: '8px',
                              marginBottom: '8px',
                              display: 'flex',
                              justifyContent: 'center',
                              alignItems: 'center',
                              overflow: 'hidden',
                            }}
                          >
                            <Image
                              style={{
                                maxWidth: '100px',
                                maxHeight: '100px',
                                width: '100%',
                                height: '100%',
                                objectFit: 'contain',
                              }}
                              src={item.url}
                            />
                          </div>
                          <div style={{ textAlign: 'center' }}>
                            {item?.name}
                          </div>
                        </div>
                      )
                    }
                  }
                )}
              </Descriptions.Item>
            )}
          </Descriptions>
        </Spin>
      </div>
      <Divider />

      <div>
        <Descriptions
          title={<p style={{ color: '#CC0000' }}>THÔNG TIN GIÁ BÁN</p>}
        ></Descriptions>

        <div
          style={{
            padding: '10px 20px',
          }}
        >
          <Table
            bordered
            dataSource={dataTable}
            pagination={false}
            scroll={{
              x: 800,
              scrollToFirstRowOnChange: true,
            }}
            loading={isLoadingTable}
          >
            <Column
              width="220px"
              title="Kho"
              dataIndex="nameStock"
              key="nameStock"
              render={(value: any, _, index: any) => {
                const obj: any = {
                  children: value,
                  props: {},
                }
                obj.props.rowSpan = rowSpans[index]
                return obj
              }}
            />
            {listAttributes?.map((item: any, index: number) => {
              return (
                <Column
                  width="200px"
                  title={listAttributes[index].name || `Phân loại ${index + 1}`}
                  dataIndex={`attribute_groups_${index}`}
                  key={`attribute_groups_${index}`}
                  render={(value: any, record: any, indexCol: number) => {
                    return (
                      <p style={{ maxWidth: '200px' }}>
                        {value !== '' ? value : 'Thuộc tính'}
                      </p>
                    )
                  }}
                />
              )
            })}
            <Column
              width="200px"
              title="Giá"
              dataIndex="price"
              key="price"
              render={(value_col: any, record: any, index_col: number) => (
                <p>{value_col ? formatPrice(value_col) : 0}</p>
              )}
            />
            <Column
              width="100px"
              title="Số lượng"
              dataIndex="amount"
              key="amount"
              render={(value_col: any, record: any, index_col: number) => (
                <p>{value_col ? Number(value_col) : 0}</p>
              )}
            />
            <Column
              width="45px"
              title="Ngừng bán"
              dataIndex="status"
              key="status"
              render={(value: any, record: any, index: number) => (
                <Checkbox
                  disabled
                  key={index}
                  checked={record?.status === 1 ? false : true}
                  onChange={e => {
                    const row_record = {
                      ...record,
                      status: e.target.checked === true ? 0 : 1,
                    }
                    dataTable[index] = { ...row_record }
                    setDataTable([...dataTable])
                  }}
                />
              )}
            />
          </Table>
        </div>
        <Modal
          visible={isShowModalPreview}
          footer={null}
          onCancel={() => setShowModalPreview(false)}
        >
          <img
            alt="example"
            style={{ width: '100%', marginTop: '20px' }}
            src={previewImage}
          />
        </Modal>
      </div>
    </>
  )
}
