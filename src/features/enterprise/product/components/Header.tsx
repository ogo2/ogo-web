import {
  CloseCircleOutlined,
  DownloadOutlined,
  PlusCircleOutlined,
  UploadOutlined,
} from '@ant-design/icons'
import {
  Button,
  message,
  Modal,
  PageHeader,
  Popconfirm,
  Space,
  Upload,
} from 'antd'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router-dom'
// import { createFormData } from 'utils/createFormData'
import ExportCsv from 'utils/ExportCSV'
import R from 'utils/R'
import { formatPrice } from 'utils/ruleForm'
import { allProducts, deleteProducts, ImportFilePath } from '../ProductApi'

type Props = {
  selectedRowKey: Array<{}>
  keySearch: any
  productSelected: any
  // checkedTable: any
}

export default function Header(
  //   {
  //   productSelected,
  //   keySearch,
  //   selectedRowKey,
  // }: any
  { selectedRowKey, keySearch, productSelected }: Props
) {
  const [isModalVisible, setIsModalVisible] = useState<boolean>(false)
  const [fileSelected, setFileSelected] = useState<any>({})
  const history = useHistory()
  const [isLoadingBtnExportData, setLoadingBtnExportData] = useState<boolean>(
    false
  )

  const handleDataSelect = () => {
    const dataSelected = selectedRowKey?.map((o: any, i: number) => {
      return {
        STT: i + 1,
        'Tên sảm phẩm': o.name || '',
        'Mã sảm phẩm': o.code || '',
        'Danh mục': o.category_name + '--' + o.sub_category_name || '',
        'Trạng thái hàng': o.status === 1 ? 'Còn hàng' : 'Hết hàng',
        'Trạng thái': o.status === 1 ? 'Đang hoạt động' : 'Ngừng hoạt động',
        'Đã bán': o.total_amount == null ? '0' : o.total_amount,
        'Doanh số':
          o.total_price == null ? '0đ' : formatPrice(o.total_price) + 'đ',
      }
    })
    setDataExport(dataSelected)
  }

  useEffect(() => {
    handleDataSelect()
  }, [selectedRowKey])

  const [dataExport, setDataExport] = useState<any>([])
  // const [loadingExport, setLoadingExport] = useState<boolean>(false)
  const onExportDataToExcel = async (fn: any) => {
    // setLoadingBtnExportData(true)
    try {
      setLoadingBtnExportData(true)
      if (selectedRowKey.length === 0) {
        const dataListProductNotPaging: any = await allProducts(keySearch)
        console.log(dataListProductNotPaging)
        const dataExport = dataListProductNotPaging?.data?.map(
          (o: any, i: number) => {
            return {
              STT: String(i + 1),
              'Tên sảm phẩm': o.name || '',
              'Mã sảm phẩm': o.code || '',
              'Giá Niêm Yết': formatPrice(o.price) || '0',
              'Giá Bán Lẻ': formatPrice(o.price_stock) || '0',
              'Mô Tả': o.description || '',
            }
          }
        )
        setDataExport(dataExport)
        fn()
      } else {
        fn()
      }
    } catch (error) {
      message.error(
        'Đã có lỗi trong quá trình export dữ liệu, vui lòng thử lại sau!'
      )
    } finally {
      setLoadingBtnExportData(false)
    }
  }
  const createFormData = (dataObject: any) => {
    let formdata = new FormData()
    const keys = Object.keys(dataObject)
    if (!dataObject.file) {
      message.error('Bạn chưa chọn file!')
      return null
    } else {
      keys.forEach(key => {
        formdata.append(key, dataObject[key])
      })
      return formdata
    }
  }
  const handleImportFile = async () => {
    try {
      const form = createFormData({
        file: fileSelected?.originFileObj,
      })
      if (form) {
        const rest = await ImportFilePath(form)
        if (rest.status === 1) {
          message.success('Import file thành công')
          await setFileSelected(null)
          setIsModalVisible(false)
          window.location.reload()
        } else {
          message.error('Import file thất bại!')
          setIsModalVisible(false)
        }
      }
    } catch (error) {
      message.error(`${error}`)
    }
  }
  console.log('fileSelected', fileSelected)
  const handleRemove = () => {
    setFileSelected(null)
  }
  return (
    <PageHeader
      title="Sản phẩm"
      extra={[
        <Button
          icon={<UploadOutlined />}
          type="primary"
          onClick={() => setIsModalVisible(true)}
        >
          Import
        </Button>,
        <Modal
          title="Import sản phẩm"
          visible={isModalVisible}
          onOk={handleImportFile}
          onCancel={() => {
            setIsModalVisible(false)
            setFileSelected(null)
          }}
        >
          <Upload
            name="file"
            maxCount={1}
            fileList={fileSelected?.status ? [fileSelected] : []}
            accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
            onChange={info => {
              setFileSelected({ ...info.fileList[0] })
            }}
            onRemove={handleRemove}
          >
            <Button type="primary" icon={<UploadOutlined />}>
              Chọn file
            </Button>
          </Upload>
        </Modal>,

        <Space>
          <ExportCsv
            style={{ backgroundColor: '#00ac00' }}
            loading={isLoadingBtnExportData}
            onClick={fn => onExportDataToExcel(fn)}
            sheetName={['ProductList']}
            sheets={{
              ProductList: ExportCsv.getSheets(dataExport),
            }}
            fileName="Dánh sách sản phẩm"
          >
            <DownloadOutlined />
            &nbsp; Export
          </ExportCsv>
        </Space>,
        <Popconfirm
          placement="bottomRight"
          title="Bạn chắc chắn muốn xoá sản phẩm này!"
          onConfirm={async () => {
            if (productSelected.length >= 1) {
              try {
                let arr: any = []
                productSelected.map((item: any, index: any) => {
                  arr.push(item.id)
                })
                const payload = { id: arr }
                const data = await deleteProducts(payload)
                if (data?.code === 1) {
                  message.success(data?.message)
                  window.location.reload()
                }
              } catch (error) {
                console.log(error)
              }
            } else {
              message.error('Chọn ít nhất 1 sản phẩm để xóa')
              console.log('ccccccc', productSelected)
            }
          }}
        >
          <Button
            type="primary"
            danger
            // onClick={async () => {
            // if (productSelected.length >= 1) {
            //   try {
            //     let arr: any = []
            //     productSelected.map((item: any, index: any) => {
            //       arr.push(item.id)
            //     })
            //     const payload = { id: arr }
            //     const data = await deleteProducts(payload)
            //     if (data?.code === 1) {
            //       message.success(data?.message)
            //       window.location.reload()
            //     }
            //   } catch (error) {
            //     console.log(error)
            //   }
            // } else {
            //   message.error('Chọn ít nhất 1 sản phẩm để xóa')
            //   console.log('ccccccc', productSelected)
            // }
            // }
            // }
          >
            <CloseCircleOutlined />
            Xóa
          </Button>
        </Popconfirm>,
        <Button
          style={{
            borderColor: '#00abba',
            backgroundColor: '#00abba',
            color: 'white',
          }}
          onClick={() => {
            history.push({
              pathname: `${ENTERPRISE_ROUTER_PATH.ADD_PRODUCT}`,
            })
          }}
        >
          <PlusCircleOutlined />
          {R.strings().action_create}
        </Button>,
      ]}
    />
  )
}
