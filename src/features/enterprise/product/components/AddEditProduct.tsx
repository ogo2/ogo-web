import React from 'react'
import './css/AddEditProduct.css'
import { Button, PageHeader, Popconfirm, message, Row, Col } from 'antd'
import { useHistory, useParams } from 'react-router-dom'
import { useEffect, useState } from 'react'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import ImagesInformationProduct from '../components/ImagesInformationProduct'
import SalesInformationProduct from '../components/SalesInformationProduct'
import GeneralInformationProduct from '../components/GeneralInformationProduct'
import { createProduct, detailProducts, updateProduct } from '../ProductApi'
import { CheckCircleOutlined, CloseCircleOutlined } from '@ant-design/icons'

interface ProductGroupItem {
  product_group_items: Array<any>
  name: string
  image_customs: Array<any>
}
interface IFireList {
  path: string
  url: string
}
interface IAttributesOption {
  id?: number | null
  name: string
  url?: string
  fileList: Array<IFireList>
}
interface IAttributes {
  name: string
  AttributesOption: Array<IAttributesOption>
}
export default function AddEditProduct() {
  const param: { id: string } = useParams()
  const history = useHistory()
  const [isLoading, setIsLoading] = useState(false)
  const [dataTable, setDataTable] = useState<Array<any>>([])
  const [listAttributes, setListAttributes] = useState<Array<IAttributes>>([])
  const [createPost, setCreatePost] = useState<Number>(0)
  const [listImages, setListImages] = useState<Array<any>>(
    Array.from(Array(4).keys()).map(i => {
      return {
        id: i,
        fileList: [],
        buffer: null,
        url: '',
      }
    })
  )

  const [videoProduct, setVideoProduct] = useState<any>({
    id: 0,
    fileList: [],
    buffer: null,
    url: '',
  })
  const [generalData, setGeneralData] = useState({
    description: '',
    name: '',
    category_id: -1,
    parent_id: -1,
    code: '',
    status: -1,
    stock_status: -1,
  })

  useEffect(() => {
    if (param.id) {
      getProductDetail(parseInt(param.id))
    }
  }, [param])

  const getProductDetail = async (id: number) => {
    try {
      setIsLoading(true)
      const payload = { id: id, type: 1 }
      const productDetail = await detailProducts(payload)
      if (productDetail.data === null) {
        history.replace('/add-product')
      }
      // phần thông tin chung
      generalData.description = productDetail.data.description
      generalData.name = productDetail.data.name
      generalData.category_id = productDetail.data.Category.id
      generalData.parent_id = productDetail.data.Category.parent_id
      generalData.code = productDetail.data.code
      generalData.status = productDetail.data.status
      generalData.stock_status = productDetail.data.stock_status

      //phần dataTable
      const dataPrice: Array<any> = productDetail.data.ProductPrices.map(
        (item: any) => {
          return {
            attribute_groups_0: item?.tier_index[0]
              ? null
              : listAttributes[0]?.AttributesOption[0].name,
            attribute_groups_1: item.tier_index[1]
              ? null
              : listAttributes[1]?.AttributesOption[item.tier_index[1]].name,
            key: item?.id,
            stock_id: item?.stock_id,
            nameStock: item?.name,
            price: item.price,
            amount: item?.amount,
            status: item.status,
            tier_index: item.tier_index,
          }
        }
      )

      setDataTable([...dataPrice])
      //phần thông tin bán hàng
      let new_record: Array<any> = productDetail.data.ProductCustomAttributes.map(
        (item: any, index: number) => {
          return {
            name: item.name,
            AttributesOption: item.ProductCustomAttributeOptions.map(
              (itemA: any) => {
                return {
                  id: itemA?.id,
                  name: itemA?.name,
                  url: itemA?.media_url ? itemA?.media_url : '',
                  fileList: itemA?.media_url
                    ? [
                        {
                          path: itemA?.media_url
                            ? itemA?.media_url?.replace(
                                'http://api.ogo.winds.vn/',
                                ''
                              )
                            : '',
                          url: itemA?.media_url ? itemA?.media_url : '',
                        },
                      ]
                    : [],
                }
              }
            ),
          }
        }
      )
      setListAttributes(new_record)
      // phần hình ảnh sản phẩm
      let new_record_images: any = []
      if (productDetail.data?.media_url) {
        new_record_images.push({
          url: productDetail.data?.media_url,
          fileList: [
            {
              url: productDetail.data?.media_url,
              path: productDetail.data.media_url
                ? productDetail?.data?.media_url.replace(
                    'http://api.ogo.winds.vn/',
                    ''
                  )
                : '',
            },
          ],
        })
      }

      if (productDetail.data?.ProductMedia.length > 0) {
        productDetail.data?.ProductMedia.map((item: any, index: number) => {
          if (item?.path_url?.indexOf('.mp4') === -1) {
            new_record_images.push({
              url: item?.media_url,
              fileList: [
                {
                  url: item?.media_url,
                  path: item?.path_url,
                },
              ],
            })
          }
        })
      }
      setListImages(new_record_images)
      // phần video
      if (productDetail.data.video_url) {
        setVideoProduct({
          url: productDetail?.data?.video_url,
          fileList: {
            url: productDetail?.data?.video_url,
            path: productDetail?.data?.video_url?.replace(
              'http://api.ogo.winds.vn/',
              ''
            ),
          },
        })
      }
    } catch (error) {
      message.error('Truy vấn lỗi.')
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  const validateBeforeConfirm = () => {
    if (generalData?.code.trim() === '') {
      message.error('Hãy nhập mã sản phẩm')
      document.getElementById('code-product')?.focus()
      return false
    }
    if (generalData?.code.trim().indexOf(' ') !== -1) {
      message.error('Mã sản phẩm không được có khoảng trống ở giữa')
      document.getElementById('code-product')?.focus()
      return false
    }
    if (generalData?.code.trim().length < 3) {
      message.error('Nhập mã sản phẩm từ 3 tới 25 ký tự')
      document.getElementById('code-product')?.focus()
      return false
    }
    if (generalData?.code.trim().length > 25) {
      message.error('Nhập mã sản phẩm từ 3 tới 25 ký tự')
      document.getElementById('code-product')?.focus()
      return false
    }
    if (generalData?.name.trim() === '') {
      message.error('Hãy nhập tên sản phẩm')
      document.getElementById('name-product')?.focus()
      return false
    }
    if (generalData?.parent_id === -1 || undefined) {
      message.error('Hãy chọn danh mục cha')
      return false
    }
    if (generalData?.category_id === -1 || undefined) {
      message.error('Hãy chọn danh mục con')
      return false
    }
    if (generalData?.description.trim() === '') {
      message.error('Hãy nhập mô tả sản phẩm')
      document.getElementById('description-product')?.focus()
      return false
    }
    if (listAttributes[0]?.name.trim() === '') {
      message.error('Hãy nhập tên nhóm phân loại')
      document.getElementById('input-name-card0')?.focus()
      return false
    }
    if (
      listAttributes[0]?.name.trim().length < 3 ||
      listAttributes[0]?.name.trim().length > 50
    ) {
      message.error('Tên nhóm phân loại hàng phải từ 3 tới 50 ký tự')
      document.getElementById('input-name-card0')?.focus()
      return false
    }
    for (let i = 0; i < listAttributes[0]?.AttributesOption.length; i++) {
      if (listAttributes[0].AttributesOption[i].name === '') {
        message.error('Hãy nhập tên phân loại')
        document.getElementById(`input-attribute-card0${i}`)?.focus()
        return false
      }
    }
    if (listAttributes[1]?.name.trim() === '') {
      message.error('Hãy nhập tên nhóm phân loại')
      document.getElementById('input-name-card1')?.focus()
      return false
    }
    if (
      listAttributes[1]?.name.trim().length < 3 ||
      listAttributes[1]?.name.trim().length > 50
    ) {
      message.error('Tên nhóm phân loại hàng phải từ 3 tới 50 ký tự')
      document.getElementById('input-name-card1')?.focus()
      return false
    }
    for (let i = 0; i < listAttributes[1]?.AttributesOption.length; i++) {
      if (listAttributes[1].AttributesOption[i].name === '') {
        message.error('Hãy nhập tên phân loại')
        document.getElementById(`input-attribute-card1${i}`)?.focus()
        return false
      }
    }
    for (let i = 0; i < dataTable.length; i++) {
      if (dataTable[i].price === 0) {
        message.error('Hãy nhập giá cho sản phẩm')
        document.getElementById(`price-product-${i}`)?.focus()
        return false
      }
      // if (dataTable[i].price > 50000000000) {
      //   message.error('Giá trị sản phẩm không thể vượt quá 50 tỷ')
      //   document.getElementById(`price-product-${i}`)?.focus()
      //   return false
      // }
    }
    // for (let i = 0; i < dataTable.length; i++) {
    //   console.log('amount', dataTable[i].amount)

    if (dataTable.length === 0) {
      message.error('Chưa có kho nào!')
      return false
    }
    // if (dataTable[i].price > 50000000000) {
    //   message.error('Giá trị sản phẩm không thể vượt quá 50 tỷ')
    //   document.getElementById(`price-product-${i}`)?.focus()
    //   return false
    // }
    // }
    if (listImages[0].url === '') {
      message.error('Hãy chọn ảnh bìa')
      return false
    }
    return true
  }

  const onSave = async () => {
    setIsLoading(true)
    try {
      if (validateBeforeConfirm() === true && dataTable?.length > 0) {
        const product_custom_attribute: Array<any> = []
        const images: Array<any> = []
        if (listAttributes.length > 0) {
          listAttributes.map((item: any, index: any) => {
            const mini: ProductGroupItem = {
              product_group_items: [],
              name: '',
              image_customs: [],
            }
            item.AttributesOption.map((itemA: any, indexA: any) => {
              if (itemA?.url !== '') {
                mini?.image_customs.push(itemA?.fileList[0]?.path)
              }
              mini?.product_group_items.push(itemA.name)
            })
            mini.name = item?.name
            if (mini.image_customs.length !== 0) {
              product_custom_attribute.unshift(mini)
            } else {
              product_custom_attribute.push(mini)
            }
          })
        }
        if (videoProduct.url !== '') {
          images.push(videoProduct.fileList.path)
        }
        if (listImages.length > 0) {
          listImages.map((item: any) => {
            if (item.url !== '') {
              images.push(item.fileList[0].path)
            }
          })
        }
        const dataPush = {
          items: dataTable,
          product_custom_attribute: product_custom_attribute,
          images: images,
          stock_status: param.id ? generalData?.stock_status : 1,
          status: param.id ? generalData?.status : 1,
          price: 0,
          description: generalData?.description,
          name: generalData?.name,
          category_id: generalData?.category_id,
          code: generalData?.code,
          id: -1,
          is_post: createPost,
        }
        if (!param.id) {
          const result = await createProduct(dataPush)
          if (result?.code === 1) {
            message.success('Thêm sản phẩm thành công')
            history.push({
              pathname: `${ENTERPRISE_ROUTER_PATH.PRODUCT}`,
            })
          }
        } else {
          dataPush.id = parseInt(param.id)
          const result = await updateProduct(dataPush)
          console.log('dataPush', dataPush)
          if (result?.code === 1) {
            message.success('Sửa sản phẩm thành công')
            history.push({
              pathname: `${ENTERPRISE_ROUTER_PATH.PRODUCT}`,
            })
          }
        }
      }
    } catch (error) {
      console.log('error', error)
      if (!param.id) message.error('Thêm sản phẩm thất bại')
      else message.error('Sửa sản phẩm thất bại')
    } finally {
      setIsLoading(false)
    }
  }

  return (
    <div>
      <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
        <PageHeader
          title={param.id ? 'Sửa sản phẩm' : 'Thêm sản phẩm'}
          onBack={() => {
            history.push(ENTERPRISE_ROUTER_PATH.PRODUCT)
          }}
          extra={[
            <Popconfirm
              placement="bottomRight"
              title="Bạn có chắc chắn muốn hủy?"
              onConfirm={() => {
                history.push(ENTERPRISE_ROUTER_PATH.PRODUCT)
              }}
              okText="Đồng ý"
              cancelText="Huỷ"
              okButtonProps={{
                danger: true,
                type: 'primary',
              }}
            >
              <Button danger style={{ height: '35px', fontWeight: 800 }}>
                <CloseCircleOutlined /> Hủy
              </Button>
            </Popconfirm>,
            <Button
              loading={isLoading}
              type="primary"
              onClick={onSave}
              style={{
                backgroundColor: '#00abba',
                borderColor: '#00abba',
                height: '35px',
                fontWeight: 800,
              }}
            >
              <CheckCircleOutlined />
              Lưu
            </Button>,
          ]}
        />
      </div>
      <GeneralInformationProduct
        key="0"
        id={param?.id}
        generalData={generalData}
        setGeneralData={setGeneralData}
        setCreatePost={setCreatePost}
      />
      <SalesInformationProduct
        key="1"
        id={param?.id}
        dataTable={dataTable}
        setDataTable={setDataTable}
        setListAttributes={setListAttributes}
        listAttributes={listAttributes}
      />
      <ImagesInformationProduct
        key="2"
        id={param?.id}
        listImages={listImages}
        setListImages={setListImages}
        videoProduct={videoProduct}
        setVideoProduct={setVideoProduct}
        listAttributes={listAttributes}
        setListAttributes={setListAttributes}
      />

      <Row
        gutter={16}
        justify="end"
        style={{
          padding: '15px 15px 15px',
          margin: '5px 10px 15px',
          backgroundColor: 'white',
          // marginTop: '20px',
          // paddingBottom: '30px',
        }}
      >
        <Col>
          <Popconfirm
            placement="bottomRight"
            title="Bạn có chắc chắn muốn hủy?"
            style={{ margin: 'auto' }}
            onConfirm={() => {
              history.push(ENTERPRISE_ROUTER_PATH.PRODUCT)
            }}
            okText="Đồng ý"
            cancelText="Huỷ"
            okButtonProps={{
              danger: true,
              type: 'primary',
            }}
          >
            <Button danger style={{ height: '35px', fontWeight: 800 }}>
              <CloseCircleOutlined /> Hủy
            </Button>
          </Popconfirm>
        </Col>
        <Col>
          <Button
            loading={isLoading}
            type="primary"
            onClick={onSave}
            style={{
              backgroundColor: '#00abba',
              borderColor: '#00abba',
              height: '35px',
              fontWeight: 800,
            }}
          >
            <CheckCircleOutlined />
            Lưu
          </Button>
        </Col>
      </Row>
    </div>
  )
}
