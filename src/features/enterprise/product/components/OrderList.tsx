import { Row, Table, Descriptions } from 'antd'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import React, { useEffect, useState } from 'react'
import { useParams, useHistory } from 'react-router-dom'
import { formatPrice } from 'utils/ruleForm'
import { detailProducts } from '../ProductApi'

export default function OrderList() {
  const history = useHistory()
  const [isLoading, setIsLoading] = useState(false)
  const [dataSource, setDataSource] = useState<Array<any>>([])
  const param: { id: string } = useParams()
  useEffect(() => {
    if (param) {
      getProductDetail(parseInt(param.id))
    }
  }, [param])
  const getProductDetail = async (id: number) => {
    setIsLoading(true)
    try {
      const payload = { id: id, type: 3 }
      const orderList = await detailProducts(payload)
      const data = orderList.data.data.map((item: any, index: any) => {
        return {
          key: item.id,
          id: item.id,
          stt: index + 1,
          name: item.User?.name,
          code: item.code,
          total_amount_order: item.total_amount_order,
          total_price: item.total_price,
        }
      })
      setDataSource(data)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  const columnsOrder = [
    {
      title: 'STT',
      dataIndex: 'stt',
    },
    {
      title: 'Mã đơn',
      dataIndex: 'code',
    },
    {
      title: 'Tên khách hàng',
      dataIndex: 'name',
    },
    {
      title: 'Số lượng',
      dataIndex: 'total_amount_order',
    },
    {
      title: 'Doanh thu thực tế',
      dataIndex: 'total_price',
      render: (value: any) => {
        return value ? formatPrice(value) + 'đ' : '0đ'
      },
    },
  ]

  return (
    <div style={{ backgroundColor: 'white', paddingBottom: '20px' }}>
      <Row>
        <Descriptions
          title={<p style={{ color: '#CC0000' }}>DANH SÁCH ĐƠN HÀNG</p>}
          style={{ padding: '10px 10px' }}
        ></Descriptions>

        <div
          style={{
            padding: '0px 10px',
          }}
        >
          <Table
            scroll={{
              x: 800,
              scrollToFirstRowOnChange: true,
              y: 'calc(100vh - 340px)',
            }}
            bordered
            loading={isLoading}
            columns={columnsOrder}
            dataSource={dataSource}
            onRow={(record: any) => {
              return {
                onDoubleClick: () => {
                  history.push({
                    pathname: `${ENTERPRISE_ROUTER_PATH.ORDER_DETAIL}/${record.id}`,
                    state: record,
                  })
                },
              }
            }}
          />
        </div>
      </Row>
    </div>
  )
}
