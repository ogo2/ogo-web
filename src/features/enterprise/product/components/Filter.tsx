import { Col, Empty, Input, Row, Select } from 'antd'
import { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { listProducts, getStocks, getCategory } from '../ProductApi'
import './css/Header.css'

const { Option } = Select

export default function Filter({
  getDataSearch,
  listCategory,
  keySearch,
  setKeySearch,
}: any) {
  const authState = useSelector((state: any) => state.authReducer)
  const [paramsCategory, setParamsCategory] = useState({ page: 1, search: '' })
  const [stockList, setStockList] = useState<Array<any>>([])
  // const [categoryList, setCategoryList] = useState([])
  const [isLoading, setIsLoading] = useState<boolean>(false)
  // const [searchKey, setSearchKey] = useState({
  //   shop_id: authState.userInfo?.Shop?.id,
  //   search: '',
  //   status: null,
  //   stock: null,
  //   category_id: null,
  //   children_category_id: null,
  //   stocks_status: null,
  // })
  const [categorySubList, setCategorySubList] = useState([])
  const [paramsCategorySub, setParamsCategorySub] = useState({
    page: 1,
    search: '',
    parent_id: '',
  })
  const [selectedCategorySubId, setSelectedCategorySubId] = useState<string>()

  useEffect(() => {
    getStockList()
  }, [])

  useEffect(() => {
    getDataSearch(keySearch)
  }, [keySearch])

  const getStockList = async () => {
    const res = await getStocks()
    setStockList(res.data)
  }

  //lấy danh sách danh mục cha
  // const getCategoryList = async () => {
  //   setIsLoading(true)
  //   try {
  //     const response = await getCategory(paramsCategory)
  //     setCategoryList(response.data)
  //   } catch (error) {
  //     console.log(error)
  //   } finally {
  //     setIsLoading(false)
  //   }
  // }

  // useEffect(() => {
  //   getCategoryList()
  // }, [paramsCategory])

  //lấy danh sách danh mục con
  const getCategorySubList = async () => {
    try {
      if (paramsCategorySub.parent_id) {
        const response = await getCategory(paramsCategorySub)
        setCategorySubList(response.data)
      }
    } catch (error) {
      console.log(error)
    }
  }

  useEffect(() => {
    getCategorySubList()
  }, [paramsCategorySub])

  const onChangeSearch = (e: any) => {
    setKeySearch({ ...keySearch, search: e.target.value })
  }
  const onChangeCategory = (value: any) => {
    let row_record = {
      ...keySearch,
      category_id: value === undefined ? null : value,
      children_category_id: null,
    }
    setParamsCategorySub({ ...paramsCategorySub, parent_id: value })
    setKeySearch(row_record)
  }

  const onChangeCategorySub = (value: any) => {
    let row_record = {
      ...keySearch,
      children_category_id: value === undefined ? null : value,
    }
    setKeySearch(row_record)
  }

  const onChangeStatus = (value: any) => {
    setKeySearch({ ...keySearch, status: value })
  }
  const onChangeStock = (value: any) => {
    let row_record = {
      ...keySearch,
      stock_id: value === undefined ? null : value,
    }
    setKeySearch(row_record)
  }

  return (
    <Row gutter={[16, 16]} justify="end">
      <Col span={6}>
        <Input.Search
          // style={{ minWidth: '300px' }}
          allowClear
          placeholder="Nhập tên hoặc mã sản phẩm ..."
          // addonAfter={<Icon type="close-circle-o" />}
          onChange={e => {
            onChangeSearch(e)
          }}
        />
      </Col>

      <Col span={4}>
        <Select
          allowClear
          optionFilterProp="children"
          loading={isLoading}
          placeholder="DM sản phẩm"
          style={{ width: '170px' }}
          showSearch
          // onSearch={(value: string) =>
          //   setParamsCategory({ ...paramsCategory, search: value, page: 1 })
          // }
          onClear={() => {
            setCategorySubList([])
            setSelectedCategorySubId(undefined)
            let row_record = {
              ...keySearch,
              category_id: null,
            }
            setKeySearch(row_record)
          }}
          onChange={(value: string) => {
            setSelectedCategorySubId(undefined)
            setParamsCategorySub({
              ...paramsCategorySub,
              parent_id: value,
              page: 1,
            })
            onChangeCategory(value)
          }}
        >
          {listCategory.map((item: any, index: number) => {
            return (
              <Option value={item.id} key={index}>
                {item.name}
              </Option>
            )
          })}
        </Select>
      </Col>

      <Col span={4}>
        <Select
          allowClear
          optionFilterProp="children"
          loading={isLoading}
          style={{ width: '170px' }}
          placeholder="DM con sản phẩm"
          showSearch
          value={selectedCategorySubId}
          onChange={(value: string) => {
            setSelectedCategorySubId(value)
            onChangeCategorySub(value)
          }}
          notFoundContent={
            <Empty
              image={Empty.PRESENTED_IMAGE_SIMPLE}
              description={<span>Vui lòng chọn danh mục sản phẩm!</span>}
            />
          }
        >
          {categorySubList.map((item: any, index: number) => {
            return (
              <Option value={item.id} key={index}>
                {item.name}
              </Option>
            )
          })}
        </Select>
      </Col>

      <Col span={4}>
        <Select
          allowClear
          placeholder="Trạng thái"
          style={{ width: '160px' }}
          onChange={(value: any) => {
            onChangeStatus(value)
          }}
        >
          <Option value={1}>Đang hoạt động</Option>
          <Option value={0}>Ngừng hoạt động</Option>
        </Select>
      </Col>
      <Col span={4}>
        <Select
          allowClear
          style={{ width: '150px' }}
          onChange={(value: any) => {
            onChangeStock(value)
          }}
          placeholder="Kho hàng"
          optionFilterProp="children"
          showSearch
        >
          {stockList.length > 0 &&
            stockList.map((item: any) => {
              return <Select.Option value={item.id}>{item.name}</Select.Option>
            })}
        </Select>
      </Col>
    </Row>
  )
}
