import './css/AddEditProduct.css'
import { Collapse, Col, Row, Input, Select, Checkbox } from 'antd'
import { useEffect, useState } from 'react'
import { getCategory } from '../ProductApi'
import { useHistory, useParams } from 'react-router-dom'
const { Panel } = Collapse
const Option = Select
const { TextArea } = Input

type GeneralDataState = {
  description: string
  name: string
  category_id: number
  parent_id: number | undefined
  code: string
  status: number
  stock_status: number
}
type Props = {
  id?: string
  generalData: GeneralDataState
  setGeneralData: any
  setCreatePost: any
}
type TypeParamCategory = {
  search: string
  status: 0 | 1
  parent_id?: number | null
  from_date: Date | '' | null
  to_date: Date | '' | null
}

export default function GeneralInformationProduct({
  id,
  generalData,
  setGeneralData,
  setCreatePost,
}: Props) {
  const [listCategory, setListCategory] = useState<Array<any>>([])
  const [listSubCategory, setListSubCategory] = useState<Array<any>>([])
  const [paramCategory, setParamCategory] = useState<TypeParamCategory>({
    search: '',
    status: 1,
    parent_id: null,
    from_date: '',
    to_date: '',
  })
  const [paramSubCategory, setParamSubCategory] = useState<TypeParamCategory>({
    search: '',
    status: 1,
    parent_id: null,
    from_date: '',
    to_date: '',
  })

  useEffect(() => {
    getListCategory()
    update()
  }, [paramCategory, generalData.parent_id])

  const update = async () => {
    if (id) {
      const resSub = await getCategory({
        search: '',
        status: 1,
        parent_id: generalData.parent_id,
        from_date: '',
        to_date: '',
      })
      setListSubCategory(resSub.data)
    }
  }
  useEffect(() => {
    if (paramSubCategory.parent_id) {
      getListSubCategory()
    } else setListSubCategory([])
  }, [paramSubCategory])

  const getListCategory = async () => {
    try {
      const res = await getCategory(paramCategory)
      setListCategory(res.data)
    } catch (error) {
      console.log(error)
    }
  }
  const getListSubCategory = async () => {
    try {
      const res = await getCategory(paramSubCategory)
      setListSubCategory(res.data)
    } catch (error) {
      console.log(error)
    }
  }
  const onChangeStatusProduct = async (value: any) => {
    setGeneralData({ ...generalData, stock_status: value })
  }
  const onChangeStatus = async (value: any) => {
    setGeneralData({ ...generalData, status: value })
  }
  const onChangename = (value: any) => {
    setGeneralData({ ...generalData, name: value })
  }
  const onChangeCode = (value: any) => {
    setGeneralData({ ...generalData, code: value })
  }
  const onChangeDescription = (value: any) => {
    setGeneralData({ ...generalData, description: value })
  }
  const onChangeCategory = (value: any) => {
    if (value) {
      setGeneralData({
        ...generalData,
        parent_id: value,
        category_id: -1,
      })
      setParamSubCategory({
        ...paramSubCategory,
        parent_id: value,
      })
    } else {
      setGeneralData({ ...generalData, category_id: -1, parent_id: -1 })
      setParamSubCategory({ ...paramSubCategory, parent_id: null })
    }
  }
  const onChangeSubCategory = async (value: any) => {
    setGeneralData({ ...generalData, category_id: value })
  }
  const onAppendNewLine = (e: any) => {
    const data = generalData.description + '\n '
    setGeneralData({
      ...generalData,
      description: data,
    })
    e.preventDefault()
  }

  return (
    <div className="container-add-product">
      <Collapse
        className="collapse-product"
        defaultActiveKey={['1']}
        expandIconPosition="right"
      >
        <Panel header={<span>THÔNG TIN CHUNG</span>} key="1">
          <Row>
            <Col span={12} style={{ marginBottom: '15px' }}>
              <Row align="middle">
                <Col span={6}>
                  <b>Mã sản phẩm </b>
                  <b style={{ color: 'red' }}>&#42;</b>
                </Col>
                <Col span={16}>
                  <Input
                    id="code-product"
                    placeholder="Nhập mã sản phẩm"
                    onChange={e => {
                      onChangeCode(e.target.value)
                    }}
                    value={generalData.code}
                  />
                </Col>
              </Row>
            </Col>
            <Col span={12} style={{ marginBottom: '15px' }}>
              <Row align="middle">
                <Col span={6}>
                  <b>Danh mục </b>
                  <b style={{ color: 'red' }}>&#42;</b>
                </Col>
                <Col span={18}>
                  <Select
                    allowClear
                    showSearch
                    optionFilterProp="children"
                    placeholder="Danh mục cha"
                    size="large"
                    style={{ width: '100%' }}
                    onChange={onChangeCategory}
                    value={
                      id && listCategory !== []
                        ? generalData.parent_id
                        : undefined
                    }
                  >
                    {listCategory?.map((item: any) => {
                      return <Option value={item.id}>{item.name}</Option>
                    })}
                  </Select>
                </Col>
              </Row>
            </Col>
            <Col span={12} style={{ marginBottom: '15px' }}>
              <Row align="middle">
                <Col span={6}>
                  <b>Tên sản phẩm </b>
                  <b style={{ color: 'red' }}>&#42;</b>
                </Col>
                <Col span={16}>
                  <Input
                    id="name-product"
                    placeholder="Nhập tên sản phẩm"
                    onChange={e => {
                      onChangename(e.target.value)
                    }}
                    value={generalData.name}
                  />
                </Col>
              </Row>
            </Col>
            <Col span={12} style={{ marginBottom: '15px' }}>
              <Row align="middle">
                <Col span={6}>
                  <span></span>
                </Col>
                <Col span={18}>
                  <Select
                    allowClear
                    showSearch
                    optionFilterProp="children"
                    id="category-main"
                    placeholder="Danh mục con"
                    size="large"
                    style={{ width: '100%' }}
                    onChange={onChangeSubCategory}
                    value={
                      generalData.category_id === -1
                        ? undefined
                        : generalData.category_id
                    }
                  >
                    {listSubCategory?.map((item: any) => {
                      return <Option value={item.id}>{item.name}</Option>
                    })}
                  </Select>
                </Col>
              </Row>
            </Col>
            {id ? (
              <>
                {/* <Col span={12} style={{ marginBottom: '15px' }}>
                  <Row align="middle">
                    <Col span={6}>
                      <b>Trạng thái hàng </b>
                      <b style={{ color: 'red' }}>&#42;</b>
                    </Col>
                    <Col span={16}>
                      <Select
                        placeholder="Trạng thái hàng"
                        size="large"
                        style={{ width: '100%' }}
                        onChange={onChangeStatusProduct}
                        value={
                          generalData.stock_status === 1
                            ? 'Còn hàng'
                            : 'Hết hàng'
                        }
                      >
                        <Option value={1}>Còn hàng</Option>
                        <Option value={0}>Hết hàng</Option>
                      </Select>
                    </Col>
                  </Row>
                </Col> */}
                <Col span={12} style={{ marginBottom: '15px' }}>
                  <Row align="middle">
                    <Col span={6}>
                      <b>Trạng thái hoạt động </b>
                      <b style={{ color: 'red' }}>&#42;</b>
                    </Col>
                    <Col span={16}>
                      <Select
                        placeholder="Trạng thái hoạt động"
                        size="large"
                        style={{ width: '100%' }}
                        onChange={onChangeStatus}
                        value={
                          generalData.status === 1
                            ? 'Đang hoạt động'
                            : 'Ngừng hoạt động'
                        }
                      >
                        <Option value={1}>Đang hoạt động</Option>
                        <Option value={0}>Ngừng hoạt động</Option>
                      </Select>
                    </Col>
                  </Row>
                </Col>
              </>
            ) : (
              ''
            )}
          </Row>
          <Row>
            <b style={{ marginBottom: '10px' }}>Mô tả sản phẩm </b>
            <b style={{ color: 'red' }}> &#42;</b>
            <TextArea
              id="text-area-product"
              allowClear
              placeholder="Nhập mô tả sản phẩm"
              onChange={e => {
                onChangeDescription(e.target.value)
              }}
              onKeyPress={e =>
                e.shiftKey && e.key === 'Enter' ? onAppendNewLine(e) : ''
              }
              value={generalData.description}
            />
          </Row>
          <Row>
            {!id ? (
              <Checkbox
                style={{
                  marginTop: '7px',
                  fontWeight: 'bold',
                  color: '#2261BA',
                }}
                onChange={e => {
                  setCreatePost(e.target.checked ? 1 : 0)
                }}
              >
                Đăng bài lên diễn đàn
              </Checkbox>
            ) : (
              <></>
            )}
          </Row>
        </Panel>
      </Collapse>
    </div>
  )
}
