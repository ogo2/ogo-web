import { CloseSquareOutlined, PlusOutlined } from '@ant-design/icons'
import {
  Button,
  Col,
  Collapse,
  Form,
  message,
  Row,
  Upload,
  Select,
  Checkbox,
} from 'antd'
import Modal from 'antd/lib/modal/Modal'
import { useState } from 'react'
import { useParams } from 'react-router'
import { uploadImage, uploadVideo } from 'utils/uploadMedia'
import './css/AddEditProduct.css'

const { Panel } = Collapse
const { Option } = Select

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 10 },
    md: { span: 7 },
    lg: { span: 4 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 14 },
    md: { span: 17 },
    lg: { span: 20 },
  },
}
const tailFormItemLayout = {
  wrapperCol: {
    sm: {
      span: 24,
      offset: 10,
    },
    md: {
      span: 24,
      offset: 14,
    },
    lg: {
      span: 24,
      offset: 17,
    },
  },
}
interface IAttributesOption {
  id?: number | null
  name: string
  url?: string
  fileList: Array<any>
}
interface IAttributes {
  name: string
  AttributesOption: Array<IAttributesOption>
}

const getBase64 = (file: any) => {
  return new Promise((resolve, reject) => {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result)
    reader.onerror = error => reject(error)
  })
}
interface ImagesInformationProductProps {
  id?: string
  listImages: any
  setListImages: any
  videoProduct: any
  setVideoProduct: any
  listAttributes: Array<IAttributes>
  setListAttributes: any
}

export default function ImagesInformationProduct({
  id,
  listImages,
  setListImages,
  videoProduct,
  setVideoProduct,
  listAttributes,
  setListAttributes,
}: ImagesInformationProductProps) {
  const [isLoadingUpload, setIsLoadingUpload] = useState(false)
  const [previewImage, setPreviewImage] = useState<string>('')
  const [isShowModalPreview, setShowModalPreview] = useState<boolean>(false)
  const [selectAttribute, setSelectAttribute] = useState(0)
  const handleChangeImage = async (file: any, index: number) => {
    // case remove image
    if (file.file.status === 'removed') {
      listImages[index].fileList = []
      listImages[index].url = ''
      setListImages([...listImages])
      return
    }
    const isLt3M = file.file.type ? file.file.size / 1024 / 1024 < 3 : true
    const isJpgOrPng =
      file.file.type === 'image/jpeg' || file.file.type === 'image/png'
    if (!isJpgOrPng) {
      message.error('Bạn chỉ có thể upload ảnh có định dạng JPG/PNG!')
      return
    } else if (!isLt3M) {
      message.error('Dung lượng ảnh tối đa là 3MB!')
      return
    }
    // case uploading image
    if (file?.fileList[0]?.status === 'uploading') {
      listImages[index].fileList = file.fileList || []
      setListImages([...listImages])
    }
    // case upload image
    else if (file.file.status !== 'removed') {
      try {
        setIsLoadingUpload(true)
        const res = await uploadImage(file)
        listImages[index].fileList = [
          {
            status: 'done',
            size: '10000',
            type: 'image/jpeg',
            uid: index,
            url: res?.url,
            path: res?.path,
          },
        ]
        listImages[index].url = res?.url
        setListImages([...listImages])
      } catch (error) {
        message.error('Tải ảnh thất bại, vui lòng kiểm tra kết nối và thử lại.')
      } finally {
        setIsLoadingUpload(false)
      }
    }
  }

  const handleChangeImageAttribute = async (file: any, index: number) => {
    // case remove image
    if (file.file.status === 'removed') {
      listAttributes[selectAttribute].AttributesOption[index].fileList = []
      listAttributes[selectAttribute].AttributesOption[index].url = ''
      setListAttributes([...listAttributes])
      return
    }
    const isLt3M = file.file.type ? file.file.size / 1024 / 1024 < 3 : true
    const isJpgOrPng =
      file.file.type === 'image/jpeg' || file.file.type === 'image/png'
    if (!isJpgOrPng) {
      message.error('Bạn chỉ có thể upload ảnh có định dạng JPG/PNG!')
      return
    } else if (!isLt3M) {
      message.error('Dung lượng ảnh tối đa là 3MB!')
      return
    }
    // case uploading image
    if (file?.fileList[selectAttribute]?.status === 'uploading') {
      listAttributes[selectAttribute].AttributesOption[index].fileList =
        file.fileList || []
      setListAttributes([...listAttributes])
    }
    // case upload image
    else if (file.file.status !== 'removed') {
      try {
        setIsLoadingUpload(true)
        const res = await uploadImage(file)
        listAttributes[selectAttribute].AttributesOption[index].fileList = [
          {
            status: 'done',
            size: '10000',
            type: 'image/jpeg',
            uid: index,
            url: res?.url,
            path: res?.path,
          },
        ]
        listAttributes[selectAttribute].AttributesOption[index].url = res?.url
        setListAttributes([...listAttributes])
      } catch (error) {
        message.error('Tải ảnh thất bại, vui lòng kiểm tra kết nối và thử lại.')
      } finally {
        setIsLoadingUpload(false)
      }
    }
  }

  const handlePreviewImage = async (file: any) => {
    if (!file.url && !file.preview) {
      file.preview = await getBase64(file.originFileObj)
    }
    setPreviewImage(file.url || file.preview)
    setShowModalPreview(true)
  }

  const handleChangeVideo = async (file: any) => {
    if (file.fileList[0]?.type !== 'video/mp4') {
      message.error(
        'Định dạng video không được hỗ trợ. Vui lòng chọn video khác.'
      )
    }

    try {
      setIsLoadingUpload(true)
      const res = await uploadVideo(file)
      videoProduct.fileList = {
        status: 'done',
        size: '10000',
        type: 'image/jpeg',
        uid: 0,
        url: res?.url,
        path: res?.path,
      }
      videoProduct.url = res?.url
      setVideoProduct({ ...videoProduct })
    } catch (error) {
      message.error('Tải video thất bại, vui lòng kiểm tra kết nối và thử lại.')
    } finally {
      setIsLoadingUpload(false)
    }
  }
  const checkAtributeSelected = () => {
    for (
      let i = 0;
      i < listAttributes[selectAttribute].AttributesOption.length;
      i++
    ) {
      if (listAttributes[selectAttribute].AttributesOption[i].url !== '')
        return true
    }
    return false
  }
  return (
    <div className="container-add-product">
      <Collapse
        className="collapse-product"
        expandIconPosition="right"
        defaultActiveKey={['1']}
      >
        <Panel header={<span>THÔNG TIN HÌNH ẢNH</span>} key="1">
          <>
            <Form.Item
              name="type_image"
              label={<b>Hình ảnh sản phẩm</b>}
              rules={[
                {
                  required: true,
                },
              ]}
            >
              <div className="flex-container">
                {listImages.map((item: any, index: number) => {
                  return index === 0 ? (
                    <Upload
                      listType="picture-card"
                      accept="image/jpeg,image/png,image/jpg"
                      fileList={item?.fileList}
                      onPreview={handlePreviewImage}
                      onChange={(value: any) => {
                        handleChangeImage(value, index)
                      }}
                    >
                      {item?.fileList.length >= 1 ? null : (
                        <div>
                          <PlusOutlined />
                          <div style={{ fontWeight: 600 }}>Ảnh bìa</div>
                        </div>
                      )}
                    </Upload>
                  ) : (
                    <Upload
                      listType="picture-card"
                      accept="image/jpeg,image/png,image/jpg"
                      fileList={item?.fileList}
                      onPreview={handlePreviewImage}
                      onChange={(value: any) => {
                        handleChangeImage(value, index)
                      }}
                    >
                      {item?.fileList.length >= 1 ? null : (
                        <div>
                          <PlusOutlined />
                          <div style={{ fontWeight: 600 }}>Ảnh số {index}</div>
                        </div>
                      )}
                    </Upload>
                  )
                })}
                {listImages.length < 10 ? (
                  <Button
                    style={{
                      width: '104px',
                      height: '104px',
                      border: '1px dashed #d9d9d9 ',
                      backgroundColor: '#fafafa',
                      display: 'inline-block',
                    }}
                    onClick={() => {
                      setListImages([
                        ...listImages,
                        {
                          id: listImages.length,
                          fileList: [],
                          buffer: null,
                          url: '',
                        },
                      ])
                    }}
                  >
                    <div>
                      <PlusOutlined />
                      <div style={{ fontWeight: 600 }}>Thêm ảnh</div>
                    </div>
                  </Button>
                ) : (
                  ''
                )}
              </div>
            </Form.Item>

            <Form.Item name="type_video" label={<b>Video sản phẩm</b>}>
              <Row style={{ marginLeft: '35px' }}>
                {videoProduct && videoProduct?.fileList?.length === 0 ? (
                  <Upload
                    accept="video/*"
                    listType="picture-card"
                    fileList={videoProduct.fileList}
                    onChange={handleChangeVideo}
                  >
                    {videoProduct?.fileList?.length >= 1 ? null : (
                      <div>
                        <PlusOutlined />
                        <div style={{ fontWeight: 600 }}>
                          Đội dài video 10s - 60s
                        </div>
                      </div>
                    )}
                  </Upload>
                ) : (
                  <div
                    style={{
                      border: '1px solid dotted',
                      position: 'relative',
                    }}
                  >
                    <CloseSquareOutlined
                      style={{
                        position: 'absolute',
                        fontSize: 20,
                        right: '-150%',
                        cursor: 'pointer',
                        color: 'red',
                      }}
                      onClick={() => {
                        videoProduct.fileList = []
                        videoProduct.url = ''
                        setVideoProduct({ ...videoProduct })
                      }}
                    />
                    <video
                      controls
                      src={videoProduct.url}
                      className="uploaded-pic img-thumbnail "
                      style={{ marginLeft: '14px' }}
                      width={310}
                      height={217.5}
                    />
                  </div>
                )}
                {/* <Col lg={13} md={24} sm={24}>
                  <div>
                    <p>
                      1. Kích thước: Tối đa 30Mb, độ phân giải không vượt quá
                      1280x1280px
                    </p>
                    <p>2. Độ dài: 10s-60s</p>
                    <p>3. Định dạng: MP4 (Không hỗ trợ vp9)</p>
                  </div>
                </Col> */}
              </Row>
            </Form.Item>

            {listAttributes?.length > 0 && (
              <>
                <Form.Item label="Phân loại">
                  <Row style={{ marginLeft: '60px' }}>
                    <Select
                      style={{
                        width: 180,
                        height: 30,
                      }}
                      value={selectAttribute}
                      onChange={value => {
                        setSelectAttribute(value)
                      }}
                      disabled={false}
                      // disabled={checkAtributeSelected()}
                    >
                      {listAttributes?.map((item: any, index: number) => {
                        return <Option value={index}>{item.name}</Option>
                      })}
                    </Select>
                  </Row>
                </Form.Item>
                <Form.Item>
                  <Row style={{ marginLeft: '130px' }}>
                    {listAttributes[selectAttribute].AttributesOption?.map(
                      (item: any, index: number) => {
                        return (
                          <Upload
                            key={index}
                            id={`upload-image-attribute-${index}`}
                            listType="picture-card"
                            accept="image/jpeg,image/png,image/jpg"
                            fileList={item?.fileList}
                            onPreview={handlePreviewImage}
                            beforeUpload={() => false}
                            onChange={(file: any) => {
                              handleChangeImageAttribute(file, index)
                            }}
                          >
                            {item?.fileList?.length >= 1 ? null : (
                              <div>
                                <PlusOutlined />
                                <div> {item?.name}</div>
                              </div>
                            )}
                          </Upload>
                        )
                      }
                    )}
                  </Row>
                </Form.Item>
              </>
            )}
          </>
        </Panel>
      </Collapse>
      <Modal
        visible={isShowModalPreview}
        footer={null}
        onCancel={() => setShowModalPreview(false)}
      >
        <img alt="example" style={{ width: '100%' }} src={previewImage} />
      </Modal>
    </div>
  )
}
