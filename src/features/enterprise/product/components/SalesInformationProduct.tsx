import './css/AddEditProduct.css'
import {
  Collapse,
  Col,
  Row,
  Input,
  Checkbox,
  Button,
  Table,
  Space,
  Card,
  InputNumber,
  message,
} from 'antd'
import { CheckOutlined, CloseOutlined, DeleteOutlined } from '@ant-design/icons'
import { useEffect, useState } from 'react'
import Column from 'antd/lib/table/Column'
import { formatPrice } from 'utils/ruleForm'
import AddEditStock from 'features/enterprise/stock/components/AddEditStock'
import { getStocks, createStock, getArrayStocks } from '../ProductApi'
import { notificationError } from 'utils/notification'
const { Panel } = Collapse
interface AttributesPayload {
  id?: number | null
  name: string
  url: string
}
interface OptionPayload {
  name: string
  AttributesOption: Array<AttributesPayload>
}
type Props = {
  id?: string
  dataTable?: any
  setDataTable?: any
  setListAttributes?: any
  listAttributes: Array<IAttributes>
}
type ItemDataTable = {
  attribute_groups_0: string
  attribute_groups_1: string
  id: number
  key: number
  nameStock: string
  percent: number
  price: number
  amount: number
  status: number
  stock_id: number
}
interface IAttributesOption {
  id?: number | null
  name: string
  url?: string
  fileList: Array<any>
}
interface IAttributes {
  name: string
  AttributesOption: Array<IAttributesOption>
}
// Ghép dòng cho bảng dataTable
export const getRowSpans = (arr: any, key: any) => {
  let sameValueLength = 0
  const rowSpans = []
  for (let i = arr.length - 1; i >= 0; i--) {
    if (i === 0) {
      rowSpans[i] = sameValueLength + 1
      continue
    }
    if (arr[i][key] === arr[i - 1][key]) {
      rowSpans[i] = 0
      sameValueLength++
    } else {
      rowSpans[i] = sameValueLength + 1
      sameValueLength = 0
    }
  }
  return rowSpans
}

export default function SalesInformationProduct({
  id,
  dataTable,
  setDataTable,
  listAttributes,
  setListAttributes,
}: Props) {
  const [isLoading, setIsLoading] = useState(false)
  const [isShowModelAddStock, setShowModelAddStock] = useState<boolean>(false)
  const [stockList, setStockList] = useState<Array<any>>([])
  const [valuePrice, setValuePrice] = useState<number>(0)

  useEffect(() => {
    getStockList()
  }, [])

  useEffect(() => {
    setIsLoading(true)
    const newDataTable: Array<any> = []
    // case has have attr
    if (listAttributes.length === 1) {
      stockList.forEach((stock_item: any, stock_index: any) => {
        listAttributes[0]?.AttributesOption.forEach(
          (attr_item_0: IAttributesOption, attr_index_0: number) => {
            newDataTable.push({
              attribute_groups_0: attr_item_0.name,
              attribute_groups_1: null,
              // key: id ? stock_item?.stock_id : stock_item?.id,
              stock_id: id ? stock_item?.stock_id : stock_item?.id,
              nameStock: stock_item?.name,
              price: valuePrice || 0,
              amount: stock_item?.amount,
              status: 1,
              tier_index: [attr_index_0],
              // percent: 0,
            })
          }
        )
      })
      newDataTable.map((val: any) => {
        return dataTable.forEach((item: any) => {
          if (
            item.stock_id === val.stock_id &&
            item.tier_index[0] === val.tier_index[0]
          ) {
            val.price = item.price
            val.amount = item.amount
            val.status = item.status
            return val
          } else {
            return val
          }
        })
      })
    }
    // case have 2 attr
    else if (listAttributes.length === 2) {
      stockList.forEach((stock_item: any, stock_index: any) => {
        listAttributes[0]?.AttributesOption.forEach(
          (attr_item_0: IAttributesOption, attr_index_0: number) => {
            listAttributes[1]?.AttributesOption.forEach(
              (attr_item_1: IAttributesOption, attr_index_1: number) => {
                newDataTable.push({
                  attribute_groups_0: attr_item_0.name,
                  attribute_groups_1: attr_item_1.name,
                  // key: id ? stock_item?.stock_id : stock_item?.id,
                  stock_id: id ? stock_item?.stock_id : stock_item?.id,
                  nameStock: stock_item?.name,
                  price: valuePrice || 0,
                  amount: stock_item?.amount,
                  status: 1,
                  tier_index: [attr_index_0, attr_index_1],
                  // percent: 0,
                })
              }
            )
          }
        )
      })
      newDataTable.map((val: any) => {
        return dataTable.forEach((item: any) => {
          if (
            item.stock_id === val.stock_id &&
            item.tier_index[0] === val.tier_index[0] &&
            item.tier_index[1] === val.tier_index[1]
          ) {
            val.price = item.price
            val.amount = item.amount
            val.status = item.status
            return val
          } else {
            return val
          }
        })
      })
    }
    // case haven't attr
    else {
      stockList.forEach((stock_item: any, index: any) => {
        newDataTable.push({
          // key: id ? stock_item?.stock_id : stock_item?.id,
          stock_id: id ? stock_item?.stock_id : stock_item?.id,
          nameStock: stock_item?.name,
          price: valuePrice || 0,
          amount: stock_item?.amount,
          status: 1,
          tier_index: [],
          // percent: 0,
        })
      })
      newDataTable.map((val: any) => {
        return dataTable.forEach((item: any) => {
          if (item.stock_id === val.stock_id) {
            val.price = item.price
            val.amount = item.amount
            val.status = item.status
            return val
          } else {
            return val
          }
        })
      })
    }
    setTimeout(() => {
      setDataTable(newDataTable)
      setIsLoading(false)
    }, 400)
  }, [listAttributes, stockList])

  const getStockList = async () => {
    try {
      if (id) {
        const res = await getArrayStocks(parseInt(id))
        setStockList(res.data)
      } else {
        const res = await getStocks()
        setStockList(res.data)
      }
    } catch (error) {
      console.log(error)
    }
  }
  const onCreateStock = async (stockData: any) => {
    try {
      stockData.product_id = id ? parseInt(id) : 0
      const res = await createStock(stockData)
      setStockList(
        stockList.concat([
          {
            ...res.data,
            stock_id: res.data.id,
          },
        ])
      )
      message.success('Tạo kho mới thành công')
      setShowModelAddStock(false)
    } catch (error) {
      // message.error('Thêm kho thất bại!')
      console.log(error)
    }
  }
  const rowSpans: any = getRowSpans(dataTable, 'nameStock')

  return (
    <div className="container-add-product">
      <Collapse
        className="collapse-product"
        expandIconPosition="right"
        defaultActiveKey={['1']}
      >
        <Panel header={<span>THÔNG BÁN HÀNG</span>} key="1">
          <Space direction="vertical" style={{ marginBottom: '10px' }}>
            {listAttributes?.map((item: any, indexGroup: number): any => (
              <Card
                title={'Phân loại nhóm ' + (indexGroup + 1)}
                extra={
                  <CloseOutlined
                    style={{ cursor: 'pointer' }}
                    onClick={() => {
                      listAttributes.splice(indexGroup, 1)
                      setListAttributes([...listAttributes])
                    }}
                  />
                }
                style={{ width: '500px' }}
              >
                <Row align="middle">
                  <Col span={8}>
                    <span>Tên nhóm</span>
                  </Col>
                  <Col span={16}>
                    <Input
                      placeholder="Tên nhóm phân loại hàng (VD: kích thước)"
                      id={'input-name-card' + indexGroup}
                      onChange={(e: any) => {
                        if (e.target.value.length > 50)
                          return message.warning(
                            'Chỉ được phép nhập tối đa 50 kí tự.'
                          )
                        if (listAttributes.length > 0) {
                          listAttributes[indexGroup].name = e.target.value
                          setListAttributes([...listAttributes])
                        }
                      }}
                      value={item.name}
                    />
                  </Col>
                </Row>
                {listAttributes[indexGroup].AttributesOption?.map(
                  (item: any, indexAttribute: any) => {
                    return (
                      <Row align="middle" className="custom-row">
                        <Col span={8}>
                          {indexAttribute === 0 ? (
                            <span>Tên phân loại </span>
                          ) : (
                            <span></span>
                          )}
                        </Col>
                        <Col span={16}>
                          <Input
                            placeholder="Nhập phân loại hàng (VD: S,M,L)"
                            id={`input-attribute-card${indexGroup}${indexAttribute}`}
                            suffix={
                              <DeleteOutlined
                                onClick={() => {
                                  if (
                                    listAttributes[indexGroup].AttributesOption
                                      .length > 1
                                  ) {
                                    listAttributes[
                                      indexGroup
                                    ].AttributesOption.splice(indexAttribute, 1)
                                    setListAttributes([...listAttributes])
                                  } else {
                                    message.warning(
                                      'Cần có ít nhất 1 phân loại'
                                    )
                                  }
                                }}
                              />
                            }
                            value={item.name}
                            onChange={(e: any) => {
                              if (indexAttribute < 12) {
                                const value = e.target.value
                                if (value.length < 50) {
                                  listAttributes[indexGroup].AttributesOption[
                                    indexAttribute
                                  ].name = value
                                  setListAttributes([...listAttributes])
                                } else {
                                  message.error('Tên phân loại tối đa 50 kí tự')
                                }
                              }
                            }}
                          />
                        </Col>
                      </Row>
                    )
                  }
                )}

                <Row align="middle" className="custom-row">
                  <Col span={8}></Col>
                  <Col span={16}>
                    {listAttributes[indexGroup]?.AttributesOption?.length <
                    10 ? (
                      <Button
                        type="dashed"
                        style={{ width: '100%' }}
                        onClick={() => {
                          listAttributes[indexGroup].AttributesOption.push({
                            id: null,
                            name: '',
                            url: '',
                            fileList: [],
                          })
                          setListAttributes([...listAttributes])
                        }}
                      >
                        Thêm phân loại hàng
                      </Button>
                    ) : (
                      ''
                    )}
                  </Col>
                </Row>
              </Card>
            ))}
          </Space>

          {listAttributes.length < 2 && (
            <Row
              align="middle"
              className="custom-row"
              style={{ marginBottom: 30 }}
            >
              <Col span={5}>
                <b>Phân loại hàng</b>
              </Col>
              <Col span={6} offset={1}>
                <Button
                  type="dashed"
                  onClick={() => {
                    if (stockList.length !== 0) {
                      setListAttributes([
                        ...listAttributes,
                        {
                          name: '',
                          AttributesOption: [
                            { id: null, name: '', url: '', fileList: [] },
                          ],
                        },
                      ])
                    } else {
                      message.error(
                        'Bạn phải thêm kho trước khi chọn phân loại hàng'
                      )
                    }
                  }}
                >
                  Thêm nhóm phân loại hàng
                </Button>
              </Col>
            </Row>
          )}

          <Row align="middle">
            <Col span={5}>
              <b>Thiết lập phân loại hàng </b>
              <b style={{ color: 'red' }}>&#42;</b>
            </Col>
            <Col span={6}>
              <Input
                style={{ width: '100%' }}
                placeholder="Giá"
                id="input-price"
                onChange={(e: any) => {
                  let value = e.target.value?.replace(/[^\w\s]/gi, '')
                  if (parseInt(value) > 50000000000) {
                    notificationError(
                      'Giá sản phẩm không vượt quá 50,000,000,000'
                    )
                  } else {
                    setValuePrice(parseInt(value))
                  }
                }}
                value={valuePrice ? formatPrice(valuePrice) : 0}
              />
            </Col>
            <Col span={6} offset={1}>
              <Button
                style={{
                  borderColor: '#00abba',
                  backgroundColor: '#00abba',
                  color: 'white',
                }}
                onClick={() => {
                  dataTable.map((item: any, index: number) => {
                    item.price = valuePrice
                  })
                  setDataTable([...dataTable])
                }}
              >
                <CheckOutlined />
                Áp dụng cho tất cả
              </Button>
            </Col>
          </Row>
          <Button
            type="link"
            onClick={() => {
              setShowModelAddStock(true)
            }}
          >
            <span
              style={{
                color: 'red',
                textDecorationLine: 'underline',
                fontWeight: 'bold',
                fontSize: '16px',
              }}
            >
              + Thêm kho hàng
            </span>
          </Button>
          <Table
            style={{ fontWeight: 600 }}
            bordered
            loading={isLoading}
            dataSource={dataTable}
            pagination={false}
            scroll={{
              x: 800,
              scrollToFirstRowOnChange: true,
            }}
          >
            <Column
              width="220px"
              title={
                <b style={{ fontSize: '16px', letterSpacing: '1.5px' }}>Kho</b>
              }
              dataIndex="nameStock"
              key="nameStock"
              render={(value: any, _, index: any) => {
                const obj: any = {
                  children: value,
                  props: {},
                }
                obj.props.rowSpan = rowSpans[index]
                return obj
              }}
            />
            {listAttributes?.map((item: any, index: number) => {
              return (
                <Column
                  width="200px"
                  title={listAttributes[index].name || `Phân loại ${index + 1}`}
                  dataIndex={`attribute_groups_${index}`}
                  key={`attribute_groups_${index}`}
                  render={(value: any, record: any, indexCol: number) => {
                    return (
                      <p style={{ maxWidth: '200px' }}>
                        {value !== '' ? value : 'Thuộc tính'}
                      </p>
                    )
                  }}
                />
              )
            })}
            <Column
              width="200px"
              title={
                <b style={{ fontSize: '16px', letterSpacing: '1.5px' }}>Giá</b>
              }
              dataIndex="price"
              key="price"
              render={(value_col: any, record: any, index_col: number) => (
                <Input
                  id={`price-product-${index_col}`}
                  style={{ border: '0px solid #fff', width: '100%' }}
                  value={value_col ? formatPrice(value_col) : 0}
                  onChange={(e: any) => {
                    let value = e.target.value?.replace(/[^\w\s]/gi, '')
                    if (parseInt(value) > 50000000000) {
                      notificationError(
                        'Giá sản phẩm không vượt quá 50,000,000,000'
                      )
                    } else {
                      const new_data_record: any = {
                        ...record,
                        price: parseInt(value),
                      }
                      dataTable[index_col] = new_data_record
                      setDataTable([...dataTable])
                    }
                  }}
                />
              )}
            />
            <Column
              width="100px"
              title={
                <b style={{ fontSize: '16px', letterSpacing: '1.5px' }}>
                  Số lượng
                </b>
              }
              dataIndex="amount"
              key="amount"
              render={(value_col: any, record: any, index_col: number) => (
                <Input
                  id={`amount-product-${index_col}`}
                  style={{ border: '0px solid #fff', width: '100%' }}
                  value={value_col ? formatPrice(value_col) : 0}
                  onChange={(e: any) => {
                    let value = e.target.value?.replace(/[^\w\s]/gi, '')
                    if (parseInt(value) > 50000000000) {
                      notificationError(
                        'Số lượng sản phẩm không vượt quá 50,000,000'
                      )
                    } else {
                      const new_data_record: any = {
                        ...record,
                        amount: parseInt(value),
                      }
                      dataTable[index_col] = new_data_record
                      setDataTable([...dataTable])
                    }
                  }}
                />
              )}
            />
            <Column
              width="45px"
              title={
                <b style={{ fontSize: '16px', letterSpacing: '1.5px' }}>
                  Ngừng bán
                </b>
              }
              dataIndex="status"
              key="status"
              render={(value: any, record: any, index: number) => (
                <Checkbox
                  key={index}
                  checked={record?.status === 1 ? false : true}
                  onChange={e => {
                    const row_record = {
                      ...record,
                      status: e.target.checked === true ? 0 : 1,
                    }
                    dataTable[index] = { ...row_record }
                    setDataTable([...dataTable])
                  }}
                />
              )}
            />
          </Table>
        </Panel>
      </Collapse>
      <AddEditStock
        isLoadingButton={isLoading}
        visible={isShowModelAddStock}
        onCancel={() => {
          setShowModelAddStock(false)
        }}
        onCreateStock={onCreateStock}
      />
    </div>
  )
}
