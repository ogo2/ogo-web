import React from 'react'
import { useState, useEffect } from 'react'
import Header from './components/Header'
import Filter from './components/Filter'
import { Table, Switch } from 'antd'
import { listProducts, changeStatusProduct } from './ProductApi'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import { useHistory } from 'react-router-dom'
import { formatPrice } from 'utils/ruleForm'
import { useSelector } from 'react-redux'

export default function ContentList() {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const authState = useSelector((state: any) => state.authReducer)
  const [isLoadingTable, setIsLoadingTable] = useState(false)
  const [dataSource, setDataSource] = useState<Array<any>>([])
  const [itemSelected, setItemSelected] = useState({ id: 0, rowIndex: 0 })
  const [productSelected, setProductSelected] = useState<Array<any>>([])
  const [isLoadingChangeStatus, setIsLoadingChangeStatus] = useState(false)
  const [selectedRowKey, setSelectedRowKey] = useState<Array<any>>([])
  const [keySearch, setKeySearch] = useState({
    shop_id: authState.userInfo?.Shop?.id,
    search: '',
    status: null,
    stock: null,
    category_id: null,
    children_category_id: null,
    stock_id: null,
  })
  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })

  const [listCategory, setListCategory] = useState<Array<any>>([])
  const history = useHistory()

  const getListProduct = async () => {
    try {
      const res = await listProducts(keySearch)
      res?.data?.rows.map((item: any, index: any) => {
        item.key = index + res.paging.limit * (res.paging.page - 1) + 1
        let array_category = [
          item.Category?.parent_category?.name
            ? item.Category?.parent_category?.name
            : '',
          item.Category?.name,
        ]
        item.list_category = array_category
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setDataSource(res?.data.rows)
      setListCategory(res.data.listCategory)
      setPaging(formattedPaging)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoadingTable(false)
    }
  }

  useEffect(() => {
    setIsLoadingTable(true)
    getListProduct()
  }, [])

  useEffect(() => {
    setIsLoadingTable(true)
    setTimeout(() => {
      getListProduct()
    }, 600)
  }, [keySearch])

  const getDataSearch = (value: any) => {
    setKeySearch(value)
  }
  const onChangeStatus = async (checked: any, e: any) => {
    e.stopPropagation()
    try {
      setIsLoadingChangeStatus(true)
      const payload = {
        product_id: dataSource[itemSelected.rowIndex].id,
        data: {
          status: (dataSource[itemSelected.rowIndex].status = checked ? 1 : 0),
        },
      }
      const res = await changeStatusProduct(payload)
      if (res.code === 1) {
        dataSource[itemSelected.rowIndex].status = checked ? 1 : 0
      }
    } catch (error) {
      console.log('error', error)
    } finally {
      setIsLoadingChangeStatus(false)
    }
  }
  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any) => {
      const selectedRowID = selectedRows.map((item: any) => {
        return item
      })
      setProductSelected(selectedRowID)
      setSelectedRowKey(selectedRowID)
    },
    getCheckboxProps: (record: any) => ({
      id: record.id,
    }),
  }

  const columns = [
    {
      title: 'STT',
      dataIndex: 'key',
    },
    {
      title: 'Mã sản phẩm',
      dataIndex: 'code',
    },
    {
      title: 'Tên sản phẩm',
      dataIndex: 'name',
    },
    {
      title: 'Danh mục',
      dataIndex: 'list_category',
      render: (value: any) => {
        return (
          <td>
            {value.map((item: any, index: any) => {
              let str = '--'
              return <p>{str.repeat(index) + ' ' + item}</p>
            })}
          </td>
        )
      },
    },
    {
      title: 'Trạng thái hàng',
      dataIndex: 'amount',
      render: (value: any) => {
        return <td>{Number(value) >= 1 ? 'Còn hàng' : 'Hết hàng'}</td>
      },
    },
    {
      title: 'Trạng thái',
      dataIndex: 'status',
      render: (value: any) => {
        return (
          <td>
            {value === 1 ? (
              <Switch
                checked
                onChange={onChangeStatus}
                loading={isLoadingChangeStatus}
              />
            ) : (
              <Switch
                onChange={onChangeStatus}
                loading={isLoadingChangeStatus}
              />
            )}
          </td>
        )
      },
    },
    {
      title: 'Đã bán',
      dataIndex: 'total_amount',
    },
    {
      title: 'Doanh số',
      dataIndex: 'total_price',
      render: (value: any) => {
        return (
          <td>
            {value != '0' || null ? `${formatPrice(parseInt(value))} đ` : '0đ'}
          </td>
        )
      },
    },
    {
      title: 'Số lượng',
      dataIndex: 'amount',
      render: (value: any) => {
        return <td>{Number(value) <= 0 ? '0' : value}</td>
      },
    },
  ]

  return (
    <>
      <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
        <Header
          selectedRowKey={selectedRowKey}
          productSelected={productSelected}
          keySearch={keySearch}
          // checkedTable={checkedTable}
        />
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <Filter
          getDataSearch={getDataSearch}
          listCategory={listCategory}
          keySearch={keySearch}
          setKeySearch={setKeySearch}
        />
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <p>
          Kết quả lọc: <b>{paging.total}</b>
        </p>
        <Table
          className="table-expanded-custom-no-image"
          rowSelection={{
            ...rowSelection,
          }}
          scroll={{
            x: 800,
            scrollToFirstRowOnChange: true,
            // y: 'calc(100vh - 280px)',
            y: `calc(${heightWeb}px - 410px)`,
          }}
          bordered
          dataSource={dataSource}
          // pagination={{
          //   ...paging,
          //   onChange: async (page, pageSize) => {
          //     setParams({ ...params, page })
          //   },
          // }}
          loading={isLoadingTable}
          columns={columns}
          // expandedRowKeys={[currentSelected.id]}
          onRow={(record, rowIndex) => ({
            onDoubleClick: () => {
              history.push({
                pathname: `${ENTERPRISE_ROUTER_PATH.PRODUCT_DETAIL}/${record.id}`,
                state: record,
              })
            },
            onMouseEnter: event => {
              setItemSelected({ id: record.id, rowIndex: Number(rowIndex) })
            },
          })}
        />
      </div>
    </>
  )
}
