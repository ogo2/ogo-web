import { DeleteFilled, EditOutlined, UndoOutlined } from '@ant-design/icons'
import { Button, Card, Descriptions, Popconfirm } from 'antd'
import React from 'react'
import AddEditWareHouse from './components/AddEditWareHouse'

type Props = {
  data: any
  showEditWareHouse: boolean
  setShowAddWareHouse: any
  visible: boolean
}

const ContentView = () => {
  return (
    <Descriptions title="Thông tin kho">
      <Descriptions.Item label="Tên kho">Hà Nội</Descriptions.Item>
      <Descriptions.Item label="Địa chỉ">Thanh Xuân</Descriptions.Item>
      <Descriptions.Item label="Ngày tạo">12/12/2012</Descriptions.Item>
    </Descriptions>
  )
}

export default function WareHouseDetail({
  data,
  showEditWareHouse,
  setShowAddWareHouse,
  visible,
}: Props) {
  return (
    <>
      {showEditWareHouse && (
        <AddEditWareHouse
          visible={visible}
          onCancel={() => {
            setShowAddWareHouse(false)
          }}
        />
      )}
      <Card
        style={{ width: '100%', backgroundColor: '#f6f9ff' }}
        actions={[
          <Button
            onClick={() => {
              // props.updateAccount(data)
              setShowAddWareHouse(true)
            }}
            type="text"
            size="large"
            icon={<EditOutlined color="red" />}
          >
            Chỉnh sửa
          </Button>,

          <Popconfirm
            title={'Bạn chắc chắn muốn xoá kho này'}
            onConfirm={async () => {
              // alert('Delete')
              try {
                // onDeleteAccount(data.id)
              } catch (error) {
              } finally {
              }
            }}
            okText="Xoá"
            cancelText="Quay lại"
            okButtonProps={{
              danger: true,
              type: 'primary',
              // loading: isShowDeleteConfirm,
            }}
          >
            <Button type="text" size="large" danger icon={<DeleteFilled />}>
              Xoá kho
            </Button>
          </Popconfirm>,
        ]}
      >
        {ContentView()}
      </Card>
    </>
  )
}
