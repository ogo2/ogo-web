import Icon from '@ant-design/icons'
import { Col, Input, Row } from 'antd'
import React, { useState } from 'react'

export default function Filter() {
  const [searchKey, setSearchKey] = useState('')

  return (
    <>
      <Row align="middle">
        <Col md={10}>
          <Input.Search
            allowClear
            placeholder="Nhập tên kho ..."
            addonAfter={
              <Icon
                type="close-circle-o"
                // onClick={() => {
                //   props.onSearchSubmit('')
                // }}
              />
            }
            // onKeyDown={e => {
            //   if (e.keyCode == 13) {
            //     props.onSearchSubmit(searchKey)
            //   }
            // }}
            // onSearch={(value, event) => {
            //   props.onSearchSubmit(value)
            // }}
            onChange={e => {
              setSearchKey(e.target.value)
            }}
          />
        </Col>
      </Row>
    </>
  )
}
