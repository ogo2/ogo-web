import { Button, PageHeader, Table } from 'antd'
import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import reactotron from 'ReactotronConfig'
import WareHouseDetail from '../WareHouseDetail'
import AddEditWareHouse from './AddEditWareHouse'
import Filter from './Filter'
import Header from './Header'

export default function ContentList() {
  const [showAddWareHouse, setShowAddWareHouse] = useState(false)
  const [showEditWareHouse, setShowEditWareHouse] = useState(false)
  const [currentSelected, setCurrentSelected] = useState({ id: -1 })

  const history = useHistory()

  const CreateWareHouse = () => {
    setShowAddWareHouse(true)
  }

  const columns = [
    {
      title: 'STT',
      dataIndex: 'stt',
      key: 'stt',
    },
    {
      title: 'Tên kho',
      dataIndex: 'ware_house_name',
      key: 'ware_house_name',
    },
    {
      title: 'Địa chỉ',
      dataIndex: 'address',
      key: 'address',
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'create_at',
      key: 'create_at',
    },
  ]

  const data = [
    {
      key: 1,
      stt: 1,
      id: 1,
      ware_house_name: 'Gian hàng Ba Đình',
      address: 'Hà Nội',
      create_at: '12/12/2012',
    },
    {
      key: 2,
      stt: 2,
      id: 2,
      ware_house_name: 'Gian hàng Hoàn Kiếm',
      address: 'Hưng Yên',
      create_at: '12/12/2112',
    },
  ]

  return (
    <>
      <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
        <Header CreateWareHouse={CreateWareHouse} />
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <Filter />
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <Table
          scroll={{
            x: 800,
            scrollToFirstRowOnChange: true,
            y: 'calc(100vh - 280px)',
          }}
          bordered
          dataSource={data}
          //   loading={isLoading}
          columns={columns}
          expandedRowKeys={[currentSelected.id]}
          onRow={r => ({
            onClick: () => {
              if (currentSelected !== r) setCurrentSelected(r)
              else setCurrentSelected({ id: -1 })
              reactotron.log!(r)
            },
          })}
          expandable={{
            expandedRowRender: (record: any) => (
              <WareHouseDetail
                data={record}
                showEditWareHouse={showEditWareHouse}
                setShowAddWareHouse={(isShow: any) => {
                  setShowAddWareHouse(isShow)
                }}
                visible={showAddWareHouse}
              />
            ),
          }}
          //   pagination={{
          //     ...paging,
          //     onChange: async (page, pageSize) => {
          //       setParams({ ...params, page })
          //     },
          //   }}
        />
      </div>

      <AddEditWareHouse
        visible={showAddWareHouse}
        onCancel={() => {
          setShowAddWareHouse(false)
        }}
      ></AddEditWareHouse>
    </>
  )
}
