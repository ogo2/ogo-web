import { Button, Col, Form, Input, Modal, Row } from 'antd'
import React from 'react'

type Props = { visible: boolean; onCancel: any }

const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
}
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
}

export default function AddEditWareHouse({ visible, onCancel }: Props) {
  const [form] = Form.useForm()

  const onFinish = (values: any) => {
    console.log('Success:', values)
  }

  return (
    <Modal
      title="Thêm kho hàng"
      visible={visible}
      maskClosable={false}
      footer={null}
      onCancel={() => {
        form.resetFields()
        onCancel()
      }}
    >
      <Form
        {...formItemLayout}
        labelAlign="left"
        form={form}
        onFinish={(values: any) => onFinish(values)}
        scrollToFirstError
      >
        <Form.Item
          label="Tên kho"
          name="ware_house_name"
          rules={[
            {
              type: 'string',
              message: 'Nhập tên kho',
            },
            {
              required: true,
              message: 'Vui lòng nhập tên kho!',
            },
            {
              min: 1,
              max: 50,
              message: 'Vui lòng nhập từ 1 đến 50 ký tự!',
            },
          ]}
        >
          <Input placeholder="Nhập tên kho" />
        </Form.Item>

        <Form.Item
          label="Địa chỉ"
          name="address"
          rules={[
            {
              type: 'string',
              message: 'Nhập địa chỉ kho',
            },
            {
              required: true,
              message: 'Vui lòng nhập địa chỉ kho!',
            },
            {
              min: 1,
              max: 50,
              message: 'Vui lòng nhập từ 1 đến 50 ký tự!',
            },
          ]}
        >
          <Input placeholder="Nhập địa chỉ kho" />
        </Form.Item>

        <Form.Item {...tailFormItemLayout}>
          <Button type="primary" htmlType="submit">
            Thêm mới
          </Button>
        </Form.Item>
      </Form>
    </Modal>
  )
}
