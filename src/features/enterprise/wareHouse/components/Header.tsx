import { Button, PageHeader } from 'antd'
import React from 'react'
import R from 'utils/R'

type Props = { CreateWareHouse: any }

export default function Header({ CreateWareHouse }: Props) {
  return (
    <PageHeader
      title="Kho hàng"
      extra={[
        <Button
          type="primary"
          onClick={() => {
            CreateWareHouse()
          }}
        >
          {R.strings().action_create}
        </Button>,
      ]}
    />
  )
}
