import { Col, DatePicker, Input, Row, Select } from 'antd'
import TypingAutoSearch from 'common/components/TypingAutoSearch'
import React from 'react'
import { ORDER_STATUS } from 'utils/constants'

const { Option } = Select
const { RangePicker } = DatePicker
const { Search } = Input

type Props = {
  onSearchSubmit: (searchKey: string) => void
  isSearchLoading: boolean
  onTimeSubmit: any
  onStatusSubmit: any
}

export default function Filter({
  onSearchSubmit,
  isSearchLoading,
  onTimeSubmit,
  onStatusSubmit,
}: Props) {
  return (
    <Row gutter={[16, 16]} justify="end">
      <Col span={8}>
        <TypingAutoSearch
          onSearchSubmit={(key: string) => {
            onSearchSubmit(key.trim())
          }}
          isSearchLoading={isSearchLoading}
          placeholder="Mã đơn, tên khách hàng, SĐT khách hàng ..."
        />
        {/* <Search
          className="input-search_account"
          style={{ width: '100%' }}
          placeholder="Mã đơn, tên khách hàng, SĐT khách hàng ..."
          onChange={(e: any) => {
            onSearchSubmit(e.target.value)
          }}
        /> */}
      </Col>

      <Col span={4}>
        <Select
          allowClear
          placeholder="Trạng thái đơn hàng"
          style={{ width: '150px' }}
          onChange={(value: any) => {
            onStatusSubmit(value)
          }}
        >
          <Option value="">Tất cả</Option>
          <Option value={ORDER_STATUS.PENDING}>Chờ xác nhận</Option>
          <Option value={ORDER_STATUS.CONFIRMED}>Đã xác nhận</Option>
          <Option value={ORDER_STATUS.SHIP}>Đang giao</Option>
          <Option value={ORDER_STATUS.SUCCCESS}>Hoàn thành</Option>
          <Option value={ORDER_STATUS.CANCELED}>Huỷ</Option>
        </Select>
      </Col>

      <Col span={5}>
        <RangePicker
          placeholder={['Từ ngày', 'Đến ngày']}
          onChange={(dates, dateStrings) => {
            onTimeSubmit(dateStrings[0], dateStrings[1])
          }}
        />
      </Col>
    </Row>
  )
}
