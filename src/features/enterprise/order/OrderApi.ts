import { ApiClient } from 'services/ApiService'

export const getOrder = (payload: any) => ApiClient.get('/order', payload)

export const getOrderDetail = (payload: any) =>
  ApiClient.get(`/order/${payload}`)

export const confirmListOrder = (payload: any) =>
  ApiClient.put('/order/confirm-list-order', payload)

export const confirmOrder = (payload: any) =>
  ApiClient.put('/order/confirm-order', payload)

export const allOrder = (payload: any) => ApiClient.get(`order/all`, payload)
