import { DownloadOutlined, SaveOutlined } from '@ant-design/icons'
import {
  Button,
  message,
  PageHeader,
  Popconfirm,
  Space,
  Table,
  Tag,
} from 'antd'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { useSelector } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { ORDER_STATUS } from 'utils/constants'
import ExportCsv from 'utils/ExportCSV'
import { formatPrice } from 'utils/ruleForm'
import Filter from './components/Filter'
import { allOrder, confirmListOrder, getOrder } from './OrderApi'

export default function Order() {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const [listOrder, setListOrder] = useState<Array<any>>([])
  const [selectedRowList, setSelectedRowList] = useState<Array<any>>([])
  const [isSearchLoading, setIsSearchLoading] = useState<boolean>(false)
  const [isLoading, setIsLoading] = useState<boolean>(false)
  const [isLoadingBtnExportData, setLoadingBtnExportData] = useState<boolean>(
    false
  )
  const [params, setParams] = useState({
    search: '',
    order_status: '',
    from_date: '',
    to_date: '',
    page: 1,
  })
  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })
  const history = useHistory()

  const columns = [
    {
      width: 70,
      title: 'STT',
      dataIndex: 'index',
      key: 'index',
      render: (text: any, record: any, index: any) => (
        <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
          {(paging.current - 1) * paging.pageSize + index + 1}
        </td>
      ),
      ellipsis: true,
    },
    {
      title: 'Mã đơn',
      dataIndex: 'code',
      key: 'code',
    },
    {
      title: 'Khách hàng',
      dataIndex: 'User',
      key: 'User',
      render: (value: { id: number; name: string; phone: string }) => {
        return (
          <>
            <tr>{value.name}</tr>
            <tr>({value.phone})</tr>
          </>
        )
      },
    },
    {
      title: 'Người nhận hàng',
      dataIndex: 'UserAddress',
      key: 'UserAddress',
      render: (value: { id: number; name: string; phone: string }) => {
        return (
          <>
            <tr>{value.name}</tr>
            <tr>({value.phone})</tr>
          </>
        )
      },
    },
    {
      width: 100,
      title: 'Số SP',
      dataIndex: 'total_amount',
      key: 'total_amount',
    },
    {
      title: 'Tổng tiền',
      dataIndex: 'total_price',
      key: 'total_price',
      render: (value: any) => {
        return <td>{value == null ? '0đ' : formatPrice(value) + 'đ'}</td>
      },
    },
    {
      title: 'TT đơn hàng',
      dataIndex: 'status',
      key: 'status',
      render: (value: any) => {
        switch (value) {
          case ORDER_STATUS.PENDING:
            return <Tag color="blue">Chờ xác nhận</Tag>
          case ORDER_STATUS.CONFIRMED:
            return <Tag>Đã xác nhận</Tag>
          case ORDER_STATUS.SHIP:
            return <Tag>Đang giao</Tag>
          case ORDER_STATUS.SUCCCESS:
            return <Tag color="green">Hoàn thành</Tag>
          case ORDER_STATUS.CANCELED:
            return <Tag color="red">Huỷ</Tag>
          default:
            break
        }
        // if (value === ORDER_STATUS.PENDING)
        //   return <Tag color="blue">Chờ xác nhận</Tag>
        // if (value === ORDER_STATUS.CONFIRMED) return <Tag>Đã xác nhận</Tag>
        // if (value === ORDER_STATUS.SHIP) return <Tag>Đang giao</Tag>
        // if (value === ORDER_STATUS.SUCCCESS)
        //   return <Tag color="green">Hoàn thành</Tag>
        // if (value === ORDER_STATUS.CANCELED) return <Tag color="red">Huỷ</Tag>
      },
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'create_at',
      key: 'create_at',
      render: (record: any) => {
        return <td>{moment(record).format('DD-MM-YYYY')}</td>
      },
    },
  ]

  const getData = async () => {
    setIsLoading(true)
    try {
      const res = await getOrder(params)
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      const dataTable = res.data.map((item: any, index: number) => {
        return { ...item, key: item.id }
      })
      setListOrder(dataTable)
      setPaging(formattedPaging)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
      setIsSearchLoading(false)
    }
  }

  useEffect(() => {
    setTimeout(() => {
      getData()
    }, 300)
  }, [params])

  const rowSelection = {
    onChange: (selectedRowKeys: React.Key[], selectedRows: any) => {
      const selectedRowID = selectedRows.map((item: any) => {
        return item
      })
      setSelectedRowList(selectedRowID)
    },
    getCheckboxProps: (record: any) => ({
      id: record.id,
    }),
  }

  const acceptOrderList = async () => {
    const checked = selectedRowList.filter((item: any) => {
      return item.status === ORDER_STATUS.PENDING
    })
    if (checked.length < 1) {
      message.error('Vui lòng chọn đơn hàng chờ xác nhận')
    } else {
      const list_order_id = checked.map((item: any) => item.id)
      const payload = {
        status: ORDER_STATUS.CONFIRMED,
        list_order_id,
      }
      try {
        const res = await confirmListOrder(payload)
        console.log(res)
      } catch (error) {
        console.log(error)
      } finally {
        getData()
      }
    }
  }

  const cancelOrderList = async () => {
    const checked = selectedRowList.filter((item: any) => {
      return item.status !== ORDER_STATUS.SUCCCESS
    })
    if (checked.length < 1) {
      message.error('Vui lòng chọn đơn hàng chờ xác nhận')
    } else {
      const list_order_id = checked.map((item: any) => item.id)
      const payload = {
        status: ORDER_STATUS.CANCELED,
        list_order_id,
      }
      try {
        const res = await confirmListOrder(payload)
        console.log(res)
      } catch (error) {
        console.log(error)
      } finally {
        getData()
      }
    }
  }

  const [dataExport, setDataExport] = useState<any>([])

  const handleDataSelect = () => {
    const dataSelected = selectedRowList.map((o: any, i: number) => {
      function checkStatus() {
        switch (o.status) {
          case ORDER_STATUS.PENDING:
            return 'Chờ xác nhận'
          case ORDER_STATUS.CONFIRMED:
            return 'Đã xác nhận'
          case ORDER_STATUS.SHIP:
            return 'Đang giao'
          case ORDER_STATUS.SUCCCESS:
            return 'Hoàn thành'
          case ORDER_STATUS.CANCELED:
            return 'Hủy'
          default:
            return ''
        }
      }
      return {
        STT: i + 1,
        'Mã đơn': o.code || '',
        'Khách hàng': o.user_name || '',
        'Gian hàng': o.shop_name || '',
        'Số sp': o.total_amount || '',
        'Tổng tiền':
          o.total_price == null ? '0đ' : formatPrice(o.total_price) + 'đ',
        'TT đơn hàng': checkStatus(),
        'Ngày tạo': moment(o.create_at).format('DD-MM-YYYY'),
      }
    })
    setDataExport(dataSelected)
  }

  useEffect(() => {
    handleDataSelect()
  }, [selectedRowList])

  const onExportDataToExcel = async (fn: any) => {
    try {
      setLoadingBtnExportData(true)
      if (selectedRowList.length === 0) {
        const dataListOrderNotPaging: any = await allOrder(params)
        const dataExport = dataListOrderNotPaging?.data?.map(
          (o: any, i: number) => {
            function checkStatus() {
              switch (o.status) {
                case ORDER_STATUS.PENDING:
                  return 'Chờ xác nhận'
                case ORDER_STATUS.CONFIRMED:
                  return 'Đã xác nhận'
                case ORDER_STATUS.SHIP:
                  return 'Đang giao'
                case ORDER_STATUS.SUCCCESS:
                  return 'Hoàn thành'
                case ORDER_STATUS.CANCELED:
                  return 'Hủy'
                default:
                  return ''
              }
            }
            return {
              STT: i + 1,
              'Mã đơn': o.code || '',
              'Khách hàng': o.user_name || '',
              // 'Gian hàng': o.shop_name || '',
              'Số sp': o.total_amount || '',
              'Tổng tiền':
                o.total_price == null ? '0đ' : formatPrice(o.total_price) + 'đ',
              // 'TT đơn hàng...': o.status,
              'TT đơn hàng': checkStatus(),
              'Ngày tạo': moment(o.create_at).format('DD-MM-YYYY'),
            }
          }
        )
        setDataExport(dataExport)
        fn()
      } else {
        fn()
      }
    } catch (error) {
      message.error(
        'Đã có lỗi trong quá trình export dữ liệu, vui lòng thử lại sau!'
      )
    } finally {
      setLoadingBtnExportData(false)
    }
  }

  return (
    <>
      <div
        style={{
          backgroundColor: 'white',
          margin: '5px 10px 15px',
        }}
      >
        <PageHeader
          title="Đơn hàng"
          extra={[
            <Popconfirm
              placement="bottomRight"
              title="Bạn chắc chắn muốn từ chối đơn hàng "
              onConfirm={() => {
                cancelOrderList()
              }}
              okText="Đồng ý"
              cancelText="Huỷ"
              okButtonProps={{
                danger: true,
                type: 'primary',
              }}
              disabled={selectedRowList.length === 0 ? true : false}
            >
              <Button
                type="primary"
                danger
                disabled={selectedRowList.length === 0 ? true : false}
              >
                Từ chối
              </Button>
            </Popconfirm>,
            <Popconfirm
              placement="bottomRight"
              title="Bạn chắc chắn muốn xác nhận đơn hàng "
              onConfirm={() => {
                acceptOrderList()
              }}
              okText="Đồng ý"
              cancelText="Huỷ"
              okButtonProps={{
                danger: false,
                type: 'primary',
              }}
              disabled={selectedRowList.length === 0 ? true : false}
            >
              <Button
                style={
                  selectedRowList.length === 0
                    ? {
                        color: 'rgba(0, 0, 0, 0.25)',
                        background: '#f5f5f5',
                        borderColor: '#d9d9d9',
                        textShadow: 'none',
                        boxShadow: 'none',
                      }
                    : { backgroundColor: 'green', color: 'white' }
                }
                disabled={selectedRowList.length === 0 ? true : false}
              >
                Xác nhận
              </Button>
            </Popconfirm>,
            <Space>
              <ExportCsv
                loading={isLoadingBtnExportData}
                onClick={fn => onExportDataToExcel(fn)}
                sheetName={['OrderList']}
                sheets={{
                  OrderList: ExportCsv.getSheets(dataExport),
                }}
                fileName="Dánh sách sản phẩm"
              >
                <DownloadOutlined />
                &nbsp;Export
              </ExportCsv>
            </Space>,
          ]}
        />
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '5px 10px 15px',
          padding: '10px 15px',
        }}
      >
        <Filter
          isSearchLoading={isSearchLoading}
          onSearchSubmit={(searchKey: string) => {
            setIsSearchLoading(true)
            setParams({ ...params, search: searchKey, page: 1 })
          }}
          onTimeSubmit={(from_date: string, to_date: string) => {
            setParams({
              ...params,
              from_date: from_date,
              to_date: to_date,
              page: 1,
            })
          }}
          onStatusSubmit={(status: string) => {
            setParams({ ...params, order_status: status, page: 1 })
          }}
        />
      </div>

      <div
        style={{
          backgroundColor: 'white',
          margin: '0px 10px 15px',
          padding: '10px 20px',
        }}
      >
        <p>
          Kết quả lọc: <b>{paging.total}</b>
        </p>
        <Table
          className="table-expanded-custom-no-image"
          loading={isLoading}
          bordered
          columns={columns}
          dataSource={listOrder}
          rowSelection={{
            ...rowSelection,
          }}
          scroll={{
            x: 800,
            scrollToFirstRowOnChange: true,
            // y: 'calc(100vh - 410px)',
            y: `calc(${heightWeb}px - 420px)`,
          }}
          onRow={(record, rowIndex) => ({
            onDoubleClick: () => {
              history.push({
                pathname: `${ENTERPRISE_ROUTER_PATH.ORDER_DETAIL}/${record.id}`,
                state: record,
              })
            },
          })}
          pagination={{
            ...paging,
            showSizeChanger: false,
            onChange: async (page, pageSize) => {
              setParams({ ...params, page })
            },
          }}
        />
      </div>
    </>
  )
}
