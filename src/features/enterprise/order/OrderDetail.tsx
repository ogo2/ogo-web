import { CheckOutlined, CloseCircleOutlined } from '@ant-design/icons'
import {
  Button,
  Col,
  Descriptions,
  PageHeader,
  Popconfirm,
  Row,
  Spin,
  Table,
  Timeline,
  message,
} from 'antd'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'
import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { useHistory, useParams } from 'react-router-dom'
import { ORDER_STATUS } from 'utils/constants'
import { formatPrice } from 'utils/ruleForm'
import { confirmOrder, getOrderDetail } from './OrderApi'

export default function OrderDetail() {
  const [isLoading, setIsLoading] = useState(false)
  const [data, setData] = useState<any>()
  const [listProduct, setListProduct] = useState()

  const history = useHistory()
  const params: any = useParams()

  const getData = async () => {
    setIsLoading(true)
    try {
      const res = await getOrderDetail(params.id)
      if (res.data === null) {
        message.warn('Đơn hàng không tồn tại!')
        history.push('/order')
      }
      const orderHistory = res.data.OrderStatusHistories.reverse()
      setData({ ...res.data, orderHistory })
      const tableData = res.data.OrderItems.map((item: any, index: number) => {
        return { ...item, key: item.id, stt: index + 1 }
      })
      setListProduct(tableData)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  useEffect(() => {
    getData()
  }, [params])

  const columns = [
    {
      title: 'STT',
      dataIndex: 'stt',
      key: 'stt',
    },
    {
      title: 'Tên sản phẩm',
      dataIndex: 'name',
      key: 'name',
    },
    {
      title: 'Kho hàng',
      dataIndex: 'stock_name',
      key: 'stock_name',
    },
    {
      title: 'Số lượng',
      dataIndex: 'amount',
      key: 'amount',
    },
    {
      title: 'Giá bán',
      dataIndex: 'price',
      key: 'price',
      render: (value: any) => {
        return <td>{formatPrice(value)} đ</td>
      },
    },
    {
      title: 'Thành tiền',
      dataIndex: '',
      key: '',
      render: (record: any) => {
        return <td>{formatPrice(record.amount * record.price)} đ</td>
      },
    },
  ]

  return (
    <Spin size="large" spinning={isLoading}>
      <div style={{ backgroundColor: 'white', margin: '5px 10px 15px' }}>
        <PageHeader
          title={`Đơn hàng ${data?.code}`}
          onBack={() => {
            history.push(ENTERPRISE_ROUTER_PATH.ORDER)
          }}
          extra={
            data?.status === ORDER_STATUS.PENDING ? (
              [
                <Popconfirm
                  placement="bottomRight"
                  title="Bạn chắc chắn muốn từ chối đơn hàng "
                  onConfirm={async () => {
                    try {
                      await confirmOrder({
                        status: ORDER_STATUS.CANCELED,
                        order_id: data.id,
                      })
                    } catch (error) {
                      console.log(error)
                    } finally {
                      getData()
                    }
                  }}
                  okText="Đồng ý"
                  cancelText="Huỷ"
                  okButtonProps={{
                    danger: true,
                    type: 'primary',
                  }}
                >
                  <Button type="primary" danger>
                    <CloseCircleOutlined />
                    Từ chối
                  </Button>
                </Popconfirm>,
                <Popconfirm
                  placement="bottomRight"
                  title="Bạn chắc chắn muốn xác nhận đơn hàng "
                  onConfirm={async () => {
                    try {
                      await confirmOrder({
                        status: ORDER_STATUS.CONFIRMED,
                        order_id: data.id,
                      })
                    } catch (error) {
                      console.log(error)
                    } finally {
                      getData()
                    }
                  }}
                  okText="Đồng ý"
                  cancelText="Huỷ"
                  okButtonProps={{
                    danger: false,
                    type: 'primary',
                    style: {
                      borderColor: '#00abba',
                      backgroundColor: '#00abba',
                      color: 'white',
                    },
                  }}
                >
                  <Button
                    type="primary"
                    style={{
                      borderColor: '#00abba',
                      backgroundColor: '#00abba',
                      color: 'white',
                    }}
                  >
                    <CheckOutlined />
                    Xác nhận
                  </Button>
                </Popconfirm>,
              ]
            ) : data?.status === ORDER_STATUS.CONFIRMED ? (
              [
                <Popconfirm
                  placement="bottomRight"
                  title="Bạn chắc chắn muốn huỷ đơn hàng "
                  onConfirm={async () => {
                    try {
                      await confirmOrder({
                        status: ORDER_STATUS.CANCELED,
                        order_id: data.id,
                      })
                    } catch (error) {
                      console.log(error)
                    } finally {
                      getData()
                    }
                  }}
                  okText="Đồng ý"
                  cancelText="Huỷ"
                  okButtonProps={{
                    danger: true,
                    type: 'primary',
                  }}
                >
                  <Button type="primary" danger>
                    Huỷ
                  </Button>
                </Popconfirm>,
                <Popconfirm
                  placement="bottomRight"
                  title="Bạn chắc chắn muốn giao đơn hàng "
                  onConfirm={async () => {
                    try {
                      await confirmOrder({
                        status: ORDER_STATUS.SHIP,
                        order_id: data.id,
                      })
                    } catch (error) {
                      console.log(error)
                    } finally {
                      getData()
                    }
                  }}
                  okText="Đồng ý"
                  cancelText="Huỷ"
                  okButtonProps={{
                    danger: false,
                    type: 'primary',
                  }}
                >
                  <Button type="primary">Giao</Button>
                </Popconfirm>,
              ]
            ) : data?.status === ORDER_STATUS.SHIP ? (
              [
                <Popconfirm
                  placement="bottomRight"
                  title="Bạn chắc chắn muốn hoàn thành đơn hàng "
                  onConfirm={async () => {
                    try {
                      await confirmOrder({
                        status: ORDER_STATUS.SUCCCESS,
                        order_id: data.id,
                      })
                    } catch (error) {
                      console.log(error)
                    } finally {
                      getData()
                    }
                  }}
                  okText="Đồng ý"
                  cancelText="Huỷ"
                  okButtonProps={{
                    danger: false,
                    type: 'primary',
                  }}
                >
                  <Button type="primary">Hoàn thành</Button>
                </Popconfirm>,
              ]
            ) : (
              <></>
            )
          }
        />
      </div>

      <div>
        <Row
          style={{
            backgroundColor: 'white',
          }}
        >
          <Col
            xxl={8}
            xl={8}
            lg={7}
            style={{
              display: 'flex',
            }}
          >
            <div style={{ borderLeft: '10px solid #f0f2f5' }}></div>
            <div>
              <Descriptions
                title="Thông tin khách hàng"
                style={{ flex: '1', padding: '20px' }}
              >
                <Descriptions.Item label="Tên" span={3}>
                  {data?.User.name}
                </Descriptions.Item>

                <Descriptions.Item label="Số điện thoại" span={3}>
                  {data?.User.phone}
                </Descriptions.Item>
              </Descriptions>

              <div
                style={{
                  borderBottom: '2px solid #d1d4d7',
                  width: '90%',
                  marginLeft: '5%',
                }}
              ></div>

              <Descriptions
                title="Thông tin Người nhận hàng"
                style={{ flex: '1', padding: '20px' }}
              >
                <Descriptions.Item label="Tên người nhận" span={3}>
                  {data?.UserAddress.name}
                </Descriptions.Item>

                <Descriptions.Item label="Số điện thoại" span={3}>
                  {data?.UserAddress.phone}
                </Descriptions.Item>

                <Descriptions.Item label="Địa chỉ">
                  {data?.UserAddress.address}, {data?.UserAddress.DFWard.name},{' '}
                  {data?.UserAddress.DFDistrict.name},{' '}
                  {data?.UserAddress.DFProvince.name}
                </Descriptions.Item>
              </Descriptions>
            </div>
            <div style={{ borderLeft: '10px solid #f0f2f5' }}></div>
          </Col>

          <Col
            xxl={8}
            xl={8}
            lg={9}
            style={{
              display: 'flex',
            }}
          >
            <div style={{ borderLeft: '10px solid #f0f2f5' }}></div>
            <Descriptions
              title="Thông tin đơn hàng"
              style={{ flex: '1', padding: '20px' }}
              extra={[
                <div>
                  <p style={{ fontWeight: 'bold', color: 'orange' }}>
                    {data?.status === ORDER_STATUS.PENDING ? (
                      'Chờ xác nhận'
                    ) : data?.status === ORDER_STATUS.CONFIRMED ? (
                      'Đã xác nhận'
                    ) : data?.status === ORDER_STATUS.SHIP ? (
                      'Đang giao'
                    ) : data?.status === ORDER_STATUS.SUCCCESS ? (
                      'Hoàn thành'
                    ) : data?.status === ORDER_STATUS.CANCELED ? (
                      'Huỷ'
                    ) : (
                      <></>
                    )}
                  </p>
                </div>,
              ]}
            >
              <Descriptions.Item label="Gian hàng" span={3}>
                {data?.Shop.name}
              </Descriptions.Item>
              <Descriptions.Item label="Mã đơn hàng" span={3}>
                {data?.code}
              </Descriptions.Item>

              <Descriptions.Item label="Sản phẩm" span={3}>
                {data?.OrderItems.length} sản phẩm
              </Descriptions.Item>

              <Descriptions.Item label="Tổng tiền" span={3}>
                {formatPrice(data?.total_price_old)} đ
              </Descriptions.Item>

              {data?.gift_id ? (
                <>
                  <Descriptions.Item label="Mã giảm giá" span={3}>
                    {data?.gift_name} giảm {data?.gift_discount_percent}% tối đa{' '}
                    {formatPrice(data?.gift_max_discount_money)}đ
                  </Descriptions.Item>
                  <Descriptions.Item label="Số tiền giảm giá" span={3}>
                    {formatPrice(data?.total_price_old - data?.total_price)} đ
                  </Descriptions.Item>
                </>
              ) : (
                <></>
              )}

              <Descriptions.Item label="Tổng tiền thanh toán" span={3}>
                {formatPrice(data?.total_price)} đ
              </Descriptions.Item>
            </Descriptions>
            <div style={{ borderLeft: '10px solid #f0f2f5' }}></div>
          </Col>

          <Col
            xxl={8}
            xl={8}
            lg={8}
            style={{
              display: 'flex',
            }}
          >
            <div style={{ borderLeft: '10px solid #f0f2f5' }}></div>
            <div style={{ flex: '1', padding: '20px' }}>
              <Descriptions title="Lịch sử đơn hàng" />
              <Timeline mode="left">
                {data?.orderHistory.map((item: any) => {
                  return (
                    <Timeline.Item label={item.describe}>
                      {moment(item.create_at).format('HH:mm DD-MM-YYYY')}
                    </Timeline.Item>
                  )
                })}
              </Timeline>
            </div>
            <div style={{ borderLeft: '10px solid #f0f2f5' }}></div>
          </Col>
        </Row>
        <Row>
          <div
            style={{
              margin: '10px 10px 0px',
              padding: '10px',
              width: '100%',
              // marginRight: '1%',
              backgroundColor: 'white',
            }}
          >
            <p style={{ fontWeight: 'bold', fontSize: '16px' }}>
              Danh sách sản phẩm
            </p>
            <Table
              size="small"
              scroll={{
                x: 400,
                scrollToFirstRowOnChange: true,
                // y: 'calc(100vh - 280px)',
              }}
              bordered
              columns={columns}
              dataSource={listProduct}
              onRow={(record, rowIndex) => ({
                onDoubleClick: () => {
                  history.push({
                    pathname: `${ENTERPRISE_ROUTER_PATH.PRODUCT_DETAIL}/${record.product_id}`,
                    state: record,
                  })
                },
              })}
            />
          </div>
        </Row>
      </div>
    </Spin>
  )
}
