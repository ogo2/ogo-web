import {
  EnvironmentOutlined,
  GiftOutlined,
  HeartOutlined,
  PhoneOutlined,
  UserOutlined,
} from '@ant-design/icons'
import { Card, Descriptions, Tabs, Col, Row } from 'antd'
import moment from 'moment'
import { formatPrice } from 'utils/ruleForm'
import HistoryPoint from './HistoryPoint'
import PurchaseInforView from './PurchaseInforView'

const { TabPane } = Tabs
type Props = {
  data: any
  getData?: any
  onDeleteCustomer?: any
}

const ContentView = (data: any) => {
  return (
    <Row>
      <Col span="12">
        <Descriptions size="default" column={1} title="Thông tin chung">
          <Descriptions.Item label={<UserOutlined />}>
            {data.name}
          </Descriptions.Item>
          <Descriptions.Item label={<PhoneOutlined />}>
            {data.user_name}
          </Descriptions.Item>
          <Descriptions.Item label={<GiftOutlined />}>
            {data.date_of_birth
              ? moment(data.date_of_birth).format('DD/MM/YYYY')
              : 'Chưa có ngày sinh'}
          </Descriptions.Item>
          {/* <Descriptions.Item label={<EnvironmentOutlined />}>
            {data.UserAddresses[0]?.location_address
              ? data.UserAddresses[0]?.location_address
              : 'Chưa có địa chỉ'}
          </Descriptions.Item> */}
          <Descriptions.Item label={<HeartOutlined />}>
            {data.topic
              ? 'Chủ đề quam tâm: ' + data.topic
              : ' Chưa có chủ đề quan tâm '}
          </Descriptions.Item>
        </Descriptions>
      </Col>
      <Col span="12">
        <Descriptions size="default" column={1} title="Tổng quan bán hàng">
          <Descriptions.Item>
            <Col span="12">
              <span
                style={{
                  color: 'red',
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                {data.total_price_success !== '0'
                  ? formatPrice(data.total_price_success) + ' đ'
                  : '0 đ'}
              </span>
              <span
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                Doanh số trên đơn
              </span>
            </Col>
            <Col span="12">
              <span
                style={{
                  color: 'green',
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                {data.total_price_order_pending !== 0
                  ? formatPrice(data.total_price_order_pending) + ' đ'
                  : '0 đ'}
              </span>
              <span
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                Doanh số đơn chưa hoàn thành
              </span>
            </Col>
          </Descriptions.Item>
          <Descriptions.Item>
            <Col span="12">
              <span
                style={{
                  color: 'blue',
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                {data.total_amount_order !== '0'
                  ? formatPrice(data.total_amount_order)
                  : 0}
              </span>
              <span
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                Sản phẩm đã mua
              </span>
            </Col>
            <Col span="12">
              <span
                style={{
                  color: 'blue',
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                {data.total_count_order
                  ? formatPrice(data.total_count_order)
                  : 0}
              </span>
              <span
                style={{
                  display: 'flex',
                  justifyContent: 'center',
                }}
              >
                Tổng đơn hàng
              </span>
            </Col>
          </Descriptions.Item>
        </Descriptions>
      </Col>
    </Row>
  )
}

function DetailCustomer({ data, onDeleteCustomer }: Props) {
  return (
    <div>
      <Card
        style={{
          backgroundColor: '#f6f9ff',
          borderColor: '#1890ff',
          borderTop: 'none',
        }}
      >
        <Tabs defaultActiveKey="1">
          <TabPane tab="Thông tin chung" key="1">
            {ContentView(data)}
          </TabPane>
          <TabPane tab="Thông tin mua hàng" key="2">
            <PurchaseInforView id={data.id} />
          </TabPane>
          <TabPane tab="Lịch sử tích điểm" key="3">
            <HistoryPoint id={data.id} />
          </TabPane>
        </Tabs>
      </Card>
    </div>
  )
}
export default DetailCustomer
