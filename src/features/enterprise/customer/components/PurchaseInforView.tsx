import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { DatePicker, Table, Tag } from 'antd'
import { getListOrder } from '../CustomerApi'
import { formatPrice } from 'utils/ruleForm'
import { ORDER_STATUS } from 'utils/constants'
import { useHistory } from 'react-router'
import { ENTERPRISE_ROUTER_PATH } from 'common/config'

const { RangePicker } = DatePicker
type Props = {
  id: number
}
export default function PurchaseInforView({ id }: Props) {
  const history = useHistory()
  const [dataPurchaseInfor, setDataPurchaseInfor] = useState<Array<any>>([])
  const [isLoadingPurchaseInfor, setIsLoadingPurchaseInfor] = useState(false)
  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })
  const [params, setParams] = useState({
    user_id: id,
    from_date: '',
    to_date: '',
    page: 1,
  })
  const columnsPurchaseInfor = [
    {
      width: 60,
      title: 'STT',
      render: (text: any, record: any, index: any) => (
        <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
          {(paging.current - 1) * paging.pageSize + index + 1}
        </td>
      ),
      ellipsis: true,
    },
    { width: 140, title: 'Mã đơn', dataIndex: 'code', key: 'code' },
    {
      title: 'Khách hàng ',
      dataIndex: 'user_name',
      key: 'user_name',
    },
    {
      title: 'Gian hàng',
      dataIndex: 'shop_name',
      key: 'shop_name',
    },
    {
      width: 60,
      title: 'Số SP ',
      dataIndex: 'total_amount',
      key: 'total_amount',
    },
    {
      title: 'Tổng tiền ',
      dataIndex: 'total_price',
      key: 'total_price',
      render: (value: any) => {
        return <p>{value ? formatPrice(value) + ' đ' : '0 đ'}</p>
      },
    },
    {
      width: '130px',
      title: 'Trạng thái',
      dataIndex: 'status',
      key: 'status',
      render: (value: any) => {
        switch (value) {
          case ORDER_STATUS.PENDING:
            return <Tag color="blue">Chờ xác nhận</Tag>
          case ORDER_STATUS.CONFIRMED:
            return <Tag color="gold">Đã xác nhận</Tag>
          case ORDER_STATUS.SHIP:
            return <Tag color="geekblue">Đang giao</Tag>
          case ORDER_STATUS.SUCCCESS:
            return <Tag color="green">Hoàn thành</Tag>
          case ORDER_STATUS.CANCELED:
            return <Tag color="volcano">Đã hủy</Tag>
        }
      },
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'create_at',
      key: 'create_at',
      render: (value: any) => {
        return <>{moment(value).format('DD/MM/YYYY')}</>
      },
    },
  ]
  useEffect(() => {
    getData()
  }, [params])

  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    try {
      setIsLoadingPurchaseInfor(true)
      const res = await getListOrder(params)
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setPaging(formattedPaging)
      setDataPurchaseInfor(res.data)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoadingPurchaseInfor(false)
    }
  }
  return (
    <div>
      <div>
        <b style={{ float: 'left', fontSize: '16px' }}>Lịch sử mua hàng</b>
      </div>
      <RangePicker
        className="rangerpicker_topic"
        style={{ marginBottom: '15px', float: 'right' }}
        placeholder={['Từ ngày', 'đến ngày']}
        onChange={(value, dateString) => {
          setParams({
            ...params,
            from_date: dateString[0],
            to_date: dateString[1],
          })
        }}
      />
      <Table
        scroll={{
          x: 800,
          scrollToFirstRowOnChange: true,
          y: 'calc(100vh - 280px)',
        }}
        bordered
        dataSource={dataPurchaseInfor}
        loading={isLoadingPurchaseInfor}
        columns={columnsPurchaseInfor}
        onRow={(record: any) => {
          return {
            onDoubleClick: () => {
              history.push({
                pathname: `${ENTERPRISE_ROUTER_PATH.ORDER_DETAIL}/${record.id}`,
                state: record,
              })
            },
          }
        }}
        pagination={{
          ...paging,
          onChange: async (page, pageSize) => {
            setParams({ ...params, page })
          },
        }}
      />
    </div>
  )
}
