import { Table, Select, Input, DatePicker, Row, Col } from 'antd'
import moment from 'moment'
import { useEffect, useRef, useState } from 'react'
import reactotron from 'ReactotronConfig'
import { formatPrice } from 'utils/ruleForm'
import DetailCustomer from './DetailCustomer'
import { getCustomerList } from '../CustomerApi'
import HeaderCustomer from '../components/HeaderCustomer'
import { useSelector } from 'react-redux'

export default function ListCustomer() {
  const heightWeb = useSelector(
    (state: any) => state.configReducer.dimension.height
  )
  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })
  const [listCustomer, setListCustomer] = useState<Array<any>>([])
  const [isLoading, setIsLoading] = useState(false)
  const [currentSelected, setCurrentSelected] = useState({ id: -1 })
  const [params, setParams] = useState<any>({
    search: '',
    province_id: undefined,
    page: 1,
    from_date: '',
    to_date: '',
  })
  const timeOut = useRef<any>(null)
  const [isTyping, setIsTyping] = useState(false)

  useEffect(() => {
    if (timeOut.current) {
      setIsTyping(true)
      clearTimeout(timeOut.current)
    }
    timeOut.current = setTimeout(() => {
      setCurrentSelected({ id: -1 })
      getData()
      setIsTyping(false)
    }, 300)
  }, [params])

  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    try {
      setIsLoading(true)
      setCurrentSelected({ id: -1 })
      const res = await getCustomerList(params)
      const data_list = res.data.map((item: any) => {
        return {
          ...item,
          key: item.id,
          province_name: item.UserAddresses[0]?.province_name,
        }
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setPaging(formattedPaging)
      setListCustomer(data_list)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoading(false)
    }
  }

  const columns = [
    {
      width: 70,
      title: 'STT',
      dataIndex: 'key',
      key: 'key',
      render: (text: any, record: any, index: any) => (
        <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
          {(paging.current - 1) * paging.pageSize + index + 1}
        </td>
      ),
      ellipsis: true,
    },
    { title: 'Tên khách hàng', dataIndex: 'name', key: 'name' },
    {
      title: 'Số điện thoại',
      dataIndex: 'user_name',
      key: 'user_name',
    },
    // {
    //   title: 'Tỉnh thành phố',
    //   dataIndex: 'province_name',
    //   key: 'province_name',
    //   render: (value: any) => <>{!value ? '--' : value}</>,
    // },
    {
      title: 'Doanh số trên đơn hàng',
      dataIndex: 'total_price_success',
      key: 'total_price_success',
      render: (value: any) => (
        <>{value == 0 ? '0đ' : formatPrice(value) + 'đ'}</>
      ),
    },
    {
      title: 'Doanh số thực tế',
      dataIndex: 'total_price_pending',
      key: 'total_price_pending',
      render: (value: any) => (
        <>{value == 0 ? '0đ' : formatPrice(value) + 'đ'}</>
      ),
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'create_at',
      key: 'create_at',
      render: (value: any) => {
        return <>{moment(value).format('DD-MM-YYYY')}</>
      },
    },
  ]
  return (
    <div>
      <HeaderCustomer
        params={params}
        onSearchSubmit={(searchKey: string) => {
          setParams({ ...params, search: searchKey.trim(), page: 1 })
        }}
        onProvinceSubmit={(provinceKey: string) => {
          setParams({
            ...params,
            province_id: provinceKey === '' ? undefined : provinceKey,
            page: 1,
          })
        }}
        onDateSubmit={(from_date: string, to_date: string) => {
          setParams({
            ...params,
            from_date: from_date,
            to_date: to_date,
            page: 1,
          })
        }}
      />
      <div>
        <div
          style={{
            backgroundColor: 'white',
            margin: '0px 10px 15px',
            padding: '10px 20px',
          }}
        >
          <p>
            Kết quả lọc: <b>{paging.total}</b>
          </p>
          <Table
            className="table-expanded-custom-no-image"
            scroll={{
              x: 800,
              scrollToFirstRowOnChange: true,
              // y: 'calc(100vh - 280px)',
              y: `calc(${heightWeb}px - 420px)`,
            }}
            bordered
            dataSource={listCustomer}
            loading={isLoading}
            columns={columns}
            expandedRowKeys={[currentSelected.id]}
            onRow={r => ({
              onClick: () => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            })}
            expandable={{
              expandedRowRender: (record: any) => (
                <DetailCustomer
                  onDeleteCustomer={async (id: any) => {}}
                  data={record}
                />
              ),
              onExpand: (status: any, r: any) => {
                if (currentSelected !== r) setCurrentSelected(r)
                else setCurrentSelected({ id: -1 })
                reactotron.log!(r)
              },
            }}
            pagination={{
              ...paging,
              showSizeChanger: false,
              onChange: async (page, pageSize) => {
                setParams({ ...params, page })
              },
            }}
          />
        </div>
      </div>
    </div>
  )
}
