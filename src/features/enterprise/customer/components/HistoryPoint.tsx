import moment from 'moment'
import React, { useEffect, useState } from 'react'
import { DatePicker, Table } from 'antd'
import { getListPointHistory } from '../CustomerApi'

const { RangePicker } = DatePicker
type Props = {
  id: number
}
export default function HistoryPoint({ id }: Props) {
  const [dataHistoryPoint, setDataHistoryPoint] = useState<Array<any>>([])
  const [isLoadingHistoryPoint, setIsLoadingHistoryPoint] = useState(false)
  const [paging, setPaging] = useState({
    total: 0,
    current: 1,
    pageSize: 0,
  })
  const [params, setParams] = useState({
    user_id: id,
    from_date: '',
    to_date: '',
    page: 1,
  })
  const columnsHistoryPoint = [
    {
      width: 70,
      title: 'STT',
      render: (text: any, record: any, index: any) => (
        <td style={{ textAlign: 'center', display: 'block' }} id={record.id}>
          {(paging.current - 1) * paging.pageSize + index + 1}
        </td>
      ),
      ellipsis: true,
    },
    {
      title: 'Khách hàng ',
      dataIndex: 'customer_name',
      key: 'customer_name',
    },
    {
      title: 'Loại tích điểm',
      dataIndex: 'kind_of_point',
      key: 'kind_of_point',
    },
    {
      title: 'Số điểm ',
      dataIndex: 'point',
      key: 'point',
    },
    {
      title: 'Số dư',
      dataIndex: 'current_point',
      key: 'current_point',
    },
    {
      title: 'Ngày tạo',
      dataIndex: 'create_at',
      key: 'create_at',
      render: (value: any) => {
        return <>{moment(value).format('DD-MM-YYYY')}</>
      },
    },
  ]
  useEffect(() => {
    getData()
  }, [params])

  useEffect(() => {
    getData()
  }, [])

  const getData = async () => {
    try {
      setIsLoadingHistoryPoint(true)
      const res = await getListPointHistory(params)
      const res_data = res.data.map((item: any) => {
        return {
          ...item,
          kind_of_point: item.DFTypeTransactionPoint?.name,
          customer_name: item.User?.name,
          point: item.type ? '+' + item.point : '-' + item.point,
        }
      })
      const formattedPaging = {
        total: res.paging.totalItemCount,
        current: res.paging.page,
        pageSize: res.paging.limit,
      }
      setPaging(formattedPaging)
      setDataHistoryPoint(res_data)
    } catch (error) {
      console.log(error)
    } finally {
      setIsLoadingHistoryPoint(false)
    }
  }
  return (
    <div>
      <div>
        <b style={{ float: 'left', fontSize: '16px' }}>Lịch sử tích điểm</b>
      </div>
      <RangePicker
        className="rangerpicker_topic"
        style={{ marginBottom: '15px', float: 'right' }}
        placeholder={['Từ ngày', 'đến ngày']}
        onChange={(value, dateString) => {
          setParams({
            ...params,
            from_date: dateString[0],
            to_date: dateString[1],
          })
        }}
      />
      <Table
        scroll={{
          x: 800,
          scrollToFirstRowOnChange: true,
          y: 'calc(100vh - 280px)',
        }}
        bordered
        dataSource={dataHistoryPoint}
        loading={isLoadingHistoryPoint}
        columns={columnsHistoryPoint}
        pagination={{
          ...paging,
          onChange: async (page, pageSize) => {
            setParams({ ...params, page })
          },
        }}
      />
    </div>
  )
}
