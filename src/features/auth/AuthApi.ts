import { ApiClient } from 'services/ApiService'

interface PayloadLogin {
  user_name: string
  password: string
}

export const requestLogin = (payload: PayloadLogin) =>
  ApiClient.put('/users/login', payload)
export const requestLogout = () => ApiClient.put('/users/logout')
export const requestGetUserInfo = () => ApiClient.get('/users/info')
