import { ADMIN_ROUTER_PATH, ENTERPRISE_ROUTER_PATH } from 'common/config'
import AdAccounts from 'features/admin/account/Accounts'
import AdConfig from 'features/admin/config'
import AdCustomer from 'features/admin/customer'
import AdDashBoard from 'features/admin/dashboard'
import AdGiftChange from 'features/admin/gift/giftChange'
import AdGift from 'features/admin/gift/listGift'
import AdLiveStream from 'features/admin/liveStream'
import AdLiveStreamDetail from 'features/admin/liveStream/LiveStreamDetail'
import AdOrder from 'features/admin/order'
import AdOrderDetail from 'features/admin/order/OrderDetail'
import AdCommunityPost from 'features/admin/post/communityPost'
import AddEditPost from 'features/admin/post/communityPost/components/AddEditCommunityPost'
import DetailPost from 'features/admin/post/communityPost/DetailPost'
import AdTopicPost from 'features/admin/post/topicPost/TopicPosts'
import AdCategoryProduct from 'features/admin/product/categoryProduct'
import AdProduct from 'features/admin/product/listProduct'
import AdProductDetail from 'features/admin/product/listProduct/DetailProduct'
import AdLiveStreamReport from 'features/admin/report/liveStreamReport'
import AdSaleReport from 'features/admin/report/salesReport'
import AdShopReport from 'features/admin/report/shopReport'
import AdService from 'features/admin/service/Services'
import AdShop from 'features/admin/shop'
import AdShopDetail from 'features/admin/shop/ShopDetail'
import Account from 'features/enterprise/account'
import Chat from 'features/enterprise/chat'
import Customer from 'features/enterprise/customer'
import DashBoard from 'features/enterprise/dashboard'
import ForumPost from 'features/enterprise/forumPost'
import DetailForumPost from 'features/enterprise/forumPost/DetailPost'
import LiveStream from 'features/enterprise/liveStream'
import LiveStreamDetail from 'features/enterprise/liveStream/LiveStreamDetail'
import Order from 'features/enterprise/order'
import OrderDetail from 'features/enterprise/order/OrderDetail'
import Product from 'features/enterprise/product'
import AddProduct from 'features/enterprise/product/components/AddEditProduct'
import DetailProduct from 'features/enterprise/product/DetailProduct'
import LiveStreamReport from 'features/enterprise/report/liveStreamReport'
import SaleReport from 'features/enterprise/report/salesReport'
import Stock from 'features/enterprise/stock'

interface RouterProps {
  path: string
  component: React.FC | any
  param?: any
  exact?: boolean
}

const adminRouter: Array<RouterProps> = [
  {
    path: ADMIN_ROUTER_PATH.HOME,
    component: AdDashBoard,
  },
  {
    path: ADMIN_ROUTER_PATH.DASH_BOARD,
    component: AdDashBoard,
  },
  {
    path: ADMIN_ROUTER_PATH.SHOP,
    component: AdShop,
  },
  {
    path: ADMIN_ROUTER_PATH.SHOP_DETAIL + '/:id',
    component: AdShopDetail,
  },
  {
    path: ADMIN_ROUTER_PATH.PRODUCT,
    component: AdProduct,
  },
  {
    path: ADMIN_ROUTER_PATH.PRODUCT_DETAIL + '/:id',
    component: AdProductDetail,
  },
  {
    path: ADMIN_ROUTER_PATH.CATEGORY_PRODUCT,
    component: AdCategoryProduct,
  },
  {
    path: ADMIN_ROUTER_PATH.CUSTOMER,
    component: AdCustomer,
  },
  {
    path: ADMIN_ROUTER_PATH.ORDER,
    component: AdOrder,
  },
  {
    path: ADMIN_ROUTER_PATH.ORDER_DETAIL + '/:id',
    component: AdOrderDetail,
  },
  {
    path: ADMIN_ROUTER_PATH.LIVE_STREAM,
    component: AdLiveStream,
  },
  {
    path: ADMIN_ROUTER_PATH.LIVE_STREAM_DETAIL + '/:id',
    component: AdLiveStreamDetail,
  },
  {
    path: ADMIN_ROUTER_PATH.SERVICE_PACK,
    component: AdService,
  },
  {
    path: ADMIN_ROUTER_PATH.TOPIC_POST,
    component: AdTopicPost,
  },
  {
    path: ADMIN_ROUTER_PATH.COMMUNITY_POST,
    component: AdCommunityPost,
  },
  {
    path: ADMIN_ROUTER_PATH.COMMUNITY_POST + '/:id',
    component: DetailPost,
  },
  {
    path: ADMIN_ROUTER_PATH.GIFT_CHANGE,
    component: AdGiftChange,
  },
  {
    path: ADMIN_ROUTER_PATH.GIFT_CHANGE + '/:key/:id',
    component: AdGiftChange,
  },
  {
    path: ADMIN_ROUTER_PATH.GIFT,
    component: AdGift,
  },
  {
    path: ADMIN_ROUTER_PATH.SALES_REPORT,
    component: AdSaleReport,
  },
  {
    path: ADMIN_ROUTER_PATH.LIVE_STREAM_REPORT,
    component: AdLiveStreamReport,
  },
  {
    path: ADMIN_ROUTER_PATH.SHOP_REPORT,
    component: AdShopReport,
  },
  {
    path: ADMIN_ROUTER_PATH.CONFIG,
    component: AdConfig,
  },
  {
    path: ADMIN_ROUTER_PATH.ACCOUNTS,
    component: AdAccounts,
  },
  {
    path: ADMIN_ROUTER_PATH.ADD_POST,
    component: AddEditPost,
  },
  {
    path: ADMIN_ROUTER_PATH.EDIT_POST + '/:id',
    component: AddEditPost,
  },
]

const enterpriseRouter: Array<RouterProps> = [
  {
    path: ENTERPRISE_ROUTER_PATH.HOME,
    component: DashBoard,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.DASH_BOARD,
    component: DashBoard,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.PRODUCT,
    component: Product,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.STOCK,
    component: Stock,
  },
  // {
  //   path: ENTERPRISE_ROUTER_PATH.STALL_DETAIL + '/:id',
  //   component: StallDetail,
  // },
  {
    path: ENTERPRISE_ROUTER_PATH.CUSTOMER,
    component: Customer,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.ORDER,
    component: Order,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.ORDER_DETAIL,
    component: OrderDetail,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.LIVE_STREAM,
    component: LiveStream,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.LIVE_STREAM_DETAIL + '/:id',
    component: LiveStreamDetail,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.CHAT,
    component: Chat,
    exact: false,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.FORUM_POST,
    component: ForumPost,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.FORUM_POST + '/:id',
    component: DetailForumPost,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.SALES_REPORT,
    component: SaleReport,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.LIVE_STREAM_REPORT,
    component: LiveStreamReport,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.ACCOUNTS,
    component: Account,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.ADD_PRODUCT,
    component: AddProduct,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.EDIT_PRODUCT + '/:id',
    component: AddProduct,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.PRODUCT_DETAIL + '/:id',
    component: DetailProduct,
  },
  {
    path: ENTERPRISE_ROUTER_PATH.ORDER_DETAIL + '/:id',
    component: OrderDetail,
  },
]

export default adminRouter.concat(enterpriseRouter)
